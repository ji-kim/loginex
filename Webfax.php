<?php
error_reporting(E_ALL);

ini_set("display_errors", 1);

header("Content-Type: text/html; charset=EUC-KR");

//각 회사별 팩스 번호에 따른 폴더명 분류 (추후 DB화)
$faxNumbers = array("023334444" => "hdh",
                    "024443333" => "dev");

$mysqli = new MySQLi("13.125.162.78:4417","loginex","logi1661nex*)*!","loginex");
$mysqli->set_charset("euckr");

$sourceDir = "/home/loginex_dev";

// Fax 발송 관련 확인 쿼리
$result = $mysqli->query("SELECT * FROM FC_META_TRAN WHERE TR_SENDSTAT != '-' AND TR_ISCHECKED = 0");

//발송 내용 처리 쿼리
while($row = $result->fetch_assoc()){
    //primary
    $tr_batchid = $row['TR_BATCHID'];

    // Fax 발송이 완료된 쿼리를 각자의 번호를 확인해 폴더명을 가져온다.
    $folderName = $faxNumbers[$row['TR_SENDFAXNUM']];

    // 파일이 있다면 파일을 옮기고 파일이 옮겨졌다면 TR_ISCHECKED를 1로 변경하고 해당 파일의 경로를 정확히 기입한다.

    // 원본 파일 path
    $filePath = $sourceDir."/lgwebfax/SendDoc/";
    // 대상 파일 path
    $targetFilePath = $sourceDir."/uploads/webfax/".$folderName."/";
    // 만약 폴더가 없다면 디렉토리를 생성한다
    if(!file_exists($targetFilePath)) {
        mkdir($targetFilePath,755);
    }

    // DB내에 기입된 파일을 이동시킨다.
    $files = explode("|",$row['TR_DOCNAME']);

    $newFiles = "";
    foreach($files as $file){
        $fileName = time()."_".$file;
        if($newFiles != ""){
            $newFiles .= "|";
        }
        $newFiles .= $fileName;

        rename($filePath.$file,$targetFilePath.$fileName);
    }

    // DB의 값을 변경한다.
    $mysqli->query("UPDATE FC_META_TRAN SET TR_ISCHECKED = 1 , TR_FILENAMES = '".$newFiles."' , TR_FILEPATH = '".$folderName."' WHERE TR_BATCHID = ".$tr_batchid);
}

// 수신 내용 처리 쿼리
$resultRecv = $mysqli->query("SELECT * FROM FC_RECV_TRAN WHERE TR_ISCHECKED = 0");

while($row = $resultRecv->fetch_assoc()){
    //primary
    $tr_msgid = $row['TR_MSGID'];

    // Fax 발송이 완료된 쿼리를 각자의 번호를 확인해 폴더명을 가져온다.
    $folderName = $faxNumbers[$row['TR_RECVFAXNUM']];

    // 파일이 있다면 파일을 옮기고 파일이 옮겨졌다면 TR_ISCHECKED를 1로 변경하고 해당 파일의 경로를 정확히 기입한다.

    // 원본 파일 path
    $filePath = $sourceDir."/lgwebfax/SendDoc/";
    // 대상 파일 path
    $targetFilePath = $sourceDir."/uploads/webfax/".$folderName."/";

    // 만약 폴더가 없다면 디렉토리를 생성한다
    if(!file_exists($targetFilePath)) {
        mkdir($targetFilePath,755);
    }

    // DB내에 기입된 파일을 이동시킨다.
    $files = explode("|",$row['TR_FILENAMELIST']);

    $newFiles = "";
    foreach($files as $file){
        $fileName = time()."_".$file;
        if($newFiles != ""){
            $newFiles .= "|";
        }
        $newFiles .= $fileName;

        rename($filePath.$file,$targetFilePath.$fileName);
    }

    // DB의 값을 변경한다.
    $mysqli->query("UPDATE FC_RECV_TRAN SET TR_ISCHECKED = 1 , TR_FILENAMELIST = '".$newFiles."' , TR_FILEPATH = '".$folderName."' WHERE TR_MSGID = ".$tr_msgid);
}


$mysqli->close();