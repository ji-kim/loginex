<?php
$lang['opportunities'] = '진행중영업(사업)';
$lang['opportunities_state_reason'] = 'Opportunities State Reason';
$lang['opportunities_state'] = '진행상태';
$lang['stages'] = '상황';
$lang['won'] = '성공(수주)';
$lang['abandoned'] = '폐기';
$lang['suspended'] = '중단';
$lang['lost'] = '실패';
$lang['negotiation'] = '협상';
$lang['update'] = '업데이트';
$lang['all_opportunities'] = '전체 영업(사업)';
$lang['new_opportunities'] = '신규 영업';
$lang['opportunity_name'] = '제목';
$lang['qualification'] = '자격';
$lang['proposition'] = '제안';
$lang['dead'] = 'Dead';
$lang['probability'] = 'Probability Of Winning';
$lang['close_date'] = 'Forecast Close Date';
$lang['who_responsible'] = 'Who\'s Responsible';
$lang['expected_revenue'] = 'Expected Revenue';
$lang['new_link'] = 'Add New Link';
$lang['next_action'] = '다음 액션';
$lang['next_action_date'] = '나음 액션 일';
$lang['current_state'] = '현재상태';
$lang['change_state'] = '상태변경';
$lang['opportunity_details'] = '상세내용';
$lang['activity_update_opportunity'] = 'Update Opportunity Deatils';
$lang['update_opportunity'] = 'Update Opportunity information successfully';
$lang['activity_save_opportunity'] = 'Save Opportunity Deatils';
$lang['save_opportunity'] = 'Save Opportunity information successfully';
$lang['save_opportunity_call'] = 'Save Opportunity Call information successfully';
$lang['activity_save_opportunity_call'] = 'Save Opportunity Call Deatils';
$lang['activity_update_opportunity_call'] = 'Update Opportunity Call Deatils';
$lang['update_opportunity_call'] = 'Update Opportunity Call information successfully';
$lang['activity_opportunity_call_deleted'] = 'Opportunity Call Deleted';
$lang['opportunity_call_deleted'] = 'Opportunity Call information successfully deleted!';
$lang['save_opportunity_metting'] = 'Save Opportunity Mettings information successfully';
$lang['activity_save_opportunity_metting'] = 'Save Opportunity Mettings Deatils';
$lang['activity_update_opportunity_metting'] = 'Update Opportunity Mettings Deatils';
$lang['update_opportunity_metting'] = 'Update Opportunity Mettings information successfully';
$lang['activity_opportunity_metting_deleted'] = 'Opportunity Mettings Deleted';
$lang['opportunity_metting_deleted'] = 'Opportunity Mettings information successfully deleted!';
$lang['activity_new_opportunity_comment'] = 'Opportunity New Comment added!';
$lang['opportunity_comment_save'] = 'Opportunity Comment information successfully Saved!';
$lang['task_comment_deleted'] = 'Comment information successfully Deleted';
$lang['opportunity_file_updated'] = 'Opportunity File information successfully Updated';
$lang['opportunity_file_added'] = 'Opportunity File information successfully Added';
$lang['activity_new_opportunity_attachment'] = 'Opportunity File Added';
$lang['opportunity_attachfile_deleted'] = 'Opportunity File Deleted';
$lang['activity_opportunity_attachfile_deleted'] = 'Opportunity File Deleted';
$lang['opportunity_deleted'] = 'Opportunity information successfully Deleted';
$lang['activity_opportunity_deleted'] = 'Opportunity information Deleted';


/* End of file opportunities_lang.php */
/* Location: ./application/language/korean/opportunities_lang.php */
