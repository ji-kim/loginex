<?php
$lang['recent_leads'] = 'Recent Leads';
$lang['source'] = 'Source';
$lang['facebook_profile_link'] = 'Facebook URL';
$lang['twitter_profile_link'] = 'Twitter URL';
$lang['linkedin_profile_link'] = 'Linkedin URL';
$lang['leads_name'] = '잠재고객 명';
$lang['mettings_deleted'] = 'Meetings information successfully Deleted';
$lang['convert'] = 'Convert';
$lang['convert_to_client'] = 'Convert To Client';
$lang['activity_convert_to_client'] = 'Convert Lead To Client';
$lang['convert_to_client_suucess'] = 'Convert Lead To Client Successfully';
$lang['import_leads'] = '잠재고객 가져오기';
$lang['all_leads'] = '전체 잠재고객';
$lang['new_leads'] = '새 잠재고객';
$lang['lead_name'] = '제목';
$lang['contact_name'] = '담당자명';
$lang['state'] = '상태';
$lang['leads'] = '잠재고객';
$lang['leads_details'] = '잠재고객 상세';
$lang['call'] = 'Call';
$lang['mettings'] = 'Meetings';
$lang['call_summary'] = 'Call Summary';
$lang['contact'] = 'Contact With';
$lang['responsible'] = 'Responsible';
$lang['all_call'] = 'All Call';
$lang['new_call'] = 'New Call';
$lang['all_metting'] = 'All Meetings';
$lang['new_metting'] = 'New Meetings';
$lang['metting_subject'] = 'Meetings Subject';
$lang['activity_leads_attachfile_deleted'] = 'Leads Attachment File Deleted';
$lang['save_leads_call'] = 'Save Leads Call information successfully';
$lang['activity_save_leads_call'] = 'Save Leads Call Deatils';
$lang['activity_update_leads_call'] = 'Update Leads Call Deatils';
$lang['update_leads_call'] = 'Update Leads Call information successfully';
$lang['activity_leads_call_deleted'] = 'Leads Call Deleted';
$lang['leads_call_deleted'] = 'Leads Call information successfully deleted!';
$lang['save_leads_metting'] = 'Save Leads Meetings information successfully';
$lang['activity_save_leads_metting'] = 'Save Leads Meetings Deatils';
$lang['activity_update_leads_metting'] = 'Update Leads Meetings Deatils';
$lang['update_leads_metting'] = 'Update Leads Meetings information successfully';
$lang['activity_leads_metting_deleted'] = 'Leads Meetings Deleted';
$lang['leads_metting_deleted'] = 'Leads Meetings information successfully deleted!';
$lang['by_status'] = '상태별';
$lang['by_source'] = '소스별';
$lang['lead_source'] = '잠재고객소스';
$lang['new_lead_source'] = 'New Leads Source';
$lang['activity_added_a_lead_source'] = 'New Leads Source Added';
$lang['lead_status_added'] = 'Leads Status Added Successfully Updated';
$lang['lead_source_added'] = 'Leads Source Added Successfully Updated';
$lang['lead_source_deleted'] = 'Leads Source Added Successfully Deleted';


/* End of file leads_lang.php */
/* Location: ./application/language/korean/leads_lang.php */
