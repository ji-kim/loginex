<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// HRM Code
$lang['leave_management'] = '휴가관리';
$lang['my_leave'] = '나의연차';
$lang['apply'] = '적용';
$lang['approval'] = '결제';
$lang['all_leave'] = '전체 연차신청내역';
$lang['new_leave'] = '신규 연차';
$lang['leave_report'] = '연차 사용 내역';
$lang['all_required_fill'] = '필수 입력항목 모두 입력해주세요';
$lang['current_date_error'] = '해당일에 연차 신청이 불가능합니다. ';
$lang['end_date_less_than_error'] = 'The end date can not be less then the start date';
$lang['leave_successfully_save'] = 'Leave Information Successfully Saved';
$lang['leave_date_conflict'] = 'conflict: You already have leave in the selected time';
$lang['application_details'] = 'Application Details';
$lang['leave_from'] = 'Applied From ';
$lang['leave_to'] = 'To ';
$lang['details_of'] = 'Details Of';
$lang['applied_on'] = 'Applied On';
$lang['at'] = ' at ';
$lang['approved_by'] = '승인자 ';
$lang['approved'] = '승인';
$lang['reject'] = '반려';
$lang['rejected'] = '반력';
$lang['give_comment'] = '메모';
$lang['application_status_changed'] = 'Application Status Successfully Changed';
$lang['leave_application_delete'] = 'Application Status Successfully Delete';
$lang['activity_leave_deleted'] = 'Leave Application Deleted';
$lang['activity_leave_save'] = 'New Leave Application Saved';
$lang['activity_leave_change'] = 'Application Status Chnaged';
$lang['duration'] = 'Duration';
$lang['single_day'] = 'Single day';
$lang['multiple_days'] = 'Multiple days';
$lang['taken'] = 'Taken';

