<?php


class Faxbox_Model extends MY_Model
{

    public function get_recv_message($tel = "023334444") {
        $this->faxdb = $this->load->database('fax', TRUE);
        $query_result = $this->faxdb->query('SELECT * FROM FC_META_TRAN WHERE TR_SENDFAXNUM = "'.$tel.'" ORDER BY TR_BATCHID DESC');

        $result = $query_result->result();
        return $result;
    }

    public function get_send_message($tel = "023334444") {
        $this->faxdb = $this->load->database('fax', TRUE);
        $query_result = $this->faxdb->query('SELECT * FROM FC_META_TRAN WHERE TR_SENDFAXNUM = "'.$tel.'" ORDER BY TR_BATCHID DESC');

        $result = $query_result->result();
        return $result;
    }

    public function send_fax($data){
        $this->faxdb = $this->load->database('fax',TRUE);
        $this->faxdb->insert('FC_META_TRAN' , $data);
        return $this->faxdb->insert_id();
    }

    public function insert_send_fax_subdata($data){
        $this->faxdb = $this->load->database('fax',TRUE);
        $this->faxdb->insert('FC_MSG_TRAN' , $data);
    }
}