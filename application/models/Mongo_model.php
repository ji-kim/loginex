<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mongo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('mongo_db');
    }

    // 출근 차량 전체 목록
    public function getworklist() {
        $data = $this->mongo_db->get('df_working_drivers');
        return $data;
    }

    // 차량 운행 경로 데이터
    public function gpsLog($carnum, $date, $inout)
    {
        $search = array(
            "carNum" => $carnum,
            "driveDate" => $date
        );
	
	if($inout == "all") {
            $filter = array();
        } else {
            $filter = array("carNum", "check{$inout}");
        }

        $data = $this->mongo_db->select($filter)->where($search)->get('df_driver_logs');

        return $data;
    }
}
