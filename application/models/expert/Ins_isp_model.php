<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Ins_isp_model
 *
 * @author NaYeM
 */
class Ins_isp_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insurance_info($truck_id) {
        $result = $this->db->where('type', '자동차')->where('tr_id', $truck_id)->where('active_yn', 'Y')->get('tbl_asset_truck_insur')->row_array();
       // $result = $this->invoice_model->check_by(array('project_id' => $project_id), 'tbl_project');
        return $result;
    }

    public function inspection_info($truck_id) {
        $result = $this->db->where('tr_id', $truck_id)->get('tbl_asset_truck_check')->row_array();
        return $result;
    }

}
