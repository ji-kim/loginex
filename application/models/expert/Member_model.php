<?php
/**
 * Description of Project_Model
 *
 * @author NaYeM
 */
class Member_model extends MY_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('mongo_db');
        $this->load->database();
    }

    // 회원정보
    public function member($userid)
    {
        $query = $this->db->get_where('tbl_members', array("REPLACE(userid,'-','')" => $userid));
        return $query->row_array();
    }

    // 로그인체크
    public function login($userid, $userpwd)
    {
        $query = $this->db->get_where('tbl_members', array('userid' => $userid, 'userpwd' => $userpwd));
        return $query->row_array();
    }

    // 출근 체크
    public function work($userid) {
        $this->mongo_db->where('loginId', $userid);
        $data = $this->mongo_db->get('df_working_drivers');
        return $data;
    }

    // 디바이스 토큰 삽입
    public function setDeviceToken($userid , $device_token){
        $data['device_token'] = '';
        $this->db->where('device_token',$device_token);
        $this->db->update('tbl_members',$data);

        $data['device_token'] = $device_token;
        $this->db->where('userid',$userid);
        $this->db->update('tbl_members',$data);
    }
}
