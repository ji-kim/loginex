<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Project_Model
 *
 * @author NaYeM
 */
class Contract_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

	public function signWrite($insertData){
		
        return $this->db->insert('tbl_econtract', $insertData);
	}

}
