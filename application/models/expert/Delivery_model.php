<?php
if (!defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Description of Project_Model
 *
 * @author NaYeM
 */
class Delivery_model extends MY_Model{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // 거래항목
    public function transactions($ct_id) {
        $result = $this->db->query("select * from tbl_contract_detail where ct_id = '$ct_id' order by ct_id asc")->row_array();
        return $result;
    }

    // 위수탁관리비
    public function delivery_fee($userid, $month) {
        /*$this->db->select('*');
        $this->db->from('tbl_delivery_fee');
        $this->db->where('dp_id',$userid);
        $query = $this->db->get();*/
        $result = $this->db->query("select * from tbl_delivery_fee where ( userid = '$userid' or F = '$userid' ) and is_closed='Y' and df_month='$month'")->row_array(1);
        return  (array)$result;
    }

    // (분동?) 위수탁관리비 기본공제
    public function delivery_fee_fix($df_id) {
        $query = $this->db->get_where('tbl_delivery_fee_fixmfee', array('df_id' => $df_id));
        //$result = $this->db->where('df_id', $df_id)->get('tbl_delivery_fee_fixmfee')->row_array();
        return $query->row_array();
    }

    // (분동?) 일반공제
    public function delivery_fee_gongje($df_id) {
        $result = $this->db->where('df_id', $df_id)->get('tbl_delivery_fee_gongje')->row_array();
        return $result;
    }

    // (분동?) 각종보험공제
    public function delivery_fee_insur($df_id) {
        $result = $this->db->where('df_id', $df_id)->get('tbl_delivery_fee_insur')->row_array();
        return $result;
    }

    // (분동?) 환급형공제
    public function delivery_fee_refund($df_id) {
        $result = $this->db->where('df_id', $df_id)->get('tbl_delivery_fee_refund_gongje')->row_array();
        return $result;
    }

    //읽음 카운트
    public function read_count($df_id) {
        $this->db->set('read_cnt', 'read_cnt+1', FALSE);
        $this->db->where('df_id', $df_id);
        $this->db->update('tbl_delivery_fee');
    }

    //입금은행 정보
    public function bank_info($pay_account_no) {
        $result = $this->db->where('idx', $pay_account_no)->get('tbl_members_bank')->row();
        return (array)$result;
    }

    // 차량 정보
    public function asset_truck($tr_id) {
		$this->db->select('*')->from('tbl_asset_truck at');
		$this->db->join('tbl_members tm','at.ws_co_id = tm.dp_id','left');
		$this->db->where('at.idx',$tr_id);
		$result = $this->db->get()->row();
        //$result = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        return (array)$result;
    }

    //계약정보
    public function contract_info($ct_id) {
        $result = $this->db->where('idx', $ct_id)->get('tbl_contract')->row();
        return (array)$result;
    }

}
