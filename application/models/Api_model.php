<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * 20190313 Api_Model
 */

class Api_model extends MY_Model {

    private $offset;
    private $limit;
    private $start;


    public function user_count(){
        return $this->db->count_all('tbl_asset_truck');
    }


    public function search_list($search,$is_connected){

        if($is_connected === null  || $is_connected === 'all'){
            $is_connected_query = '';
        }else {
            $is_connected_query = "and a.is_connected = '$is_connected'";
        }



        $query = $this->db->query("
        select count(*) as cnt from tbl_members a join tbl_asset_truck b on
         a.tr_id = b.idx 
        where (a.co_name like '%$search%' 
        or b.car_1 like '%$search%' or a.driver like '%$search%')        
        $is_connected_query
        ");

        $result = $query->result();



        return $result;
    }


    public function user_list($limit,$start,$search,$is_connected) {


        if($is_connected === null ||  $is_connected === 'all'){
            $is_connected_query = '';
        }else{
            $is_connected_query = "and a.is_connected = '$is_connected'";
        }
        if($limit === '1' || $limit === '0' || empty($limit)) {
            $limit = '0';
        }

        if($search === null || empty($search) ){
            $search_query = '';
        }else{
            $search_query = "
            and (a.co_name like '%$search%' or b.car_1 like '%$search%' or a.driver like '%$search%')            
            ";
        }

        $query = $this->db->query("
        select b.idx,b.type,b.car_1,b.car_3,b.car_4,b.carinfo_11,a.co_name,a.driver,a.userid,a.is_connected from
        tbl_members a join tbl_asset_truck b on a.tr_id = b.idx 
        where 1=1 $search_query        
        $is_connected_query
        group by b.idx order by a.is_connected desc,b.idx desc limit $limit,$start
        
        ");
        $result = $query->result();


        return $result;
    }

    //드라이버 상세정보
    public function driver_info($idx){
        $this->db->select('b.idx,b.type,b.car_1,b.car_3,b.car_4,b.carinfo_11,a.co_name,a.driver,a.userid', false);
        $this->db->from("tbl_members a");
        $this->db->join('tbl_asset_truck b','a.tr_id = b.idx','inner');
        $this->db->where('b.idx', $idx);
        $this->db->order_by('b.idx','desc');
        $query = $this->db->get();
        $driver_info = $query->result();

        return $driver_info;
    }
    //자동차 보험정보
    public function driver_insur1($idx){
        $this->db->select('ins_co_id,type,active_yn,start_date,end_date');
        $this->db->from('tbl_asset_truck_insur');
        $this->db->where('tr_id',$idx);
        $this->db->like('type','자동차');
        $this->db->order_by('idx','desc');
        $this->db->limit('1','1');

        $query = $this->db->get();
        $driver_insur1 = $query->result();

        return $driver_insur1;
    }
    //적재물 보험정보
    public function driver_insur2($idx){
        $this->db->select('ins_co_id,type,active_yn,start_date,end_date');
        $this->db->from('tbl_asset_truck_insur');
        $this->db->where('tr_id',$idx);
        $this->db->like('type','적재물');
        $this->db->order_by('idx','desc');
        $this->db->limit('1','1');

        $query = $this->db->get();
        $driver_insur2 = $query->result();

        return $driver_insur2;
    }
    //자동차 검사 결과
    public function car_info($idx){

        //$idx = $this->get('idx');

        $this->db->select('active_yn,laste_check,next_start,next_expired');
        $this->db->from('tbl_asset_truck_check');
        $this->db->where('tr_id',$idx);
        $this->db->order_by('idx','desc');
        $this->db->limit('1','1');

        $query = $this->db->get();
        $car_info  = $query->result();

        return $car_info;
    }

    //로그인 API 개발테스트옹
    public function user_login($userid,$userpasswd){

        $userid =  $this->input->get('userid');
        $userpasswd = $this->hash($this->input->get('userpasswd'));

        $query = $this->db->query("
        select a.username,a.email,b.fullname,b.avatar,b.company,a.user_id,a.last_login,a.role_id 
        from tbl_users a, tbl_account_details b 
        where a.user_id = b.user_id 
        and a.username = '$userid' and a.password = '$userpasswd'
        ");

        $result = $query->result();

        return $result;

    }

    //사용자명명
    public function user_date($userid){
        $userid = $this->session->userdata('user_name');


        $query = $this->db->query("
        select a.username,a.email,b.fullname,b.avatar,b.company,a.user_id,a.last_login,a.role_id 
        from tbl_users a, tbl_account_details b 
        where a.user_id = b.user_id 
        and a.username = '$userid'
        ");

        $result = $query->result();

        return $result;
    }

   // 번호 차량 운전자 정보만
    public function test_list(){

       $this->db->select('dp_id as idx,userid');
       $this->db->from('tbl_members');
       $this->db->order_by('dp_id','desc');

       $query = $this->db->get();
       $result = $query->result();


       return $result;

    }

}
?>