<?php

class Allocation_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function get_branchs($branch_id=NULL)
    {
        $this->db->_order_by = 'co_name asc';
		if(!empty($branch_id)) $this->db->where('dp_id', $branch_id);
        $all_branch = $this->db->where('mb_type', 'branch')->get('tbl_members')->result();

		if (!empty($all_branch)) {
            return $all_branch;
        } else {
            return array();
        }
    }

    public function get_first_branch()
    {
		$this->db->limit(1,0);
        $first_branch = $this->db->where('mb_type', 'branch')->get('tbl_members')->row();

		if (!empty($first_branch)) {
            return $first_branch->dp_id;
        } else {
            return '';
        }
    }

    public function get_drivers_by_branch($branch_id)
    {
        $all_drivers = $this->db->where('mb_type', 'partner')->where('br_id', $branch_id)->get('tbl_members')->result();

		if (!empty($all_drivers)) {
            return $all_drivers;
        } else {
            return array();
        }
    }

    public function get_checkers($branch_id)
    {
        $all_checker = $this->db->where('br_id', $branch_id)->get('tbl_checker')->result();

		if (!empty($all_checker)) {
            return $all_checker;
        } else {
            return array();
        }
    }

    public function get_driver_info_by_id( $driver_id)
    {
		$driver = $this->db->select('co_name as driver_name, ceo, co_tel')->where('dp_id', $driver_id)->get('tbl_members')->row();

		if (!empty($driver)) {
            return $driver;
        } else {
            return array();
        }
    }

    public function get_checker_info_by_id( $checker_id)
    {
		$checker = $this->db->where('idx', $checker_id)->get('tbl_checker')->row();

		if (!empty($checker)) {
            return $checker;
        } else {
            return array();
        }
    }

    public function get_work( $work_id )
    {
		$this->db->limit(1,0);
		$work = $this->db->where('idx', $work_id)->get('tbl_allocation_works')->row();

		if (!empty($work)) {
            return $work;
        } else {
            return array();
        }
    }

    public function get_work_status( $alloc_date, $branch_id, $driver_id=NULL)
    {
		$this->db->limit(1,0);
		$work_status = $this->db->select('is_register, is_confirm, move_to, subwork_status')->where('br_id', $branch_id)->where('alloc_date', $alloc_date)->get('tbl_allocation_works')->row();

		if (!empty($work_status)) {
            return $work_status;
        } else {
            return array();
        }
    }

    public function work_list_all($alloc_date, $branch_id, $driver_id = NULL, $driver_confirm = NULL)
    {
		if(empty($alloc_date) || empty($branch_id)) exit;

		$qry  = " select * from tbl_allocation_works where 1 ";
		if(!empty($params['search_by']) && $params['search_by'] == "M" && !empty($params['alloc_month'])) {
			$qry .=  " and alloc_date like '".$params['alloc_month']."%' ";    
		} else if(!empty($alloc_date)) {      $qry .=  " and alloc_date = '".$alloc_date."' ";    }
		if(!empty($branch_id)) {      $qry .=  " and br_id = '".$branch_id."' ";    }
		if(!empty($params['checker_id'])) {      $qry .=  " and checker_id = '".$params['checker_id']."' ";    }
		if(!empty($params['driver_id'])) {      $qry .=  " and driver_id = '".$params['driver_id']."' ";    }
		if(!empty($params['is_confirm'])) {      $qry .=  " and is_confirm = '".$params['is_confirm']."' ";    }
		if(!empty($params['driver_confirm'])) {      $qry .=  " and driver_confirm = '".$params['driver_confirm']."' ";    }
		$qry .=  " order by alloc_date asc, work_from asc ";

        $this->db->_order_by = 'alloc_date asc, work_from asc';
		if(!empty($driver_id)) $this->db->where('aw.driver_id', $driver_id);
		if(!empty($driver_confirm)) $this->db->where('aw.driver_confirm', $driver_confirm);
        $work_list = $this->db->select('aw.*')->where('aw.alloc_date', $alloc_date)->where('aw.br_id', $branch_id)->get('tbl_allocation_works aw')->result();

        //$work_list = $this->db->select('aw.*, dr.ceo as driver_name, ch.checker_name')->where('aw.alloc_date', $alloc_date)->where('aw.br_id', $branch_id)->join('tbl_members dr', 'dr.dp_id = aw.driver_id', 'right')->where('dr.mb_type', 'partner')->join('tbl_checker ch', 'ch.idx = aw.checker_id', 'right')->get('tbl_allocation_works aw')->result();
       // $work_list = $this->db->query($qry);

		if (!empty($work_list)) {
            return $work_list;
        } else {
            return array();
        }
    }

	public function get_work_count_by_branch($branch_id, $alloc_date, $action = NULL)
	{
		if(empty($branch_id) || empty($alloc_date)) return false;
		$qry  = " select  ";
		$qry .=        " ifnull(amt_day,0) as sum_day, "; // 주간작업
		$qry .=        " ifnull(amt_night,0) as sum_night, "; // 야간작업
		$qry .=        " ifnull(amt_nthr,0) as sum_nthr, "; // 구분없는작업
		$qry .=        " ifnull(amt_cowork,0) as sum_cowork "; // 구분없는작업
		$qry .=   " from ( ";
		$qry .=         " select  ";
		$qry .=                " sum(case when work_type = 'D' then ".$action." else 0 end) as amt_day, "; 
		$qry .=                " sum(case when work_type = 'N' then ".$action." else 0 end) as amt_night, "; 
		$qry .=                " sum(case when work_type = '' then ".$action." else 0 end) as amt_nthr, "; 
		$qry .=                " sum(cowork_cnt) as amt_cowork "; 
		$qry .=           " from tbl_allocation_works ";
		$qry .=          " where 1";
		$qry .=  " and alloc_date = '".$alloc_date."' ";
		$qry .=  " and br_id = '".$branch_id."' ";
		$qry .=        " ) a ";

        $result = $this->db->query($qry)->row_array();
		if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
	}

	public function holiday_check($date)
	{
		$holiday = $this->db->where('start_date', $date)->get('tbl_holiday')->row();
		if (!empty($holiday)) {
            return $holiday;
        } else {
            return array();
        }
	}

}