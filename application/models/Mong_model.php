<?php

class Mong_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->library('mongo_db');
    }


    //loginId and Date 가지고 조회
    public function getList($loginId,$date)
    {
        $this->load->library('mongo_db');
        //$this->mongo_db->where_between('foo', 20, 30);
        // $this->mongo_db->where_gte('foo', 20);
        //$this->mongo_db->where_gte('foo', 20);
        //$this->mongo_db->where_lte('foo', 20);

        //array("timestamp" => ISODate("2019-03-18T05:19:21+00:00") )

        //$this->mongo_db->select(array('foo', 'bar'))->get('foobar');
        //$this->mongo_db->select('*');
        //$this->mongo_db->where_between('locations.createDt','ISODate(2019-03-17)','ISODate(2019-03-19)');
        //$this->mongo_db->where_gte('locations.createDt',array("timestamp" => ISODate("2019-03-18T05:19:21+00:00") ));
        //$this->mongo_db->where_lte();
        //$this->mongo_db->where('locations.latitude','37.4742371');
        //$this->mongo_db->order_by(array("_id"=>1));
//        $data = $this->mongo_db->get_where('df_driver_logs',
//            array(
//                'loginId' => '114-07-51923',
//                array(
//                    'loginId' => 0
//                )
//            )
//        );
//        $data = $this->mongo_db->get('df_driver_logs');
        $start_date = new MongoDate(strtotime("2019-03-18 00:00:00 Asia/Seoul"));
        $end_date = new MongoDate(strtotime("2019-03-18 18:00:00 Asia/Seoul"));

        //print_r($start_date->toDateTime());
        $mongoClient = new MongoClient('mongodb://delex_office:deltaon1234@13.209.98.31/office_db');

        try {
            $db = $mongoClient->selectDB('office_db')->selectCollection('df_driver_logs');
            $data = $db->findOne(
                array(
                    //'loginId' => '114-07-51923',
                    'loginId' => $loginId,
                    //'_isWorking' => true,
                    'locations.createDt' => array(
                        //'$gte' => new MongoDate(strtotime("2019-03-18 00:00:00")),
                        '$gte' => new MongoDate(strtotime("$date 00:00:00")),
                        '$lte' => new MongoDate(strtotime("$date 23:59:59"))
                    )
                ),
                array(
                    'loginId' => 1,
                    'locations' => 1
                )
            );
            return $data;
        } catch (Exception $e) {
            $e->getMessage();
        } finally {
            $mongoClient->close();
        }


    }


    public function getwork($loginId)
    {
        $this->load->library('mongo_db');

        $start_date = new MongoDate(strtotime("2019-03-18 00:00:00"));
        $end_date = new MongoDate(strtotime("2019-03-18 18:00:00"));

        //print_r($start_date->toDateTime());
        $mongoClient = new MongoClient('mongodb://delex_office:deltaon1234@13.209.98.31/office_db');

        try {
            $db = $mongoClient->selectDB('office_db')->selectCollection('df_working_drivers');
            $data = $db->findOne(
                array(
                    'loginId' => $loginId
                )
            );
            return $data;
        } catch (Exception $e) {
            $e->getMessage();
        } finally {
            $mongoClient->close();
        }
    }

    public function getworklist(){
        $this->load->library('mongo_db');

        $start_date = new MongoDate(strtotime("2019-03-18 00:00:00"));
        $end_date = new MongoDate(strtotime("2019-03-18 18:00:00"));

        //print_r($start_date->toDateTime());
        $mongoClient = new MongoClient('mongodb://delex_office:deltaon1234@13.209.98.31/office_db');

        try {
            $db = $mongoClient->selectDB('office_db')->selectCollection('df_working_drivers');
            $data = $db->find();

            return $data;
        } catch (Exception $e) {
            $e->getMessage();
        } finally {
            $mongoClient->close();
        }

    }


    //경로찾기(findpaths)
    public function getfindpaths($loginId,$date)
    {
        $this->load->library('mongo_db');

        //$start_date = new MongoDate(strtotime("2019-03-18 00:00:00"));
        //$end_date = new MongoDate(strtotime("2019-03-18 18:00:00"));

        //print_r($start_date->toDateTime());
        $mongoClient = new MongoClient('mongodb://delex_office:deltaon1234@13.209.98.31/office_db');

        try {
            $db = $mongoClient->selectDB('office_db')->selectCollection('df_working_drivers');
            $data = $db->findOne(
                array(
                    'loginId' => $loginId
                )
            );
            return $data;
        } catch (Exception $e) {
            $e->getMessage();
        } finally {
            $mongoClient->close();
        }
    }

    public function getList2($loginId,$date)
    {
        $this->load->library('mongo_db');
        $select_date = $this->input->get('date');


        $start_date = strtotime('-8 hours', strtotime($date . ' 00:00:00.000Z'));
        $start_date = date('Y-m-d\TH:i:s', $start_date);



        $end_date = strtotime('-8 hours', strtotime($date . ' 23:59:59.999Z'));
        $end_date = date('Y-m-d\TH:i:s', $end_date);
        $end_date = date( "Y-m-d\T23:59:59", strtotime('+9 hours', strtotime($date)) );

//        echo $start_date.'<br>';
//        echo $end_date.'<br>';
//        exit;

//        print_r(new DateTime($start_date, $date_time_zone));
//        $start_date = new DateTime($start_date, $date_time_zone);
//        $end_date = new DateTime($end_date, $date_time_zone);
//
//        print_r($start_date);
//        exit;

//        echo $start_date . ' ' . $end_date;
//        exit;

        $mongoClient = new MongoClient('mongodb://delex_office:deltaon1234@13.209.98.31/office_db');

        //echo $tt2.'23:59:59';
        /*
         * 2019-03-19T00:00:00
         * 2019-03-19T23:59:59
         */

        try {



            $db = $mongoClient->selectDB('office_db')->selectCollection('df_driver_logs');

            $query = '
            db.getCollection("df_driver_logs").aggregate([
                {
                    "$match": {
                        "loginId": "' . $loginId . '",
                    }
                },
                {
                    "$project":
                    {
                        "loginId" :"' . $loginId .'",
                        "locations":{
                            "$filter" : {
                                "input" : "$locations",
                                "as" : "locations",
                                "cond" : {
                                    "$and" :[                        
                                        {
                                           "$gte" : ["$$locations.createDt",ISODate("' . $start_date . '")]
                                        },
                                        {
                                           "$lte" : ["$$locations.createDt",ISODate("'. $end_date .'")]
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ])
            ';

            $data = $mongoClient->selectCollection("office_db", "df_driver_logs")->db->execute($query);



//                            $pipeline = array(
//                                array(
//                                    "\$match" => array(
//                                        "loginId" => "616-28-36762"
//                                    ),
//                                ),
//                                array(
//                                    "\$project" => array(
//                                            "loginId" => "\$loginId",
//                                            "locations" => array(
//                                                "\$filter" => array(
//                                                    "input" => "\$locations",
//                                                    "as" => "locations",
//                                                    "cond" => array(
//                                                        "\$and" => array(
//                                                            "\$eq" =>
//                                                        )
//                                                            //"\$lte" => "\$\$locations.createDt","2019-04-01T06:36:41.328Z",
//                                                    )
//                                                )
//                                            )
//                                        )
//                                    )
//                            );
//
//                            $options = array("explain" => true);
//
//                            $data = $db->aggregate($pipeline,$options);

/*
                                    $data = $db->findOne(
                                        array(
                                            //'loginId' => '114-07-51923',
                                            'loginId' => $loginId,
                                            //'_isWorking' => true,
                                            'locations.createDt' => array(
                                                //'$gte' => new MongoDate(strtotime("2019-03-18 00:00:00")),
                                                //'$gte' => new MongoDate(strtotime("$date 00:00:00")),
                                                //'$lte' => new MongoDate(strtotime("$date 23:59:59"))
                                                '$gte' => strtotime($start_date),
                                                '$lte' => strtotime($end_date)
                                            )
                                        ),
                                        array(
                                            'loginId' => 1,
                                            'locations' => 1
                                        )
                                    );*/
//            exit;

            return $data;

        } catch (Exception $e) {
            $e->getMessage();
        } finally {
            $mongoClient->close();
        }


    }


}

?>