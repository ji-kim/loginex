<script>
require("dotenv").config();
var mosca = require("mosca");

var mongoDBConnectionURI = `mongodb://${process.env.MONGO_USER}:${
process.env.MONGO_PASS
}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;
var ascoltatore = {
//using ascoltatore
type: "mongo",
url: mongoDBConnectionURI,
pubsubCollection: process.env.MONGO_COLLECTION,
mongo: {}
};

var settings = {
port: 1883,
backend: ascoltatore,
persistence: {
factory: mosca.persistence.Mongo,
url: mongoDBConnectionURI
},
http: {
port: 9001,
bundle: true,
static: "./"
}
};

var server = new mosca.Server(settings);

server.on("clientConnected", function(client) {
console.log("client connected", client.id);
});

// fired when a message is received
server.on("published", function(packet, client) {
console.log("Published", packet.payload);
});

server.on("ready", setup);

// fired when the mqtt server is ready
function setup() {
console.log("Mosca server is up and running");
}
</script>