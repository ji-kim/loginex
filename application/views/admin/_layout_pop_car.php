<?php
$this->load->view('admin/components/htmlheader');
$opened = $this->session->userdata('opened');
$this->session->unset_userdata('opened');

?>
<script>
	function selectCompany(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
</script>
<body class="<?php if (!empty($opened)) {
    echo 'offsidebar-open';
} ?> <?= config_item('aside-float') . ' ' . config_item('aside-collapsed') . ' ' . config_item('layout-boxed') . ' ' . config_item('layout-fixed') ?>">
<div class="wrapper">
    <!-- sidebar-->
    <?php $this->load->view('admin/components/pop_asset_car_sidebar'); ?>
    <!-- Main section-->

    <section>
        <?php
        $active_pre_loader = config_item('active_pre_loader');
        if (!empty($active_pre_loader) && $active_pre_loader == 1) {
            ?>
            <div id="loader-wrapper">
                <div id="loader"></div>
            </div>
        <?php } ?>
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-heading">
                <?php
                echo $title;

                ?>
                <div class="pull-right">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">


                    <?php echo $subview ?>




                </div>
            </div>
        </div>
    </section>
    <!-- Page footer-->

</div>
<?php
$this->load->view('admin/components/footer_pop');
$direction = $this->session->userdata('direction');
if (!empty($direction) && $direction == 'rtl') {
    $RTL = 'on';
} else {
    $RTL = config_item('RTL');
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        $("button[name$='clocktime']").click(function () {
            var ubtn = $(this);
            ubtn.html('Please wait...');
            ubtn.addClass('disabled');
        });

        $('[data-ui-slider]').slider({
            <?php
            if (!empty($RTL)) {?>
            reversed: true,
            <?php }
            ?>
        });
        /*
         * Multiple drop down select
         */
    })
</script>

<script type="text/javascript">

    $(document).ready(function () {
    })
    ;
</script>

<?php $this->load->view('admin/_layout_modal'); ?>
<?php $this->load->view('admin/_layout_modal_lg'); ?>
<?php $this->load->view('admin/_layout_modal_large'); ?>
<?php $this->load->view('admin/_layout_modal_extra_lg'); ?>

