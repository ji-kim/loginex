<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$tr_mode = "Y"; //등록모드에서 기초값 업데이트
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));

//거래항목 
$item_cnt = 0;
$items_cols = "";
if (!empty($all_items_group)) {
	foreach ($all_items_group as $items_info) {
		$item_cnt++;
		$items_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$items_info->title."</td>";
	}
}

//추가항목관련
$add_sudang_cnt = 0;
$add_gongje01_cnt = 0;
$add_gongje02_cnt = 0;
$add_gongje03_cnt = 0;

$add_sudang_cols = "";
$add_gongje01_cols = "";
$add_gongje02_cols = "";
$add_gongje03_cols = "";

if (!empty($all_sudang_group)) {
	foreach ($all_sudang_group as $sudang_info) {
		if($sudang_info->type == 'S') { //일반수당
			$add_sudang_cnt++;
			$add_sudang_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$sudang_info->title."</td>";
		}
	}
}
			
if (!empty($all_gongje_group)) {
	foreach ($all_gongje_group as $gongje_info) {
		if($gongje_info->add_type == 'GW') { //위수탁
			$add_gongje01_cnt++;
			$add_gongje01_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GN') { //일반
			$add_gongje02_cnt++;
			$add_gongje02_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GR') { //환급
			$add_gongje03_cnt++;
			$add_gongje03_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		}
	}
}

if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<script>
	function goSearch(val) {
		if(val == 'all' || val == '전체') {
			document.myform.action = "<?php echo base_url() ?>admin/logis/stransaction_finish"; //_all";
			document.myform.ws_co_id.value = '';
		} else {
			document.myform.action = "<?php echo base_url() ?>admin/logis/stransaction_finish";
		}
		document.myform.submit();
	}
</script>

            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/logis/stransaction_finish"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
                
      <!-- 검색 시작 -->
<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td width="10%" height="20" align="center" bgcolor="#efefef">년월</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff" >

                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                }
                                ?>" class="form-control monthyear" name="df_month" id="df_month"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="document.myform.submit();">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>

                </td>
                <td width="10%" align="center" bgcolor="#efefef">
					계약선택
				</td>
                <td style="padding-left:5px;" colspan="3" align="left" bgcolor="#ffffff">
					<select name="ct_id" id="ct_id" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="document.myform.submit();">
					 <option value="">선택</option>
<?php
if (!empty($all_contract_group)) {
	foreach ($all_contract_group as $contract_info) {
		//.($ct_id == $contract_info->idx)?"selected":"";
		if($ct_id == $contract_info->idx) $selected = "selected"; else $selected = "";
		echo "<option value='".$contract_info->idx."' ".  $selected .">".$contract_info->ct_title."</option>";
	}
}
?>
					</select>
				</td>
                <td align="center" bgcolor="#efefef">그룹사</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;" onClick="selectGroup('ws_co_id||ws_co_name||__||__||__||__||__');">
                </td>
			</tr>
			<tr>
                <td width="10%" height="20" align="center" >
					<select name="FIELD" id="FIELD" style="width:100%;" class="form-control input-sm">
					 <option value="">선택</option>
						<option value="E" >운전자명</option>
						<option value="D" >대표</option>
						<option value="K" >주민등록번호</option>
						<option value="Y" >전화번호</option>
						<option value="U" >차량번호</option>
						<option value="W" >차대번호</option>
					</select>
				</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="text" name="KEYWORD" id="KEYWORD" maxlength="20" value="" class="form-control" style="width:100%;">
				</td>
                <td height="20" align="center" bgcolor="#efefef">계산서 발행일</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="text" name="tax_date" id="tax_date" class="form-control datepicker" value="2018-07-31" data-date-format="yyyy-mm-dd" style="color:blue;width:100%;text-align:left;border: 1px solid #777777;">
                </td>
                <td align="center" bgcolor="#efefef">공급받는자</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<select name="p_co_id" id="p_co_id" style="width:100%;" class="form-control input-sm">
										</select>  
                </td>
                <td height="20" align="center" bgcolor="#efefef">출력</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<select name="pmode" id="pmode" class="form-control input-sm" style="width:100%;">
						  <option value="A" >양면</option>
						  <option value="S" >공급자</option>
						  <option value="C" >공급받는자</option>
					</select>
                </td>
			</tr>
			<tr>
                <td style="padding-left:5px;padding-top:10px;" colspan="6" align="center" bgcolor="#ffffff">

            <!--a href="javascript:goPrintAllTax()" class="button red" title="세금계산서"> 세금계산서전체출력 </a>

					<a href="javascript:goSearch()" class="button red" title="검색"> 검색 </a>
					<a href="javascript:goExcel()" class="button red" title="엑셀저장"> 엑셀저장 </a>
					<a href="javascript:goExcelBank()" class="button red" title="엑셀저장"> 엑셀저장(은행용) </a>
		 			<a href="javascript:goPrintAll();" class="button red" title="일괄프린트"> 일괄프린트 </a-->

					<button name="sbtn" class="dt-button buttons-print btn btn-success mr btn-xs" id="file-save-button" type="button" onClick="goSearch(document.myform.ws_co_id.value);" value="1"><i class="fa fa-search"> </i>검색</button>

					<a href="javascript:goPrintAllTax()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 세금계산서전체출력</span>
					</a>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀저장</span>
					</a>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀저장(은행용)</span>
					</a>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 일괄프린트</span>
					</a>

				</td>
					
			  </tr>
            </table>
		  </td>
        </tr>
      </table>
      <!-- 검색 끝 -->

</form>			

            </div>




<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#client_list"
                                                                   data-toggle="tab">거래등록완료현황</a></li>



                <!--li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_client"
                                                                   data-toggle="tab">신규등록</a></li>
                <li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/client/import">데이터가져오기</a-->
                </li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
	<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>마감</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>열람</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>년월</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"> <br/>수당 </td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"> <br/>공제 </td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>우체국</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>성 명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>사업자번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>주민등록번호</td>
  									  				

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=($item_cnt+1)?>">거래(운송,배달)정보</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=($item_cnt)?>">하도급거래계약단가</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=($item_cnt+1)?>">위탁배달수수료</td>
											

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="7">각종수당</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">수수료지급합계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">부과기준</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">누적금환급</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>총지급액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">지입회사</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(6+$add_gongje01_cnt)?>">위.수탁관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="6">복리후생비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">각종보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(7+$add_gongje02_cnt)?>">일반공제</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="<?=(2+$add_gongje03_cnt)?>">환급형공제</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="3">누적부가세</td>

          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;border-right:1px solid #eee;" rowspan="2"><br/>공제합계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>공제후지급액</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="5">수납인정보</td>
        </tr>
        <tr align="center" bgcolor="#e0e7ef">
          <?=$items_cols?>         
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">총계</td>

          <?=$items_cols?>         
          <?=$items_cols?>         
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리자<br>급여</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">팀(조)장<br>수당</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">난지역<br>수당</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보조금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">회식비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기타</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공급가</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">합계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세<br>누적</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과세<br>여부</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">원천징수</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세<br>환급월분</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">상조회비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">협회비</td>
		  <?=$add_gongje01_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">국민연금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">건강보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">고용보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">퇴직급여<br>충당금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">상조회비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자량보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">적재물보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보증보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">산재보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">미수금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과태료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">환경부담금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
		  <?=$add_gongje02_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">해지담보</td>
		  <?=$add_gongje03_cols?>
		
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">당월누적<br>부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">총부가세<br>누적금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세<br>누적월</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">은행</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">예금주</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">명세서보기</td>
		</tr>
                                </thead>
                                <tbody>

                                <?php
								$is_editable = 'Y';
								$i = 0;
                                if (!empty($all_stransaction_info)) {
                                    foreach ($all_stransaction_info as $st_details) {
										$dp = $this->db->where('dp_id', $st_details->dp_id)->get('tbl_members')->row();
										$ct_id = $st_details->ct_id;
										$df_id = $st_details->df_id;

 										//---------------------------------------------------------------------------------  작업 수량
										$work_vals = "";
										$ct_price_vals = "";
										$work_fee_vals = "";
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										if (!empty($all_items_group)) {
											foreach ($all_items_group as $items_info) {
												$works = $this->db->where('item_id', $items_info->idx)->where('df_id', $st_details->df_id)->where('dp_id', $st_details->dp_id)->get('tbl_delivery_pay_works')->row();
												$uprice = $this->db->where('depth', '2')->where('pid', $items_info->idx)->get('tbl_contract_sub_info')->row(); // 현규씨 뭔가 구조 잘못 되어있음 나중에 수정요망

												if(empty($works->work_cnt)) $works->work_cnt = 0;
												if(empty($uprice->unit_price)) $uprice->unit_price = 0;
												if(empty($st_details->idx)) { 
													$sql = "INSERT INTO tbl_delivery_pay_works SET df_id='".$st_details->df_id."', dp_id='".$st_details->dp_id."', item_id = '".$items_info->idx."'";
													$this->logis_model->db->query($sql);
													$wk_id = $this->db->insert_id();
												} else {
													$wk_id = $works->idx;
												}

                                                $btn_set = '<span data-placement="top" data-toggle="tooltip" title="거래 등록"><a data-toggle="modal" data-target="#myModal" href="'.base_url().'admin/logis/set_works/'.$wk_id.'" lass="text-default ml"><i class="fa fa-cog"></i></a></span>';

												$work_fee =  $works->work_cnt * $items_info->unit_price;
												$work_vals .= "<td style='text-align:right;'>".$btn_set." ".number_format($works->work_cnt,0)."</td>";
												$ct_price_vals .= "<td style='text-align:right;'>".number_format($uprice->unit_price,0)."</td>";
												$work_fee_vals .= "<td style='text-align:right;'>".number_format($work_fee,0)."</td>";
											}
										}
										//---------------------------------------------------------------------------------  작업 수량

										//차량정보
										$tr = $this->db->where('tr_id', $dp->tr_id)->where('change_date <= \''.$df_month.'-31\'')->order_by('change_date', 'DESC')->get('tbl_asset_truck_info')->row();
										
										//위수탁관리비
										$wsm_info = $this->db->where('df_id', $st_details->df_id)->get('tbl_delivery_fee_fixmfee')->row();

										if(empty($wsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $wsm_info->wst_mfee;
										if(empty($wsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $wsm_info->mfee_vat;
										if(empty($wsm_info->org_fee)) $org_fee = 0; else $org_fee = $wsm_info->org_fee;
										if(empty($wsm_info->grg_fee)) $mgrg_fee = 0; else $mgrg_fee = $wsm_info->grg_fee;
										if(empty($wsm_info->grg_fee_vat)) $mgrg_fee_vat = 0; else $mgrg_fee_vat = $wsm_info->grg_fee_vat;
										if(empty($wsm_info->etc)) $etc= 0; else $etc = $wsm_info->etc;
										$org_fee_vat = 0;
										$env_fee = 0;
										$env_fee_vat = 0;
										$car_tax = 0;
										$car_tax_vat = 0;
										$etc = 0;
										$wsm_sum = ($wst_mfee + $mfee_vat + $org_fee + $env_fee + $car_tax + $mgrg_fee + $mgrg_fee_vat + $etc);

										//각종보험
										$insur_info = $this->db->where('df_id', $st_details->df_id)->get('tbl_delivery_fee_insur')->row();
										if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
										if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
										if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
										$insur_sum = ($ins_car + $ins_load + $ins_grnt);

										//일반공제
										$gongje_info = $this->db->where('df_id', $st_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										//$gongje_info = $this->db->where('df_id', $st_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
										//if(empty($gongje_info->acc_app)) $acc_app = 0; else $acc_app = $gongje_info->acc_app;
										if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
										if(empty($gongje_info->car_tax)) $car_tax = 0; else $car_tax = $gongje_info->car_tax;
										if(empty($gongje_info->env_fee)) $env_fee = 0; else $env_fee = $gongje_info->env_fee;
										//if(empty($gongje_info->mid_pay)) $mid_pay = 0; else $mid_pay = $gongje_info->mid_pay;
										//if(empty($gongje_info->trmt_sec)) $trmt_sec = 0; else $trmt_sec = $gongje_info->trmt_sec;
										//if(empty($gongje_info->etc)) $etc = 0; else $etc = $gongje_info->etc;
										if(empty($gongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $gongje_info->grg_fee;
										if(empty($gongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $gongje_info->grg_fee_vat;
										$gongje_sum = ($fine_fee + $not_paid + $car_tax + $env_fee + $grg_fee + $grg_fee_vat);

										//환급형공제
										$rf_gongje_info = $this->db->where('df_id', $st_details->df_id)->get('tbl_delivery_fee_refund_gongje')->row();
										if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
										//if(empty($rf_gongje_info->gj_plate_mortgage)) $gj_plate_mortgage = 0; else $gj_plate_mortgage = $rf_gongje_info->gj_plate_mortgage;
										//if(empty($rf_gongje_info->gj_etc_refund)) $gj_etc_refund = 0; else $etc = $rf_gongje_info->gj_etc_refund;
										$rf_gongje_sum = ($gj_termination_mortgage);

										//---------------------------------------------------------------------------------  추가 항목
										if (!empty($all_gongje_group)) {
											foreach ($all_gongje_group as $gongje_info) {
												echo $gongje_info->add_type."xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
												$add_item = $this->db->where('pid', $gongje_info->idx)->where('df_month', $df_month)->where('dp_id', $st_details->dp_id)->get('tbl_delivery_fee_add')->row();
												if($gongje_info->add_type == 'GW') { //위수탁
													if(!empty($add_item->amount)) {
														$wsm_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje01_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje01_vals .= "<td style='text-align:right;'>*</td>";
												} else if($gongje_info->add_type == 'GN') { //일반
													if(!empty($add_item->amount)) {
														$gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje02_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje02_vals .= "<td style='text-align:right;'>*</td>";

												} else if($gongje_info->add_type == 'GR') { //환급
													if(!empty($add_item->amount)) {
														$rf_gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje03_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje03_vals .= "<td style='text-align:right;'>*</td>";
												}
											}
										}
                                      ?>
								<tr>
										<td><?=$st_details->df_id?></td>
                                        <td style="text-align:center;">
                                                <div class="btn-group">
                                                    <button class="btn btn-xs btn-<?=($st_details->is_closed=='Y')?"success":"danger"?> dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <?=($st_details->is_closed=='Y')?"마감":"마감해제"?>
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu animated zoomIn">
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/logis/close_mpay_id/<?= $df_month ?>/<?= $ct_id ?>/<?=$st_details->df_id?>/Y">마감</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/logis/close_mpay_id/<?= $df_month ?>/<?= $ct_id ?>/<?=$st_details->df_id?>/N">취소</a>
                                                        </li>
                                                    </ul>
                                                </div>
										</td>
                                        <td style="text-align:center;">
											<?=($st_details->read_cnt > 0)?"<span class='label label-success'>".$st_details->read_cnt."</span>":"<span class='label label-danger'>0</span>"?>
										</td>
										<td align="center" style="padding-left:5px;"><?= substr($st_details->df_month,0,8) ?>
										</td>
<? if (1) { ?>
<?
	$sum_sudang = 0;
	$str_sd = "";
	$pay_add = $this->db->where('df_id', $st_details->df_id)->where('df_month', $df_month)->where('add_type', '수당')->order_by('title', 'ASC')->get('tbl_delivery_pay_add')->result();
	foreach ($pay_add as $pa_details) {
		$str_sd .= $pa_details->title." : ".$pa_details->amount." \n ";
		$sum_sudang += $pa_details->amount;
		$sum[sd] += $pa_details->amount;
	}

	if($str_sd) { 
		$bg1 = "blue"; $st1="style='color:#ffffff;'"; 
	} else { $bg1 = "#ffffff"; $st1 = ""; }

?>

										<td align="center" style="padding-left:5px;" bgcolor="<?=$bg1?>"><?if($str_sd){?><a href="javascript:;" <?=$st1?> onClick="window.open('../00/scontract_add_sudang_view.php?ct_id=<?=$st_details->ct_id?>&df_month=<?=$st_details->df_month?>&dp_id=<?=$st_details->dp_id?>&df_id=<?=$st_details->df_id?>', 'wsd', 'left=50, top=50, width=600, height=400, scrollbars=1');" alt="<?=$str_sd?>" title="<?=$str_sd?>">보기</a>
										<? } ?>
											<a href="javascript:;" <?=$st1?> onClick="window.open('../03/scontract_add_sudang.php?ct_id=<?=$ct_id?>&df_month=<?=$df_month?>&dp_id=<?=$st_details->dp_id?>&df_id=<?=$st_details->df_id?>', 'wsd', 'left=50, top=50, width=600, height=400, scrollbars=1');" alt="<?=$str_sd?>" title="<?=$str_sd?>">+</a>	  
										</td>
<?
	$sum_gongje = 0;
	$str_sd = "";
	$pay_gj = $this->db->where('df_id', $st_details->df_id)->where('df_month', $df_month)->where('add_type', '공제')->order_by('title', 'ASC')->get('tbl_delivery_pay_add')->result();
	foreach ($pay_gj as $pg_details) {
		$str_sd .= $pg_details->title." : ".$pg_details->amount." \n";
		$sum_gongje += $pg_details->amount;
		$sum[gj] += $pg_details->amount;
	}

	if($str_sd) { $bg2 = "red"; $st1="style='color:#ffffff;'"; } else { $bg2 = "#ffffff"; $st2 = ""; }
?>
										<td align="center" style="padding-left:5px;" bgcolor="<?=$bg2?>"><?if($str_sd){?><a href="javascript:;" <?=$st1?> onClick="window.open('../00/scontract_add_gongje_view.php?ct_id=<?=$ct_id?>&df_month=<?=$df_month?>&dp_id=<?=$st_details->dp_id?>&df_id=<?=$st_details->df_id?>', 'wgj', 'left=50, top=50, width=600, height=400, scrollbars=1');" alt="<?=$str_sd?>" title="<?=$str_sd?>">보기</a><? } ?><a href="javascript:;" <?=$st2?> onClick="window.open('../03/scontract_add_gongje.php?ct_id=<?=$ct_id?>&df_month=<?=$df_month?>&dp_id=<?=$st_details->dp_id?>&df_id=<?=$st_details->df_id?>&co_code=<?=$st_details->co_code?>', 'wgj', 'left=50, top=50, width=600, height=400, scrollbars=1');" alt="<?=$str_sd?>" title="<?=$str_sd?>">+</a>
										</td>
<? } ?>
										<td align="center" style="padding-left:5px;"><?= $st_details->D ?></td><!-- 우체국 -->
										<td align="center" style="padding-left:5px;"><?= $st_details->ceo ?></td><!-- 이름 -->
										<td align="center" style="padding-left:5px;"><?= $st_details->bs_number ?></td><!-- 사업자 -->
										<td align="center" style="padding-left:5px;"><span class='label label-primary'><?= $tr->car_1 ?></span></td><!-- 차량번호 -->
										<td align="center" style="padding-left:5px;"><?= $st_details->reg_number ?></td><!-- 주민번호 -->
										<?php 
										//거래정보
										echo	$work_vals; ?>
										<td align="center" style="padding-left:5px;"><?=number_format($st_details->sum_transaction) ?></td><!-- 총수량 -->
										<?php
										//계약단가
										echo	$ct_price_vals;
										?>
										<?php
										//수수료
										echo	$work_fee_vals;
										?>
										<td align="center" style="padding-left:5px;text-align:right;"><?=number_format($st_details->fee_subtotal) ?></td><!-- 계 -->

                                        <td style="text-align:right;">
<?php if($is_editable == 'Y') { ?>
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="관리비 설정">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_mfee/<?= $st_details->df_id ?>/<?= $st_details->dp_id ?>/<?= $df_month ?>/<?= $ct_id ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
<?php } ?>
										<?= number_format($wst_mfee) ?></td>
                                        <td style="text-align:right;"><? if(!empty($mfee_vat)) echo number_format($mfee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee)) echo number_format($mgrg_fee,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee_vat)) echo number_format($mgrg_fee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($org_fee)) echo number_format($org_fee,0); ?></td>
									  <?=$add_gongje01_vals?>
										<td style="text-align:right;"><? if(!empty($wsm_sum)) echo number_format($wsm_sum,0); ?></td>

		  <td bgcolor="yellow"><input type="text" name="manager_pickup[<?=$i?>" id="manager_pickup<?=$i?>" value="<?=$st_details->manager_pickup ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('manager_pickup','<?= $st_details->df_id ?>',this.value);hide_inputbox('manager_pickup<?=$i?>');" onDblClick="view_inputbox('manager_pickup<?=$i?>');"></td><!-- 관리자픽업 -->
          <td bgcolor="yellow"><input type="text" name="Z[<?=$i?>" id="Z<?=$i?>" value="<?=$st_details->Z ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('Z','<?= $st_details->df_id ?>',this.value);hide_inputbox('Z<?=$i?>');" onDblClick="view_inputbox('Z<?=$i?>');"></td><!-- 팀조장수당 -->
          <td bgcolor="yellow"><input type="text" name="V[<?=$i?>" id="V<?=$i?>" value="<?=$st_details->V ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('V','<?= $st_details->df_id ?>',this.value);hide_inputbox('V<?=$i?>');" onDblClick="view_inputbox('V<?=$i?>');"></td><!-- 난지역 수당 -->
          
		  <td bgcolor="yellow"><input type="text" name="subside[<?=$i?>]" id="subside<?=$i?>" value="<?=$st_details->subside ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('subside','<?= $st_details->df_id ?>',this.value);hide_inputbox('subside<?=$i?>');" onDblClick="view_inputbox('subside<?=$i?>');"></td><!-- 보조금 -->
          
		  <td bgcolor="yellow"><input type="text" name="hoisik[<?=$i?>]" id="hoisik<?=$i?>" value="<?=$st_details->hoisik ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('hoisik','<?= $st_details->df_id ?>',this.value);hide_inputbox('hoisik<?=$i?>');" onDblClick="view_inputbox('hoisik<?=$i?>');"></td><!-- 회식비 -->
<? $st_details->sudang_etc += $sum_sudang; ?>
          <td bgcolor="yellow"><input type="text" name="sudang_etc[<?=$i?>]" id="sudang_etc<?=$i?>" value="<?=$st_details->sudang_etc ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('sudang_etc','<?= $st_details->df_id ?>',this.value);hide_inputbox('sudang_etc<?=$i?>');" onDblClick="view_inputbox('sudang_etc<?=$i?>');"></td><!-- 기타 -->
<?
//수당소계
$st_details->sudang_subtotal = $st_details->manager_pickup + $st_details->Z + $st_details->V + $st_details->subside + $st_details->hoisik + $st_details->sudang_etc; // + $sum_sudang;
?>
				<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->sudang_subtotal>0)?number_format($st_details->sudang_subtotal):"0" ?></td><!-- 소계 -->
<?
$st_details->AA = $st_details->fee_subtotal + $st_details->sudang_subtotal;
$st_details->AC = round($st_details->AA / 11,0);
$st_details->AB = $st_details->AA - $st_details->AC;
//if($tr_mode == "Y") {
//	sql_query("update tbl_delivery_fee set AA='$st_details->AA',AC='$st_details->AC',AB='$st_details->AB' where df_id='$st_details->df_id'");
//}
?>
          <td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->AB>0)?number_format($st_details->AB):"" ?></td>
          <td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->AC>0)?number_format($st_details->AC):"" ?></td>
          <td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->AA>0)?number_format($st_details->AA):"" ?></td>

				<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->vat_acc_yn) ?></td><!-- 부가세누적여부 -->
				<td align="center" style="padding-left:5px;text-align:right;"><?=$st_details->applytax_yn ?></td><!-- 과세여부 -->

<!-- 누적환급금 --- 원천징수 부가세 부가세환급월분 상조회비 소계 -->
          <td bgcolor="yellow"><input type="text" name="nujeok_woncheon[<?=$i?>" id="nujeok_woncheon<?=$i?>" value="<?=$st_details->nujeok_woncheon ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('nujeok_woncheon','<?= $st_details->dp_id ?>',this.value);hide_inputbox('nujeok_woncheon<?=$i?>');" onDblClick="view_inputbox('nujeok_woncheon<?=$i?>');"></td><!-- 원천징수 -->
          <td bgcolor="yellow"><input type="text" name="nujeok_vat[<?=$i?>" id="nujeok_vat<?=$i?>" value="<?=$st_details->nujeok_vat ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('nujeok_vat','<?= $st_details->dp_id ?>',this.value);hide_inputbox('nujeok_vat<?=$i?>');" onDblClick="view_inputbox('nujeok_vat<?=$i?>');"></td><!-- 부가세 -->
          <td bgcolor="yellow"><input type="text" name="nujeok_vat_return_month[<?=$i?>" id="nujeok_vat_return_month<?=$i?>" value="<?=$st_details->nujeok_vat_return_month ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('nujeok_vat_return_month','<?= $st_details->dp_id ?>',this.value);hide_inputbox('nujeok_vat_return_month<?=$i?>');" onDblClick="view_inputbox('nujeok_vat_return_month<?=$i?>');"></td><!-- 부가세환급월분 -->
          <td bgcolor="yellow"><input type="text" name="sangjo_membership[<?=$i?>" id="sangjo_membership<?=$i?>" value="<?=$st_details->sangjo_membership ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('sangjo_membership','<?= $st_details->dp_id ?>',this.value);hide_inputbox('sangjo_membership<?=$i?>');" onDblClick="view_inputbox('sangjo_membership<?=$i?>');"></td><!-- 상조회비 -->
<? $st_details->nujeok_subtotal =  $st_details->nujeok_woncheon + $st_details->nujeok_vat + $st_details->sangjo_membership;  ?>
				<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->nujeok_subtotal>0)?number_format($st_details->nujeok_subtotal):"" ?></td> 
<?
$st_details->nujeok_total = $st_details->AA + $st_details->nujeok_subtotal;

?>
				<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->nujeok_total>0)?number_format($st_details->nujeok_total):"" ?></td> 

				<td align="center" style="padding-left:5px;text-align:left;"><?//=$mb->X?></td> <!-- 지입회사 -->

										<td style="text-align:right;"><?= number_format($car_tax) ?></td><!--//자동차세 -->
										<td style="text-align:right;"><?= number_format($env_fee) ?></td><!--//환경부담금 -->
										<td style="text-align:right;"><?= number_format($grg_fee) ?></td><!--//차고지비 -->
                                        <td style="text-align:right;"><?= number_format($grg_fee_vat) ?></td><!--//차고지비 부가세 -->
									  
									  <?=$add_gongje02_vals?>

										<td style="text-align:right;"><?= number_format($gongje_sum) ?></td>
<?
// 관리비 소계
$st_details->AO = $st_details->AL + $st_details->AL_vat + $st_details->AM + $st_details->AN + $st_details->car_tax + $st_details->AP + $st_details->non_paid + $st_details->wst_etc;
//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set AO='$st_details->AO' where df_id='$st_details->df_id'");
?>
				<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->AO>0)?number_format($st_details->AO):"" ?></td> 
<?//국민연금 불러오는 db변수 변경?>
				<td align="center" style="padding-left:5px;text-align:right;"><?//=($fixm[ins_km]>0)?number_format($fixm[ins_km]):"" ?></td><!-- 국민 -->
				<td align="center" style="padding-left:5px;text-align:right;"><?//=($fixm[ins_hl]>0)?number_format($fixm[ins_hl]):"" ?></td><!-- 건강 -->
				<td align="center" style="padding-left:5px;text-align:right;"><?//=($fixm[ins_em]>0)?number_format($fixm[ins_em]):"" ?></td><!-- 고용 -->
				<td align="center" style="padding-left:5px;text-align:right;"><?//=($st_details->retire_choongdang>0)?number_format($st_details->retire_choongdang):"" ?></td><!-- 퇴직급여충당금 -->
          <td bgcolor="yellow"><input type="text" name="wf_sangjo_membership[<?=$i?>" id="wf_sangjo_membership<?=$i?>" value="<?=$st_details->wf_sangjo_membership ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('wf_sangjo_membership','<?= $st_details->df_id ?>',this.value);hide_inputbox('wf_sangjo_membership<?=$i?>');" onDblClick="view_inputbox('wf_sangjo_membership<?=$i?>');"></td><!-- 상조회비 -->
				
<?
// 복리후생비 소계
$st_details->sum_welfare = 0; //$fixm->ins_km + $fixm->ins_hl + $fixm->ins_em + $st_details->retire_choongdang + $st_details->wf_sangjo_membership;
//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set sum_welfare='$st_details->sum_welfare' where df_id='$st_details->df_id'");

//5월 임시
if($df_month == '2016-05' && $co_code == 'hsk03') {
	$sql = "SELECT a. * , b.X FROM tbl_delivery_fee a, tbl_members b WHERE ( a.status = '마감'AND a.dp_id = b.dp_id)";
	$sql .= "AND a.G IS NULL AND a.df_month = '$df_month'  AND TRIM( a.co_code ) = 'hsk03' and a.dp_id='$st_details->dp_id'";
	$ins9 = sql_fetch($sql);
  $st_details->ASS = $ins9[ASS];
  $st_details->AT = $ins9[AT];
  $st_details->AU = $ins9[AU];
  $st_details->BE = $ins9[BE];
}
?>
				<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->sum_welfare>0)?number_format($st_details->sum_welfare):"" ?></td><!-- 소계 -->

          <td bgcolor="yellow"><input type="text" name="ASS[<?=$i?>" id="ASS<?=$i?>" value="<?=$st_details->ASS ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('ASS','<?= $st_details->df_id ?>',this.value);hide_inputbox('ASS<?=$i?>');" onDblClick="view_inputbox('ASS<?=$i?>');"></td><!-- 차량보험료 -->
          <td bgcolor="yellow"><input type="text" name="AT[<?=$i?>" id="AT<?=$i?>" value="<?=$st_details->AT ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('AT','<?= $st_details->df_id ?>',this.value);hide_inputbox('AT<?=$i?>');" onDblClick="view_inputbox('AT<?=$i?>');"></td><!-- 적재물보험료 보증보험료  -->
          <td bgcolor="yellow"><input type="text" name="AU[<?=$i?>" id="AU<?=$i?>" value="<?=$st_details->AU ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('AU','<?= $st_details->df_id ?>',this.value);hide_inputbox('AU<?=$i?>');" onDblClick="view_inputbox('AU<?=$i?>');"></td>
<?
// 산재보험제외 확인
//$sd = sql_fetch("select * from tbl_scontract_sudang where ct_id='$ct_id' and dp_id='$mb[dp_id]'");

if(!$st_details->BE) $st_details->BE = 0;
?>
          <td bgcolor="yellow"><input type="text" name="BE[<?=$i?>" id="BE<?=$i?>" value="<?=$st_details->BE ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('BE','<?= $st_details->dp_id ?>',this.value);hide_inputbox('BE<?=$i?>');" onDblClick="view_inputbox('BE<?=$i?>');"></td><!-- 적재물보험료 보증보험료 산재보험료 -->
<?

//		  각종보험료
 $st_details->AV = $st_details->ASS + $st_details->AT + $st_details->AU + $st_details->BE;
//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set AV='$st_details->AV' where df_id='$st_details->df_id'");
?>
         <td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->AV>0)?number_format($st_details->AV):"" ?></td>

				<td bgcolor="yellow"><input type="text" name="back_overs[<?=$i?>" id="back_overs<?=$i?>" value="<?=$st_details->back_overs ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('back_overs','<?= $st_details->dp_id ?>',this.value);hide_inputbox('back_overs<?=$i?>');" onDblClick="view_inputbox('back_overs<?=$i?>');"></td><!-- 과오납금환수 -->
          <td bgcolor="yellow"><input type="text" name="BI[<?=$i?>" id="BI<?=$i?>" value="<?=$st_details->BI ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('BI','<?= $st_details->dp_id ?>',this.value);hide_inputbox('BI<?=$i?>');" onDblClick="view_inputbox('BI<?=$i?>');"></td><!-- PDA -->
          <td bgcolor="yellow"><input type="text" name="BH[<?=$i?>" id="BH<?=$i?>" value="<?=$st_details->BH ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('BH','<?= $st_details->dp_id ?>',this.value);hide_inputbox('BH<?=$i?>');" onDblClick="view_inputbox('BH<?=$i?>');"></td><!-- 민원처리 -->
          <td bgcolor="yellow"><input type="text" name="fine_fee[<?=$i?>" id="fine_fee<?=$i?>" value="<?=$st_details->fine_fee ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('fine_fee','<?= $st_details->dp_id ?>',this.value);hide_inputbox('fine_fee<?=$i?>');" onDblClick="view_inputbox('fine_fee<?=$i?>');"></td><!-- 과태료 -->
          <td bgcolor="yellow"><input type="text" name="BJ[<?=$i?>" id="BJ<?=$i?>" value="<?=$st_details->BJ ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('BJ','<?= $st_details->dp_id ?>',this.value);hide_inputbox('BJ<?=$i?>');" onDblClick="view_inputbox('BJ<?=$i?>');"></td><!-- 사고접부비 -->

<? 
//if($P=="N") {
//}

/// 임시
	//	if($mb[P]=="직원") {
		//	sql_query("update tbl_delivery_fee set applytax_yn='직원' where dp_id='$st_details->dp_id'");
	//	}
/// 임시

if($st_details->applytax_yn=="N" && $st_details->applytax_yn!="직원") {
	//if(!$st_details->nbs_fee) $st_details->nbs_fee = round($st_details->AB * 0.033,0);
	if(1) $st_details->nbs_fee = round($st_details->AB * 0.033,0);
	//if(!$st_details->nbs_back_vat) $st_details->nbs_back_vat = round($st_details->AC,0);
	if(1) $st_details->nbs_back_vat = round($st_details->AC,0);
	$st_details->nbs_back_vat = round($st_details->nbs_back_vat,0);
	//if(1)  sql_query("update tbl_delivery_fee set nbs_fee='$st_details->nbs_fee',nbs_back_vat='$st_details->nbs_back_vat' where df_id='$st_details->df_id'");
	//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set nbs_fee='$st_details->nbs_fee',nbs_back_vat='$st_details->nbs_back_vat' where df_id='$st_details->df_id'");
} else {
	$st_details->nbs_fee =0;
	$st_details->nbs_back_vat =0;
	//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set nbs_fee='$st_details->nbs_fee',nbs_back_vat='$st_details->nbs_back_vat' where df_id='$st_details->df_id'");
	//if(1)  sql_query("update tbl_delivery_fee set nbs_fee='$st_details->nbs_fee',nbs_back_vat='$st_details->nbs_back_vat' where df_id='$st_details->df_id'");
}
?>
          <td bgcolor="yellow"><input type="text" name="nbs_fee[<?=$i?>" id="nbs_fee<?=$i?>" value="<?=$st_details->nbs_fee ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('nbs_fee','<?= $st_details->df_id ?>',this.value);hide_inputbox('nbs_fee<?=$i?>');" onDblClick="view_inputbox('nbs_fee<?=$i?>');"></td><!-- 비사업자원천징수 -->
          <td bgcolor="yellow"><input type="text" name="nbs_back_vat[<?=$i?>" id="nbs_back_vat<?=$i?>" value="<?=$st_details->nbs_back_vat ?>" class="text" style="width:60px;text-align:right;border:0px; background-color:transparent;" onBlur="goSet('nbs_back_vat','<?= $st_details->df_id ?>',this.value);hide_inputbox('nbs_back_vat<?=$i?>');" onDblClick="view_inputbox('nbs_back_vat<?=$i?>');"></td><!-- 비사업자부가세환수 -->
<?
$st_details->gongje_etc += $st_details->cere_fee + $sum_gongje;
//공제  =    경조사비  +  기타 금액
//$gongje = $st_details->cere_fee + $st_details->gongje_etc;
?>
				<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->gongje_etc>0)?number_format($st_details->gongje_etc):"" ?></td><!-- 기타 -->
<?
//		  일반공제
 $st_details->BK = $st_details->back_overs + $st_details->BI + $st_details->BH + $st_details->fine_fee + $st_details->BJ + $st_details->nbs_fee + $st_details->nbs_back_vat + $st_details->gongje_etc ; //+$st_details->not_paid
//echo "$st_details->back_overs + $st_details->BI + $st_details->BH+$st_details->not_paid + $st_details->fine_fee + $st_details->BJ + $st_details->nbs_fee + $st_details->nbs_back_vat + $st_details->gongje_etc ;";
//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set BK='$st_details->BK' where df_id='$st_details->df_id'");

//부가세 누적 = acc_vat_paid_yn 부가세지급여부 acc_vat_month=부가세 누적개월
//$vv = sql_fetch("select sum(acc_vat_dmonth) as avd, count(acc_vat_dmonth) as cnt from tbl_delivery_fee where df_id ='$st_details->df_id' and  df_month < '$st_details->df_month'"); // 기존 누적 부가세
$st_details->acc_vat_sum = 0;//$vv[avd]; //총부가세누적 
//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set acc_vat_sum='$st_details->acc_vat_sum' where df_id='$st_details->df_id'");
?>
          <td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->BK>0)?number_format($st_details->BK):"" ?></td>
		<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->acc_vat_dmonth>0)?number_format($st_details->acc_vat_dmonth):"" ?></td><!-- 당월누적부가세 -->
		<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->acc_vat_sum>0)?number_format($st_details->acc_vat_sum):"" ?></td><!-- 총부가세누적금 -->
		<td align="center" style="padding-left:5px;text-align:right;"><?=($st_details->acc_vat_dmonth_cnt)?($st_details->acc_vat_dmonth_cnt):"" ?></td><!-- 부가세누적월 -->
		  
 <?
//		  공제총액 $st_details->AO  $st_details->sum_welfare $st_details->AV
 $st_details->BL = $st_details->AO + $st_details->sum_welfare + $st_details->AV + $st_details->BK; // + $st_details->gongje_etc; //$sum_gongje;// + $st_details->BL;
 //if($acc_vat_yn=="Y")  $st_details->BL += $st_details->acc_vat_dmonth;
//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set BL='$st_details->BL' where df_id='$st_details->df_id'");
?>
         <td align="center" style="padding-left:5px;color:red;text-align:right;"><?=number_format($st_details->BL) ?></td><!-- 공제총액 -->
 <?
//		  공제후지급액
 $st_details->BM = $st_details->nujeok_total - $st_details->BL;
//if($tr_mode == "Y")  sql_query("update tbl_delivery_fee set BM='$st_details->BM' where df_id='$st_details->df_id'");
?>
          <td align="center" style="padding-left:5px;color:blue;text-align:right;"><?=number_format($st_details->BM) ?></td>

          <td align="center" style="padding-left:5px;"><?//=($mb[AA]) ?></td>
          <td align="center" style="padding-left:5px;"><?//=($mb[AB]) ?></td>
          <td align="center" style="padding-left:5px;"><?//=($mb[AC]) ?></td>
          <td align="center" style="padding-left:5px;"><?//=($mb[AD]) ?></td>
          <td align="center">
            <a href="javascript:goDetailView('<?= $st_details->df_id ?>')" class="button grey" title="상세보기"> 상세보기 </a>
            <a href="javascript:management('<?= $st_details->df_id ?>')" class="button grey" title="관리비"> 관리비 </a>
			
<? if($tr_mode == "Y" && $st_details->acc_vat_sum>0) { ?>
            <a href="javascript:goRefund('<?= $st_details->dp_id ?>')" class="button red" title="누적부가세"> 누적부가세 환급 </a>
<? } ?>
<? 
if($tr_mode == "Y" && $st_details->applytax_yn=="Y") { // && $st_details->applytax_yn == "직워" ) { ?>
            <a href="javascript:goPrintTax('<?= $st_details->df_id ?>')" class="button red" title="세금계산서"> 세금계산서 </a>
<? } ?>
<? if($tr_mode == "Y" && $st_details->applytax_yn=="Y") {// && $mb->P == "Y" ) { ?>
            <a href="javascript:gobugasse('<?= $st_details->df_id ?>')" class="button red" title="부가세신고"> 부가세신고 </a>
<? } ?>
		  </td>
        </tr>                                        <?php
  								$i++;
                                  }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>

<ul class="pagination">
<?
$pagelist = get_paging(10, $page, $total_page, base_url()."admin/basic/partner_list_all/",'','');
echo $pagelist;
?>
</ul>



<?php
function get_paging($write_pages, $cur_page, $total_page, $url, $add="", $list_mode)
{
    $str = "";
    if ($cur_page > 1) {
		$str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goSearch(\'\',1,\''.$list_mode.'\');">First</a></li>';
        //$str .= "<a href='" . $url . "1{$add}'>처음</a>";
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goSearch(\'\','.($start_page-1).',\''.$list_mode.'\');">Previous</a></li>';
//$str .= " &nbsp;<a href='" . $url . ($start_page-1) . "{$add}'>이전</a>";

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= '<li class="paginate_button" tabindex="0"><a href="javascript:goSearch(\'\','.$k.',\''.$list_mode.'\');">'.$k.'</a></li>';
			//" &nbsp;<a href='$url$k{$add}'><span>$k</span></a>";
            else
                $str .= '<li class="paginate_button active" tabindex="0"><a href="javascript:goSearch(\'\','.$k.',\''.$list_mode.'\');">'.$k.'</a></li>';
//                $str .= " &nbsp;<b>$k</b> ";
        }
    }

    if ($total_page > $end_page) $str .= '<li class="paginate_button next" tabindex="0"><a href="javascript:goSearch(\'\','.($end_page+1).',\''.$list_mode.'\');">Next</a></li>';
	//$str .= " &nbsp;<a href='" . $url . ($end_page+1) . "{$add}'>다음</a>";

    if ($cur_page < $total_page) {
        //$str .= "[<a href='$url" . ($cur_page+1) . "'>다음</a>]";
		$str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goSearch(\'\','.$total_page.',\''.$list_mode.'\');">Last</a></li>';
//        $str .= " &nbsp;<a href='$url$total_page{$add}'>맨끝</a>";
    }
    $str .= "";

    return $str;
}
?>










                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_client"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/client/save_client/<?php
                              if (!empty($client_info)) {
                                  echo $client_info->client_id;
                              }
                              ?>" method="post" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-3"></label
                                <div class="col-sm-6">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_compnay"
                                                                  data-toggle="tab"><?= lang('general') ?></a>
                                            </li>
                                            <li><a href="#contact_compnay"
                                                   data-toggle="tab"><?= lang('client_contact') . ' ' . lang('details') ?></a>
                                            </li>
                                            <li><a href="#web_compnay" data-toggle="tab"><?= lang('web') ?></a></li>
                                            <li><a href="#hosting_compnay" data-toggle="tab"><?= lang('hosting') ?></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** general *************-->
                                            <div class="chart tab-pane active" id="general_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_name') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->name)) {
                                                                   echo $client_info->name;
                                                               }
                                                               ?>" name="name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_email') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="email" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->email)) {
                                                                   echo $client_info->email;
                                                               }
                                                               ?>" name="email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_vat') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->vat)) {
                                                            echo $client_info->vat;
                                                        }
                                                        ?>" name="vat">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('customer_group') ?></label>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <select name="customer_group_id"
                                                                    class="form-control select_box"
                                                                    style="width: 100%">
                                                                <?php
                                                                if (!empty($all_customer_group)) {
                                                                    foreach ($all_customer_group as $customer_group) : ?>
                                                                        <option
                                                                            value="<?= $customer_group->customer_group_id ?>"<?php
                                                                        if (!empty($client_info->customer_group_id) && $client_info->customer_group_id == $customer_group->customer_group_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $customer_group->customer_group; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                            <?php if (!empty($created)) { ?>
                                                                <div class="input-group-addon"
                                                                     title="<?= lang('new') . ' ' . lang('customer_group') ?>"
                                                                     data-toggle="tooltip" data-placement="top">
                                                                    <a data-toggle="modal" data-target="#myModal"
                                                                       href="<?= base_url() ?>admin/client/customer_group"><i
                                                                            class="fa fa-plus"></i></a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('language') ?></label>
                                                    <div class="col-sm-5">
                                                        <select name="language" class="form-control select_box"
                                                                style="width: 100%">
                                                            <?php

                                                            foreach ($languages as $lang) : ?>
                                                                <option
                                                                    value="<?= $lang->name ?>"<?php
                                                                if (!empty($client_info->language) && $client_info->language == $lang->name) {
                                                                    echo 'selected';
                                                                } elseif (empty($client_info->language) && $this->config->item('language') == $lang->name) {
                                                                    echo 'selected';
                                                                } ?>
                                                                ><?= ucfirst($lang->name) ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('currency') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="currency" class="form-control select_box"
                                                                style="width: 100%">

                                                            <?php if (!empty($currencies)): foreach ($currencies as $currency): ?>
                                                                <option
                                                                    value="<?= $currency->code ?>"
                                                                    <?php
                                                                    if (!empty($client_info->currency) && $client_info->currency == $currency->code) {
                                                                        echo 'selected';
                                                                    } elseif (empty($client_info->currency) && $this->config->item('default_currency') == $currency->code) {
                                                                        echo 'selected';
                                                                    } ?>
                                                                ><?= $currency->name ?></option>
                                                                <?php
                                                            endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('short_note') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="short_note"><?php
                                                if (!empty($client_info->short_note)) {
                                                    echo $client_info->short_note;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <?php
                                                if (!empty($client_info)) {
                                                    $client_id = $client_info->client_id;
                                                } else {
                                                    $client_id = null;
                                                }
                                                ?>
                                                <?= custom_form_Fields(12, $client_id); ?>
                                            </div><!-- ************** general *************-->

                                            <!-- ************** Contact *************-->
                                            <div class="chart tab-pane" id="contact_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_phone') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->phone)) {
                                                            echo $client_info->phone;
                                                        }
                                                        ?>" name="phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_mobile') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->mobile)) {
                                                                   echo $client_info->mobile;
                                                               }
                                                               ?>" name="mobile">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('zipcode') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->zipcode)) {
                                                            echo $client_info->zipcode;
                                                        }
                                                        ?>" name="zipcode">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_city') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->city)) {
                                                            echo $client_info->city;
                                                        }
                                                        ?>" name="city">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_country') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="country" class="form-control select_box"
                                                                style="width: 100%">
                                                            <optgroup label="Default Country">
                                                                <option
                                                                    value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                                            </optgroup>
                                                            <optgroup label="<?= lang('other_countries') ?>">
                                                                <?php if (!empty($countries)): foreach ($countries as $country): ?>
                                                                    <option
                                                                        value="<?= $country->value ?>" <?= (!empty($client_info->country) && $client_info->country == $country->value ? 'selected' : NULL) ?>><?= $country->value ?>
                                                                    </option>
                                                                    <?php
                                                                endforeach;
                                                                endif;
                                                                ?>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_fax') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->fax)) {
                                                            echo $client_info->fax;
                                                        }
                                                        ?>" name="fax">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_address') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="address"><?php
                                                if (!empty($client_info->address)) {
                                                    echo $client_info->address;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">
                                                        <a href="#"
                                                           onclick="fetch_lat_long_from_google_cprofile(); return false;"
                                                           data-toggle="tooltip"
                                                           data-title="<?php echo lang('fetch_from_google') . ' - ' . lang('customer_fetch_lat_lng_usage'); ?>"><i
                                                                id="gmaps-search-icon" class="fa fa-google"
                                                                aria-hidden="true"></i></a>
                                                        <?= lang('latitude') . '( ' . lang('google_map') . ' )' ?>
                                                    </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->latitude)) {
                                                            echo $client_info->latitude;
                                                        }
                                                        ?>" name="latitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('longitude') . '( ' . lang('google_map') . ' )' ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->longitude)) {
                                                                   echo $client_info->longitude;
                                                               }
                                                               ?>" name="longitude">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Contact *************-->
                                            <!-- ************** Web *************-->
                                            <div class="chart tab-pane" id="web_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_domain') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->website)) {
                                                            echo $client_info->website;
                                                        }
                                                        ?>" name="website">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('skype_id') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->skype_id)) {
                                                            echo $client_info->skype_id;
                                                        }
                                                        ?>" name="skype_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('facebook_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->facebook)) {
                                                            echo $client_info->facebook;
                                                        }
                                                        ?>" name="facebook">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('twitter_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->twitter)) {
                                                            echo $client_info->twitter;
                                                        }
                                                        ?>" name="twitter">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('linkedin_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->linkedin)) {
                                                            echo $client_info->linkedin;
                                                        }
                                                        ?>" name="linkedin">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Web *************-->
                                            <!-- ************** Hosting *************-->
                                            <div class="chart tab-pane" id="hosting_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hosting_company') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hosting_company)) {
                                                            echo $client_info->hosting_company;
                                                        }
                                                        ?>" name="hosting_company">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hostname') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hostname)) {
                                                            echo $client_info->hostname;
                                                        }
                                                        ?>" name="hostname">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('username') ?> </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->username)) {
                                                            echo $client_info->username;
                                                        }
                                                        ?>" name="username">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('password') ?></label>
                                                    <div class="col-lg-5">
                                                        <?php
                                                        if (!empty($client_info->password)) {
                                                            $password = strlen(decrypt($client_info->password));
                                                        }
                                                        ?>
                                                        <input type="password" name="password" value=""
                                                               placeholder="<?php
                                                               if (!empty($password)) {
                                                                   for ($p = 1; $p <= $password; $p++) {
                                                                       echo '*';
                                                                   }
                                                               }
                                                               ?>" class="form-control">
                                                        <strong id="show_password" class="required"></strong>
                                                    </div>
                                                    <?php if (!empty($client_info->password)) { ?>
                                                        <div class="col-lg-3">
                                                            <a data-toggle="modal" data-target="#myModal"
                                                               href="<?= base_url('admin/client/see_password/c_' . $client_info->client_id) ?>"
                                                               id="see_password"><?= lang('see_password') ?></a>
                                                            <strong id="hosting_password" class="required"></strong>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('port') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->port)) {
                                                            echo $client_info->port;
                                                        }
                                                        ?>" name="port">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Hosting *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                        </div>
                                        <div class="col-lg-3">
                                            <button type="submit" name="save_and_create_contact" value="1"
                                                    class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button>
                                        </div>

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"').val();
        data.city = $('input[name="city"').val();
        data.country = $('select[name="country" option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"').val(data.lat);
                $('input[name="longitude"').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>