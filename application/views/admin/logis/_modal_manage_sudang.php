<script>
function goDelete(id) {
	location.href = '<?php echo base_url() ?>admin/logis/delete_sudang/'+id;
}
</script>

<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">추가<?php echo $title;?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form id="form_validation" name="myform"
              action="<?php echo base_url() ?>admin/logis/update_sudang/<?=$df_id?>/<?=$add_type?>" method="post" class="form-horizontal form-groups-bordered">

            <div class="form-group" id="border-none">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="trm_table">
        <tr align="center" bgcolor="#e0e7ef">
          <td height="30" style="color:#ffffff;background-color: #777777;" width="40">항목</td>
		  <td align="center" style="padding-left:5px;" width="100"><input type="text" name="title" value="<?//=$ccC ?>" class='form-control' style="width:90%;" ></td>
          <td style="color:#ffffff;background-color: #777777;" width="40">금액</td>
		  <td align="center" style="padding-left:5px;" width="100"><input type="text" name="amount" value="<?//=$cc[C ?>" class='form-control' style="width:90%;" ></td>
          <td style="color:#ffffff;background-color: #777777;" width="40">내역</td>
		  <td align="center" style="padding-left:5px;"><input type="text" name="memo" value="<?//=$cc[C ?>" class='form-control' style="width:98%;" ></td>
		  <td style="color:#00000;background-color: #ffffff;" width="70">
                  <button type="submit" class="dt-button buttons-print btn btn-danger btn-xs mr"><span><i class="fa fa-plus-square"> </i> 저장</span></button>
		  </td>
        </tr>
		</table>
	  
	  
	  
	  
	  
	  
	  
	  
	  <div style="padding-top:5px;"></div>


      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="trm_table">
        <tr align="center" bgcolor="#e0e7ef">
          <td height="30" width="40" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
				  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" width="160">항목</td>
					<td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" width="100">금액</td>
					<td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">내역</td>
				  <td style="color:#ffffff;background-color: #777777;" width="60">삭제</td>
        </tr>

<?
$i = 0;
if (!empty($all_sudang_info)) {
	foreach ($all_sudang_info as $sudang_details) {
		$sn_no = $i+1;
		$sn_bg = "#ffffff";

?>
        <tr bgcolor="<?=$sn_bg?>">
          <td height="25" align="center"><?=$sn_no?></td>
          <td align="left" style="padding-left:5px;"><input type="text" name="vtitle<?=$i?>" id="vtitle<?=$i?>" value="<?=$sudang_details->title ?>" style="width:90%;text-align:center;border:1px; background-color:#ffffff;" onBlur="goSet('title','<?= $sudang_details->title ?>',this.value);"></td>
          <td align="left" style="padding-left:5px;"><input type="text" name="vamount<?=$i?>" id="vamount<?=$i?>" value="<?=$sudang_details->amount ?>" style="width:90%;text-align:right;border:1px; background-color:#ffffff;" onBlur="goSet('amount','<?= $sudang_details->amount ?>',this.value);"></td>
          <td align="left" style="padding-left:5px;"><?= $sudang_details->memo ?></td>
          <td align="center" style="padding-left:5px;">
		  
              <a href="javascript:goDelete('<?=$sudang_details->idx ?>')" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables" title="삭제"> 삭제 </a>
		  
		  </td>
       </tr>
<?	
		$i++;
      }
} else {
?>
        <tr bgcolor="#ffffff">
          <td colspan="11" align="center">등록된 정보가 없습니다.</td>
        </tr>
<?
    }
?>
      </table>

      <div style="padding-top:10px;"></div>


    </td>
  </tr>
</table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
            </div>
        </form>
    </div>
</div>
