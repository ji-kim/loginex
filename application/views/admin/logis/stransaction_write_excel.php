<?php
// 거래항목
$scnt = 0;
$srows = "";
if (!empty($all_stransaction_items)) {
	foreach ($all_stransaction_items as $stransaction_item) {
		if($stransaction_item->title) {
			$srows .= "<td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$stransaction_item->title."</td>";
			$scnt++;
		}
	}
}

//추가항목관련
$add_sudang_cnt = 0;
$add_gongje01_cnt = 0;
$add_gongje02_cnt = 0;
$add_gongje03_cnt = 0;

$add_sudang_cols = "";
$add_gongje01_cols = "";
$add_gongje02_cols = "";
$add_gongje03_cols = "";

if (!empty($all_sudang_group)) {
	foreach ($all_sudang_group as $sudang_info) {
		if($sudang_info->type == 'S') { //일반수당
			$add_sudang_cnt++;
			$add_sudang_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$sudang_info->title."</td>";
		}
	}
}
			
if (!empty($all_gongje_group)) {
	foreach ($all_gongje_group as $gongje_info) {
		if($gongje_info->add_type == 'GW') { //위수탁
			$add_gongje01_cnt++;
			$add_gongje01_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GN') { //일반
			$add_gongje02_cnt++;
			$add_gongje02_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GR') { //환급
			$add_gongje03_cnt++;
			$add_gongje03_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		}
	}
}

?>
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
							<table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">기사<br/>확인</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>년월</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>공제청구사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>실수요처</td>

          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>성 명</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>사업자번호</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>차량번호</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>주민등록번호</td>
  									  				

          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" colspan="<?=($scnt+1)?>">거래(운송,배달)정보</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" colspan="<?=($scnt)?>">하도급거래계약단가</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" colspan="<?=($scnt+1)?>">위탁배달수수료</td>
											
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" colspan="3">수수료지급합계</td>

          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>총지급액</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(6+$add_gongje01_cnt)?>">위.수탁관리비</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" colspan="3">각종보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(7+$add_gongje02_cnt)?>">일반공제</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(2+$add_gongje03_cnt)?>">환급형공제</td>

          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>공제합계</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" rowspan="2"><br/>공제후지급액</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;" colspan="4">수납인정보</td>
        </tr>
        <tr align="center" bgcolor="#e0e7ef">
          <?=$srows?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">총계</td>
          <?=$srows?>
          <?=$srows?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">공급가</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">합계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">협회비</td>
		  <?=$add_gongje01_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">자량보험료</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">적재물보험료</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">소계</td>

		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">미수금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과태료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">환경부담금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
		  <?=$add_gongje02_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">해지담보</td>
		  <?=$add_gongje03_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소  계</td>

          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">은행</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">계좌번호</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">예금주</td>
          <td style="color:#ffffff;background-color: #777;border-right:1px solid #eee;">관계</td>
		</tr>
                                </thead>
                                <tbody>
                                <?php
								$cnt = 0;
                                if (!empty($all_stransaction_ready)) {
                                    foreach ($all_stransaction_ready as $str_details) {
										$sn = $total_count--;
										$add_sudang_vals = "";
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										//우체국
										$dp = $this->db->where('dp_id', $str_details->dp_id)->get('tbl_members')->row();
										$cs = $this->db->where('code', $str_details->co_code)->where('mb_type', 'customer')->get('tbl_members')->row();


										//공제청구사
										$gj_rq_co = "";
										if(!empty($str_details->gongje_req_co)) {
											$gj_rq_co = $this->db->where('dp_id', $str_details->gongje_req_co)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//차량정보
										$truck = $this->db->where('idx', $str_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										//위수탁관리비
//$wsm_info = $this->db->where('dp_id', $str_details->dp_id)->where('df_month', $df_month)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();
										$wsm_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_fixmfee')->row();
										//$wsm_info = $this->db->where('df_id', $str_details->df_id)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();

										if(empty($wsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $wsm_info->wst_mfee;
										if(empty($wsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $wsm_info->mfee_vat;
										if(empty($wsm_info->org_fee)) $org_fee = 0; else $org_fee = $wsm_info->org_fee;
										//if(empty($wsm_info->env_fee)) $env_fee = 0; else $env_fee = $wsm_info->env_fee;
										//if(empty($wsm_info->car_tax)) $car_tax = 0; else $car_tax = $wsm_info->car_tax;
										if(empty($wsm_info->grg_fee)) $mgrg_fee = 0; else $mgrg_fee = $wsm_info->grg_fee;
										if(empty($wsm_info->grg_fee_vat)) $mgrg_fee_vat = 0; else $mgrg_fee_vat = $wsm_info->grg_fee_vat;
										if(empty($wsm_info->etc)) $etc= 0; else $etc = $wsm_info->etc;
										$org_fee_vat = 0;
										$env_fee = 0;
										$env_fee_vat = 0;
										$car_tax = 0;
										$car_tax_vat = 0;
										$etc = 0;
										$wsm_sum = ($wst_mfee + $mfee_vat + $org_fee + $env_fee + $car_tax + $mgrg_fee + $mgrg_fee_vat + $etc);

										//각종보험
										$insur_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_insur')->row();
										if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
										if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
										if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
										$insur_sum = ($ins_car + $ins_load + $ins_grnt);

										//일반공제
										$gongje_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										//$gongje_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
										//if(empty($gongje_info->acc_app)) $acc_app = 0; else $acc_app = $gongje_info->acc_app;
										if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
										if(empty($gongje_info->car_tax)) $car_tax = 0; else $car_tax = $gongje_info->car_tax;
										if(empty($gongje_info->env_fee)) $env_fee = 0; else $env_fee = $gongje_info->env_fee;
										//if(empty($gongje_info->mid_pay)) $mid_pay = 0; else $mid_pay = $gongje_info->mid_pay;
										//if(empty($gongje_info->trmt_sec)) $trmt_sec = 0; else $trmt_sec = $gongje_info->trmt_sec;
										//if(empty($gongje_info->etc)) $etc = 0; else $etc = $gongje_info->etc;
										if(empty($gongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $gongje_info->grg_fee;
										if(empty($gongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $gongje_info->grg_fee_vat;
										$gongje_sum = ($fine_fee + $not_paid + $car_tax + $env_fee + $grg_fee + $grg_fee_vat);

										//환급형공제
										$rf_gongje_info = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_refund_gongje')->row();
										if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
										//if(empty($rf_gongje_info->gj_plate_mortgage)) $gj_plate_mortgage = 0; else $gj_plate_mortgage = $rf_gongje_info->gj_plate_mortgage;
										//if(empty($rf_gongje_info->gj_etc_refund)) $gj_etc_refund = 0; else $etc = $rf_gongje_info->gj_etc_refund;
										$rf_gongje_sum = ($gj_termination_mortgage);


										if (!empty($all_gongje_group)) {
											foreach ($all_gongje_group as $gongje_info) {
												$add_item = $this->db->where('pid', $gongje_info->idx)->where('df_month', $df_month)->where('dp_id', $str_details->dp_id)->get('tbl_delivery_fee_add')->row();
												if($gongje_info->add_type == 'GW') { //위수탁
													if(!empty($add_item->amount)) {
														$wsm_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje01_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje01_vals .= "<td style='text-align:right;'>*</td>";
												} else if($gongje_info->add_type == 'GN') { //일반
													if(!empty($add_item->amount)) {
														$gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje02_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje02_vals .= "<td style='text-align:right;'>*</td>";

												} else if($gongje_info->add_type == 'GR') { //환급
													if(!empty($add_item->amount)) {
														$rf_gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje03_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje03_vals .= "<td style='text-align:right;'>*</td>";
												}
											}
										}
										//---------------------------------------------------------------------------------  추가 항목

										$tot_gongje = ($wsm_sum + $insur_sum + $gongje_sum + $rf_gongje_sum);
										$tot_gongje_req = ($tot_gongje - $rf_gongje_sum);

										$lv = $this->db->where('df_id', $str_details->df_id)->get('tbl_delivery_fee_levy')->row();
										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($lv->pay_status)) {
											if($lv->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($lv->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($lv->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
										}

										$is_editable = "Y";
										if($close_yn == "Y" || $str_details->is_confirm=='Y' || $str_details->is_tclosed=='Y') $is_editable = "N";
										if($ws_mode == "IC") { // 등록마감처리 
											$sql_insure = "";
											$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
											$qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and a.tr_id = '".$str_details->tr_id."' and a.active_yn='Y'";
											$ins = $this->cowork_model->db->query($qry)->row();
											if(!empty($ins->pay_amt)) {
												$ins_car = $ins->pay_amt;
											} else {
												$ins_car = 0;
											}
											$sql_insure .= "ins_car ='".$ins_car."'"; 

											$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
											$qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and a.tr_id = '".$str_details->tr_id."' and a.active_yn='Y'";
											$lins = $this->cowork_model->db->query($qry)->row();
											if(!empty($lins->pay_amt)) {
												$ins_load = $lins->pay_amt;
											} else {
												$ins_load = 0;
											}
											$sql_insure .= ",ins_load ='".$ins_load."'";


											//if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
											//if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;

											$sql = "update tbl_delivery_fee set is_confirm = '".$close_yn."' where df_id='".$str_details->df_id."'";
											$this->cowork_model->db->query($sql);
											$sql = "update tbl_delivery_fee_insur set $sql_insure where df_id='".$str_details->df_id."'";
											$this->cowork_model->db->query($sql);
											$this->cowork_model->db->query($sql);

											$insur_sum = ($ins_car + $ins_load + $ins_grnt);
											$str_details->is_confirm = $close_yn;

										}
?>
		<tr bgcolor="#ffffff" >
          <td height="25" align="center">
                <?= $sn ?>
		  </td>
                                        <td style="text-align:center;">
										<?=($str_details->read_cnt > 0)?"<span class='label label-success'>".$str_details->read_cnt."</span>":"<span class='label label-danger'>0</span>"?>
								
										</td>
										  <td align="center" style="padding-left:5px;"><?= $str_details->df_month ?>	  
										  </td>

                                        <td>
                                            <span class='label label-primary'><?php if(!empty($gj_rq_co)) echo $gj_rq_co; ?></span>
										</td>
                                        <td><?= $str_details->D ?></td>

                                        <td><?= $dp->ceo ?>(<?= $dp->driver ?>)</td>
                                        <td><?= $dp->bs_number ?></td>
                                        <td>
										
										<span class='label label-primary'><?= $truck_no ?></span>
			</td>
          <!--td align="center" style="padding-left:5px;">서울85바3950</td--><!-- 차량번호 -->
                                        <td><?= $dp->reg_number ?></td>
<?php
if(empty($str_details->G)) $str_details->G = 0;
if(empty($str_details->H)) $str_details->H = 0;
if(empty($str_details->I)) $str_details->I = 0;
if(empty($str_details->AD)) $str_details->AD = 0;
?>
          <td><?=$str_details->G?></td>

          <td><?=$str_details->H?></td>

          <td><?=$str_details->I?></td>

          <td><?=$str_details->AD?></td>

				<td align="center" style="padding-left:5px;">
				
				0</td><!-- 총수량 -->
<?php
// 단가
$unit_price1 = 0;
$unit_price2 = 0;
$unit_price3 = 0;
$unit_price4 = 0;
$unit_prices = $this->db->where('dp_id', $str_details->dp_id)->where('ct_id', $str_details->ct_id)->get('tbl_scontract_co')->row();
if(!empty($unit_prices->val1)) $unit_price1 = $unit_prices->val1;
if(!empty($unit_prices->val2)) $unit_price2 = $unit_prices->val2;
if(!empty($unit_prices->val3)) $unit_price3 = $unit_prices->val3;
if(!empty($unit_prices->val4)) $unit_price4 = $unit_prices->val4;
?>
          <td align="center" style="padding-left:5px;text-align:right;"><?php if(!empty($unit_prices->val1)) echo $unit_prices->val1?></td>
          <td align="center" style="padding-left:5px;text-align:right;"><?php if(!empty($unit_prices->val2)) echo $unit_prices->val2?></td>
          <td align="center" style="padding-left:5px;text-align:right;"><?php if(!empty($unit_prices->val3)) echo $unit_prices->val3?></td>
          <td align="center" style="padding-left:5px;text-align:right;"><?php if(!empty($unit_prices->val4)) echo $unit_prices->val4?></td>
<?php
//수수료
$tr_tot1 = $str_details->G * $unit_price1;
$tr_tot2 = $str_details->H * $unit_price2;
$tr_tot3 = $str_details->I * $unit_price3;
$tr_tot4 = $str_details->AD * $unit_price4;
$tr_sub_total = $tr_tot1 + $tr_tot2 + $tr_tot3 + $tr_tot4;

$tr_vat = $tr_sub_total / 11;
$tr_supp = $tr_sub_total - $tr_vat;

?>
		<td align="center" style="padding-left:5px;background-color:#ffffff;"><?=number_format($tr_tot1,0)?></td>
		<td align="center" style="padding-left:5px;background-color:#ffffff;"><?=number_format($tr_tot2,0)?></td>
		<td align="center" style="padding-left:5px;background-color:#ffffff;"><?=number_format($tr_tot3,0)?></td>
		<td align="center" style="padding-left:5px;background-color:#ffffff;"><?=number_format($tr_tot4,0)?></td>
        <td align="center" style="padding-left:5px;text-align:right;"><?=number_format($tr_sub_total,0)?></td><!-- 계 -->


		<td align="center" style="padding-left:5px;text-align:right;"><?=number_format($tr_supp,0)?></td>
        <td align="center" style="padding-left:5px;text-align:right;"><?=number_format($tr_vat,0)?></td>
        <td align="center" style="padding-left:5px;text-align:right;"><?=number_format($tr_sub_total,0)?></td>

		<td align="center" style="padding-left:5px;text-align:right;"><?=number_format($tr_sub_total,0)?></td> <!-- 총지급액 -->


                                        <td style="text-align:right;">
										<?= number_format($wst_mfee) ?></td>
                                        <td style="text-align:right;"><? if(!empty($mfee_vat)) echo number_format($mfee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee)) echo number_format($mgrg_fee,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee_vat)) echo number_format($mgrg_fee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($org_fee)) echo number_format($org_fee,0); ?></td>
									  <?=$add_gongje01_vals?>
										<td style="text-align:right;"><? if(!empty($wsm_sum)) echo number_format($wsm_sum,0); ?></td>

										<td style="text-align:right;">
										<?= number_format($ins_car) ?></td>
										<td style="text-align:right;"><?= number_format($ins_load) ?></td>
										<td style="text-align:right;"><?= number_format($insur_sum) ?></td>

										<td style="text-align:right;">
										<?= number_format($not_paid) ?></td>
										<td style="text-align:right;">
										<?= number_format($fine_fee) ?>
										<?php if(!empty($gongje_info->fine_fee_memo)) { ?>
                                                <span data-placement="top" data-toggle="tooltip" title="<?=$gongje_info->fine_fee_memo?>">
                                            [비고]
                                                </span>
										<?php } ?>
										</td>

										<td style="text-align:right;"><?= number_format($car_tax) ?></td><!--//자동차세 -->
										<td style="text-align:right;"><?= number_format($env_fee) ?></td><!--//환경부담금 -->
										<td style="text-align:right;"><?= number_format($grg_fee) ?></td><!--//차고지비 -->
                                        <td style="text-align:right;"><?= number_format($grg_fee_vat) ?></td><!--//차고지비 부가세 -->
									  
									  <?=$add_gongje02_vals?>

										<td style="text-align:right;"><?= number_format($gongje_sum) ?></td>

                                        <td style="text-align:right;">
										<?= number_format($gj_termination_mortgage) ?></td>
									  <?=$add_gongje03_vals?>
										<td style="text-align:right;"><?= number_format($rf_gongje_sum) ?></td>
		  
										<td style="text-align:right;"><?= number_format($tot_gongje) ?></td>
<?php
$total_amount = $tr_sub_total - $tot_gongje;
//if($total_amount<0) $total_amount = 0;
//$total_amount = $str_details->tot_transaction;
?>
										<td style="text-align:right;"><?= number_format($total_amount,0) ?></td>

          <td align="center" style="padding-left:5px;"><?php if(!empty($dp->AA)) echo $dp->AA; ?></td>
          <td align="center" style="padding-left:5px;"><?php if(!empty($dp->AB)) echo $dp->AB; ?></td>
          <td align="center" style="padding-left:5px;"><?php if(!empty($dp->AC)) echo $dp->AC; ?></td>
          <td align="center" style="padding-left:5px;"><?php if(!empty($dp->AD)) echo $dp->AD; ?></td>
        </tr>
<?php
$cnt++;
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="31">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
