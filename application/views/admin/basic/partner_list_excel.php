<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>실수요처</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="27">파트너 정보</td>
        </tr>
        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전자</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">직위</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">주민등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전면허번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종사자번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약종료일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과세여부</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세누적여부</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">업태</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종목</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업장주소</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">위수탁관리사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">핸드폰번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">PDA번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">은행</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">예금주</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이메일</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;">주소</td>
        </tr>
                                </thead>

                                <tbody>

                                <?php
                                if (!empty($all_partner_info)) {
                                    foreach ($all_partner_info as $partner_details) {
										// 실수요처
										$rco = $this->db->where('mb_type', 'customer')->where('code', $partner_details->code)->get('tbl_members')->row();
                                        ?>
                                        <tr>
					  <td><?= $partner_details->dp_id ?></td>
					  <td><? if(!empty($rco->co_name)) echo $rco->co_name; ?></td>
					  <td><?= $partner_details->co_name ?></td>
					  <td><?= $partner_details->ceo ?></td>
					  <td><?= $partner_details->driver ?></td>
					  <td><?= $partner_details->partner_status ?>
								<?=$partner_details->position?>
					  </td>
					  <td><?= $partner_details->reg_number ?></td>
					  <td><?= $partner_details->L ?></td>
					  <td><?= $partner_details->M ?></td>
					  <td><?= $partner_details->N ?></td>
					  <td><?= $partner_details->O ?></td>
					  <td><?=(!empty($partner_details->P))?$partner_details->P:"" ?></td>
					  <td><?= $partner_details->acc_vat_yn ?></td>
						<td><?= $partner_details->bs_number ?></td><!-- 사업자등록번호 -->
						<td><?= $partner_details->bs_type1 ?></td><!-- 업태 -->
						<td><?= $partner_details->bs_type2 ?></td><!-- 종목 -->
						<td align="left" style="padding-left:5px;"><?= $partner_details->T ?></td><!-- 사업장주소 -->

						<td>
							<?= $partner_details->car_1 ?>
						</td><!-- 차량번호 -->
						<td><?= $partner_details->car_3 ?></td><!-- 용도 -->
						<td><?= $partner_details->car_7?></td><!-- 차대번호 -->
		  
					  <td><?= $partner_details->ws_co_name ?></td><!-- 지입사명 -->
					  <td><?= $partner_details->co_tel ?></td>
					  <td><?= $partner_details->Z ?></td>
					  <td><?= $partner_details->AA ?></td>
					  <td><?= $partner_details->AB ?></td>
					  <td><?= $partner_details->AC ?></td>
					  <td><?= $partner_details->AD ?></td>
					  <td><?= $partner_details->email ?></td>
					  <td align="left" style="padding-left:5px;"><?= $partner_details->co_address ?></td>
                                        </tr>
                                        <?php

                                   }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
