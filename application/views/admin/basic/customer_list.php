<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<?php


if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:10px';
    ?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 40px !important;
        padding-right: 40px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function popAsWindow(id) {
	  window.open('<?php echo base_url(); ?>admin/basic/set_item/'+id+'/sudang/01', 'winAdd', 'left=50, top=50, width=800, height=700, scrollbars=1');
	}
	function goSearch(id) {
	  document.myform.submit();
	}
</script>


            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/basic/customer_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
				<div class="row ">
					<div class="col-sm-12" style="margin:10px">

						<div class="form-group" id="border-none">
							<div class="col-sm-2">
								<select name="baecha_co_id" id="baecha_co_id" style="width:100%;background-color:#ffffff;" class="form-control input-sm">
										<option value="">선택조회</option>
										<option value="wmjd" > 거래처명 </option>
										<option value="0003" >대표</option>
										<option value="0001" >사업자번호</option>
										<option value="0001" >휴대폰번호</option>
										<option value="0001" >홈페이지</option>
										<option value="0001" >팩스</option>
								</select>				
							</div>
							<div class="col-sm-3">
								<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:#ffffff;" >
							</div>
							<div class="col-sm-3">
							</div>
							<div class="col-sm-4" style="padding-right:30px;">
                                                            <div class="input-group">
																<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="Search">
                                                                <span class="input-group-btn">
																	<button class="btn btn-success" type="button" onclick="goSearch();">
                                                                        검색
                                                                        <i class="fa fa-search"></i>
                                                                    </button>

																	<button class="btn btn-warning" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-file-excel-o"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>
						</div>
					</div>
				</div>

				</form>
      <!-- 검색 끝 -->

				

            </div>


<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
		
		
		<div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'group') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/client/manage_client"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($all_customer_group) > 0) { ?>
                    <li class="dropdown-submenu pull-left groups <?php if (!empty($id)) {
                        if ($search_by == 'group') {
                            echo 'active';
                        }
                    } ?>">
                        <a href="#" tabindex="-1"><?php echo lang('customer_group'); ?></a>
                        <ul class="dropdown-menu dropdown-menu-left"
                            style="<?php if (!empty($search_by) && $search_by == 'group') {
                                echo 'display:block';
                            } ?>">
                            <?php foreach ($all_customer_group as $group) {
                                ?>
                                <li class="<?php if (!empty($id)) {
                                    if ($search_by == 'group') {
                                        if ($id == $group->customer_group_id) {
                                            echo 'active';
                                        }
                                    }
                                } ?>">
                                    <a href="<?= base_url() ?>admin/client/manage_client/group/<?php echo $group->customer_group_id; ?>"><?php echo $group->customer_group; ?></a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#customer_list"
                                                                   data-toggle="tab">거래처관리</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_customer"
                                                                   data-toggle="tab">신규등록</a></li>
                <!--li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/client/import">데이터가져오기</a>
                </li-->
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="customer_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
							<table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%">
                                <thead>
                                <tr>
           
                                    <th>No</th>
                                    <th>Code</th>
                                    <th class="hidden-sm">거래처명</th>
                                    <!--th>항목설정 </th-->
                                    <th>대표 </th>
                                    <th>사업자등록번호 </th>
                                    <th>업태 </th>
                                    <th>종목 </th>
                                    <th>사업장주소 </th>
                                    <th>홈페이지 </th>
                                    <th>핸드폰번호 </th>
                                    <th>팩스 </th>
                                    <th>이메일 </th>
                                    <?php $show_custom_fields = custom_form_table(12, null);
                                    if (!empty($show_custom_fields)) {
                                        foreach ($show_custom_fields as $c_label => $v_fields) {
                                            if (!empty($c_label)) {
                                                ?>
                                                <th><?= $c_label ?> </th>
                                            <?php }
                                        }
                                    }
                                    ?>
                                    <th class="hidden-print"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>


                                <?php
                                if (!empty($all_customer_info)) {
                                    foreach ($all_customer_info as $customer_details) {
                                       // $client_transactions = $this->db->select_sum('amount')->where(array('paid_by' => $customer_details->client_id))->get('tbl_transactions')->result();
                                       // $customer_group = $this->db->where('customer_group_id', $customer_details->customer_group_id)->get('tbl_customer_group')->row();
                                       // $client_outstanding = $this->invoice_model->client_outstanding($customer_details->client_id);
                                       // $client_currency = $this->invoice_model->client_currency_sambol($customer_details->client_id);
                                       // if (!empty($client_currency)) {
                                       //     $cur = $client_currency->symbol;
                                       // } else {
                                       //     $currency = $this->db->where(array('code' => config_item('default_currency')))->get('tbl_currencies')->row();
                                       //     $cur = $currency->symbol;
                                       // }
                                        ?>
                                    <tr>
                                        <td>
                                            <?= $customer_details->dp_id ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->code ?>
                                        </td>
                                        <td>
                                            <a href="<?= base_url() ?>admin/basic/customer_details/<?= $customer_details->dp_id ?>"
                                                   class="text-info"><?= $customer_details->co_name ?></a>
										</td>
                                        <!--td>

                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="수당항목 관리">
                                            <a href="javascript:;" onClick="popAsWindow('<?= $customer_details->dp_id ?>');" class="text-default ml"><i class="fa fa-cog"></i>설정</a>
                                                </span>
										
										</td-->
                                        <td>
                                            <?= $customer_details->ceo ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->bs_number ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->bs_type1 ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->bs_type2 ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->co_address ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->homepage ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->ceo_hp ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->fax ?>
                                        </td>
                                        <td>
                                            <?= $customer_details->email ?>
                                        </td>
                                        <td>
                                                <?php if (!empty($edited)) { ?>
                                                    <?php echo btn_edit('admin/basic/customer_list/edit_customer/' . $customer_details->dp_id) ?>
                                                <?php }
                                                if (!empty($deleted)) {
                                                    ?>
                                                    <?php echo btn_delete('admin/basic/delete_customer/' . $customer_details->dp_id) ?>
                                                <?php } ?>
                                                <?php echo btn_view('admin/basic/customer_list/edit_customer/' . $customer_details->dp_id) ?>
										</td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="13">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
						include "./application/views/admin/basic/_customer_edit.inc.php";
					?>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>