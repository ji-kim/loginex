<?php

//지난달 정보
$last_month = date("Y-m",strtotime($mfee_info->df_month.' -1month')); 
$pwsm_info = $this->db->where('dp_id', $mfee_info->dp_id)->where('master_yn', 'Y')->get('tbl_delivery_fee_fixmfee_set')->row();
if(empty($pwsm_info->mfeeset_id)) $mfeeset_id = ''; else $mfeeset_id = $pwsm_info->mfeeset_id;
if(empty($pwsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $pwsm_info->wst_mfee;
if(empty($pwsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $pwsm_info->mfee_vat;
if(empty($pwsm_info->org_fee)) $org_fee = 0; else $org_fee = $pwsm_info->org_fee;
if(empty($pwsm_info->org_fee_vat)) $org_fee_vat = 0; else $org_fee_vat = $pwsm_info->org_fee_vat;
if(empty($pwsm_info->env_fee)) $env_fee = 0; else $env_fee = $pwsm_info->env_fee;
if(empty($pwsm_info->env_fee_vat)) $env_fee_vat = 0; else $env_fee_vat = $pwsm_info->env_fee_vat;
if(empty($pwsm_info->car_tax)) $car_tax = 0; else $car_tax = $pwsm_info->car_tax;
if(empty($pwsm_info->car_tax_vat)) $car_tax_vat = 0; else $car_tax_vat = $pwsm_info->car_tax_vat;
if(empty($pwsm_info->grg_fee)) $grg_fee = 0; else $grg_fee = $pwsm_info->grg_fee;
if(empty($pwsm_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $pwsm_info->grg_fee_vat;
if(empty($pwsm_info->etc)) $etc= 0; else $etc = $pwsm_info->etc;

		//파트너
		$dp = $this->db->where('dp_id', $mfee_info->dp_id)->get('tbl_members')->row();

		//공제청구사
		$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
		if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;

		//차량정보
		$truck = $this->db->where('idx', $mfee_info->tr_id)->get('tbl_asset_truck')->row();
		if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 위.수탁관리비 설정 - <!--[<?php echo $mfee_info->df_month ?>]-->  <?php echo $dp->E ?> / <?= $truck_no ?> <?=$mfee_info->dp_id?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">

            <!--div class="form-group" id="border-none">


						<a href="javascript:goPrindtAllTax()" tabindex="0" class="dt-button buttons-print btn btn-primary mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-cog"> </i> 관리비 설정</span>
					</a>
					<a id="button02" data-toggle="modal" data-target="#myModal2" 
data-dismiss="modal" href="<?= base_url() ?>admin/cowork/_modal_ngongje/<?= $mfee_info->df_id ?>" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs">
					<span><i class="fa fa-cog"> </i> 일반공제 설정</span>
					</a>
<a data-toggle="modal" data-target="#myModal2" 
data-dismiss="modal" href="<?= base_url() ?>admin/cowork/_modal_rgongje/<?= $mfee_info->df_id ?>" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs">
					<span><i class="fa fa-cog"> </i> 환급형공제 설정</span>
					</a>
<a data-toggle="modal" data-target="#myModal2" 
data-dismiss="modal" href="<?= base_url() ?>admin/cowork/_modal_insur/<?= $mfee_info->df_id ?>" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs">
					<span><i class="fa fa-cog"> </i> 보험 설정</span>
					</a>
<a data-toggle="modal" data-target="#myModal2" 
data-dismiss="modal" href="<?= base_url() ?>admin/cowork/_modal_misu/<?= $mfee_info->df_id ?>" tabindex="0" class="dt-button buttons-print btn btn-warning mr btn-xs">
					<span><i class="fa fa-warning"> </i> 초기미수액</span>
					</a>
            </div-->


        <form id="form_validation"
              action="<?php echo base_url() ?>admin/cowork/save_mfee"
              method="post" class="form-horizontal form-groups-bordered">
		<input type="hidden" name="mfeeset_id" value="<?php echo $mfeeset_id; ?>">
		<input type="hidden" name="dp_id" value="<?php echo $mfee_info->dp_id; ?>">
		<input type="hidden" name="df_month" value="<?php echo $mfee_info->df_month; ?>">
		<input type="hidden" name="gongje_req_co" value="<?php echo $mfee_info->gongje_req_co; ?>">

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label" style="border-right:1px solid #eee;color:#ffffff;background-color: #777777;">항목 <br/><br/></label>
                <label for="field-1" class="col-sm-2 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">공급가 <br/><br/></label>
                <label for="field-1" class="col-sm-2 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">부가세 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">합계 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="text-align:center;color:#ffffff;background-color:green;">최근설정<br/><?=$last_month?> </label>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">관리비 </label>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $wst_mfee; ?>" class="form-control" name="wst_mfee" >
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $mfee_vat; ?>" class="form-control" name="mfee_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($mfee_vat+$wst_mfee); ?>" class="form-control" name="mfee_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($wst_mfee)?> / <?=number_format($mfee_vat)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">협회비 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $org_fee; ?>" class="form-control" name="org_fee">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $org_fee_vat; ?>" class="form-control" name="org_fee_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($org_fee + $org_fee_vat); ?>" class="form-control" name="mfee_vat">
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($org_fee)?> / <?=number_format($org_fee_vat)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label" style="font-size:11px;">환경부담금 </label>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $env_fee; ?>" class="form-control" name="env_fee">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $env_fee_vat; ?>" class="form-control" name="env_fee_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($env_fee+$env_fee_vat); ?>" class="form-control" name="org_fee_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($env_fee)?> / <?=number_format($env_fee_vat)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">자동차세 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $car_tax; ?>" class="form-control" name="car_tax">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $car_tax_vat; ?>" class="form-control" name="car_tax_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($car_tax+$car_tax_vat); ?>" class="form-control" name="car_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($car_tax)?> / <?=number_format($car_tax_vat)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">차고지비 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $grg_fee; ?>" class="form-control" name="grg_fee">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $grg_fee_vat; ?>" class="form-control" name="grg_fee_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($grg_fee+$grg_fee_vat); ?>" class="form-control" name="grg_fee_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($grg_fee)?> / <?=number_format($grg_fee_vat)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">기타 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $etc; ?>" class="form-control" name="etc">
                </div>
                <div class="col-sm-2">
                 </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($etc); ?>" class="form-control" name="etc_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($etc)?></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
