<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();

if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 40px !important;
        padding-right: 40px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function popCtWindow(dp_id) {
	  window.open('<?php echo base_url(); ?>prn/contract.html?dp_id='+dp_id, 'winCT', 'left=50, top=50, width=870, height=800, scrollbars=1');
	}
	function popRCtWindow(dp_id) {
	  window.open('<?php echo base_url(); ?>prn/recontract.html?dp_id='+dp_id, 'winCT', 'left=50, top=50, width=870, height=800, scrollbars=1');
	}
	function popDaepaeWindow(dp_id) {
	  window.open('<?php echo base_url(); ?>admin/basic/partner_daepae/'+dp_id, 'winDP', 'left=50, top=50, width=870, height=800, scrollbars=1');
	}

	
	function popCtRWindow(dp_id) {
	  window.open('<?php echo base_url(); ?>prn/seoul02.html?dp_id='+dp_id, 'winCTR', 'left=50, top=50, width=870, height=800, scrollbars=1');
	}
	function selectTruck(params) {
	  window.open('<?php echo base_url(); ?>admin/asset/select_truck/'+params, 'winTR', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function selectPartner(params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/coop/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}

	function goSearch() {
		var action = "list";
		var page = document.myform.page.value;
		var filter = document.myform.filter.value;
		var ws_co_id = document.myform.ws_co_id.value;
		var baecha_co_id = document.myform.baecha_co_id.value;
		var search_keyword = document.myform.search_keyword.value;
		var url = "";

		if(ws_co_id == 'all' || ws_co_id == '') 
			url = "<?php echo base_url() ?>admin/basic/partner_list_all/";
		else
			url = "<?php echo base_url() ?>admin/basic/partner_list/";
		//document.myform.submit();
		location.href = url + action + '/' + page + '/' + filter + '/' + ws_co_id + '/' + baecha_co_id + '/' + search_keyword;
	}

	function goPage(page) {
		document.myform.page.value = page;
		goSearch();
	}

	function goFilter(filter) {
		document.myform.filter.value = filter;
		goSearch();
	}

	function setPartnerStatus(dp_id,status) {
		document.myform.tdp_id.value = dp_id;
		document.myform.action = "<?php echo base_url() ?>admin/basic/partner_set_status/"+dp_id+"/"+status+"/";
		document.myform.submit();
	}

	function goExcel() {

		document.myform.action = '<?php echo base_url() ?>admin/basic/partner_list_all/excel/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	function editPartner(dp_id) {
		document.myform.action = '<?php echo base_url() ?>admin/basic/partner_list_all/edit_partner/<?php echo $page ?>/<?php echo $filter ?>/<?php echo $ws_co_id ?>/<?php echo $baecha_co_id ?>/<?php echo $search_keyword ?>/'+dp_id;
		document.myform.submit();
	}
</script>

            <div class="col-sm-12 bg-white " style="margin-bottom:10px;">
                
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/basic/partner_list_all"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
					  <input type="hidden" name="page" value="<?php if(!empty($page)) echo $page;?>">
 					  <input type="hidden" name="filter" value="<?= $filter ?>">
				<div>
					<div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">

						<div class="form-group" id="border-none" style="margin:0px">
							<div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">위수탁관리사</label>
                                        <div class="col-lg-7">
											<select name="ws_co_id" id="ws_co_id" style="width:100%;background-color:#ffffff;" class="form-control input-sm" onChange="goSearch();">
												 <option value="all" <?=($ws_co_id=="all")?"selected":""?> value="">전체</option>
													<?php
													if (!empty($all_ws_info)) {
														foreach ($all_ws_info as $ws_details) {
															if(!empty($ws_details->co_name) && $ws_details->co_name != "") {
													?>
																		<option value="<?=$ws_details->ws_co_id?>" <?=($ws_co_id == $ws_details->ws_co_id) ? "selected" : ""?>><?=$ws_details->co_name?></option>
													<?php
															}
														}
													}
													?>
											</select>				
                                        </div>
                                    </div>
							</div>
							<div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">배차지</label>
                                        <div class="col-lg-8">
											<select name="baecha_co_id" id="baecha_co_id" style="width:100%;background-color:#ffffff;" class="form-control input-sm" onChange="goSearch();">
											 <option value="" <?=($baecha_co_id=="")?"selected":""?> value="">선택</option>
												<?php
												if (!empty($all_bc_info)) {
													foreach ($all_bc_info as $bc_details) {
														if(!empty($bc_details->co_name) && $bc_details->co_name != "") {
												?>
																	<option value="<?=$bc_details->baecha_co_id?>" <?=($baecha_co_id == $bc_details->baecha_co_id) ? "selected" : ""?>><?=$bc_details->co_name?></option>
												<?php
														}
													}
												}
												?>
											</select>				
                                        </div>
                                    </div>
							</div>
							<div class="col-sm-5" style="padding-right:0px;">
                                                            <div class="input-group">
																<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="통합검색">
                                                                <span class="input-group-btn">
																	<button class="btn btn-success" type="button" onclick="goSearch();">
                                                                        검색
                                                                        <i class="fa fa-search"></i>
                                                                    </button>

																	<button class="btn btn-warning" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-file-excel-o"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>




							<div class="col-sm-12" style="padding-top:0px;">

								<div class="input-group">
									<span class="input-group-btn text-center pt">
										<button class="btn btn-<?=($filter=="ALL")?"warning":"default"?>" style="font-size:12px;" type="button" onclick="goFilter('ALL');">전체파트너현황 <i class="fa fa-address-book"></i></button>
										<button class="btn btn-<?=($filter=="IN")?"warning":"default"?>" style="font-size:12px;" type="button" onclick="goFilter('IN');">계약중파트너현황 <i class="fa fa-address-book"></i></button>
										<button class="btn btn-<?=($filter=="WT")?"warning":"default"?>" style="font-size:12px;" type="button" onclick="goFilter('WT');">계약종료대기현황 <i class="fa fa-address-book"></i></button>
										<?php if($filter=="WT" || $filter=="WTU") { ?>
											<button class="btn btn-<?=($filter=="WTU")?"warning":"default"?>" style="font-size:12px;" type="button" onclick="goFilter('WTU');">챠량정보갱신 <i class="fa fa-address-book"></i></button>
										<?php } ?>
										<button class="btn btn-<?=($filter=="EP")?"warning":"default"?>" style="font-size:12px;" type="button" onclick="goFilter('EP');">계약종료현황 <i class="fa fa-address-book"></i></button>
										<button class="btn btn-<?=($filter=="RC")?"warning":"default"?>" style="font-size:12px;" type="button" onclick="goFilter('RC');">전자계약대기현황 <i class="fa fa-address-book"></i></button>
									</span>
								</div>
							
							</div>
						
						</div>
					</div>
				</div>

				</form>
      <!-- 검색 끝 -->

				

            </div>




	
	
<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#client_list"
                                                                   data-toggle="tab">파트너관리</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_partner"
                                                                   data-toggle="tab">신규등록</a></li>
                <!--li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/client/import">데이터가져오기</a>
                </li-->
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
<?php if($filter=="RC") { ?><td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>전자계약</td><?php } ?>
<?php if($filter=="WT" || $filter=="WTU") { ?><td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>계약관리</td><?php } ?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>배차지</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>소속지사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>납부계좌</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="27">파트너 정보</td>
          <td width="160px" style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;" rowspan="2"></td>
        </tr>

        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전자</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">직위</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">주민등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전면허번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종사자번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약종료일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과세여부</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세누적여부</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">업태</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종목</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업장주소</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">위수탁관리사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">핸드폰번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">PDA번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">은행</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">예금주</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이메일</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;">주소</td>
        </tr>
                                </thead>

                                <tbody>

                                <?php
								$cnt = 0;
                                if (!empty($all_partner_info)) {
                                    foreach ($all_partner_info as $partner_details) {
										
										if ($filter<>"RC"  || ($filter=="RC" && $cnt <7)) { //임시처리
										$cnt++;
										// 배차지
										$rco = $this->db->where('mb_type', 'customer')->where('code', $partner_details->code)->get('tbl_members')->row();

                                        ?>


                                        <tr>
          <td><?= $partner_details->dp_id ?></td>
<?php if($filter=="RC") { 
?>
			<td>

                    <span data-placement="top" data-toggle="tooltip"  title="재계약">
                        <a href="javascript:;" onClick="popCtWindow('<?= $partner_details->dp_id ?>');"  class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-cog"></i>전자계약</a>
                    </span>
			</td>
<?php } ?>
<?php if($filter=="WT" || $filter=="WTU") { 
		$chk = $this->db->where('inv_co_id', $partner_details->dp_id)->get('tbl_asset_truck')->row();
		//$this->db->join('tbl_members mb', 'mb.dp_id = tr.inv_co_id', 'left');
?>
			<td>

                    <span data-placement="top" data-toggle="tooltip"  title="계약종료 설정">
                        <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/basic/set_partner_status/CLS/<?= $partner_details->dp_id ?>/<?= $page ?>/<?= $filter ?>/<?= $ws_co_id ?>"  class="dt-button buttons-print btn btn-warning btn-xs mr"><i class="fa fa-cog"></i>계약종료</a>
                    </span>

                    <span data-placement="top" data-toggle="tooltip"  title="대.폐차신청">
                        <a href="javascript:;" onClick="popDaepaeWindow('<?= $partner_details->dp_id ?>');"  class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-cog"></i>대.폐차신청</a>
                    </span>

                    <!--span data-placement="top" data-toggle="tooltip"  title="재계약">
                        <a href="javascript:;" onClick="popRCtWindow('<?= $partner_details->dp_id ?>');"  class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-cog"></i>재계약</a>
                    </span>

                    <span data-placement="top" data-toggle="tooltip"  title="계약해지">
                        <a href="javascript:;" onClick="popCtRWindow('<?= $partner_details->dp_id ?>');"  class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-cog"></i>계약해지</a>
                    </span-->
			</td>
<?php } ?>
		  <td><? if(!empty($rco->co_name)) echo $rco->co_name; ?></td>
		  <td><?php if(!empty($partner_details->br_name)) echo $partner_details->br_name ?></td>
          <td>
			<a data-toggle="modal" data-target="#myModal" title="납부계좌설정" href="<?= base_url() ?>admin/basic/set_partner_payment/set/<?= $partner_details->dp_id ?>/<?= $page ?>/<?= $filter ?>/<?= $ws_co_id ?>" class="btn btn-xs btn-purple"><i class="fa fa-copy"></i></a>
			<? if(!empty($partner_details->p_account_name)) echo $partner_details->p_account_name; ?>
			<? if(!empty($partner_details->p_bank_name)) echo $partner_details->p_bank_name; ?>
			<? if(!empty($partner_details->p_account_no)) echo $partner_details->p_account_no; ?>
		  </td>
          <td><?= $partner_details->co_name ?></td>
          <td><?= $partner_details->ceo ?></td>
          <td><?= $partner_details->driver ?></td>
          <td>
				<select name="position" id="position" class="select" style="width:60px;" onChange="goSet('position','<?= $partner_details->dp_id ?>',this.value);">
					<option value="팀원" <?=($partner_details->position == "팀원") ? "selected" : ""?>>팀원</option>
					<option value="조장" <?=($partner_details->position == "조장") ? "selected" : ""?>>조장</option>
					<option value="팀장" <?=($partner_details->position == "팀장") ? "selected" : ""?>>팀장</option>
					<option value="현장대리인" <?=($partner_details->position == "현장대리인") ? "selected" : ""?>>현장대리인</option>
			</select>
		  		  		  
		  
		  
		  </td>
          <!--td><?= $partner_details->H ?></td>
          <td><?= $partner_details->I ?></td>
          <td><?= $partner_details->J ?></td-->
          <td><?= $partner_details->reg_number ?></td>
          <td><?= $partner_details->L ?></td>
          <td><?= $partner_details->M ?></td>
          <td><?= $partner_details->N ?></td>
          <td><?= $partner_details->O ?></td>
				  <td align="center" style="padding-left:5px;background-color:<?= ($partner_details->tax_yn == "Y")? "red":""?>;">
				<select name="tax_yn" id="tax_yn" class="select" style="width:60px;" onChange="goSet('tax_yn','<?= $partner_details->dp_id ?>',this.value);">
					<option value="" >선택</option>
					<option value="Y" <?=($partner_details->tax_yn == "Y") ? "selected" : ""?>>Y</option>
					<option value="N" <?=($partner_details->tax_yn == "N") ? "selected" : ""?>>N</option>
					<option value="직원" <?=($partner_details->tax_yn == "직원") ? "selected" : ""?>>직원</option>
			</select>
				  </td><!-- 과세여부 -->
				  <td align="center" style="padding-left:5px;background-color:<?= ($partner_details->acc_vat_yn == "Y")? "red":""?>">


				<select name="acc_vat_yn" id="acc_vat_yn" class="select" style="width:60px;" onChange="goSet('acc_vat_yn','<?= $partner_details->dp_id ?>',this.value);">
					<option value="" >선택</option>
					<option value="Y" <?=($partner_details->acc_vat_yn == "Y") ? "selected" : ""?>>Y</option>
					<option value="N" <?=($partner_details->acc_vat_yn == "N") ? "selected" : ""?>>N</option>
			</select>
				  </td><!-- 부가세누적여부 -->
						<td><?= $partner_details->bs_number ?></td><!-- 사업자등록번호 -->
						<td><?= $partner_details->bs_type1 ?></td><!-- 업태 -->
						<td><?= $partner_details->bs_type2 ?></td><!-- 종목 -->
						<td align="left" style="padding-left:5px;"><?= $partner_details->T ?></td><!-- 사업장주소 -->

						<td>
							<?php if(!empty($partner_details->car_1) && !empty($partner_details->cert_id_type)) { ?>
								<!--	<?=($partner_details->cert_id_type=="B")?"<span class='label label-warning'>사</span>":"<span class='label label-primary'>주</span>"?>-->
							<?php } ?>
							<?= $partner_details->car_1 ?>
							<?php if(!empty($partner_details->car_1)) { ?>
								<!--div class="btn-group">
                                                    <button class="btn btn-xs btn-success dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        검사설정
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu animated zoomIn">
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/basic/set_certtype/<?= $partner_details->dp_id . '/B' ?>">사업자번호</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/basic/set_certtype/<?= $partner_details->dp_id . '/I' ?>">주민번호</a>
                                                        </li>
                                                    </ul>
								</div-->
							<?php } ?>
						</td><!-- 차량번호 -->
						<td><?= $partner_details->car_3 ?></td><!-- 용도 -->
						<td><?= $partner_details->car_7?></td><!-- 차대번호 -->
		  
					  <td><?= $partner_details->ws_co_name ?></td><!-- 지입사명 -->
					  <td><?= $partner_details->co_tel ?></td>
					  <td><?= $partner_details->Z ?></td>
					  <td><?= $partner_details->AA ?></td>
					  <td><?= $partner_details->AB ?></td>
					  <td><?= $partner_details->AC ?></td>
					  <td><?= $partner_details->AD ?></td>
					  <td><?= $partner_details->email ?></td>
					  <td align="left" style="padding-left:5px;"><?= $partner_details->co_address ?></td>
					  <td align="center">
							<a href="javascript:editPartner('<?=$partner_details->dp_id?>');" class="btn btn-primary btn-xs" title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i></a>
                                            <?//= btn_edit('admin/basic/partner_list_all/edit_partner/' . $partner_details->dp_id) ?>
                                            <?php if (!empty($can_edit) && !empty($edited)) { ?>

                                            <?php }
                                             ?>
                                               <?php echo btn_delete('admin/basic/delete_partner/' . $page . '/' . $filter . '/' . $ws_co_id . '/' . $baecha_co_id . '/' . $search_keyword . '/' . $partner_details->dp_id) ?>
					  </td>
                                        </tr>
                                        <?php

// 임시 마이그레이션
if(0) { //!empty($partner_details->tr_id)) {
	$tr_id = $partner_details->tr_id;
	//1. 기존 is_master = 'N'
	$qry = "update tbl_asset_truck_info set is_master = 'N' where tr_id='$tr_id'";
	$this->basic_model->db->query($qry);
	echo $qry."<br/>";

	// 현 차주 insert
	$sql_add = "";
	if(!empty($partner_details->dp_id)) $sql_add .= ", owner_id = '".$partner_details->dp_id."'";
	if(!empty($partner_details->ceo)) $sql_add .= ", owner_name = '".$partner_details->ceo."'";
	if(!empty($partner_details->driver)) $sql_add .= ", driver_name = '".$partner_details->driver."'";
	$qry = "INSERT INTO tbl_asset_truck_info  SET tr_id='$tr_id', reason = '데이터보정'".$sql_add;
	$qry .= ",reg_datetime=now(), is_master='Y'";
	$this->basic_model->db->query($qry);
	echo $qry."<br/>";
	
	$qry = "update tbl_asset_truck set owner_id = '".$partner_details->dp_id."' where idx='$tr_id'";
	$this->basic_model->db->query($qry);
	echo $qry."<br/><br/><br/>";
}


                                    }
									}
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
						include "./application/views/admin/basic/_partner_edit.inc.php";
					?>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>

<ul class="pagination">
<?
$pagelist = get_paging(10, $page, $total_page);
echo $pagelist;
?>
</ul>



<?php
function get_paging($write_pages, $cur_page, $total_page)
{
    $str = "";
    if ($cur_page > 1) {
		$str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goPage(1);">First</a></li>';
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goPage('.($start_page-1).');">Previous</a></li>';

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= '<li class="paginate_button" tabindex="0"><a href="javascript:goPage('.$k.');">'.$k.'</a></li>';
            else
                $str .= '<li class="paginate_button active" tabindex="0"><a href="javascript:goPage('.$k.');">'.$k.'</a></li>';
        }
    }

    if ($total_page > $end_page) $str .= '<li class="paginate_button next" tabindex="0"><a href="javascript:goPage('.($end_page+1).');">Next</a></li>';

    if ($cur_page < $total_page) {
		$str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goPage('.$total_page.');">Last</a></li>';
    }
    $str .= "";

    return $str;
}
?>