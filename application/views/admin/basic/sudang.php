<?php echo message_box('success') ?>

<div class="row">
    <!-- Start Form -->
    <div class="col-lg-12">


<div class="panel panel-custom">
    <header class="panel-heading "><?= $title ?> </header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped ">
                <thead>
                <tr>

                    <th>항목명</th>
                    <th>순서</th>
                    <th>설명</th>
                    <?php if (!empty($edited) || !empty($deleted)) { ?>
                        <th>작업</th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($all_item_group)) {
                    foreach ($all_item_group as $item_info) {
                        ?>
                        <tr id="item_info_<?= $item_info->idx?>">
                            <td><?php
                                    if (!empty($item_info->title)) {
                                        echo $item_info->title;
                                    }
                                    ?>
							</td>
                            <td><?php
                                    if (!empty($item_info->dp_order)) {
                                        echo $item_info->dp_order;
                                    }
                                    ?>
							</td>
                            <td><?php
                                    if (!empty($item_info->remark)) {
                                        echo $item_info->remark;
                                    }
                                    ?>
							</td>
                            <td>
                                     <?php echo ajax_anchor(base_url("admin/basic/sudang_gongje/".$mode."/delete/" . $item_info->idx), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#item_info_" . $item_info->idx)); ?>
                            </td>
                        </tr>
                    <?php }
                }
               // if (!empty($created) || !empty($edited)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/basic/sudang_gongje/<?=$mode?>/insert/"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
						  <input type="hidden" name="add_type" value="<?=$add_type?>">
                        <tr>
                            <td><input type="text" name="title" class="form-control"
                                       placeholder="항목명" required></td>
                            <td><input type="text" name="dp_order" class="form-control"
                                       placeholder="순서" required></td>
                            <td>
                                <textarea name="remark" rows="1" class="form-control"></textarea>
                            </td>
                            <td><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php // } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



        </form>
    </div>
    <!-- End Form -->
</div>

