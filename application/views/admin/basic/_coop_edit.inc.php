                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_coop"
                        style="position: relative;">



                        <form role="form" method='POST' enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/basic/save_coop/<?php
                              if (!empty($coop_info)) {
                                  echo $coop_info->dp_id;
                              }
                              ?>" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-10">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_info" data-toggle="tab">기본정보</a></li>
                                        </ul>
                                        <div class="tab-content bg-white">



                                            <!-- ************** 기본정보 *************-->
                                            <div class="chart tab-pane active" id="general_info">
                                                
												<div class="form-group">
													<label class="col-lg-2 control-label"></label>
														<div class="col-lg-4 fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 210px;">
																<?php
																if (!empty($coop_info)) :
																	?>
																	<img src="<?php echo base_url() . $coop_info->mb_pic; ?>">
																<?php else: ?>
																	<img src="http://placehold.it/350x260"
																		 alt="Please Connect Your Internet">
																<?php endif; ?>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail"
																 style="width: 210px;"></div>
															<div>
															<span class="btn btn-default btn-file">
																<span class="fileinput-new">
																	<input type="file" name="mb_pic" value="upload"
																		   data-buttonText="이미지선택" id="myImg"/>
																	<span class="fileinput-exists">변경</span>    
																</span>
																<a href="#" class="btn btn-default fileinput-exists"
																   data-dismiss="fileinput">삭제</a>

															</div>
															<div id="valid_msg" style="color: #e11221"></div>
														</div>

													<label class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
                                                    </div>
                                                </div>
											   
											   
                                                <div class="form-group">

													<label class="col-lg-2 control-label">거래처명<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
															<input type="text" name="co_name" id="co_name" value="<?=(empty($coop_info->co_name ))?"":$coop_info->co_name  ?>" class="form-control">
                                                    </div>
                                                    <label class="col-lg-2 control-label">대표
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($coop_info->ceo)) {
                                                                   echo $coop_info->ceo;
                                                               }
                                                               ?>" name="ceo">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                   <label class="col-lg-2 control-label">사업자등록번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($coop_info->bs_number)) {
                                                                   echo $coop_info->bs_number;
                                                               }
                                                               ?>" name="bs_number">
                                                    </div>
                                                    <label class="col-lg-2 control-label">업태 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($coop_info->bs_type1)) {
                                                                   echo $coop_info->bs_type1;
                                                               }
                                                               ?>" name="bs_type1">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">종목 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($coop_info->bs_type2)) {
                                                                   echo $coop_info->bs_type2;
                                                               }
                                                               ?>" name="bs_type2">
                                                    </div>
                                                    <label class="col-lg-2 control-label">홈페이지 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($coop_info->homepage)) {
                                                                   echo $coop_info->homepage;
                                                               }
                                                               ?>" name="homepage">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">주소 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($coop_info->co_address)) {
                                                                   echo $coop_info->co_address;
                                                               }
                                                               ?>" name="co_address">
                                                    </div>
                                                    <label class="col-lg-2 control-label">과세여부 </label>
                                                    <div class="col-lg-4">
                                                          <select name="tax_yn"
                                                                    class="form-control select_box"
                                                                    style="width: 100%">
														  <option <?=(!empty($coop_info->tax_yn) && $coop_info->tax_yn=="")?"selected":""?> value="">선택</option>
														  <option <?=(!empty($coop_info->tax_yn) && $coop_info->tax_yn=="Y")?"selected":""?> value="Y">예</option>
														  <option <?=(!empty($coop_info->tax_yn) && $coop_info->tax_yn=="N")?"selected":""?> value="N">아니오</option>
														</select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-2 control-label">메모</label>
                                                    <div class="col-lg-4">
                                            <textarea class="form-control" name="remark"><?php
                                                if (!empty($coop_info->remark)) {
                                                    echo $coop_info->remark;
                                                }
                                                ?></textarea>

													</div>
												</div>
                                            </div>
											
											<!-- ************** 기본정보 *************-->


                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                        </div>
                                        <div class="col-lg-3">
                                        </div>

                                    </div>
											
											
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                </div>
                        </form>
