<?php
$this->load->view('admin/components/htmlheader');
?>
<script type="text/javascript">

function goSearch() {
  $("#myform").attr("target", "_self");
  $("#myform").attr("action", "<?php echo base_url() ?>admin/basic/tr_add_partner/<?=$ct_id?>/<?=$page?>");
  $("#myform").submit();
}

function go(mode) {
  $("#mode").val(mode);
  $("#myform").attr("target", "_self");
  $("#myform").attr("action", "<?php echo base_url() ?>admin/basic/tr_add_partner/<?=$ct_id?>/<?=$page?>");
  $("#myform").submit();
}

function goList(page) {
	$("#page").val(page);
  $("#myform").attr("target", "_self");
  $("#myform").attr("action", "<?php echo base_url() ?>admin/basic/tr_add_partner/<?=$ct_id?>/"+page);
  $("#myform").submit();
}
function goSet(dp_id,mode,sc_id) {
    $("#mode").val(mode);
	$("#dp_id").val(dp_id);
	$("#sc_id").val(sc_id);
	$("#myform").submit();
}
</script>
<body class="<?php if (!empty($opened)) {
    echo 'offsidebar-open';
} ?> <?= config_item('aside-float') . ' ' . config_item('aside-collapsed') . ' ' . config_item('layout-boxed') . ' ' . config_item('layout-fixed') ?>">
<div class="wrapper">
    <!-- sidebar-->
    <?php $this->load->view('admin/components/add_partner_sidebar'); ?>
    <!-- Main section-->
    <section>
        <?php
        $active_pre_loader = config_item('active_pre_loader');
        if (!empty($active_pre_loader) && $active_pre_loader == 1) {
            ?>
            <div id="loader-wrapper">
                <div id="loader"></div>
            </div>
        <?php } ?>
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-heading">
                <?php
                echo $title;

                ?>
                <div class="pull-right">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">


<form method="post" name="myform" id="myform" action="<?php echo base_url() ?>admin/basic/tr_add_partner/<?=$ct_id?>/<?=$page?>">
	<input type="hidden" name="dp_id" id="dp_id" value="" />
	<input type="hidden" name="sc_id" id="sc_id" value="" />
	<input type="hidden" name="mode" id="mode" value="" />
                    <?php echo $subview ?>
</form>




                </div>
            </div>
        </div>
    </section>
    <!-- Page footer-->

</div>
<?php
$this->load->view('admin/components/footer_pop');
$direction = $this->session->userdata('direction');
if (!empty($direction) && $direction == 'rtl') {
    $RTL = 'on';
} else {
    $RTL = config_item('RTL');
}
?>

<script type="text/javascript">

    $(document).ready(function () {
    })
    ;
</script>

