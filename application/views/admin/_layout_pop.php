<?php
$this->load->view('admin/components/htmlheader');
$opened = $this->session->userdata('opened');
$this->session->unset_userdata('opened');
$opener_id = explode("%7C%7C",$params);

?>
<script type="text/javascript">
function selectMember(inv_co_id,inv_co_name,inv_reg_no,inv_bs_no,inv_ceo,inv_tel,inv_location,inv_fax,code,driver) {
//	alert(inv_co_name);
//	alert(code);
  <?if(!empty($opener_id[0]) && $opener_id[0]!="__") {?>$("#<?=$opener_id[0]?>",opener.document).val(inv_co_id);<? } ?>
  <?if(!empty($opener_id[1]) && $opener_id[1]!="__") {?>$("#<?=$opener_id[1]?>",opener.document).val(inv_co_name);<? } ?>
  <?if(!empty($opener_id[2]) && $opener_id[2]!="__") {?>$("#<?=$opener_id[2]?>",opener.document).val(inv_bs_no);<? } ?>
  <?if(!empty($opener_id[3]) && $opener_id[3]!="__") {?>$("#<?=$opener_id[3]?>",opener.document).val(inv_reg_no);<? } ?>
  <?if(!empty($opener_id[4]) && $opener_id[4]!="__") {?>$("#<?=$opener_id[4]?>",opener.document).val(inv_location);<? } ?>
  <?if(!empty($opener_id[5]) && $opener_id[5]!="__") {?>$("#<?=$opener_id[5]?>",opener.document).val(inv_tel);<? } ?>
  <?if(!empty($opener_id[6]) && $opener_id[6]!="__") {?>$("#<?=$opener_id[6]?>",opener.document).val(inv_ceo);<? } ?>
  <?if(!empty($opener_id[7]) && $opener_id[7]!="__") {?>$("#<?=$opener_id[7]?>",opener.document).val(inv_fax);<? } ?>
  <?if(!empty($opener_id[8]) && $opener_id[8]!="__") {?>$("#<?=$opener_id[8]?>",opener.document).val(code);<? } ?>
  <?if(!empty($opener_id[9]) && $opener_id[9]!="__") {?>$("#<?=$opener_id[9]?>",opener.document).val(driver);<? } ?>
	window.close();
}

function goSearch() {
  $("#myform").attr("target", "_self");
  $("#myform").attr("action", "<?php echo base_url() ?>admin/basic/select_company/<?=$co_type?>/<?=$params?>");
  $("#myform").submit();
}

function go(mode) {
  $("#mode").val(mode);
  $("#myform").attr("target", "_self");
  $("#myform").attr("action", "<?php echo base_url() ?>admin/basic/select_company/<?=$co_type?>/<?=$params?>");
  $("#myform").submit();
}

function goList(page) {
	$("#page").val(page);
  $("#myform").attr("target", "_self");
  $("#myform").attr("action", "<?php echo base_url() ?>admin/basic/select_company/<?=$co_type?>/<?=$params?>");
  $("#myform").submit();
}
</script>
<body class="<?php if (!empty($opened)) {
    echo 'offsidebar-open';
} ?> <?= config_item('aside-float') . ' ' . config_item('aside-collapsed') . ' ' . config_item('layout-boxed') . ' ' . config_item('layout-fixed') ?>">
<div class="wrapper">
    <!-- sidebar-->
    <?php $this->load->view('admin/components/basic_sidebar'); ?>
    <!-- Main section-->

    <section>
        <?php
        $active_pre_loader = config_item('active_pre_loader');
        if (!empty($active_pre_loader) && $active_pre_loader == 1) {
            ?>
            <div id="loader-wrapper">
                <div id="loader"></div>
            </div>
        <?php } ?>
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-heading">
                <?php
                echo $title;

                ?>
                <div class="pull-right">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">


<form method="post" name="myform" id="myform" action="<?php echo base_url() ?>admin/basic/select_company/<?=$co_type?>/<?=$params?>">
	<input type="hidden" name="page" id="page" value="<?= $page ?>" />
                    <?php echo $subview ?>
</form>




                </div>
            </div>
        </div>
    </section>
    <!-- Page footer-->

</div>
<?php
$this->load->view('admin/components/footer_pop');
$direction = $this->session->userdata('direction');
if (!empty($direction) && $direction == 'rtl') {
    $RTL = 'on';
} else {
    $RTL = config_item('RTL');
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        $("button[name$='clocktime']").click(function () {
            var ubtn = $(this);
            ubtn.html('Please wait...');
            ubtn.addClass('disabled');
        });

        $('[data-ui-slider]').slider({
            <?php
            if (!empty($RTL)) {?>
            reversed: true,
            <?php }
            ?>
        });
        /*
         * Multiple drop down select
         */
    })
</script>

<script type="text/javascript">

    $(document).ready(function () {
    })
    ;
</script>

<?php $this->load->view('admin/_layout_modal'); ?>
<?php $this->load->view('admin/_layout_modal_lg'); ?>
<?php $this->load->view('admin/_layout_modal_large'); ?>
<?php $this->load->view('admin/_layout_modal_extra_lg'); ?>

