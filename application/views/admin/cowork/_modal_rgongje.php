<?php

if(empty($rgongje_info->idx )) $rgs_id = ''; else $rgs_id = $rgongje_info->idx ;
if(empty($rgongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rgongje_info->gj_termination_mortgage;

//파트너
$dp = $this->db->where('dp_id', $df_info->dp_id)->get('tbl_members')->row();

//공제청구사
$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;

//차량정보
$truck = $this->db->where('idx', $df_info->tr_id)->get('tbl_asset_truck')->row();
if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 환급형공제 설정 - [<?php echo $df_info->df_month ?>]  <?php echo $dp->driver ?> / <?= $truck_no ?> <?=$df_info->dp_id?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">

        <form id="form_validation"
              action="<?php echo base_url() ?>admin/cowork/save_rgongje"
              method="post" class="form-horizontal form-groups-bordered">
		<input type="hidden" name="rgs_id" value="<?php echo $rgs_id; ?>">
		<input type="hidden" name="df_id" value="<?php echo $df_info->df_id; ?>">
		<input type="hidden" name="dp_id" value="<?php echo $df_info->dp_id; ?>">
		<input type="hidden" name="ct_id" value="<?php echo $df_info->ct_id; ?>">
		<input type="hidden" name="df_month" value="<?php echo $df_info->df_month; ?>">
		<input type="hidden" name="gongje_req_co" value="<?php echo $df_info->gongje_req_co; ?>">

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;color:#ffffff;background-color: #777777;">항목 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">금액<br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">부가세<br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">합계<br/><br/></label>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label">해지담보 </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo $gj_termination_mortgage; ?>" class="form-control" name="gj_termination_mortgage" >
                </div>
                <div class="col-sm-3">
                     
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($gj_termination_mortgage,0)?></div>
            </div>
<?php
if (!empty($all_rgongje_group)) {
?>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;color:#ffffff;background-color: #333333;">추가항목 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #333333;">금액<br/><br/></label>
                <label for="field-1" class="col-sm-6 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #333333;">메모<br/><br/></label>
            </div>

<?php
	$i = 0;
	foreach ($all_rgongje_group as $rgongje_info) {
		$add_item = $this->db->where('df_month', $df_month)->where('dp_id', $dp_id)->get('tbl_delivery_fee_add')->row();
?>
			<input type=hidden name=chk[] value='<?=$i?>'>
			<input type="hidden" name="item_pidx[<?=$i?>]" value="<?php echo $rgongje_info->idx; ?>">
			<input type="hidden" name="item_idx[<?=$i?>]" value="<?php if(!empty($add_item->idx)) echo $add_item->idx; ?>">
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label"><?php echo $rgongje_info->title; ?> </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php if(!empty($add_item->amount)) echo $add_item->amount; ?>" class="form-control" name="item_amount[<?=$i?>]" >
                </div>
                <div class="col-sm-6">
                    <input type="text" value="<?php if(!empty($add_item->memo)) echo $add_item->memo; ?>" class="form-control" name="memo[<?=$i?>]" >
                </div>
            </div>

<?php
		$i++;
	}
}
?>



            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
