<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
//임시 처리
$pdate = $df_month . "-01";
$pm=date("Y-m",strtotime($pdate.'-1month')); 


$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


if (1) { //$this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function popAsWindow(md,tr_id) {
	  window.open('<?php echo base_url(); ?>admin/asset/pop_carinfo/'+md+'/'+tr_id, 'winAS', 'left=50, top=50, width=1480, height=700, scrollbars=1');
	}
	function selectPartner(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}

	function goSearch() {
		var action = "list";
		var df_month = document.myform.df_month.value;
		var page = document.myform.page.value;
		var gongje_req_co = document.myform.gongje_req_co.value;
		var search_keyword = document.myform.search_keyword.value;
		var url = "";

		url = "<?php echo base_url() ?>admin/cowork/jiip_gongje_all/";
		location.href = url + action + '/' + df_month + '/' + page + '/' + gongje_req_co + '/' + search_keyword;
	}

	function goPage(page) {
		document.myform.page.value = page;
		goSearch();
	}

	function popSpecifications() {
	  window.open('http://delex.delta-on.com/prn/work.htm', 'winSF', 'left=50, top=50, width=710, height=650, scrollbars=1');
	}
	function goPrintAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje_all/'+df_month+'/'+gongje_req_co+'/print/';
		document.myform.target = '_blank';
		document.myform.submit();
	}
	function goExcelAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje_all/'+df_month+'/'+gongje_req_co+'/excel/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	function goExcelListAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje_all/'+df_month+'/'+gongje_req_co+'/excel_list/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	//임시처리
	function goExcelSingle(df_id) {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje_all/'+df_month+'/'+gongje_req_co+'/excel/'+df_id+'/';
		document.myform.target = '_blank';
		document.myform.submit();
	}
</script>


            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                
    <!-- 검색 시작 -->
			<?php 
				if (!empty($gongje_req_co)) {  // 공제사가 선택되었을경우에만 관리비 생성가능
						include "./application/views/admin/cowork/_search_default.php";
				} else { 
						include "./application/views/admin/cowork/_search_all.php";
                } ?>
      <!-- 검색 끝 -->
            </div>




<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#client_list" data-toggle="tab"><span style="color:blue;"><?php echo $df_month; ?></span> 위수탁관리비청구현황(총 <?=number_format($total_count)?>건)</a></li>



            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane active" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>

								
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <!--td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">등록<br/>마감</td-->
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">마감</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">기사<br/>확인</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>공제청구사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>실수요처</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>운영공제(송금)사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>사업자등록</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2"><br/>계약기간</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="6">위.수탁관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">각종보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">일반공제</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="2">환급형공제</td>
          <!--td style="color:#ffffff;background-color: #777777;" colspan="3">누적금환급</td-->

          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;border-right:1px solid #eee;" rowspan="2"><br/>공제총액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">거래지급</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">환급금지급후<br>공제청구총액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">상세<br>보기</td>
        </tr>
															

        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료일</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">협회비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">적재물</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소  계</td>

		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">미수금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과태료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">해지담보</td>
          <td style="color:#ffffff;background-color: #777777;">소  계</td>
		</tr>
								
								
								</thead>
                                <tbody>



                                <?php
                                if (!empty($all_delivery_fee_info)) {
                                    foreach ($all_delivery_fee_info as $delivery_fee_details) {
										$sn = $total_count--;


										//파트너
										//$dp = $this->db->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_members')->row();

										//공제청구사
										$gj_rq_co = "";
										if(!empty($delivery_fee_details->gongje_req_co)) {
											$gj_rq_co = $this->db->where('dp_id', $delivery_fee_details->gongje_req_co)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//차량정보
										$truck = $this->db->where('idx', $delivery_fee_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										//위수탁관리비
//$wsm_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $df_month)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();
										$wsm_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_fixmfee')->row();
										//$wsm_info = $this->db->where('df_id', $delivery_fee_details->df_id)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();

										if(empty($wsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $wsm_info->wst_mfee;
										if(empty($wsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $wsm_info->mfee_vat;
										if(empty($wsm_info->org_fee)) $org_fee = 0; else $org_fee = $wsm_info->org_fee;
										if(empty($wsm_info->env_fee)) $env_fee = 0; else $env_fee = $wsm_info->env_fee;
										if(empty($wsm_info->car_tax)) $car_tax = 0; else $car_tax = $wsm_info->car_tax;
										if(empty($wsm_info->grg_fee)) $mgrg_fee = 0; else $mgrg_fee = $wsm_info->grg_fee;
										if(empty($wsm_info->grg_fee_vat)) $mgrg_fee_vat = 0; else $mgrg_fee_vat = $wsm_info->grg_fee_vat;
										if(empty($wsm_info->etc)) $etc= 0; else $etc = $wsm_info->etc;
										$org_fee_vat = 0;
										$env_fee = 0;
										$env_fee_vat = 0;
										$car_tax = 0;
										$car_tax_vat = 0;
										$etc = 0;
										$wsm_sum = ($wst_mfee + $mfee_vat + $org_fee + $env_fee + $car_tax + $mgrg_fee + $mgrg_fee_vat + $etc);

										//각종보험
										//$insur_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_insur')->row();
										$insur_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_insur')->row();
										if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
										if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
										if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
										$insur_sum = ($ins_car + $ins_load + $ins_grnt);

										//일반공제
										//$gongje_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_gongje')->row();
										$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										//$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
										//if(empty($gongje_info->acc_app)) $acc_app = 0; else $acc_app = $gongje_info->acc_app;
										if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
										if(empty($gongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $gongje_info->grg_fee;
										if(empty($gongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $gongje_info->grg_fee_vat;
										// 벌금
										$fine_sum = $this->db->select('sum(amount) as sum')->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get("tbl_delivery_fee_gongje_finefee")->row();
										if(!empty($fine_sum->sum) && $fine_sum->sum > 0) { // 기존버전과 호환 유지
											$fine_fee += $fine_sum->sum;
										}


										$gongje_sum = ($fine_fee + $not_paid + $grg_fee + $grg_fee_vat);

										//환급형공제
										$rf_gongje_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_refund_gongje')->row();
										if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
										//if(empty($rf_gongje_info->gj_plate_mortgage)) $gj_plate_mortgage = 0; else $gj_plate_mortgage = $rf_gongje_info->gj_plate_mortgage;
										//if(empty($rf_gongje_info->gj_etc_refund)) $gj_etc_refund = 0; else $etc = $rf_gongje_info->gj_etc_refund;
										$rf_gongje_sum = ($gj_termination_mortgage);

										$tot_gongje = ($wsm_sum + $insur_sum + $gongje_sum + $rf_gongje_sum);
										$tot_gongje_req = ($tot_gongje - $rf_gongje_sum);

 
										$lv = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $delivery_fee_details->df_month)->get('tbl_delivery_fee_levy')->row();
										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($lv->pay_status)) {
											if($lv->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($lv->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($lv->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
										}

										$is_editable = "Y";
										if($delivery_fee_details->is_closed=='Y') $is_editable = "N";
//임시 처리 -- 전월회계상 미수금액 당월에 등록 
if(0) { //!empty($plv->pay_status)) {
$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
if($delivery_fee_details->df_month == "2018-09") { // 9월이면서 전달 완납이 아닌경우
	if(!empty($plv->pay_status) && $plv->pay_status != 'B') { //납부전상태가 아니면
		$not_paid = $plv->balance_amount;

		$sql = "update tbl_delivery_fee_gongje set not_paid = '".$not_paid."' where df_id='".$delivery_fee_details->df_id."'";
		$this->cowork_model->db->query($sql);
	echo $delivery_fee_details->E."-".$sql."<br/>";
	}
}
}

//-> 당월 회계에 신규 등록
if(0) { //$delivery_fee_details->ceo == "김영록") { //!empty($plv->pay_status)) {
	$balance_amount = 0;
	$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
	$lv = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
	//if(empty($lv->idx)) { // 
		$cmisu_amount = 0;
		if(!empty($plv->balance_amount)) {
			$cmisu_amount = $plv->balance_amount;
		}

		// 부과액 = 협업총액, 잔액 = 협업총액 - 납부액
		$gn_amount = $tot_gongje_req;
		$balance_amount = $tot_gongje_req; // + $cmisu_amount;

		$pay_status = "N";
		if($balance_amount == 0) $pay_status = "F";
		else if($balance_amount == $gn_amount) $pay_status = "N";
		else if(!empty($lv->pay_amount) && $gn_amount < $lv->pay_amount) $pay_status = "O";
		else $pay_status = "P";

// 미납데이터 임시저장
//		$chk = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
		if (!empty($lv)) {
			if(empty($plv->balance_amount)) $ba = 0; else $ba = $plv->balance_amount;
			$sql = "update tbl_delivery_fee_levy set gn_amount = '".$tot_gongje_req."',pay_amount = '0',balance_amount = '".$tot_gongje_req."',prev_misu = '".$ba."' where df_id='".$delivery_fee_details->df_id."'";
		} else {
			
			$sql = "insert into tbl_delivery_fee_levy set df_id = '".$delivery_fee_details->df_id."', gongje_req_co = '".$delivery_fee_details->gongje_req_co."' ,df_month = '".$delivery_fee_details->df_month."',dp_id = '".$delivery_fee_details->dp_id."',gn_amount = '".$tot_gongje_req."',paid_amount = '0', balance_amount = '".$balance_amount."', pay_status = '".$pay_status."'";
			if(!empty($plv->balance_amount)) $sql .= ",prev_misu = '".$plv->balance_amount."'";
		}
		$this->cowork_model->db->query($sql);
	echo $delivery_fee_details->ceo."-".$sql."<br/>";

	//}    
}


//?????
if(0) {//$delivery_fee_details->df_month == "2018-08") {
	$cmisu_amount = 0;
	$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
	$pdf = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee')->row();
	$cmisu_amount = 0;
	if(!empty($pdf->is_pay_closed) && $pdf->is_pay_closed == 'Y') {
		$cmisu_amount = $plv->balance_amount;
	}
	if(!empty($plv->balance_amount)) $cmisu_amount = $plv->balance_amount;
	//echo $cmisu_amount;
    
	// Update into tbl_delivery_fee_gongje
   // $levy_data = array(
   //     'not_paid' => $cmisu_amount,
    //);
    //$this->cowork_model->_table_name = "tbl_delivery_fee_gongje"; //table name
    //$this->cowork_model->_primary_key = "df_id";
    //$this->cowork_model->save($levy_data, $delivery_fee_details->df_id);
	$chk = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
    if (!empty($chk)) {
		$sql = "update tbl_delivery_fee_gongje set not_paid = '".$cmisu_amount."' where df_id='".$delivery_fee_details->df_id."'";
	} else {
		
		$sql = "insert into tbl_delivery_fee_gongje set df_id = '".$delivery_fee_details->df_id."',co_code = '".$delivery_fee_details->co_code."' ,ws_co_id = '".$delivery_fee_details->gongje_req_co."' ,df_month = '".$delivery_fee_details->df_month."',dp_id = '".$delivery_fee_details->dp_id."',not_paid = '".$cmisu_amount."'";
	}
	//$this->cowork_model->db->query($sql);
	echo 'dp_id'. $delivery_fee_details->dp_id."-".$sql."<br/>";
	$not_paid = $cmisu_amount;
}


// 보정 --- 회계금액  
if(0) {
	$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
	$lv = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
	if(!empty($lv->idx)) { // 
		$cmisu_amount = 0;
		if(!empty($plv->balance_amount)) {
			$cmisu_amount = $plv->balance_amount;
		}

		if(empty($lv->pay_amount)) $lv->pay_amount = 0;
		// 부과액 = 협업총액, 잔액 = 협업총액 - 납부액
		$gn_amount = $tot_gongje_req;
		$balance_amount = $tot_gongje_req - $lv->pay_amount + $cmisu_amount;

		$pay_status = "N";
		if($balance_amount == 0) $pay_status = "F";
		else if($balance_amount == $gn_amount) $pay_status = "N";
		else if($gn_amount < $lv->pay_amount) $pay_status = "O";
		else $pay_status = "P";

		//$sql = "update tbl_delivery_fee_levy set prev_misu = '".$not_paid."',gn_amount = '".$gn_amount."', balance_amount = '".$balance_amount."', pay_status = '".$pay_status."' where df_id='".$delivery_fee_details->df_id."'";
		$sql = "update tbl_delivery_fee_levy set prev_misu = '".$cmisu_amount."',gn_amount = '".$gn_amount."', balance_amount = '".$balance_amount."', pay_status = '".$pay_status."' where df_id='".$delivery_fee_details->df_id."'";
		//$this->cowork_model->db->query($sql);
		echo $delivery_fee_details->E."-[(".$not_paid."/".$lv->prev_misu.")".$lv->gn_amount."||||".$sql."<br/>";//."]-".$sql."<br/>";

		$sql = "update tbl_delivery_fee_gongje set not_paid = '".$cmisu_amount."' where df_id='".$delivery_fee_details->df_id."'";
		echo "update tbl_delivery_fee_gongje set not_paid = '".$cmisu_amount."' where df_id='".$delivery_fee_details->df_id."'<br/>";
		//$this->cowork_model->db->query($sql);
	} else {
		echo "xxxxxxxx";
	}
}

// balance 만 조정
if(0) {
	$lv = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
	if(!empty($lv->idx)) { // 
		$cmisu_amount = $not_paid;

		if(empty($lv->paid_amount)) $lv->pay_amount = 0;
		// 부과액 = 협업총액, 잔액 = 협업총액 - 납부액
		$gn_amount = $tot_gongje_req;
		$balance_amount = $tot_gongje_req - $lv->paid_amount - $lv->tr_amount;

		$pay_status = "N"; //미납
		if($balance_amount == 0) $pay_status = "F"; //완납
		else if($balance_amount == $gn_amount) $pay_status = "N"; // 미납
		else if($gn_amount < $lv->paid_amount) $pay_status = "O"; // 과납
		else $pay_status = "P";

		//if($delivery_fee_details->tr_type

		$sql = "update tbl_delivery_fee_levy set  balance_amount = '".$balance_amount."', pay_status = '".$pay_status."' where df_id='".$delivery_fee_details->df_id."'";
		//$this->cowork_model->db->query($sql);
		echo $delivery_fee_details->tr_type."-[(".$not_paid."/".$lv->prev_misu.")".$lv->gn_amount."||".$lv->tr_amount."||".$sql."<br/>";//."]-".$sql."<br/>";
	}
}


//일할 보정
if(0) {
	$lastday=array(0,31,28,31,30,31,30,31,31,30,31,30,31);
	$inm = "";
	$outm = "";
	$ilhal_days = 0;
	$sql_mf = "";
	$sql_ilhal = "";
	if(!empty($delivery_fee_details->N)) $inm = substr($delivery_fee_details->N,0,7);
	if(!empty($delivery_fee_details->O)) $outm = substr($delivery_fee_details->O,0,7);

	$tmp = substr($df_month,5,2);
	$endDay = $lastday[(int)$tmp];
	
	$is_ilhal = "N";

	// 추가사항 첫날과 마지막날 일경우제외 월별 일수 데이터 필요
	if($df_month == $inm || $df_month == $outm) {
		$ilhal_days = 0;
		$s = substr($delivery_fee_details->N,8,2);
		$e = substr($delivery_fee_details->O,8,2);
		if($df_month == $inm && $df_month == $outm) { // 당월 입.퇴사자
			$ilhal_days = (int)$e - (int)$s + 1;
		} else if($df_month == $inm) { // 당월 입사자
			$ilhal_days = ($endDay - (int)$s) + 1;
		} else if($df_month == $outm) { // 당월 퇴사자
			$ilhal_days = ((int)$e);
		}

		if($ilhal_days < $endDay && $ilhal_days > 0) $is_ilhal = "Y";
		$sql_ilhal = ", remark ='일할계산(".$ilhal_days."일)'";

								// 일할계산 - 당월 계약 종료자가 15일 이하일경우만 50%로 계산하고 나머진 무조건 일할계산
								if($is_ilhal == "Y") {
									if($df_month == $outm && $ilhal_days < 15) {
										if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)($wst_mfee/2);
										if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)($mfee_vat/2);
									} else {
										if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)(($wst_mfee/$endDay) * $ilhal_days);
										if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)(($mfee_vat/$endDay) * $ilhal_days);
									}
									$sql_mf .= "wst_mfee='".$wst_mfee."'"; 
									$sql_mf .= ",mfee_vat='".$mfee_vat."'";

		$sql = "update tbl_delivery_fee_fixmfee set ".$sql_mf." where df_id='".$delivery_fee_details->df_id."'";
		$this->cowork_model->db->query($sql);
	echo($delivery_fee_details->ceo."::".$delivery_fee_details->N."~".$sql."</br>");
								}
	}

}

?>
                                    <tr>
                                        <td>
                                            <span class='label label-<?=$pay_status_color?>'><?=$pay_status?></span>
                                            <?= $sn ?>
                                        </td>
                                        <td style="text-align:center;">
                                                <div class="btn-group">
                                                    <button class="btn btn-xs btn-<?=($delivery_fee_details->is_closed=='Y')?"success":"danger"?> dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <?=($delivery_fee_details->is_closed=='Y')?"마감":"마감전"?>
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu animated zoomIn">
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/Y">마감하기</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/N">마감취소</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                        </td>
                                        <!--td style="text-align:center;">
                                                <div class="btn-group">
                                                    <button class="btn btn-xs btn-<?=($delivery_fee_details->is_pay_closed=='Y')?"success":"danger"?> dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <?=($delivery_fee_details->is_pay_closed=='Y')?"마감":"마감전"?>
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu animated zoomIn">
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_payment/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/Y">마감하기</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_payment/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/N">마감취소</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                        </td-->
                                        <td style="text-align:center;">
										<?=($delivery_fee_details->read_cnt > 0)?"<span class='label label-success'>".$delivery_fee_details->read_cnt."</span>":"<span class='label label-danger'>0</span>"?>
								
										</td>
                                        <td>
                                            <span class='label label-primary'><?=$gj_rq_co ?></span>
										</td>
                                        <td><?= $delivery_fee_details->co_name ?></td>
                                        <td><?= $delivery_fee_details->ceo ?></td>
                                        <td><?= $delivery_fee_details->bs_number ?></td>
                                        <td>
										<?php if(!empty($truck->idx)) { ?>
                                              <span data-placement="top" data-toggle="tooltip" title="차량정보 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_info','<?=(!empty($truck->idx))? $truck->idx:"" ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										
										<span class='label label-primary'><?= $truck_no ?></span>
										<?php } ?>
										</td>
                                        <td><?php if(!empty($delivery_fee_details->N)) echo $delivery_fee_details->N; ?></td>
                                        <td><?php if(!empty($delivery_fee_details->O)) echo $delivery_fee_details->O; ?></td>


                                        <td style="text-align:right;">
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="관리비 설정">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_mfee/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										<?= number_format($wst_mfee) ?></td>
                                        <td style="text-align:right;"><? if(!empty($mfee_vat)) echo number_format($mfee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee)) echo number_format($mgrg_fee,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee_vat)) echo number_format($mgrg_fee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($org_fee)) echo number_format($org_fee,0); ?></td>
										<td style="text-align:right;"><? if(!empty($wsm_sum)) echo number_format($wsm_sum,0); ?></td>

										<td style="text-align:right;">
                                              <span data-placement="top" data-toggle="tooltip" title="보험 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_ins','<?= $truck->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										<?= number_format($ins_car) ?></td>
										<td style="text-align:right;"><?= number_format($ins_load) ?></td>
										<td style="text-align:right;"><?= number_format($insur_sum) ?></td>

										<td style="text-align:right;">
											<span data-placement="top" data-toggle="tooltip" title="초기미수금액 설정">
												<a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/cowork/set_misu/<?= $delivery_fee_details->df_id ?>" class="text-default ml"><i class="fa fa-cog"></i></a>
                                            </span>
										<?= number_format($not_paid) ?></td>
										<td style="text-align:right;">
                                                <span data-placement="top" data-toggle="tooltip" title="일반공제 업데이트">
													<a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/cowork/set_ngongje/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $delivery_fee_details->gongje_req_co ?>" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
                                                <span data-placement="top" data-toggle="tooltip" title="과태료 등록">
													<a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/cowork/set_finefee/list/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $delivery_fee_details->gongje_req_co ?>/<?=$is_editable?>" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										
										<?= number_format($fine_fee) ?>
										</td>

										<td style="text-align:right;"><?= number_format($grg_fee) ?></td><!--//차고지비 -->
                                        <td style="text-align:right;"><?= number_format($grg_fee_vat) ?></td><!--//차고지비 부가세 -->
										<td style="text-align:right;"><?= number_format($gongje_sum) ?></td>

                                        <td style="text-align:right;">
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="환급형공제 업데이트">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_rgongje/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										<?= number_format($gj_termination_mortgage) ?></td>
										<td style="text-align:right;"><?= number_format($rf_gongje_sum) ?></td>

										<td style="text-align:right;"><?= number_format($tot_gongje) ?></td>
										<td style="text-align:right;"><?= (!empty($lv->tr_amount))? number_format($lv->tr_amount):"-" ?></td>
										<td style="text-align:right;"><?= number_format($tot_gongje_req) ?></td>
										<td><?php //echo btn_edit('admin/basic/manage_customer/' . $delivery_fee_details->df_id) ?>
											<a href="javascript:popSpecifications()" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables" title="명세서"> 명세서 </a>
										</td>

                                    </tr>
<?php
if(0) { //!empty($lv->tr_amount)) {
	if($lv->tr_amount > $tot_gongje_req) {
		$bal = $lv->tr_amount - $tot_gongje_req;	
		echo $bal."  xxxxxxxxx</br>";
	} else if($lv->tr_amount < $tot_gongje_req) {
		$bal = $tot_gongje_req - $lv->tr_amount;	
		echo $bal."  qqqqqqqqqqqqqqq</br>";
	}
}


if(0) {//!empty($lv->tr_amount)) {

/*
	$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();

	$cmisu_amount = 0;
	if(!empty($plv->balance_amount)) {
		$cmisu_amount = $plv->balance_amount;
	}

	$sql = "update tbl_delivery_fee_gongje set not_paid = '".$cmisu_amount."' where df_id='".$delivery_fee_details->df_id."'";
	echo $delivery_fee_details->ceo . "-" . $sql."</br>";
	$this->cowork_model->db->query($sql);
*/
/*
	//수납생성 - 관리비가 있는 사람경우 / 거래액 > 0 경우 1) 관리비 - 거래액 <= 0 경우 부과액 0원  2) 거래이력 공제로 이력등록 
	$paid_amount = 0;
	$pay_amount = 0;
	$stot_mfee = $tot_gongje_req;//당월 관리비
	$stot_transaction = $lv->tr_amount; //당월 거래액
	$tot_transaction = 0; //정산후 거래액(거래액 - 관리비) > 0 경우값 저장 -인경우 0
	$gn_amount = $stot_mfee - $stot_transaction; // 정산후 관리비 부과액
	//if(!empty($str_details->stot_transaction)) $stot_transaction = $str_details->stot_transaction;
	if($gn_amount < 0) {
		$gn_amount = 0;
		$paid_amount = $stot_mfee; // 실부과액이 0원이 되었을경우 납부액=부과액 > 로그기록
		$pay_amount = $stot_mfee;
	} else {
		$tot_transaction = $stot_transaction - $stot_mfee;
		$paid_amount = $stot_transaction; // 실부과액이 0원 이상일경우 납부액 = 거래액
		$pay_amount = $paid_amount;
	}
	$balance_amount = $gn_amount; 
	$tot_mfee = $gn_amount; //실부과 관리비(관리비 - 거래액) <= 0 경우 모두 0으로

	$qry_add = " prev_misu='".$cmisu_amount."'"; 
	$qry_add .= ", gn_amount='".$gn_amount."'"; 
	$qry_add .= ", balance_amount='".$balance_amount."'"; 
	$qry_add .= ", mf_amount='".$stot_mfee."'"; 
	//$qry_add .= ", tr_amount='".$stot_transaction."'"; 
	$qry_add .= ", paid_amount='".$paid_amount."'"; 
	//$qry_add .= ", tot_transaction='".$tot_transaction."'";

	if(empty($lv->df_id)) {
		if($lv->paid_amount > 0) {
			$balance_amount = $balance_amount - $lv->paid_amount;
		}

		$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id='".$delivery_fee_details->df_id."', dp_id='".$delivery_fee_details->dp_id."', df_month='$df_month', ".$qry_add;
		$lv_id = $this->cowork_model->db->insert_id(); 

	} else {
		$sql = "UPDATE tbl_delivery_fee_levy SET ".$qry_add." WHERE df_id='".$delivery_fee_details->df_id."'";
		$lv_id = $lv->df_id;
	}
	echo $sql;
	$this->cowork_model->db->query($sql);

	//이력
	$sql = "INSERT INTO tbl_delivery_fee_levy_log SET ";
	$sql .= "pid='".$lv_id."'";
	$sql .= ",df_id='".$delivery_fee_details->df_id."'";
	$sql .= ",dp_id='".$delivery_fee_details->dp_id."'";
	$sql .= ",pay_amount='".$pay_amount."'";
	$sql .= ",pay_date='".date('Y-m-d')."'";
	$sql .= ",balance_amount='".$balance_amount."'";
	$sql .= ",remark='거래내역 자동정산'";
	$sql .= ",reg_datetime = now()";
	echo $sql."</br>";
	$this->cowork_model->db->query($sql);
*/
}
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="31">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>






                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<ul class="pagination">
<?
$pagelist = get_paging(10, $page, $total_page, base_url()."admin/cowork/jiip_gongje_all/",'','');
echo $pagelist;
?>
</ul>


<?php
function get_paging($write_pages, $cur_page, $total_page)
{
    $str = "";
    if ($cur_page > 1) {
		$str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goPage(1);">First</a></li>';
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goPage('.($start_page-1).');">Previous</a></li>';

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= '<li class="paginate_button" tabindex="0"><a href="javascript:goPage('.$k.');">'.$k.'</a></li>';
            else
                $str .= '<li class="paginate_button active" tabindex="0"><a href="javascript:goPage('.$k.');">'.$k.'</a></li>';
        }
    }

    if ($total_page > $end_page) $str .= '<li class="paginate_button next" tabindex="0"><a href="javascript:goPage('.($end_page+1).');">Next</a></li>';

    if ($cur_page < $total_page) {
		$str .= '<li class="paginate_button previous" tabindex="0"><a href="javascript:goPage('.$total_page.');">Last</a></li>';
    }
    $str .= "";

    return $str;
}
?>