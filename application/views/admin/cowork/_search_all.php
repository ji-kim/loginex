                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/cowork/jiip_gongje_all"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
					  <input type="hidden" name="page" value="<?php if(!empty($page)) echo $page;?>">
				<div>
					<div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">

						<div class="form-group" id="border-none" style="margin:0px">
							<div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">년월</label>
                                        <div class="col-lg-7">
											<div class="input-group">
												<input type="text" value="<?php
												if (!empty($df_month)) {
													echo $df_month;
												}
												?>" class="form-control monthyear" name="df_month" id="df_month"
													   data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="goSearch();">

												<div class="input-group-addon">
													<a href="#"><i class="fa fa-calendar"></i></a>
												</div>
											</div>
                                        </div>
                                    </div>
							</div>
							<div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">공제청구사</label>
                                        <div class="col-lg-8">
											<input type="hidden" name="gongje_req_co" id="gongje_req_co" value="<?php
                                                               if (!empty($gongje_req_co)) {
                                                                   echo $gongje_req_co;
                                                               }
                                                               ?>">
											<input type="text" name="gongje_req_co_name" id="gongje_req_co_name" value="<?=(empty($rco_info->co_name))?"":$rco_info->co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('group','gongje_req_co||gongje_req_co_name||__||__||__||__||__||__');" onChange="goSearch()">
                                        </div>
                                    </div>
							</div>
							<div class="col-sm-5" style="padding-right:0px;">
                                                            <div class="input-group">
																<input type="search" value="<?=$search_keyword?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="통합검색">
                                                                <span class="input-group-btn">
																	<button class="btn btn-success" type="button" onclick="goSearch();">
                                                                        검색
                                                                        <i class="fa fa-search"></i>
                                                                    </button>

																	<button class="btn btn-warning" type="button" onclick="goExcelListAll();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-file-excel-o"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>




						
						</div>
					<font style='color:red;font-weight:bold;'>* 관리비 생성 및 세금계산서 발행은 공제청구사를 선택하신 후 이용 가능합니다.</font>
					</div>
				</div>

				</form>
