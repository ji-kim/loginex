<?php

//파트너
$dp = $this->db->where('dp_id', $df_info->dp_id)->get('tbl_members')->row();

//공제청구사
$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;

//차량정보
$truck = $this->db->where('idx', $df_info->tr_id)->get('tbl_asset_truck')->row();
if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 과태로 등록 - [<?php echo $df_info->df_month ?>]  <?php echo $dp->driver ?> / <?= $truck_no ?> <?=$df_info->dp_id?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">

                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="col-sm-1>No</th>
                                <th class="col-sm-2">금액</th>
                                <th class="col-sm-3">위반일시</th>
                                <th class="col-sm-1">첨부</th>
                                <th class="col-sm-5">비고</th>
                            </tr>
                            </thead>
                            <tbody>
<?php
if (!empty($all_finefee_group)) {
	$i = 0;
	foreach ($all_finefee_group as $finefee_info) {
?>
                                <tr>
                                    <td><?=($i+1)?></td>
                                    <td><?=$finefee_info->amount?></td>
                                    <td><?=$finefee_info->due_date?></td>
                                    <td>
				<?php 
				if(!empty($finefee_info->attach1)) {
					echo "<a href='". base_url() . "/". $finefee_info->attach1 ."' class='text-success' target='_blank'>[열람]</a>";
				}
				?>				
									</td>
                                    <td><?=$finefee_info->memo?></td>

                                </tr>

<?php
		$i++;
	}
}
?>
</table>

<?php
$action = "add";

if(empty($amount)) $amount = 0;
if(empty($due_date)) $due_date = date('Y-m-d');;
?>
        <form id="form_validation" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/cowork/set_finefee/<?=$action?>/<?= $df_id ?>/<?= $dp_id ?>/<?= $df_month ?>/<?= $ws_co_id ?>" method="post" class="form-horizontal form-groups-bordered">
		 
     <div class="modal-body wrap-modal wrap">
           <div class="form-group" id="border-none">
                <div class="col-sm-1" style="border-right:1px solid #eee;text-align:center;color:#fff;background-color: #777;font-size:9px;"></div>
                <div class="col-sm-2"><input type="text" value="<?php echo $amount; ?>" class="form-control" name="amount" ></div>
                <div class="col-sm-3">
							<div class="input-group">
                                <input type="text" value="<?php echo $due_date; ?>" class="form-control datepicker" name="due_date" data-date-format="yyyy/mm/dd" style="width:100%;">
                            </div>
				</div>
                <div class="col-sm-2">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <?php if (!empty($ngongje_info->fine)): ?>
                            <span class="btn btn-default btn-file"><span class="fileinput-new"
                                                                         style="display: none">선택</span>
                                        <span class="fileinput-exists"
                                              style="display: block">변경</span>
                                        <input type="hidden" name="fine" value="<?php echo $ngongje_info->fine ?>">
                                        <input type="file" name="attach1">
                                    </span>
                            <span class="fileinput-filename"> <?php echo $ngongje_info->fine_filename ?></span>
                        <?php else: ?>
                            <span class="btn btn-default btn-file"><span
                                    class="fileinput-new">선택</span>
                                        <span class="fileinput-exists">변경</span>
                                        <input type="file" name="attach1">
                                    </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                               style="float: none;">&times;</a>
                        <?php endif; ?>

                    </div>
                    <div id="msg_pdf" style="color: #e11221"></div>
				</div>
                <div class="col-sm-4"><input type="text" value="<?php //echo $car_tax; ?>" class="form-control" name="memo" ></div>
            </div>
     </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                <?php if(!empty($is_editable) && $is_editable=="Y") { ?><button type="submit" class="btn btn-primary"> 저 장 </button><?php } ?>
            </div>
            <div class="modal-footer">
                * 값이 변경(추가)되면 위.수탁 관리비도 새로 생성해야 적용됩니다.
            </div>
        </form>
    </div>
</div>
