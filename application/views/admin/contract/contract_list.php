<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<script>
	function goSearch(ct_status) {
		document.myform.action = "<?php echo base_url() ?>admin/contract/contract_list/"+ct_status;
		document.myform.submit();
	}
</script>

            <div class="col-sm-12 bg-white " style="margin-bottom:10px;">
                
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/contract/contract_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
				<div>
					<div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">

						<div class="form-group" id="border-none" style="margin:0px">
							<div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">계약명</label>
                                        <div class="col-lg-8">
											<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="계약명">
										</div>
                                    </div>
							</div>
							<div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">실수요처</label>
                                        <div class="col-lg-8">
											<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="실수요처">
                                        </div>
                                    </div>
							</div>
							<div class="col-sm-5" style="padding-right:0px;">
                                                            <div class="input-group">
																<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="통합검색">
                                                                <span class="input-group-btn">
																	<button class="btn btn-success" type="button" onclick="goSearch();">
                                                                        검색
                                                                        <i class="fa fa-search"></i>
                                                                    </button>

																	<button class="btn btn-warning" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-file-excel-o"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>



						
						</div>
					</div>
				</div>

				


	<div class="col-sm-12 bg-white p0">
        <div class="col-md-7">
            <div class="row row-table pv-lg">
                <div class="col-xs-2">
					<a href="javascript:goSearch('RD');" tabindex="0" class="dt-button buttons-print btn btn-<?=($ct_status=="RD")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약대기현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('IN');" tabindex="0" class="dt-button buttons-print btn btn-<?=($ct_status=="IN")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 진행계약현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('RF');" tabindex="0" class="dt-button buttons-print btn btn-<?=($ct_status=="RF")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 만료대기현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('EX');" tabindex="0" class="dt-button buttons-print btn btn-<?=($ct_status=="EX")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 연장계약현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('FN');" tabindex="0" class="dt-button buttons-print btn btn-<?=($ct_status=="FN")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약만료현황</span>
					</a>
                </div>

            </div>

        </div>
    </div>

            </div>

				</form>
      <!-- 검색 끝 -->




<?php }

$id = $this->uri->segment(20);
$search_by = $this->uri->segment(20);
$created = can_action('20', 'created');
$edited = can_action('20', 'edited');
$deleted = can_action('20', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#contract_list"
                                                                   data-toggle="tab">원청계약관리</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_contract"
                                                                   data-toggle="tab">신규계약등록</a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="contract_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
							<table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약상태</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">하도급계약</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약(공급)사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실수요처</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약만료일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약형태</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">등록일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약금액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">잔여일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
        </tr>
                                </thead>
                                <tbody>

                                <?php
                                if (!empty($all_contract_info)) {
                                    foreach ($all_contract_info as $contract_details) {

$setdate =  trim(date("Y-m-d"));
$sdate = trim($contract_details->ct_edate); //계약만료일 가지고옴
$days_left = intval((strtotime($sdate)-strtotime($setdate)) / 86400); //오늘날짜와 계약만료일 계산
if($contract_details->sct_status == "FN" || $contract_details->sct_status =="RD") {
	if(($sdate && $days_left < 31 && $contract_details->sct_status=="FN") || ($sdate && $sdate < $setdate)) { //계약완료인 상태이면서 계약만료일이 0보다 작을때 실행 
		//$sql ="update c4_tsst_ptuchal set ct_status='계약만료대기' where wr_id='{$list[$i][wr_id]}'";
		//sql_query($sql);
		//$sql2 = "update az_contract set ct_status='계약만료대기' where wr_id='{$list[$i][wr_id]}'";
		//sql_query($sql2);
	} 
}
if($sdate < $setdate || $days_left<1) {
	$days_left = "-";
} else $days_left = $days_left."일전";

                                        ?>
                                        <tr>
										  <td height="25" align="center"><?=$contract_details->idx?></td>
										  <td align="center" style="padding-left:5px;">
		  
<?
	// RD IN RF EX FN
if((($contract_details->sct_status =="FN" || $contract_details->sct_status =="RD") && $contract_details->tr_status =="RD") || $contract_details->sct_status =="RF") { ?>

<input type="button" style="WIDTH: 70pt; HEIGHT: 18pt;background-color:red;color:#ffffff;" value="<?//=$contract_details->idx ?> 거래등록 " onClick="location.href='<?php echo base_url(); ?>admin/logis/stransaction_write/input/<?=$contract_details->idx ?>/<?= $contract_details->s_co_id  ?>';">		 
<?
} else if($contract_details->sct_status =="FN" && $contract_details->tr_status  =="RD") { ?>

<input type="button" style="WIDTH: 50pt; HEIGHT: 18pt;background-color:blue;color:#ffffff;" value=" 등록 " onClick="location.href='<?php echo base_url(); ?>admin/logis/stransaction_write/generate/<?=$contract_details->idx ?>';">		 
<?
} else if($contract_details->sct_status =="FN") {
?>		 
<input type="button" style="WIDTH: 50pt; HEIGHT: 18pt;background-color:#ffbbff;" value=" 만료 " onClick="location.href='<?php echo base_url(); ?>admin/logis/stransaction_write/<?=$contract_details->idx ?>';">		 
<? } else echo "-"; ?>
		  
		  <?= $contract_details->tr_status  ?></td>
										  <td align="center" style="padding-left:5px;"><?= $contract_details->is_subcontract  ?></td>
										  <td align="left" style="padding-left:5px;"><?= $contract_details->s_co  ?><?//= $contract_details->s_co_id  ?></td>
										  <td align="left" style="padding-left:5px;"><?= $contract_details->r_co  ?></td>
										  <td align="left" style="padding-left:5px;"><?= $contract_details->tr_title  ?></td>



										  <td align="center" style="padding-left:5px;"><?= $contract_details->ct_sdate  ?></td>
										  <td align="center" style="padding-left:5px;"><?= $contract_details->ct_edate  ?></td>
										  <td align="center" style="padding-left:5px;">
										  <?php
										  if($contract_details->ct_type == "01") echo "단가 > 건당단가";
										  else if($contract_details->ct_type == "02") echo "총액 > 건당단가";
										  ?>
										  </td>
										  <td align="center" style="padding-left:5px;"><?= $contract_details->reg_date  ?></td>
										  <td align="right" style="padding-left:5px;"><?= number_format($contract_details->ct_amount,0)  ?></td>
										  <td align="center" style="padding-left:5px;"><?= $days_left?></td>
										  <td align="center" style="padding-left:5px;">
										  
                                            <?php if (!empty($edited)) { ?>

                                                <?php echo btn_edit('admin/contract/contract_list/edit_contract/' . $contract_details->idx); ?>
                                            <?php }
                                            if (!empty($deleted)) { ?>
                                                <?php echo btn_delete('admin/contract/delete_contract/' . $contract_details->idx); ?>
                                            <?php } ?>
										  
										  </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            자료가 없습니다.
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
						include "./application/views/admin/contract/_contract_edit.inc.php";
					?>
                        
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>