						
						
						
						<div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_contract"
                        style="position: relative;">

                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/contract/save_contract/<?php
                              if (!empty($contract_info)) {
                                  echo $contract_info->contract_id;
                              }
                              ?>" method="post" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-3"></label
                                <div class="col-sm-6">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_compnay"
                                                                  data-toggle="tab">기본계약정보</a>
                                            </li>
                                            <li><a href="#contact_details"
                                                   data-toggle="tab">계약상세정보</a>
                                            </li>
                                            <li><a href="#hosting_compnay" data-toggle="tab">계약단가설정</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** general *************-->
                                            <div class="chart tab-pane active" id="general_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">계약(공급)사
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->supp_co_id)) {
                                                                   echo $contract_info->supp_co_id;
                                                               }
                                                               ?>" name="supp_co_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">실수요처
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($contract_info->rr_co_id)) {
                                                                   echo $contract_info->rr_co_id;
                                                               }
                                                               ?>" name="rr_co_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">거래용역명</label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->ct_title)) {
                                                            echo $contract_info->ct_title;
                                                        }
                                                        ?>" name="ct_title">
                                                    </div>
                                                </div>




                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('customer_group') ?></label>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <select name="customer_group_id"
                                                                    class="form-control select_box"
                                                                    style="width: 100%">
                                                                <?php
                                                                if (!empty($all_customer_group)) {
                                                                    foreach ($all_customer_group as $customer_group) : ?>
                                                                        <option
                                                                            value="<?= $customer_group->customer_group_id ?>"<?php
                                                                        if (!empty($contract_info->customer_group_id) && $contract_info->customer_group_id == $customer_group->customer_group_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $customer_group->customer_group; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                            <?php if (!empty($created)) { ?>
                                                                <div class="input-group-addon"
                                                                     title="<?= lang('new') . ' ' . lang('customer_group') ?>"
                                                                     data-toggle="tooltip" data-placement="top">
                                                                    <a data-toggle="modal" data-target="#myModal"
                                                                       href="<?= base_url() ?>admin/contract/customer_group"><i
                                                                            class="fa fa-plus"></i></a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('language') ?></label>
                                                    <div class="col-sm-5">
                                                        <select name="language" class="form-control select_box"
                                                                style="width: 100%">
                                                            <?php

                                                            foreach ($languages as $lang) : ?>
                                                                <option
                                                                    value="<?= $lang->name ?>"<?php
                                                                if (!empty($contract_info->language) && $contract_info->language == $lang->name) {
                                                                    echo 'selected';
                                                                } elseif (empty($contract_info->language) && $this->config->item('language') == $lang->name) {
                                                                    echo 'selected';
                                                                } ?>
                                                                ><?= ucfirst($lang->name) ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('currency') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="currency" class="form-control select_box"
                                                                style="width: 100%">

                                                            <?php if (!empty($currencies)): foreach ($currencies as $currency): ?>
                                                                <option
                                                                    value="<?= $currency->code ?>"
                                                                    <?php
                                                                    if (!empty($contract_info->currency) && $contract_info->currency == $currency->code) {
                                                                        echo 'selected';
                                                                    } elseif (empty($contract_info->currency) && $this->config->item('default_currency') == $currency->code) {
                                                                        echo 'selected';
                                                                    } ?>
                                                                ><?= $currency->name ?></option>
                                                                <?php
                                                            endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('short_note') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="short_note"><?php
                                                if (!empty($contract_info->short_note)) {
                                                    echo $contract_info->short_note;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <?php
                                                if (!empty($contract_info)) {
                                                    $contract_id = $contract_info->contract_id;
                                                } else {
                                                    $contract_id = null;
                                                }
                                                ?>
                                                <?= custom_form_Fields(12, $contract_id); ?>
                                            </div><!-- ************** general *************-->

                                            <!-- ************** Contact *************-->
                                            <div class="chart tab-pane" id="contact_details">


												<div class="form-group">
													<label class="col-lg-3 control-label">계약체결일<span
															class="text-danger">*</span></label>
													<div class="col-lg-5">
														<div class="input-group">
															<input required type="text" id="start_date" name="start_date"
																   class="form-control datepicker"
																   value="<?php
																   if (!empty($contract_info->start_date)) {
																	   echo date('Y-m-d', strtotime($contract_info->start_date));
																   }
																   ?>"
																   data-date-format="<?= config_item('date_picker_format'); ?>">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label">계약시작일 <span
															class="text-danger">*</span></label>
													<div class="col-lg-5">
														<div class="input-group">
															<input required type="text" id="start_date" name="start_date"
																   class="form-control datepicker"
																   value="<?php
																   if (!empty($contract_info->start_date)) {
																	   echo date('Y-m-d', strtotime($contract_info->start_date));
																   }
																   ?>"
																   data-date-format="<?= config_item('date_picker_format'); ?>">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label">계약종료일 <span
															class="text-danger">*</span></label>
													<div class="col-lg-5">
														<div class="input-group">
															<input required type="text" id="end_date" name="end_date"
																   data-rule-required="true"
																   data-msg-greaterThanOrEqual="end_date_must_be_equal_or_greater_than_start_date"
																   data-rule-greaterThanOrEqual="#start_date"
																   class="form-control datepicker"
																   value="<?php
																   if (!empty($contract_info->end_date)) {
																	   echo date('Y-m-d', strtotime($contract_info->end_date));
																   }
																   ?>"
																   data-date-format="<?= config_item('date_picker_format'); ?>">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
													</div>
												</div>













                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_phone') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->phone)) {
                                                            echo $contract_info->phone;
                                                        }
                                                        ?>" name="phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_mobile') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($contract_info->mobile)) {
                                                                   echo $contract_info->mobile;
                                                               }
                                                               ?>" name="mobile">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('zipcode') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->zipcode)) {
                                                            echo $contract_info->zipcode;
                                                        }
                                                        ?>" name="zipcode">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_city') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->city)) {
                                                            echo $contract_info->city;
                                                        }
                                                        ?>" name="city">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_country') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="country" class="form-control select_box"
                                                                style="width: 100%">
                                                            <optgroup label="Default Country">
                                                                <option
                                                                    value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                                            </optgroup>
                                                            <optgroup label="<?= lang('other_countries') ?>">
                                                                <?php if (!empty($countries)): foreach ($countries as $country): ?>
                                                                    <option
                                                                        value="<?= $country->value ?>" <?= (!empty($contract_info->country) && $contract_info->country == $country->value ? 'selected' : NULL) ?>><?= $country->value ?>
                                                                    </option>
                                                                    <?php
                                                                endforeach;
                                                                endif;
                                                                ?>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_fax') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->fax)) {
                                                            echo $contract_info->fax;
                                                        }
                                                        ?>" name="fax">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_address') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="address"><?php
                                                if (!empty($contract_info->address)) {
                                                    echo $contract_info->address;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">
                                                        <a href="#"
                                                           onclick="fetch_lat_long_from_google_cprofile(); return false;"
                                                           data-toggle="tooltip"
                                                           data-title="<?php echo lang('fetch_from_google') . ' - ' . lang('customer_fetch_lat_lng_usage'); ?>"><i
                                                                id="gmaps-search-icon" class="fa fa-google"
                                                                aria-hidden="true"></i></a>
                                                        <?= lang('latitude') . '( ' . lang('google_map') . ' )' ?>
                                                    </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->latitude)) {
                                                            echo $contract_info->latitude;
                                                        }
                                                        ?>" name="latitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('longitude') . '( ' . lang('google_map') . ' )' ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($contract_info->longitude)) {
                                                                   echo $contract_info->longitude;
                                                               }
                                                               ?>" name="longitude">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Contact *************-->
                                            <!-- ************** Web *************-->
                                            <div class="chart tab-pane" id="web_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_domain') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->website)) {
                                                            echo $contract_info->website;
                                                        }
                                                        ?>" name="website">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('skype_id') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->skype_id)) {
                                                            echo $contract_info->skype_id;
                                                        }
                                                        ?>" name="skype_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('facebook_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->facebook)) {
                                                            echo $contract_info->facebook;
                                                        }
                                                        ?>" name="facebook">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('twitter_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->twitter)) {
                                                            echo $contract_info->twitter;
                                                        }
                                                        ?>" name="twitter">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('linkedin_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->linkedin)) {
                                                            echo $contract_info->linkedin;
                                                        }
                                                        ?>" name="linkedin">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Web *************-->
                                            <!-- ************** Hosting *************-->
                                            <div class="chart tab-pane" id="hosting_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hosting_company') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->hosting_company)) {
                                                            echo $contract_info->hosting_company;
                                                        }
                                                        ?>" name="hosting_company">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hostname') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->hostname)) {
                                                            echo $contract_info->hostname;
                                                        }
                                                        ?>" name="hostname">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('username') ?> </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->username)) {
                                                            echo $contract_info->username;
                                                        }
                                                        ?>" name="username">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('password') ?></label>
                                                    <div class="col-lg-5">
                                                        <?php
                                                        if (!empty($contract_info->password)) {
                                                            $password = strlen(decrypt($contract_info->password));
                                                        }
                                                        ?>
                                                        <input type="password" name="password" value=""
                                                               placeholder="<?php
                                                               if (!empty($password)) {
                                                                   for ($p = 1; $p <= $password; $p++) {
                                                                       echo '*';
                                                                   }
                                                               }
                                                               ?>" class="form-control">
                                                        <strong id="show_password" class="required"></strong>
                                                    </div>
                                                    <?php if (!empty($contract_info->password)) { ?>
                                                        <div class="col-lg-3">
                                                            <a data-toggle="modal" data-target="#myModal"
                                                               href="<?= base_url('admin/contract/see_password/c_' . $contract_info->contract_id) ?>"
                                                               id="see_password"><?= lang('see_password') ?></a>
                                                            <strong id="hosting_password" class="required"></strong>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('port') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($contract_info->port)) {
                                                            echo $contract_info->port;
                                                        }
                                                        ?>" name="port">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Hosting *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                        </div>
                                        <div class="col-lg-3">
                                            <button type="submit" name="save_and_create_contact" value="1"
                                                    class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button>
                                        </div>

                                    </div>
                                </div>
                        </form>
