<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function popAsWindow(id) {
	  window.open('<?php echo base_url(); ?>admin/basic/set_item/'+id+'/tr_item/01', 'winAdd', 'left=50, top=50, width=1300, height=700, scrollbars=1');
	}

	function goSearch(ct_status) {
		document.myform.action = "<?php echo base_url() ?>admin/contract/scontract_list/"+ct_status;
		document.myform.submit();
	}
</script>

            <div class="col-sm-12 bg-white " style="margin-bottom:10px;">
                
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/contract/scontract_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
				<div>
					<div class="col-sm-12" style="margin-top:10px;margin-bottom:5px;">

						<div class="form-group" id="border-none" style="margin:0px">
							<div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">계약명</label>
                                        <div class="col-lg-8">
											<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="계약명">
										</div>
                                    </div>
							</div>
							<div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">실수요처</label>
                                        <div class="col-lg-8">
											<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="실수요처">
                                        </div>
                                    </div>
							</div>
							<div class="col-sm-5" style="padding-right:0px;">
                                                            <div class="input-group">
																<input type="search" value="<?//=$params['sc_key'];?>" name="search_keyword" id="search_keyword" class="form-control" placeholder="통합검색">
                                                                <span class="input-group-btn">
																	<button class="btn btn-success" type="button" onclick="goSearch();">
                                                                        검색
                                                                        <i class="fa fa-search"></i>
                                                                    </button>

																	<button class="btn btn-warning" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-file-excel-o"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>



						
						</div>
					</div>
				</div>

				


        <div class="col-md-7">
            <div class="row row-table pv-lg">
                <div class="col-xs-2">
					<a href="javascript:goSearch('RD');" tabindex="0" class="dt-button buttons-print btn btn-<?=($sct_status=="RD")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약대기현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('IN');" tabindex="0" class="dt-button buttons-print btn btn-<?=($sct_status=="IN")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 진행계약현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('RF');" tabindex="0" class="dt-button buttons-print btn btn-<?=($sct_status=="RF"||$sct_status=="RF")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 만료대기현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('EX');" tabindex="0" class="dt-button buttons-print btn btn-<?=($sct_status=="EX"||$sct_status=="EX")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 연장계약현황 </span>
					</a>
                </div>
                <div class="col-xs-2">
					<a href="javascript:goSearch('FN');" tabindex="0" class="dt-button buttons-print btn btn-<?=($sct_status=="FN")?"success":"default"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약만료료현황</span>
					</a>
                </div>

            </div>

        </div>
    </div>

            </div>
                </form>



<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#client_list" data-toggle="tab">하도급계약관리</a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_client" data-toggle="tab">신규등록</a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
							<table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>
									<tr align="center" bgcolor="#e0e7ef">
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">설정</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약상태</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약(공급)사</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실수요처</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약명</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약형태</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약만료일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">잔여일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">등록일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
									</tr>
                                </thead>
                                <tbody>

                                <?php
                                if (!empty($all_scontract_info)) {
                                    foreach ($all_scontract_info as $scontract_details) {

$setdate =  trim(date("Y-m-d"));
$sdate = trim($scontract_details->ct_edate); //계약만료일 가지고옴
$days_left = intval((strtotime($sdate)-strtotime($setdate)) / 86400); //오늘날짜와 계약만료일 계산
if($scontract_details->sct_status == "FN" || $scontract_details->sct_status =="RD") {
	if(($sdate && $days_left < 31 && $scontract_details->sct_status=="FN") || ($sdate && $sdate < $setdate)) { //계약완료인 상태이면서 계약만료일이 0보다 작을때 실행 
		//$sql ="update c4_tsst_ptuchal set ct_status='계약만료대기' where wr_id='{$list[$i][wr_id]}'";
		//sql_query($sql);
		//$sql2 = "update az_contract set ct_status='계약만료대기' where wr_id='{$list[$i][wr_id]}'";
		//sql_query($sql2);
	} 
}
if($sdate < $setdate || $days_left<1) {
	$days_left = "-";
} else $days_left = $days_left."일전";

                                        ?>
                                        <tr>
										  <td height="25" align="center">

                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="수당항목 관리">
                                            <a href="javascript:;" onClick="popAsWindow('<?= $scontract_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i>설정</a>
                                                </span>
										  
										  </td>
										  <td align="center" style="padding-left:5px;">
										  <?php
	// RD IN RF EX FN
										  if($scontract_details->sct_status == "RD") echo "계약대기";
										  else if($scontract_details->sct_status == "IN") echo "진행중";
										  else if($scontract_details->sct_status == "RF") echo "만료대기";
										  else if($scontract_details->sct_status == "EX") echo "계약연장";
										  else if($scontract_details->sct_status == "FN") echo "계약만료";
										  ?>
										  </td>
										  <td align="center" style="padding-left:5px;"><?//= $scontract_details->co_name  ?><?//= $scontract_details->s_co_id  ?></td>
										  <td align="center" style="padding-left:5px;"><?= $scontract_details->r_co  ?></td>
										  <td align="left" style="padding-left:5px;"><?= $scontract_details->tr_title  ?></td>

										  <td align="center" style="padding-left:5px;">
										  <?php
										  if($scontract_details->ct_type == "01") echo "단가 > 건당단가";
										  else if($scontract_details->ct_type == "02") echo "총액 > 건당단가";
										  else echo "기타";
										  ?>
										  </td>
										  <td align="center" style="padding-left:5px;"><?= $scontract_details->ct_sdate  ?></td>
										  <td align="center" style="padding-left:5px;"><?= $scontract_details->ct_edate  ?></td>
										  <td align="center" style="padding-left:5px;">
										  <?= $days_left  ?>
										  </td>
										  <td align="center" style="padding-left:5px;"><?= $scontract_details->reg_date  ?></td>
										  <td align="center" style="padding-left:5px;">
										  
                                            <?php if (!empty($edited)) { ?>

                                                <?php echo btn_edit('admin/contract/scontract_list/edit_scontract/' . $scontract_details->idx); ?>
                                            <?php }
                                            if (!empty($deleted)) { ?>
                                                <?php echo btn_delete('admin/contract/delete_scontract/' . $scontract_details->idx); ?>
                                            <?php } ?>
										  
										  </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_client"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/client/save_client/<?php
                              if (!empty($client_info)) {
                                  echo $client_info->client_id;
                              }
                              ?>" method="post" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-3"></label
                                <div class="col-sm-6">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_compnay"
                                                                  data-toggle="tab"><?= lang('general') ?></a>
                                            </li>
                                            <li><a href="#contact_compnay"
                                                   data-toggle="tab"><?= lang('client_contact') . ' ' . lang('details') ?></a>
                                            </li>
                                            <li><a href="#web_compnay" data-toggle="tab"><?= lang('web') ?></a></li>
                                            <li><a href="#hosting_compnay" data-toggle="tab"><?= lang('hosting') ?></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** general *************-->
                                            <div class="chart tab-pane active" id="general_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_name') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->name)) {
                                                                   echo $client_info->name;
                                                               }
                                                               ?>" name="name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_email') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="email" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->email)) {
                                                                   echo $client_info->email;
                                                               }
                                                               ?>" name="email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_vat') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->vat)) {
                                                            echo $client_info->vat;
                                                        }
                                                        ?>" name="vat">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('customer_group') ?></label>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <select name="customer_group_id"
                                                                    class="form-control select_box"
                                                                    style="width: 100%">
                                                                <?php
                                                                if (!empty($all_customer_group)) {
                                                                    foreach ($all_customer_group as $customer_group) : ?>
                                                                        <option
                                                                            value="<?= $customer_group->customer_group_id ?>"<?php
                                                                        if (!empty($client_info->customer_group_id) && $client_info->customer_group_id == $customer_group->customer_group_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $customer_group->customer_group; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                            <?php if (!empty($created)) { ?>
                                                                <div class="input-group-addon"
                                                                     title="<?= lang('new') . ' ' . lang('customer_group') ?>"
                                                                     data-toggle="tooltip" data-placement="top">
                                                                    <a data-toggle="modal" data-target="#myModal"
                                                                       href="<?= base_url() ?>admin/client/customer_group"><i
                                                                            class="fa fa-plus"></i></a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('language') ?></label>
                                                    <div class="col-sm-5">
                                                        <select name="language" class="form-control select_box"
                                                                style="width: 100%">
                                                            <?php

                                                            foreach ($languages as $lang) : ?>
                                                                <option
                                                                    value="<?= $lang->name ?>"<?php
                                                                if (!empty($client_info->language) && $client_info->language == $lang->name) {
                                                                    echo 'selected';
                                                                } elseif (empty($client_info->language) && $this->config->item('language') == $lang->name) {
                                                                    echo 'selected';
                                                                } ?>
                                                                ><?= ucfirst($lang->name) ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('currency') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="currency" class="form-control select_box"
                                                                style="width: 100%">

                                                            <?php if (!empty($currencies)): foreach ($currencies as $currency): ?>
                                                                <option
                                                                    value="<?= $currency->code ?>"
                                                                    <?php
                                                                    if (!empty($client_info->currency) && $client_info->currency == $currency->code) {
                                                                        echo 'selected';
                                                                    } elseif (empty($client_info->currency) && $this->config->item('default_currency') == $currency->code) {
                                                                        echo 'selected';
                                                                    } ?>
                                                                ><?= $currency->name ?></option>
                                                                <?php
                                                            endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('short_note') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="short_note"><?php
                                                if (!empty($client_info->short_note)) {
                                                    echo $client_info->short_note;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <?php
                                                if (!empty($client_info)) {
                                                    $client_id = $client_info->client_id;
                                                } else {
                                                    $client_id = null;
                                                }
                                                ?>
                                                <?= custom_form_Fields(12, $client_id); ?>
                                            </div><!-- ************** general *************-->

                                            <!-- ************** Contact *************-->
                                            <div class="chart tab-pane" id="contact_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_phone') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->phone)) {
                                                            echo $client_info->phone;
                                                        }
                                                        ?>" name="phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_mobile') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->mobile)) {
                                                                   echo $client_info->mobile;
                                                               }
                                                               ?>" name="mobile">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('zipcode') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->zipcode)) {
                                                            echo $client_info->zipcode;
                                                        }
                                                        ?>" name="zipcode">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_city') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->city)) {
                                                            echo $client_info->city;
                                                        }
                                                        ?>" name="city">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_country') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="country" class="form-control select_box"
                                                                style="width: 100%">
                                                            <optgroup label="Default Country">
                                                                <option
                                                                    value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                                            </optgroup>
                                                            <optgroup label="<?= lang('other_countries') ?>">
                                                                <?php if (!empty($countries)): foreach ($countries as $country): ?>
                                                                    <option
                                                                        value="<?= $country->value ?>" <?= (!empty($client_info->country) && $client_info->country == $country->value ? 'selected' : NULL) ?>><?= $country->value ?>
                                                                    </option>
                                                                    <?php
                                                                endforeach;
                                                                endif;
                                                                ?>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_fax') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->fax)) {
                                                            echo $client_info->fax;
                                                        }
                                                        ?>" name="fax">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_address') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="address"><?php
                                                if (!empty($client_info->address)) {
                                                    echo $client_info->address;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">
                                                        <a href="#"
                                                           onclick="fetch_lat_long_from_google_cprofile(); return false;"
                                                           data-toggle="tooltip"
                                                           data-title="<?php echo lang('fetch_from_google') . ' - ' . lang('customer_fetch_lat_lng_usage'); ?>"><i
                                                                id="gmaps-search-icon" class="fa fa-google"
                                                                aria-hidden="true"></i></a>
                                                        <?= lang('latitude') . '( ' . lang('google_map') . ' )' ?>
                                                    </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->latitude)) {
                                                            echo $client_info->latitude;
                                                        }
                                                        ?>" name="latitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('longitude') . '( ' . lang('google_map') . ' )' ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->longitude)) {
                                                                   echo $client_info->longitude;
                                                               }
                                                               ?>" name="longitude">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Contact *************-->
                                            <!-- ************** Web *************-->
                                            <div class="chart tab-pane" id="web_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_domain') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->website)) {
                                                            echo $client_info->website;
                                                        }
                                                        ?>" name="website">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('skype_id') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->skype_id)) {
                                                            echo $client_info->skype_id;
                                                        }
                                                        ?>" name="skype_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('facebook_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->facebook)) {
                                                            echo $client_info->facebook;
                                                        }
                                                        ?>" name="facebook">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('twitter_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->twitter)) {
                                                            echo $client_info->twitter;
                                                        }
                                                        ?>" name="twitter">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('linkedin_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->linkedin)) {
                                                            echo $client_info->linkedin;
                                                        }
                                                        ?>" name="linkedin">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Web *************-->
                                            <!-- ************** Hosting *************-->
                                            <div class="chart tab-pane" id="hosting_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hosting_company') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hosting_company)) {
                                                            echo $client_info->hosting_company;
                                                        }
                                                        ?>" name="hosting_company">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hostname') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hostname)) {
                                                            echo $client_info->hostname;
                                                        }
                                                        ?>" name="hostname">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('username') ?> </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->username)) {
                                                            echo $client_info->username;
                                                        }
                                                        ?>" name="username">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('password') ?></label>
                                                    <div class="col-lg-5">
                                                        <?php
                                                        if (!empty($client_info->password)) {
                                                            $password = strlen(decrypt($client_info->password));
                                                        }
                                                        ?>
                                                        <input type="password" name="password" value=""
                                                               placeholder="<?php
                                                               if (!empty($password)) {
                                                                   for ($p = 1; $p <= $password; $p++) {
                                                                       echo '*';
                                                                   }
                                                               }
                                                               ?>" class="form-control">
                                                        <strong id="show_password" class="required"></strong>
                                                    </div>
                                                    <?php if (!empty($client_info->password)) { ?>
                                                        <div class="col-lg-3">
                                                            <a data-toggle="modal" data-target="#myModal"
                                                               href="<?= base_url('admin/client/see_password/c_' . $client_info->client_id) ?>"
                                                               id="see_password"><?= lang('see_password') ?></a>
                                                            <strong id="hosting_password" class="required"></strong>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('port') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->port)) {
                                                            echo $client_info->port;
                                                        }
                                                        ?>" name="port">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Hosting *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                        </div>
                                        <div class="col-lg-3">
                                            <button type="submit" name="save_and_create_contact" value="1"
                                                    class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button>
                                        </div>

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>