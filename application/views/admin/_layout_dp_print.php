<?php
$lColor="364F9E";
$opened = $this->session->userdata('opened');
$this->session->unset_userdata('opened');
$time = date('h:i:s');
$r = display_time($time);
$time1 = explode(' ', $r);
if(empty($title)) $title = "위수탁청구서출력";
?>
<html>
<head>
<title><?= $title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<SCRIPT>
//	border: 1px solid #fff;
//	padding-right:5px;
		function Installed()
		{
			try
			{
				return (new ActiveXObject('IEPageSetupX.IEPageSetup'));
			}
			catch (e)
			{
				return false;
			}
		}

		function printMe()
		{
			if (Installed())
			{
				IEPageSetupX.header = "";
				IEPageSetupX.footer = "";
				IEPageSetupX.PrintBackground = true;
				IEPageSetupX.leftMargin   = "10";
				IEPageSetupX.rightMargin   = "10";
				IEPageSetupX.topMargin   = "10";
				IEPageSetupX.bottomMargin   = "10";
				IEPageSetupX.ShrinkToFit = true;
				IEPageSetupX.PaperSize  = "A4";
				IEPageSetupX.Preview();
			}
			else
				alert("컨트롤을 설치하지 않았네요.. 정상적으로 인쇄되지 않을 수 있습니다.");
		}

	</SCRIPT>

	<SCRIPT language="JavaScript" for="IEPageSetupX" event="OnError(ErrCode, ErrMsg)">
		alert('에러 코드: ' + ErrCode + "\n에러 메시지: " + ErrMsg);
	</SCRIPT>
	<?php if(!empty($dp_css)) echo $dp_css; ?>
</head>

<body topmargin="0" OnLoad="printMe();" OnUnload="if (Installed()) IEPageSetupX.RollBack();">

                    <?php echo $subview ?>


</body>
</html>
