<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">수납이력수정</h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form id="form_validation"
              action="<?php echo base_url() ?>admin/transactions/save_transactions/<?php if (!empty($log_info->idx)) echo $log_info->idx; ?>"
              method="post" class="form-horizontal form-groups-bordered">
									<input type="hidden" name="dp_id" value="<?php if (!empty($df_info->dp_id)) echo $df_info->dp_id; ?>">
									<input type="hidden" name="pid" value="<?php if (!empty($levy_info->idx)) echo $levy_info->idx; ?>">
									<input type="hidden" name="df_id" value="<?php if (!empty($df_info->df_id)) echo $df_info->df_id; ?>">
								  <input type="hidden" name="df_month" value="<?=$levy_info->df_month?>">
								  <input type="hidden" name="gn_amount" value="<?=$levy_info->gn_amount?>">
								  <input type="hidden" name="balance_amount" value="<?=$levy_info->balance_amount?>">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">부과년월</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                              <input type="text" class="form-control " value="<?php
                                            if (!empty($levy_info->df_month)) {
                                                echo $levy_info->df_month;
                                            }
                                            ?>" readonly>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">납부일자</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                            <input type="text" name="pay_date" class="form-control datepicker" value="<?php
                                            if (!empty($log_info->pay_date)) {
                                                echo $log_info->pay_date;
                                            } else {
                                                echo date('Y-m-d');
                                            }
                                            ?>" data-date-format="<?= config_item('date_picker_format'); ?>">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">수납금 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " data-parsley-type="number" style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($log_info->pay_amount)) {
                                                       echo $log_info->pay_amount;
                                                   }
                                                   ?>" name="pay_amount" required="" <?php
                                            if (!empty($levy_info)) {
                                               // echo 'disabled';
                                            }
                                            ?>>
                                        </div>
                                    </div>
                                </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">납부방법 </label>
                                        <div class="col-lg-5">
                                            <div class="input-group">
                                                <select class="form-control select_box" style="width: 100%"
                                                        name="payment_methods_id">
                                                    <option value="0">수납방법</option>
                                                    <?php
                                                    $payment_methods = $this->db->order_by('payment_methods_id', 'DESC')->get('tbl_payment_methods')->result();
                                                    if (!empty($payment_methods)) {
                                                        foreach ($payment_methods as $p_method) {
                                                            ?>
                                                            <option value="<?= $p_method->payment_methods_id ?>" <?php
                                                            if (!empty($log_info->payment_methods_id)) {
                                                                echo $log_info->payment_methods_id == $p_method->payment_methods_id ? 'selected' : '';
                                                            }
                                                            ?>><?= $p_method->method_name ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="input-group-addon"
                                                     title="<?= lang('new') . ' ' . lang('payment_method') ?>"
                                                     data-toggle="tooltip" data-placement="top">
                                                    <a data-toggle="modal" data-target="#myModal"
                                                       href="<?= base_url() ?>admin/settings/inline_payment_method"><i
                                                            class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">은행 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($log_info->bank_name)) {
                                                       echo $log_info->bank_name;
                                                   }
                                                   ?>" name="bank_name" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">예금주 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($log_info->bank_account_name)) {
                                                       echo $log_info->bank_account_name;
                                                   }
                                                   ?>" name="bank_account_name" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">계좌번호 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($log_info->bank_account_number)) {
                                                       echo $log_info->bank_account_number;
                                                   }
                                                   ?>" name="bank_account_number" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">의뢰인 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($log_info->bank_transfer_name)) {
                                                       echo $log_info->bank_transfer_name;
                                                   }
                                                   ?>" name="bank_transfer_name" required="">
                                        </div>
                                    </div>
                                </div>






                                <div class="form-group terms">
                                    <label class="col-lg-3 control-label">메모</label>
                                    <div class="col-lg-5">
                        <textarea name="remark" class="form-control"><?php
                            if (!empty($log_info->remark)) {
                                echo $log_info->remark;
                            }
                            ?></textarea>
                                    </div>
                                </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">레퍼런스 </label>
                                        <div class="col-lg-5">

                                            <input class="form-control " type="text" value="<?php
                                            if (!empty($log_info->reference)) {
                                                echo $log_info->reference;
                                            }
                                            ?>" name="reference">
                                        </div>
                                    </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>
