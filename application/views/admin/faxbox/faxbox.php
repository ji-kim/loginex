<div class="row">
    <div class="col-md-3">
        <div class="panel panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title"><?= "팩스함" ?>

                </h3>
            </div>

            <div class="panel-body ">
                <ul class="nav nav-pills nav-stacked">
                    <li class="<?php echo ($menu_active == 'inbox') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/faxbox/index/inbox"> <i class="fa fa-inbox"></i>
                            <?= "받은팩스함"?>

                        </a>
                    </li>
                    <li class="<?php echo ($menu_active == 'sent') ? 'active' : ''; ?>">
                        <a href="<?= base_url() ?>admin/faxbox/index/sent"> <i class="fa fa-envelope-o"></i>
                            <?= "보낸팩스함"?>
                        </a>
                    </li>

                </ul>
            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div><!-- /.col -->
    <div class="col-md-9">
        <?php echo message_box('success'); ?>
        <?php echo message_box('error'); ?>
        <?php $this->load->view('admin/faxbox/' . $view) ?>
    </div><!-- /.col -->
