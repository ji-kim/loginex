<div class="row">
    <div class="col-md-12">
        <form method="post" action="<?php echo base_url() ?>admin/mailbox/delete_mail/sent">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <div class="mailbox-controls">

                        <!-- Check all button -->
                        <div class="mail_checkbox mr-sm">
                            <input type="checkbox" id="parent_present">
                        </div>
                        <div class="btn-group">
                            <button class="btn btn-default btn-xs mr-sm"><i class="fa fa-trash-o"></i></button>
                        </div><!-- /.btn-group -->
                        <a href="#" onClick="history.go(0)" class="btn btn-default btn-xs mr-sm"><i
                                class="fa fa-refresh"></i></a>
                        <a href="<?php echo base_url() ?>admin/mailbox/index/compose"
                           class="btn btn-danger btn-xs mr-sm">Compose
                            +</a>
                    </div>
                </div>


                <div class="panel-body">
                    <div class="table-responsive mailbox-messages">
                        <table class="table DataTables ">
                            <thead>
                            <tr>
                                <td></td>
                                <th>발송일자</th>
                                <th>발송 대상 수</th>
                                <th>발송 총 파일</th>
                                <th>총 발송 문서 수</th>
                            </tr>
                            </thead>
                            <tbody style="font-size: 13px">
                            <?php if (!empty($faxdata)):foreach ($faxdata as $data): ?>
                                <tr>
                                    <td><input class="child_present" type="checkbox" name="selected_id[]"
                                               value="<?php echo $data->TR_BATCHID; ?>"/></td>
                                    <td>
                                        <a href="<?php echo base_url() ?>admin/faxbox/index/read_sent_fax/<?php echo $data->TR_BATCHID ?>"><?php

                                            echo $data->TR_SENDDATE;
                                            ?></a></td>
                                    <td><b class="pull-left"> <?php

                                            echo $data->TR_MSGCOUNT."명";
                                            ?>
                                    </td>
                                    <td><b class="pull-left"> <?php


                                            echo count(explode("|",$data->TR_DOCNAME))."개";

                                            ?>
                                    </td>
                                    <td><b class="pull-left"> <?php



                                            echo $data->TR_MSGCOUNT * count(explode("|",$data->TR_DOCNAME));
                                            echo "장";
                                            ?>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td><strong>보낸 팩스가 없습니다.</strong></td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table><!-- /.table -->
                    </div><!-- /.mail-box-messages -->
                </div><!-- /.box-body -->

            </div><!-- /. box -->
        </form>
    </div><!-- /.col -->
</div><!-- /.row -->
