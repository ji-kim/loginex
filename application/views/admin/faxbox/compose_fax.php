<form role="form" id="form" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/faxbox/send_fax/<?php

?>" method="post" class="form-horizontal form-groups-bordered">
    <!-- Content Header (Page header) -->
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group col-md-12">
                    <input class="form-control" value="" type="text" required="" name="TR_PHONE" placeholder="발송할 전화번호를 ,단위로 끊어 입력해주세요."/>
                </div>

                <div class="form-group col-md-12">
                    <div class="fileinput fileinput-new" data-provides="fileinput">


                        <span class="btn btn-default btn-file"><span class="fileinput-new"><i
                                    class="fa fa-paperclip"></i> <?= lang('attachment') ?></span>
                            <span class="fileinput-exists"><?= lang('change') ?></span>
                            <input type="file" name="attach_file">
                        </span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                           style="float: none;">&times;</a>

                        <p class="help-block">Max. 15 MB</p>
                    </div>
                    <div id="msg_pdf" style="color: #e11221"></div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="pull-right">

                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> <?= "발송" ?>
                    </button>
                </div>

            </div>
        </div><!-- /. box -->
    </div><!-- /.col -->
</form>
<link href="<?php echo base_url() ?>asset/css/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>asset/js/select2.js"></script>