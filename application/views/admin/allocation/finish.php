<?php include_once 'asset/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error');
$params['alloc_date'] = $alloc_date; //할당일
$params['is_view'] = "T";
$params['page'] = "1";
$params['branch_id'] = $branch_id;

$is_confirm = (!empty($work_status->is_confirm))?$work_status->is_confirm:"";
$is_register = (!empty($work_status->is_register))?$work_status->is_register:"";

$created = can_action('71', 'created');
$edited = can_action('71', 'edited');
$deleted = can_action('71', 'deleted');

?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/asset/car_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
					  <input type="hidden" name="action" id="action" value="<?php if(!empty($action)) echo $action;?>">

			<div class="col-sm-12 bg-white p0" >
				<div class="row ">
					<div class="col-sm-12" style="margin:10px">

						<div class="form-group" id="border-none">
							<div class="col-sm-2">
                                                <div class="actions pull-left margin-right-10">
                                                    <span class="input-group-btn">
                                                        <a href="javascript:goFilter('branch');" type="button" class='btn btn-<?=($action == "list_by_branch")?"warning":"default"?> grey-mint text-left <?=($action == "list_by_branch")?"active disabled' style='font-weight:bold;":""?>'>
                                                            지사별보기
                                                        </a>
														&nbsp;
                                                        <a href="javascript:goFilter('driver');" type="button" class='btn btn-<?=($action == "list_by_driver")?"warning":"default"?> grey-mint text-left <?=($action == "list_by_driver")?"active disabled' style='font-weight:bold;":""?>'>
                                                            기사별보기
                                                        </a>
                                                    </span>
                                                </div>
							</div>
							<div class="col-sm-2">
								<select class="form-control input-sm" name="branch_id" id="branch_id" onChange="goSearch();">
									<option value=''>지사선택</option>
									<?php
									$cur_branch = "전체지사 ";
									foreach ($all_branch_group as $branch_info):
										?>
											<option value='<?php echo $branch_info->dp_id ?>' <?=($branch_info->dp_id==$branch_id)?"selected":""?>>
												<?php 
												if(!empty($branch_info->co_name)) echo $branch_info->co_name; 
												if($branch_info->dp_id==$branch_id) $cur_branch = $branch_info->co_name;
												?> </option>
									<?php endforeach; ?>
								</select>
							</div>
<?php 
//기사별보기
if($action == "list_by_driver") { ?>
							<div class="col-sm-2">
								<select class="form-control input-sm" name="driver_id" id="driver_id" onChange="goSearch();">
									<option value=''>기사선택</option>
									<?php
									$cur_driver = "";
									if(!empty($all_driver_list)) {
										foreach ($all_driver_list as $driver_info):
										?>
											<option value='<?php echo $driver_info->dp_id ?>' <?=($driver_info->dp_id==$driver_id)?"selected":""?>>
												<?php 
												if(!empty($driver_info->co_name)) echo $driver_info->co_name; 
												if($driver_info->dp_id==$driver_id) $cur_driver = $driver_info->co_name;
												?> </option>
									<?php endforeach;
									}?>
								</select>
							</div>
<?php } ?>
							<div class="col-sm-3">
															<div class="input-group input-medium">
																<span class="input-group-btn">
																	<button class="btn blue" type="button" onclick="dateMove(-1);">
																		<i class="fa fa-angle-double-left"></i>
																	</button>
																</span>
																<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="alloc_date" id="alloc_date" value="<?=$alloc_date;?>">
																<span class="input-group-btn">
																	<button class="btn blue" type="button" onclick="dateMove(1);">
																		<i class="fa fa-angle-double-right"></i>
																	</button>
																</span>
																<span class="input-group-btn">
																	<button class="btn btn-primary" type="button" onclick="goSearch();">
																		<i class="fa fa-search"></i>
																	</button>
																</span>
															</div>
							</div>
<?php if($action == "list_by_branch") { ?>
							<div class="col-sm-2">
					  <input type="text" name="driver_id" id="driver_id" value="">
							</div>
<?php } ?>
							<div class="col-sm-3" style="padding-right:30px;">
                                                            <div class="input-group">
																<input type="search" value="<?//=$params['sc_key'];?>" name="sc_key" id="search_key" class="form-control" placeholder="Search">
                                                                <span class="input-group-btn">
																	<button class="btn btn-default" type="button" onclick="goSearch();">
                                                                        Search
                                                                        <i class="fa fa-search"></i>
                                                                    </button>
																	<button class="btn btn-warning" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-excel"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

<div class="col-sm-12 bg-white p0" >
	<div class="row ">
		<div class="col-sm-12" style="margin:10px">
			<div class="panel-title">
				 <strong><i class="fa fa-building"></i> <?=$cur_branch?> ( 소속기사 : <?=number_format($total_driver_count)?> 명) 배차완료</strong>
			</div>
		</div>

	</div>

	<div class="row ">
		<div class="col-sm-12">
                        <div class="box" style="padding:10px;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소속지사</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">검사원</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기사</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">현장명</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소재지</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">구분</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">수량</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실운반</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공동작업</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">중량</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종류</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비고</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 1;
		if(!empty($all_branch_list)) {
            foreach ($all_branch_list as $branch_info):

				//일정
				$params['br_id']		= $branch_info->dp_id;
//                $status = $this->allocation_model->get_work_status($params['alloc_date'], $params['br_id']); // 지사/날짜별 등록상태값 가져오기
//				$work_list = $this->allocation_model->work_list_all($params);

				// 지사정보 말풍선
				$br_info = "지사명 : ". $branch_info->co_name;
				$br_info .= " / 아이디 : ". $branch_info->username;
				$br_info .= " / 일정담당자 : ". $branch_info->ceo;
				$br_info .= " / 핸드폰 : ". $branch_info->ceo_hp;
				$br_info .= " / 연락처 : ". $branch_info->co_tel;               ?>
<?php
			if (!empty($work_list)) {
				foreach ($work_list as $work_details) {
					if(!empty($work_details->driver_id)) $driver_info = $this->allocation_model->get_driver_info_by_id($work_details->driver_id);
					$driver_name = (!empty($driver_info->driver_name))?$driver_info->driver_name:"";
							
					if(!empty($work_details->checker_id)) $checker_info = $this->allocation_model->get_checker_info_by_id($work_details->checker_id);
					$checker_name = (!empty($checker_info->checker_name))?$checker_info->checker_name:"";
					include "./application/views/admin/allocation/_scheduling_finish.inc.php"; //2
					$cnt++;
				}

			}

			
			endforeach; ?>
         <?php } else { ?>
                                <tr>
									<td colspan="16">
										<strong>등록된 데이터가 없습니다</strong>
									</td>
                                </tr>
         <?php } ?>
                            </tbody>
                        </table>
					</div>

    </div>
    </div>
</div>


<script type="text/javascript">
<!--
$(document).ready(function() {
});

function goSearch() {
	var alloc_date = $("#alloc_date").val();
	var action = $("#action").val();
	var driver_id = $("#driver_id").val();
	var branch_id = $("#branch_id option:selected").val();
	location.href = '<?php echo base_url() ?>admin/allocation/finish/'+alloc_date+'/' + action + '/'+branch_id+ '/'+driver_id;
}

function goFilter(mode) {
  if(mode == 'branch') $("#driver_id").val('');
  $("#action").val('list_by_'+mode);
  goSearch();
}

function goExcel() {
  $("#myform").attr("method", "get");
  $("#myform").attr("target", "hiddenframe");
  $("#myform").attr("action","./batcha_office_excel.php");
  $("#myform").submit();
}

function copy(txt)
{
    window.clipboardData.setData('Text', txt);
    alert("복사되었습니다.");
}


//-->
</script>

