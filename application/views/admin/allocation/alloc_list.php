<?php include_once 'asset/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error');
$params['alloc_date'] = $alloc_date; //할당일
$params['is_view'] = "T";
$params['page'] = "1";
$params['branch_id'] = $branch_id;

$is_confirm = (!empty($work_status->is_confirm))?$work_status->is_confirm:"";
$is_register = (!empty($work_status->is_register))?$work_status->is_register:"";

$created = can_action('71', 'created');
$edited = can_action('71', 'edited');
$deleted = can_action('71', 'deleted');

?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/asset/car_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
					  <input type="hidden" name="action" id="action" value="<?php if(!empty($action)) echo $action;?>">

			<div class="col-sm-12 bg-white p0" >
				<div class="row ">
					<div class="col-sm-12" style="margin:10px">

						<div class="form-group" id="border-none">
							<div class="col-sm-2">
                                                <div class="actions pull-left margin-right-10">
                                                    <span class="input-group-btn">
                                                        <a href="/pages/batcha/batcha_office_finish.php" type="button" class="btn btn-warning grey-mint text-left active disabled" style="font-weight:bold;">
                                                            지사별보기
                                                        </a>
														&nbsp;
                                                        <a href="/pages/batcha/batcha_gisa_finish.php" type="button" class="btn btn-default grey-mint text-left">
                                                            기사별보기
                                                        </a>
                                                    </span>
                                                </div>
							</div>
							<div class="col-sm-2">
								<select class="form-control input-sm" name="branch_id" id="branch_id" onChange="goSearch();">
									<?php
									$cur_branch = "전체지사 ";
									foreach ($all_branch_group as $branch_info):
										?>
											<option value='<?php echo $branch_info->dp_id ?>' <?=($branch_info->dp_id==$branch_id)?"selected":""?>>
												<?php 
												if(!empty($branch_info->co_name)) echo $branch_info->co_name; 
												if($branch_info->dp_id==$branch_id) $cur_branch = $branch_info->co_name;
												?> </option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-3">
															<div class="input-group input-medium">
																<span class="input-group-btn">
																	<button class="btn blue" type="button" onclick="dateMove(-1);">
																		<i class="fa fa-angle-double-left"></i>
																	</button>
																</span>
																<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="alloc_date" id="alloc_date" value="<?=$alloc_date;?>">
																<span class="input-group-btn">
																	<button class="btn blue" type="button" onclick="dateMove(1);">
																		<i class="fa fa-angle-double-right"></i>
																	</button>
																</span>
																<span class="input-group-btn">
																	<button class="btn btn-primary" type="button" onclick="goSearch();">
																		<i class="fa fa-search"></i>
																	</button>
																</span>
															</div>
							</div>
							<div class="col-sm-2">
							</div>
							<div class="col-sm-3" style="padding-right:30px;">
                                                            <div class="input-group">
																<input type="search" value="<?//=$params['sc_key'];?>" name="sc_key" id="search_key" class="form-control" placeholder="Search">
                                                                <span class="input-group-btn">
																	<button class="btn btn-default" type="button" onclick="goSearch();">
                                                                        Search
                                                                        <i class="fa fa-search"></i>
                                                                    </button>
																	<button class="btn btn-default" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-excel"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

<div class="col-sm-12 bg-white p0" >
	<div class="row ">
		<div class="col-sm-12" style="margin:10px">
			<div class="panel-title">
				 <strong><i class="fa fa-building"></i> <?=$cur_branch?> ( 소속기사 : <?=number_format($total_driver_count)?> 명) 배차등록</strong>
			</div>
				<a href="<?php echo base_url(); ?>admin/allocation/finish/<?=$alloc_date?>/add_schedule/<?=$branch_id?>" class="dt-button buttons-print btn btn-warning btn-xs mr"><i class="fa fa-plus"></i> 일정추가 </a>
				<?php if($is_register==0 && $is_confirm==0) { ?>
					<a href="<?php echo base_url(); ?>admin/allocation/finish/<?=$alloc_date?>/finish/<?=$branch_id?>" class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-link"></i> 등록완료 </a>
				<?php	} ?>
				<?php if($is_register==1 && $is_confirm==0) { ?>
					<a href="<?php echo base_url(); ?>admin/allocation/finish/<?=$alloc_date?>/cancel/<?=$branch_id?>" class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-link"></i> 등록취소 </a>
				<?php	} ?>
				<?php if($is_register==1 && $is_confirm==0) { ?>
					<a href="<?php echo base_url(); ?>admin/allocation/finish/<?=$alloc_date?>/confirm/<?=$branch_id?>" class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-link"></i> 확정 </a>
				<?php	} ?>
				<?php if($is_register==1 && $is_confirm==1) { ?>
					<a href="<?php echo base_url(); ?>admin/allocation/finish/<?=$alloc_date?>/confirm_cancel/<?=$branch_id?>" class="dt-button buttons-print btn btn-success btn-xs mr"><i class="fa fa-link"></i> 확정취소 </a>
				<?php	} ?>
			( * <span class="<?=($is_register==0 && count($work_list) == 0)?"label label-default":""?>">1. 일정추가</span> 
			> <span class="<?=($is_register==0 && count($work_list) > 0)?"label label-default":""?>">2.일정등록</span> > 
			<span class="<?=($is_register==1 && $is_confirm==0)?"label label-default":""?>">3.등록완료</span> > 
			<span class="<?=($is_confirm==1)?"label label-default":""?>">4.확정</span> > 
			<span class="<?=($is_confirm==1)?"label label-default":""?>">5.기사확인</span> )
		</div>

	</div>

	<div class="row ">
		<div class="col-sm-12">
                        <div class="box" style="padding:10px;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
							  <!--td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소속지사</td-->
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">검사원</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기사</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">현장명</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소재지</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">구분</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">수량</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실운반</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공동작업</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">중량</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종류</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비고</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">작업</td>
							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 1;
		if(!empty($all_branch_list)) {
            foreach ($all_branch_list as $branch_info):

				//일정
				$params['br_id']		= $branch_info->dp_id;

				// 지사정보 말풍선
				$br_info = "지사명 : ". $branch_info->co_name;
				$br_info .= " / 아이디 : ". $branch_info->username;
				$br_info .= " / 일정담당자 : ". $branch_info->ceo;
				$br_info .= " / 핸드폰 : ". $branch_info->ceo_hp;
				$br_info .= " / 연락처 : ". $branch_info->co_tel;               ?>
<?php
			if (!empty($work_list)) {
				foreach ($work_list as $work_details) {
					$is_moved = (!empty($work_details->is_moved))?$work_details->is_moved:"";
					$move_to = (!empty($work_details->move_to))?$work_details->move_to:"";
					$is_confirm = (!empty($work_details->is_confirm))?$work_details->is_confirm:"";
					$is_register = (!empty($work_details->is_register))?$work_details->is_register:"";

					if($is_moved==1 && $move_to) { // 작업이동
						include "./application/views/admin/allocation/_scheduling_regist_all_moved.inc.php"; //3
					} else {
						if($is_register=='1' || $is_confirm=='1') { //---------- 등록완료
							if(!empty($work_details->driver_id)) $driver_info = $this->allocation_model->get_driver_info_by_id($work_details->driver_id);
							$driver_name = (!empty($driver_info->driver_name))?$driver_info->driver_name:"";
							
							if(!empty($work_details->checker_id)) $checker_info = $this->allocation_model->get_checker_info_by_id($work_details->checker_id);
							$checker_name = (!empty($checker_info->checker_name))?$checker_info->checker_name:"";
							

							include "./application/views/admin/allocation/_scheduling_view.inc.php"; //2
						} else   
							include "./application/views/admin/allocation/_scheduling_regist.inc.php"; //1
					}
					$cnt++;
				}

			}

			
			endforeach; ?>
         <?php } else { ?>
                                <tr>
									<td colspan="16">
										<strong>등록된 데이터가 없습니다</strong>
									</td>
                                </tr>
         <?php } ?>
                            </tbody>
                        </table>
					</div>

    </div>
    </div>
</div>

<form method="post" name="myhform" id="myhform" action="<?php echo base_url() ?>admin/allocation/finish/<?php echo $alloc_date ?>/update_field">
	<input type="hidden" name="swork_id" id="swork_id"/>
	<input type="hidden" name="sfield" id="sfield"/>
	<input type="hidden" name="svalue" id="svalue"/>
</form>


<script type="text/javascript">
<!--
$(document).ready(function() {
});

function goSearch() {
	var alloc_date = $("#alloc_date").val();
	var branch_id = $("#branch_id option:selected").val();
	location.href = '<?php echo base_url() ?>admin/allocation/finish/'+alloc_date+'/<?php echo $action ?>/'+branch_id;
}

// ajax 오류로 방식변경 향 후 수정 요망 , 한글깨지는 문제 추가 발생
function update_field_tmp(field, work_id, field_value) {
	alert(field_value);
	document.all.hiddenframe.src = '<?php echo base_url() ?>admin/allocation/finish/0/update_field/'+field+'/'+field_value+'/'+work_id;
}

function update_field(field,work_id,field_value) {
  $("#mode").val("UPDATE_FIELD");
  $("#swork_id").val(work_id);
  $("#sfield").val(field);
  $("#svalue").val(field_value);

//  $("#checker_id0").css('background-color','yellow');

  $("#myhform").attr("method", "POST");
  $("#myhform").attr("target", "hiddenframe");
  $("#myhform").submit();
}


function goList(page) {
  $("#page").val(page);
  $("#myform").attr("method", "get");
  $("#myform").attr("target", "_self");
  $("#myform").attr("action","./batcha_office.php");
  $("#myform").submit();
}

function goFilter(br_id) {
  $("#br_id").val(br_id);
  $("#myform").attr("method", "get");
  $("#myform").attr("target", "_self");
  $("#myform").attr("action","./batcha_office.php");
  $("#myform").submit();
}

function goExcel() {
  $("#myform").attr("method", "get");
  $("#myform").attr("target", "hiddenframe");
  $("#myform").attr("action","./batcha_office_excel.php");
  $("#myform").submit();
}

function set_schedule(mode,br_id,br_name,wk_id)
{
  $("#mode").val(mode);
  $("#wk_id").val(wk_id);
  $("#br_id").val(br_id);
  $("#myform").attr("method", "POST");
  $("#myform").attr("target", "hiddenframe");
  $("#myform").attr("action", "./batcha_update.php");
  $("#myform").submit();
}

function delete_work(wk_id) {
  if(confirm("삭제하시겠습니까?")) {
	  $("#mode").val("DELETE");
	  $("#wk_id").val(wk_id);
	  $("#myform").attr("method", "POST");
	  $("#myform").attr("target", "hiddenframe");
	  $("#myform").attr("action", "./batcha_update.php");
	  $("#myform").submit();
	}
}

function diver_confirm(mode,wk_id) {
  $("#mode").val(mode);
  $("#wk_id").val(wk_id);
  $("#myform").attr("method", "POST");
  $("#myform").attr("target", "hiddenframe");
  $("#myform").attr("action", "./batcha_update.php");
  $("#myform").submit();
}

function copy(txt)
{
    window.clipboardData.setData('Text', txt);
    alert("복사되었습니다.");
}


//-->
</script>

<iframe width=0 height=0 name='hiddenframe' style='display:none;'></iframe>
