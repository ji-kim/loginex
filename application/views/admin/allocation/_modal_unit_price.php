<?php
?>
<div class="panel panel-custom" style="padding:10px;">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 단가 전체 적용</h4>
    </div>
    <div class="modal-body wrap-modal wrap">

            <div class="form-group" id="border-none">
			* 기존설정단가도 변경 적용됩니다.<br/>
            </div>

        <form id="form_validation"
              action="<?php echo base_url() ?>admin/allocation/save_unit_price/" method="post" class="form-horizontal form-groups-bordered">
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">2,500Kg 미만 </label>
                <div class="col-sm-8">
                    <input type="text" value="0" class="form-control" name="not_paid" style="text-align:right;">
                </div>
            </div>

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">2,500Kg~3,000Kg미만 </label>
                <div class="col-sm-8">
                    <input type="text" value="0" class="form-control" name="not_paid" style="text-align:right;">
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">3,000Kg~5,000Kg미만</label>
                <div class="col-sm-8">
                    <input type="text" value="0" class="form-control" name="not_paid" style="text-align:right;">
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">공동작업 </label>
                <div class="col-sm-8">
                    <input type="text" value="0" class="form-control" name="not_paid" style="text-align:right;">
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">야간/공휴일 </label>
                <div class="col-sm-8">
                    <input type="text" value="0" class="form-control" name="not_paid" style="text-align:right;">
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">용차</label>
                <div class="col-sm-8">
                    <input type="text" value="0" class="form-control" name="not_paid" style="text-align:right;">
                </div>
            </div>

			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
