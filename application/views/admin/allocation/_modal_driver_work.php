<?php
?>
<div class="panel panel-custom" style="padding:10px;">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 기사 작업 등록</h4>
    </div>
    <div class="modal-body wrap-modal wrap">

            <div class="form-group" id="border-none">
			* 기존설정단가도 변경 적용됩니다.<br/>
            </div>

        <form id="form_validation"
              action="<?php echo base_url() ?>admin/allocation/save_driver_work/<?=$alloc_date?>/<?=$branch_id?>/<?= $work_id ?>" method="post" class="form-horizontal form-groups-bordered">

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">기사</label>
                <div class="col-sm-8">
                    <?=$driver_info->co_name?>
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">요청수량</label>
                <div class="col-sm-8">
                    <?=(!empty($work_info->req_cnt))?$work_info->req_cnt:""?>
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">공동운반</label>
                <div class="col-sm-8">
                    <?=(!empty($work_info->real_cnt))?$work_info->real_cnt:""?>
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">실운반</label>
                <div class="col-sm-8">
                    <input type="text" value="<?=(!empty($work_info->real_cnt))?$work_info->real_cnt:""?>" class="form-control" name="real_cnt" style="text-align:right;">
                </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-4 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">작업인원</label>
                <div class="col-sm-8">
                    <?=(!empty($work_info->worker_cnt))?$work_info->worker_cnt:"0"?> 
                </div>
            </div>

			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
