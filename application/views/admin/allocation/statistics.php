<?php include_once 'asset/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error');

$created = can_action('71', 'created');
$edited = can_action('71', 'edited');
$deleted = can_action('71', 'deleted');

$lastday=array(0,31,28,31,30,31,30,31,31,30,31,30,31);
$yoil = array("일","월","화","수","목","금","토");
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/asset/car_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
					  <input type="hidden" name="action" id="action" value="<?php if(!empty($action)) echo $action;?>">

			<div class="col-sm-12 bg-white p0" >
				<div class="row ">
					<div class="col-sm-12" style="margin:10px">

						<div class="form-group" id="border-none">
							<div class="col-sm-2">
                                                <div class="actions pull-left margin-right-10">
                                                    <span class="input-group-btn">
                                                        <a href="javascript:goFilter('real');" type="button" class='btn btn-<?=($action == "list_by_real")?"warning":"default"?> grey-mint text-left <?=($action == "list_by_real")?"active disabled' style='font-weight:bold;":""?>'>
                                                            실수량기준
                                                        </a>
														&nbsp;
                                                        <a href="javascript:goFilter('req');" type="button" class='btn btn-<?=($action == "list_by_req")?"warning":"default"?> grey-mint text-left <?=($action == "list_by_req")?"active disabled' style='font-weight:bold;":""?>'>
                                                            요청수량기준
                                                        </a>
                                                    </span>
                                                </div>
							</div>
							<div class="col-sm-4">
							</div>
							<div class="col-sm-6" style="padding-right:30px;">
                                                            <div class="input-group">
																<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="date_from" id="date_from" value="<?=$date_from;?>">
																<span class="input-group-btn">
																	<button class="btn blue" type="button">
																		<i class="fa fa-angle-right"></i>
																	</button>
																</span>
																<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="date_to" id="date_to" value="<?=$date_to;?>">
                                                                <span class="input-group-btn">
																	<button class="btn btn-default" type="button" onclick="goSearch();">
                                                                        Search
                                                                        <i class="fa fa-search"></i>
                                                                    </button>
																	<button class="btn btn-warning" type="button" onclick="goExcel();">
                                                                        EXCEL 저장
                                                                        <i class="fa fa-excel"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

<div class="col-sm-12 bg-white p0" >
	<div class="row ">
		<div class="col-sm-12" style="margin:10px">
			<div class="panel-title">
				 <strong><i class="fa fa-building"></i> 기간 : <?=$date_from?> ~ <?=$date_to?> </strong>
			</div>
		</div>

	</div>

	<div class="row ">
		<div class="col-sm-12">
                        <div class="box" style="padding:10px;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td height="20" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">No</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지사</td>
								<?php
								$sub_tot = array();
								$cur_day = $date_from;
								$j = $br_total['tot'] = $br_total['holiday'] = $br_total['week'] = $br_total['night'] = 0;
								while (1) {
									$sub_tot[$j] = 0;
									$d_date = $cur_day;
									$d_yoil = $yoil[date('w', strtotime($d_date))];

									$check_date = $this->allocation_model->holiday_check($d_date);
									if(!empty($check_date->holiday_id)) $d_yoil = "공";
									if($d_yoil == "토" || $d_yoil == "일" || $d_yoil == "공") $css = "color:yellow;"; else $css = "";
									echo "<td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;{$css}'>".substr($d_date,5,5)."({$d_yoil})</td>";
									if($cur_day == $date_to) break;
									$cur_day = date("Y-m-d",strtotime($d_date.'+1day')); 
									$j++;
								}
								$sub_tot[$j] = 0;
								$j++;
								$sub_tot[$j] = 0;
								$j++;
								$sub_tot[$j] = 0;
								$j++;
								$sub_tot[$j] = 0;
								$j++;
								$sub_tot[$j] = 0;
								?>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">평일</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">야간</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공휴일</td>
							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 1;
		if(!empty($all_branch_group)) {
            foreach ($all_branch_group as $branch_info):
				// 지사정보 말풍선
				$br_info = "지사명 : ". $branch_info->co_name;
				$br_info .= " / 아이디 : ". $branch_info->username;
				$br_info .= " / 일정담당자 : ". $branch_info->ceo;
				$br_info .= " / 핸드폰 : ". $branch_info->ceo_hp;
				$br_info .= " / 연락처 : ". $branch_info->co_tel;               
?>
					<tr>
									<td style="text-align:right;">
										<?=$cnt;?>
									</td>
									<td style="text-align:center;">
										<?=$branch_info->co_name;?>
									</td>
<?php
	$cur_day = $date_from;
	$j = 0;
	while (1) {
			$d_date = $cur_day;
			$d_yoil = $yoil[date('w', strtotime($d_date))];
			$bg = "#ffffff";
			$stype = ($action == "list_by_real")?"real_cnt":"work_cnt";
			$work_cnt = $this->allocation_model->get_work_count_by_branch($branch_info->dp_id, $d_date, $stype);

			$work_cnt['sum_day'] = (!empty($work_cnt->sum_day))?$work_cnt->sum_day:0;
			$work_cnt['sum_night'] = (!empty($work_cnt->sum_night))?$work_cnt->sum_night:0;
			$work_cnt['sum_nthr'] = (!empty($work_cnt->sum_nthr))?$work_cnt->sum_nthr:0;
			
			$work_count = $work_cnt['sum_day'] + $work_cnt['sum_night'] + $work_cnt['sum_nthr'];
			$sub_tot[$j] += $work_count;
			$br_total['tot'] += $work_count;

			//공휴일 체크
			$check_date =$this->allocation_model->holiday_check($d_date);
			if($check_date) $d_yoil = "공";
			if($d_yoil == "토" || $d_yoil == "일" || $d_yoil == "공")
				$br_total['holiday'] += $work_count;
			else {
				$br_total['week'] += $work_cnt['sum_day'] + $work_cnt['sum_nthr']; //주간야간 선택안된건 주간에 포함
				$br_total['night'] += $work_cnt['sum_night'];
			}

			if($d_yoil == "토" || $d_yoil == "일" || $d_yoil == "공") $css = " style='color:red;'"; else $css = "";

			echo "<td {$css}>".$work_count."</td>";
			if($cur_day == $date_to) break;
			$cur_day = date("Y-m-d",strtotime($d_date.'+1day')); 
			$j++;
		}
		$j++; $sub_tot[$j] += $br_total['tot'];
		$j++; $sub_tot[$j] += $br_total['week'];
		$j++; $sub_tot[$j] += $br_total['night'];
		$j++; $sub_tot[$j] += $br_total['holiday'];

		//마감처리
		if(0) { //$params['mode'] == "CL") {
			/*
			$uprice = $cs_uprice->uprice_view();
				 
			$params['driver_id'] = "";
			$params['br_id'] = $row['idx'];

			$params['uprice_week'] = $uprice['uprice_week'];
			$params['uprice_night'] = $uprice['uprice_night'];
			$params['uprice_holiday'] = $uprice['uprice_holiday'];
			$params['ea_week'] = $br_total['week'];
			$params['ea_night'] = $br_total['night'];
			$params['ea_holiday'] = $br_total['holiday'];
			$params['amount_week'] = $br_total['week'] * $params['uprice_week'];
			$params['amount_night'] = $br_total['night'] * $params['uprice_night'];
			$params['amount_holiday'] = $br_total['holiday'] * $params['uprice_holiday'];

			//check exist
			$balance = $cs_balance->balance_check($params);
			if($balance['idx']) {
				$params['idx'] = $balance['idx'];
				$cs_balance->balance_update($params);
			} else {
				$cs_balance->balance_insert($params);
			}
			*/
		}

?>
																		<td><?=number_format($br_total['tot'])?></td>
																		<td><?=number_format($br_total['week'])?></td>
																		<td><?=number_format($br_total['night'])?></td>
																		<td><?=number_format($br_total['holiday'])?></td>
																	</tr>
<?php
?>					</tr>
<?php
					
					$cnt++;
			
			endforeach; ?>
																<tfoot>
																	<tr role="row">
																		<td colspan="2">합계</td>	
<?php
	$cur_day = $date_from;
	$j=0;
	while (1) {
		$d_date = $cur_day;
		echo "<td>".$sub_tot[$j]."</td>";
		if($cur_day == $date_to) break;
		$cur_day = date("Y-m-d",strtotime($d_date.'+1day')); 
		$j++;
	}
		$j++;echo "<td>".$sub_tot[$j]."</td>";
		$j++;echo "<td>".$sub_tot[$j]."</td>";
		$j++;echo "<td>".$sub_tot[$j]."</td>";
		$j++;echo "<td>".$sub_tot[$j]."</td>";
?>
																	</tr>
																</tfoot>
         <?php } else { ?>
                                <tr>
									<td colspan="16">
										<strong>등록된 데이터가 없습니다</strong>
									</td>
                                </tr>
         <?php } ?>
                            </tbody>
                        </table>
					</div>

    </div>
    </div>
</div>


<script type="text/javascript">
<!--
$(document).ready(function() {
});

function goSearch() {
	var date_from = $("#date_from").val();
	var date_to = $("#date_to").val();
	var action = $("#action").val();
	var branch_id = $("#branch_id option:selected").val();
	location.href = '<?php echo base_url() ?>admin/allocation/statistics/'+date_from+'/'+date_to+'/' + action;
}

function goFilter(mode) {
  $("#action").val('list_by_'+mode);
  goSearch();
}

function goExcel() {
  $("#myform").attr("method", "get");
  $("#myform").attr("target", "hiddenframe");
  $("#myform").attr("action","./batcha_office_excel.php");
  $("#myform").submit();
}

function copy(txt)
{
    window.clipboardData.setData('Text', txt);
    alert("복사되었습니다.");
}


//-->
</script>

