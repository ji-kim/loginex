<?php include_once 'asset/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error');
$created = can_action('71', 'created');
$edited = can_action('71', 'edited');
$deleted = can_action('71', 'deleted');
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<div class="row">
    <!--div class="col-sm-3">
        <form id="existing_customer" action="<?php echo base_url() ?>admin/settings/holiday_list" method="post">
            <label for="field-1" class="control-label pull-left holiday-vertical"><strong>검색</strong></label>
            <div class="col-sm-8">
                <input type="text" name="year" class="form-control years" value="<?php
                if (!empty($year)) {
                    echo $year;
                }
                ?>" data-format="yyyy">
            </div>
            <button type="submit" id="search_product" data-toggle="tooltip" data-placement="top" title="Search"
                    class="btn btn-purple pull-right">
                <i class="fa fa-search"></i></button>
        </form>
    </div-->
    <?php if (!empty($created)) { ?>
        <div class="col-sm-12 mt">&nbsp;&nbsp;&nbsp;
            <a href="<?= base_url() ?>admin/allocation/set_unit_price" class="text-danger" data-toggle="modal"
               data-placement="top" data-target="#myModal"><span
                    class="fa fa-plus "> 단가 전체적용 </span></a>
        </div>
    <?php } ?>
</div>
<div class="row ">
    <div class="col-md-2">
        <ul class="mt nav nav-pills nav-stacked navbar-custom-nav">
            <?php
			//$cnt = 0;
			$cur_branch = "";
            foreach ($all_branch_list as $branch_info):
				//if($cnt == 0 && empty($branch_id)) $branch_id = $branch_info->dp_id;
				//$cnt++;
                ?>
                <li class="<?php
                if ($branch_id == $branch_info->dp_id) {
					$cur_branch = $branch_info->co_name;
                    echo 'active';
                }
                ?>">
                    <a href="<?= base_url() ?>admin/allocation/config/<?php echo $branch_info->dp_id ?>">
                        <i class="fa fa-fw fa-building"></i>
						<?php 
						if(!empty($branch_info->co_name)) echo $branch_info->co_name; 
						?> </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="tab-content pl0">

                    <div class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><i class="fa fa-building"></i> <?=$cur_branch?> ( 총 <?=number_format($total_driver_count)?> 명) 작업단가 적용</strong>
                            </div>

                        </div>

                        <div class="box" style="padding:10px;">
						<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                            <thead>
							<tr align="center" bgcolor="#e0e7ef">
							  <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>기사</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">평일</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">야간<br/>공휴일</td>

							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>용차</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>비고</td>
							</tr>
																				

							<tr align="center" bgcolor="#e0e7ef">
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">2,500Kg 미만</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">2,500Kg~3,000Kg미만</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">3,000Kg~5,000Kg미만</td>
							  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공동작업</td>
							</tr>

                            </thead>
                            <tbody>
            <?php
		$cnt = 0;
		if(!empty($all_driver_list)) {
            foreach ($all_driver_list as $driver_info):
				$cnt ++; 
				//단가정보
				$uprice1 = $uprice2 = $uprice3 = $uprice_cowork = $uprice_holiday = $uprice_yongcha = $remark = 0;
				$uprice = $this->db->where('driver_id', $driver_info->dp_id)->get('tbl_unit_price')->row();
				if(!empty($uprice->idx)) {
					$uprice1 = $uprice->uprice1;
					$uprice2 = $uprice->uprice2;
					$uprice3 = $uprice->uprice3;
					$uprice_cowork = $uprice->uprice_cowork;
					$uprice_holiday = $uprice->uprice_holiday;
					$uprice_yongcha = $uprice->uprice_yongcha;
					$remark = $uprice->remark;
				}
                ?>
                                <tr>
                                    <td><?php echo $cnt; ?></td>
                                    <td><?php echo $driver_info->ceo; ?></td>
									<td><input type="text" onBlur="update_field('uprice1','<?=$driver_info->dp_id?>',this.value);" class="form-control" name="uprice1" id="uprice1" value='<?=$uprice1?>' style="width:100%;"/></td>
									<td><input type="text" onBlur="update_field('uprice2','<?=$driver_info->dp_id?>',this.value);" class="form-control" name="uprice2" id="uprice2" value='<?=$uprice2?>' style="width:100%;"/></td>
									<td><input type="text" onBlur="update_field('uprice3','<?=$driver_info->dp_id?>',this.value);" class="form-control" name="uprice3" id="uprice3" value='<?=$uprice3?>' style="width:100%;"/></td>
									<td><input type="text" onBlur="update_field('uprice_cowork','<?=$driver_info->dp_id?>',this.value);" class="form-control" name="uprice_cowork" id="uprice_cowork" value='<?=$uprice_cowork?>' style="width:100%;"/></td>
									<td><input type="text" onBlur="update_field('uprice_holiday','<?=$driver_info->dp_id?>',this.value);" class="form-control" name="uprice_holiday" id="uprice_holiday" value='<?=$uprice_holiday?>' style="width:100%;"/></td>
									<td><input type="text" onBlur="update_field('uprice_yongcha','<?=$driver_info->dp_id?>',this.value);" class="form-control" name="uprice_yongcha" id="uprice_yongcha" value='<?=$uprice_yongcha?>' style="width:100%;"/></td>
									<td><input type="text" onBlur="update_field('remark','<?=$driver_info->dp_id?>',this.value);" class="form-control" name="remark" id="remark" value='<?=$remark?>' style="width:100%;"/></td>
                                </tr>
            <?php endforeach; ?>
         <?php } else { ?>
                                <tr>
									<td colspan="6">
										<strong>등록된 데이터가 없습니다</strong>
									</td>
                                </tr>
         <?php } ?>
                            </tbody>
                        </table>
					</div>


                </div>
        </div>
    </div>
</div>