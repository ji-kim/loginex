<div class="allocWrap">
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">배차현황</h2>		
	<section class="topCommon">	
		<!--상단 및 타이틀 box-->
		<div class="box">
			<!-- 폰트 레드컬러로 변경시  boxText css-->
			<div class="boxTextNavy"><?= $work_date ?></div>			
   		</div>   		
	</section>
	<!--상단 및 타이틀 box-->	
	
<?php
$cnt = 0;
if (!empty($work_list)) {
	foreach ($work_list as $work_details) {
		$cnt ++;
		if(!empty($work_details->checker_id)) $checker_info = $this->allocation_model->get_checker_info_by_id($work_details->checker_id);
		$checker_name = (!empty($checker_info->checker_name))?$checker_info->checker_name:"";
?>
	<!--01 배차정보-->
	<section class="dispatch">		
		<!--테이블 1칸-->
		<p class="title"><span class="subTitle"><?= sprintf("%02d", $cnt)?></span><?=$work_details->work_from?> ~ <?=$work_details->work_to?> <?=$work_details->branch_name?></p>
		<div class="table table-cols">
			<div class="table-cell">[<?=$work_details->work_place?>] <?=$work_details->address?></div>
			<div class="table-cell"><span>수량 :　</span><?=$work_details->work_cnt?><span>　중량 :　</span><?=$work_details->work_weight?><span>　종류 :　</span>
				<?php
					if($work_details->car_ton=="0") echo "";
					else if($work_details->car_ton=="1") echo "설치";
					else if($work_details->car_ton=="2") echo "정밀";
					else if($work_details->car_ton=="3") echo "수시";
				?>
			</div>
			<div class="table-cell"><span>검사원 :　</span><?php echo $checker_name; ?>　010-7337-3827<br/>
				<button class="navyBg">검사원 전화연결</button>
				<button class="redBg">네비게이션</button>
			</div>
		</div>
		
		<!--테이블 4칸-->
		<div class="table table-4cols">			
			<div class="table-cell tableBTop">구분</div>
			<div class="table-cell tableBTop">공동운반</div>
			<div class="table-cell tableBTop">실운반</div>
			<div class="table-cell tableBTop">작업인원</div>
			
			<div class="table-cell center">
				<?php
					if($work_details->work_type=="D") echo "주간";
					else if($work_details->work_type=="N") echo "야간";
				?>
			</div>
			<div class="table-cell center"><?=$work_details->cowork_cnt?></div>
			<div class="table-cell center"><?=$work_details->real_cnt?></div>
			<div class="table-cell center"><?=$work_details->worker_cnt?>명</div>			
		</div>
				
		<div class="sign">
			<h3 class="signTit">서명</h3>
			<div class="boxWrap">
				<p class="box">
					
				</p>
			</div>
		</div>    
	</section>
	<!--//01 배차정보-->
	
	<p class="line"></p>
<?php
	} 
} else {
?>
	<section class="dispatch">		
		<div class="sign">
			<h3 class="signTit">배차 일정이 없습니다.</h3>
		</div>    
	</section>

<?php
} 
?>
	
	
</div> <!--//allocWrap-->
