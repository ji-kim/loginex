<div class="instWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle type">보험 & 검사정보</h2>		
	<section class="topCommon">	
		<!--상단 및 타이틀 box-->
   		
		<!--보험정보-->
		<div class="infoDetail mnone">   		
			<h2 class="infoTit">보험정보</h2>	
			<!-- div 테이블 변경시 table-2cols table-4cols table-5cols -->
			<div class="table table-2cols">			
				<div class="cell40 tableBTop">소속</div>
				<div class="cell60 tableBTop">(주)아이디일일구닷컴</div>

				<div class="cell40">보험사</div>
				<div class="cell60">동부화재</div>
				
				<div class="cell40">분납횟수</div>
				<div class="cell60"><?=(!empty($ins['pay_cnt']))?$ins['pay_cnt']:""?>회</div>
				
				<div class="cell40">가입일</div>
				<div class="cell60"><?=(!empty($ins['start_date']))?$ins['start_date']:""?></div>
				
				<div class="cell40">만기일</div>
				<div class="cell60">2019.09.01</div>
				
				<div class="cell40">가입금액</div>
				<div class="cell60">1,381,510</div>
			</div>
			<!--//계약상세정보-->
			<div class="commonBtn">
				<button type="button" class="navyBg" onClick="location.href='http://delex.delta-on.com/uploads/car_ins/%EA%B0%95%EC%9B%9089%EB%B0%943021_%EB%B3%B4%ED%97%98%EC%B2%AD%EC%95%BD%EC%84%9C_18.08.30.pdf'"> 증명서보기</button>
			</div>	
		</div>
				
	</section>
	
	<!--이전검사정보-->
	<div class="basicDeduction">
		<h2 class="pdt30"> 이전검사정보</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">지정검사일</th>
				<td class="center"><?=(!empty($isp['last_assigned']))?$isp['last_assigned']:""?></td>
			</tr>
			<tr>
				<th>최종검사일</th>
				<td class="center"><?=(!empty($isp['laste_check']))?$isp['laste_check']:""?></td>
			</tr>
		</table>		
	</div>
	<!--//이전검사정보-->
	
	
	<!--다음검사정보-->	
	<div class="basicDeduction">
		<h2> 다음검사일</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">다음지정검사일</th>
				<td class="center"><?=(!empty($isp['next_assigned']))?$isp['next_assigned']:""?></td>
			</tr>
			<tr>
				<th>다음검사개시일</th>
				<td class="center"><?=(!empty($isp['next_start']))?$isp['next_start']:""?></td>
			</tr>
			<tr>
				<th>다음검사만료일</th>
				<td class="center"><?=(!empty($isp['next_expired']))?$isp['next_expired']:""?></td>
			</tr>
		</table>		
	</div>
	<!--//다음검사정보-->	
	
</div><!--//contractWrap-->