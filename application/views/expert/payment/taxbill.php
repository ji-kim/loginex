<div class="payHistoryWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">세금계산서</h2>		
	
	<!--수납정보-->
	<div class="basicDeduction">
		<h2 class="mgt20">세금계산서정보</h2>
		<table class="basicTable">
			<tr>
				<th width="27%">발행일시</th>
				<th>공급자</th>
				<th>공급가</th>
				<th>부가세</th>
				<th>합계</th>
			</tr>
			
			<?
			if(count($history) > 0){	
				foreach($history as $hist){?>
				<tr>
					<td class="center"><?=substr($hist->YMD_WRITE,0,4).".".substr($hist->YMD_WRITE,4,2).".".substr($hist->YMD_WRITE,6,2)?></td>
					<td><?=$hist->SELL_NM_CORP?></td>
					<td><?=number_format($hist->AM)?>원</td>
					<td><?=number_format($hist->AM_VAT)?>원</td>
					<td><?=number_format($hist->AMT)?>원</td>
				</tr>
				<?}
			}else{?>
				<tr>
					<td class="center" colspan="5">세금계산서 정보가 존재하지 않습니다.</td>
					
				</tr>
			<?}?>
			
		</table>
	</div>
	
	<!--//수납정보-->
	
</div><!--//contractWrap-->