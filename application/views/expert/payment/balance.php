<div class="payWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">월별정산서</h2>		
	
	<!--수납정보-->
	<div class="basicDeduction">
		<h2 class="mgt20">정산정보</h2>
		<table class="basicTable">
			<tr>
				<th width="25%">부과년월</th>
				<th>부과액</th>
				<th>납부액</th>
				<th>비고</th>
			</tr>
			<tr>
				<td class="center">2019.01</td>
				<td>861,000</td>
				<td>861,000</td>
				<td>완납</td>
			</tr>
			<tr>
				<td class="center">2019.02</td>
				<td>775,000</td>
				<td>775,000</td>
				<td>완납</td>
			</tr>
			<tr>
				<td class="center">2019.03</td>
				<td>882,000</td>
				<td>882,000</td>
				<td>부분납</td>
			</tr>
		</table>
	</div>
	<!--//수납정보-->
	
</div><!--//contractWrap-->