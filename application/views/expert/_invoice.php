<?php
    if(isset($wsm_info)) {
?>
	<!--기본공제 시작-->
	<div class="basicDeduction">
		<h2 class="pdt30"><span>공제</span> 기본공제</h2>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="30%">항목</th>
					<th>공급가</th>
					<th>부가세</th>
					<th>합계</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>위·수탁관리비</td>
					<td><?= char_num($wsm_info['wst_mfee']) ?></td>
					<td><?= char_num($wsm_info['mfee_vat']) ?></td>
					<td><?= char_num($wsm_info['wst_mfee'] + $wsm_info['mfee_vat']) ?></td>
				</tr>
				<tr>
					<td>협회비</td>
					<td><?= char_num($wsm_info['org_fee']) ?></td>
					<td>0</td>
					<td><?= char_num($wsm_info['org_fee']) ?></td>
				</tr>
				<tr>
					<td>차고지비</td>
					<td><?= char_num($wsm_info['grg_fee']) ?></td>
					<td><?= char_num($wsm_info['grg_fee_vat']) ?></td>
					<td><?= char_num($wsm_info['grg_fee'] + $wsm_info['grg_fee_vat']) ?></td>
				</tr>
				<tr>
					<td>기타</td>
					<td><?= char_num($wsm_info['etc']) ?></td>
					<td>0</td>
					<td><?= char_num($wsm_info['etc']) ?></td>
				</tr>
				<tr>
					<td class="red">소계</td>
					<td class="subTotalBg"><?= char_num($wsm_info['total_amount']) ?></td>
					<td class="subTotalBg"><?= char_num($wsm_info['total_vat']) ?></td>
					<td class="subTotalBg"><?= char_num($wsm_info['total_price']) ?></td>
				</tr>	
			</tbody>
		</table>		
	</div>
	<!--//기본공제-->
<?php
    }

    if(isset($gongje_info)) {
?>
	<!--일반공제 시작-->
	<div class="basicDeduction">
		<h2 class=""><span>공제</span> 일반공제</h2>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="29%">항목</th>
					<th>공급가</th>
					<th>부가세</th>
					<th>합계</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>범칙금</td>
					<td><?=char_num($gongje_info['fine_fee'])?></td>
					<td>0</td>
					<td><?=char_num($gongje_info['fine_fee'])?></td>
				</tr>
				<tr>
					<td>미납</td>
					<td><?=char_num($gongje_info['not_paid'])?></td>
					<td>0</td>
					<td><?=char_num($gongje_info['not_paid'])?></td>
				</tr>
				<tr>
					<td>자동차세</td>
					<td><?=char_num($gongje_info['car_tax'])?></td>
					<td>0</td>
					<td><?=char_num($gongje_info['car_tax'])?></td>
				</tr>
				<tr>
					<td>환경부담금</td>
					<td><?=char_num($gongje_info['env_fee'])?></td>
					<td>0</td>
					<td><?=char_num($gongje_info['env_fee'])?></td>
				</tr>
				<tr>
					<td>차고지비</td>
					<td><?=char_num($gongje_info['grg_fee'])?></td>
					<td><?=char_num($gongje_info['grg_fee_vat'])?></td>
					<td><?=char_num($gongje_info['grg_fee']+$gongje_info['grg_fee_vat'])?></td>
				</tr>				
				<tr>
					<td class="red">소계</td>
					<td class="subTotalBg"><?=char_num($gongje_info['total_amount'])?></td>
					<td class="subTotalBg"><?=char_num($gongje_info['total_vat'])?></td>
					<td class="subTotalBg"><?=char_num($gongje_info['total_price'])?></td>
				</tr>
			</tbody>
		</table>		
	</div>
	<!--//기본공제-->
<?php
    }

    if(isset($insur_info)) {
?>
	<!--각종보험공제 시작-->
	<div class="basicDeduction">
		<h2><span>공제</span> 각종보험공제</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">항목</th>
				<th>금액</th>
			</tr>
			<tr>
				<td class="center">자동차보험</td>
				<td><?=char_num($insur_info['ins_car'])?></td>
			</tr>
			<tr>
				<td class="center">적재물보험</td>
				<td><?=char_num($insur_info['ins_load'])?></td>
			</tr>
			<tr>
				<td class="red"> 소계</td>
				<td class="subTotalBg"><?=char_num($insur_info['total_price'])?></td>		
			</tr>
		</table>		
	</div>
	<!--//각종보험공제-->	
<?php
    }

    if(isset($rf_gongje_info)) {
?>
	<!-- 환급형공제 시작 -->
	<div class="basicDeduction">
		<h2><span>공제</span> 환급형공제</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">항목</th>
				<th>금액</th>
			</tr>
			<tr>
				<td class="center">해지담보</td>
				<td><?=char_num((empty($rf_gongje_info['gj_termination_mortgage'])?0:$rf_gongje_info['gj_termination_mortgage']))?></td>
			</tr>
			<tr>
				<td class="red">소계</td>
				<td class="subTotalBg"><?=char_num($rf_gongje_info['total_price'])?></td>		
			</tr>
		</table>		
	</div>
	<!-- //환급형공제 -->

<?php
    }
?>

