<div class="payHistoryWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">Q & A</h2>		
	
	<!--수납정보-->
	<div class="basicDeduction">
		<h2 class="mgt20">문의내용</h2>
		<table class="basicTable">				
			<tr>					
				<th class="center"><?="작성일자"?></td>						
			</tr>
			<tr>
				<td class="center"><?=$data[0]->created_date?></td>
			</tr>
			<tr>					
				<th class="center"><?="질문내용"?></td>						
			</tr>
			<tr>			
				<td class="center"><?=$data[0]->question?></td>
			</tr>
			<tr>					
				<th class="center"><?="답변내용"?></td>						
			</tr>
			<tr>
				<td class="center"><?=empty($data[0]->answer) ? "답변 대기중입니다." : $data[0]->answer?></td>				
			</tr>
		</table>
		<h2 class="mgt20">
			<a href="javascript:history.back(-1)">뒤로가기</a>
		</h2>
	</div>

	
	
	<!--//수납정보-->
	
</div><!--//contractWrap-->
