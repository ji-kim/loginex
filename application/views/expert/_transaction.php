<!--거래량 시작-->
	<div class="basicDeduction">
		<h3 class="pdt30"><span>+</span> 거래(운송,배달)정보</h3>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="30%">항목</th>
					<th>단가</th>
					<th>거래량</th>
					<th>수수료</th>
				</tr>
			</thead>
			<tbody>
			<?php echo $fee_datas;?>

			</tbody>
		</table>		
	</div>
	<!--//거래량-->

<!--기본수당 시작-->
	<div class="basicDeduction">
		<h3 class="pdt30"><span>+</span> 기본수당</h3>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="48%">항목</th>
					<th>금액</th>
				</tr>
			</thead>
			<tbody>
			<tr>
				<td class="center">관리자수당</td>
				<td><?=char_num($mf['manager_pickup'])?></td>
			</tr>
			<tr>
				<td class="center">팀장수당</td>
				<td><?=char_num($mf['team_leader'])?></td>
			</tr>
			<tr>
				<td class="center">난지역수당</td>
				<td><?=char_num($mf['diffcult_area'])?></td>
			</tr>
			<tr>
				<td class="center">보조금수당</td>
				<td><?=char_num($mf['subside'])?></td>
			</tr>
			<tr>
				<td class="center">회식수당</td>
				<td><?=char_num($mf['hoisik'])?></td>
			</tr>
			<tr>
				<td class="center">기타수당</td>
				<td><?=char_num($mf['sudang_etc'])?></td>
			</tr>
			</tbody>
			<tr>
				<td class="red"> 소계</td>
				<td class="subTotalBg"><?=char_num($mf['total_sudang'])?></td>		
			</tr>
		</table>		
	</div>
	<!--//수당-->

<?php if(0) { ?>
<!--추가수당 시작-->
	<div class="basicDeduction">
		<h3 class="pdt30"><span>+</span> 추가수당</h3>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="48%">항목</th>
					<th>금액</th>
				</tr>
			</thead>
			<tbody>
			<?php echo $extra_datas;?>

			</tbody>
		</table>		
	</div>
	<!--//수당-->
<?php } ?>
