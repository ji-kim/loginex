<div class="contractWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">계약현황</h2>		
	<section class="topCommon">	
		<!--상단 및 타이틀 box-->
		<div class="box">
			<!-- 폰트 레드컬러로 변경시  boxText css-->
			<div class="boxTextNavy "><span><?= $member['N']?> ~ <?= $member['O']?></span></div>			
   		</div>
   		
		<!--계약상세정보-->
		<div class="infoDetail">   		
			<h2 class="infoTit">계약상세정보</h2>	
			<!-- div 테이블 변경시 table-2cols table-4cols table-5cols -->
			<div class="table table-2cols">			
				<div class="cell40 tableBTop">소속</div>
				<div class="cell60 tableBTop"><?=($member['gr_name'])?$member['gr_name']:"없음"?></div>

				<div class="cell40">계약명</div>
				<div class="cell60">분동운반용역 </div>
				
				<div class="cell40">위·수탁관리사</div>
				<div class="cell60">(주)아이디일일구닷컴 </div>
				
				<div class="cell40">차량번호</div>
				<div class="cell60"><?=$truck['car_1']?> </div>
				
				<div class="cell40">계약시작일</div>
				<div class="cell60"><?= $member['N']?></div>
				
				<div class="cell40">계약종료일</div>
				<div class="cell60"><?= $member['O']?></div>				
			</div>
			<div class="commonBtn">
				<button class="navyBg" onClick="alert('대상자가 아닙니다.');">재계약 (전자계약) 신청</button>
				<button class="redBg" onClick="alert('대상자가 아닙니다.');">계약해지요청</button>
			</div>				
		</div>
		<!--//계약상세정보-->
			
	</section>
</div>