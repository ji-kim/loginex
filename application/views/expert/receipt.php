<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:26
 */
?>
<!-- [visual] 공제 후 지급 총액S -->
<section class="topTotal">
    <div class="topTotalWarp">
        <ul>
            <li class="Tit">수수료지급명세서</li>
            <li class="criterion">(<?=substr($month,0,4)?>년 <?=substr($month,5,2)?>월 기준)</li>
            <li>
                <p class="Price"><?=(isset($mf['df_id']))?char_num($total['price'])."원":"자료가 없습니다"?></p>
                <p class="Price_underline"></p>
            </li>
        </ul>
    </div>
</section>
<!-- [visual] 공제 후 지급 총액E -->
<!-- [btn] 기준월선택S -->
<section class="calendarCheck">
    <div class="calendarCheckWrap">
        <input type="text" class="datepicker-here" value="<?=$month?>">
        <button onclick="myDatepicker.show()" class="datepickerBtn"><i class="xi-calendar-check"></i> <?=substr($month,0,4)?>년 <?=substr($month,5,2)?>월</button>
    </div>
</section>
<!-- [btn] 기준월선택E -->
<!-- [table] S -->
<section class="invoiceDetail">
<?php
    if (isset($truck['car_1'])) {
        ?>
        <!-- [table] 고객정보S -->
        <div class="customerInfo">
            <p class="Tit">고객정보</p>
            <table class="customerInfoTable" cellpadding="0" cellspacing="0" border="0">
                <thead>
                <tr><td width="50%" class="grayTit">소속</td><td width="50%"><?=($member['gr_name'])?$member['gr_name']:"없음"?></td></tr>
                </thead>
                <tbody>
                <tr><td class="grayTit">위·수탁차주</td><td><?=$member['co_name']?></td></tr>
                <tr><td class="grayTit">배차지</td><td><?=$truck['baecha_co_name']?></td></tr>
                <tr><td class="grayTit">전화</td><td><?=$member['co_tel']?></td></tr>
                <tr><td class="grayTit">차량번호</td><td><?=$truck['car_1']?></td></tr>
                <tr><td class="grayTit">톤수</td><td><?=$truck['carinfo_11']?></td></tr>
                <tr><td class="grayTit">용도</td><td><?=(empty($member['c']))?"-":$member['c']?></td></tr>
                <tr><td class="btline">계약일</td><td class="btline"><?=$member['N']?></td></tr>
                </tbody>
            </table>
        </div>
        <!-- [table] 고객정보E -->
        <?php
    }
    ?>
</section>
<!-- [table] E -->

<script>
    var date = new Date();
    var setdate = new Date(<?=substr($month,0,4)?>, <?=substr($month,5,3)?>, 01);
    date.setMonth(date.getMonth()-1);
    var myDatepicker = $('.datepicker-here').datepicker({
        language: 'ko',
        minView: "months",
        view: "months",
        autoClose: true,
        position: "bottom center",
        startDate: setdate,
        maxDate: date,
        onSelect: function (fd, date) {
            // Do nothing if selection was cleared
            if (!date) return;
            location.href='/expert/invoice/'+fd;
        }
    }).data('datepicker');
</script>