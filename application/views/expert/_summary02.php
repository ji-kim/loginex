	<!-- 청구총액 시작 -->
	<div class="basicDeduction">
		<h3>지급총액</h3>
		<table class="basicTable">
			<tr>
				<th width="20%">항목</th>
				<th>공급가</th>
				<th>부가세</th>
				<th>합계</th>
			</tr>
			<tr>
				<td class="red">소계</td>
				<td class="subTotalBg"><?= char_num($total['sub']) ?></td>
				<td class="subTotalBg"><?= char_num($total['vat']) ?></td>
				<td class="subTotalBg"><?= char_num($total['total_balance']) ?></td>
			</tr>
		</table>		
	</div>
	<!-- // 청구총액 -->
