<div class="payHistoryWrap">

    <!--상단 및 타이틀 box-->
    <h2 class="subTitle">알림</h2>

    <!--수납정보-->
    <div class="basicDeduction">
        <h2 class="mgt20">알림내용</h2>
        <table class="basicTable">
            <tr>
                <th class="center"><?="알림일자"?></td>
            </tr>
            <tr>
                <td class="center"><?=$data->created_date?></td>
            </tr>
            <tr>
                <th class="center"><?="제목"?></td>
            </tr>
            <tr>
                <td class="center"><?=$data->title?></td>
            </tr>

            <tr>
                <th class="center"><?="내용"?></td>
            </tr>
            <tr>
                <td class="center">
                    <?
                    if($data->attachment != "") {
                        $attachments = json_decode($data->attachment);
                        foreach ($attachments as $attachment) {
                            ?>
                            <img src="/<?= $attachment->path ?>">
                            <br>
                            <?
                        };
                    }?>

                    <?=$data->description?></td>
            </tr>

        </table>
        <h2 class="mgt20">
            <a href="/expert/notice/notice_list">알림 목록으로</a>
        </h2>
    </div>



    <!--//수납정보-->

</div><!--//contractWrap-->
