<div class="invoiceWrap">
	<h2 class="subTitle">수수료 & 관리비 정산서</h2>	
		
	<!--상단 및 타이틀 box-->
	<section class="topCommon">			
		<div class="box">
			<div class="boxText">2,085,110<span>원</span></div>
			
			<div class="calendarCheck">
				<div class="calendarCheckWrap ">
				<input type="text" class="datepicker-here" value="">
				<button onclick="myDatepicker.show()" class="datepickerBtn" id="datepicker1">2019년 4월<span><i class="xi-angle-down"></i></span></button>
				</div>
			</div>
		</div>
   		<!--상단 및 타이틀 box-->
   		
		<!--상단고객정보-->
   		<div class="infoDetail">   		
			<h2 class="infoTit">고객정보</h2>
			<!-- div 테이블 변경시 table-2cols table-4cols table-5cols -->
			<div class="table table-2cols">			
				<div class="cell40 tableBTop">소속</div>
				<div class="cell60 tableBTop">케이티지엘에스(주)</div>

				<div class="cell40">위·수탁차주</div>
				<div class="cell60">장기영</div>

				<div class="cell40">배차지</div>
				<div class="cell60">한국승강기안전공단</div>

				<div class="cell40">전화</div>
				<div class="cell60">010-9876-5432</div>

				<div class="cell40">차량번호</div>
				<div class="cell60">광주89아2033</div>

				<div class="cell40">톤수</div>
				<div class="cell60">2.5t</div>

				<div class="cell40">용도</div>
				<div class="cell60">-</div>

				<div class="cell40">계약일</div>
				<div class="cell60">2018년 11월16일</div>
			</div>
		</div>		
		<!-- //상단고객정보-->
		
	</section>
	<!-- //타이틀 및 고객정보 내용-->

	
	<!--기본공제 시작-->
	<div class="basicDeduction">
		<h2 class="pdt30"><span>공제</span> 기본공제</h2>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="30%">항목</th>
					<th>공급가</th>
					<th>부가세</th>
					<th>합계</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>위·수탁관리비</td>
					<td>200,000</td>
					<td>20,000</td>
					<td>220,000</td>
				</tr>
				<tr>
					<td>협회비</td>
					<td>12,000</td>
					<td>0</td>
					<td>12,000</td>
				</tr>
				<tr>
					<td>차고지비</td>
					<td>10,000</td>
					<td>1,000</td>
					<td>11,000</td>
				</tr>
				<tr>
					<td>기타</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
				</tr>
				<tr>
					<td class="red">소계</td>
					<td class="subTotalBg">222,000</td>
					<td class="subTotalBg">21,000</td>
					<td class="subTotalBg">243,000</td>
				</tr>	
			</tbody>
		</table>		
	</div>
	<!--//기본공제-->
	
	<!--일반공제 시작-->
	<div class="basicDeduction">
		<h2 class=""><span>공제</span> 일반공제</h2>
		<table class="basicTable" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="29%">항목</th>
					<th>공급가</th>
					<th>부가세</th>
					<th>합계</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>범칙금</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
				</tr>
				<tr>
					<td>미납</td>
					<td>1,730,560</td>
					<td>0</td>
					<td>1,730,560</td>
				</tr>
				<tr>
					<td>자동차세</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
				</tr>
				<tr>
					<td>환경부담금</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
				</tr>
				<tr>
					<td>차고지비</td>
					<td>0</td>
					<td>0</td>
					<td>0</td>
				</tr>				
				<tr>
					<td class="red">소계</td>
					<td class="subTotalBg">222,000</td>
					<td class="subTotalBg">21,000</td>
					<td class="subTotalBg">243,000</td>
				</tr>
			</tbody>
		</table>		
	</div>
	<!--//기본공제-->
	
	<!--각종보험공제 시작-->
	<div class="basicDeduction">
		<h2><span>공제</span> 각종보험공제</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">항목</th>
				<th>금액</th>
			</tr>
			<tr>
				<td class="center">자동차보험</td>
				<td>11,550</td>
			</tr>
			<tr>
				<td class="center">적재물보험</td>
				<td>0</td>
			</tr>
			<tr>
				<td class="red"> 소계</td>
				<td class="subTotalBg">111,550</td>		
			</tr>
		</table>		
	</div>
	<!--//각종보험공제-->	
	
	<!-- 환급형공제 시작 -->
	<div class="basicDeduction">
		<h2><span>공제</span> 환급형공제</h2>
		<table class="basicTable">
			<tr>
				<th width="50%">항목</th>
				<th>금액</th>
			</tr>
			<tr>
				<td class="center">해지담보</td>
				<td>0</td>
			</tr>
			<tr>
				<td class="red">소계</td>
				<td class="subTotalBg">0</td>		
			</tr>
		</table>		
	</div>
	<!-- //환급형공제 -->
	
	<!-- 청구총액 시작 -->
	<div class="basicDeduction">
		<h2>청구총액</h2>
		<table class="basicTable">
			<tr>
				<th width="20%">항목</th>
				<th>공급가</th>
				<th>부가세</th>
				<th>합계</th>
			</tr>
			<tr>
				<td class="red">소계</td>
				<td class="subTotalBg">2,064,110</td>
				<td class="subTotalBg">21,000</td>
				<td class="subTotalBg">2,085,110</td>
			</tr>
		</table>		
	</div>
	<!-- // 청구총액 -->
	
	<!--입금계좌-->
	<div class="account">
		<div class="box">
			<div class="accountNum">
				<h3>입금계좌</h3>
				<p class="accountTit"><span class="info">국 민 은 행</span>449901-01-088498</p>
				<p class="accountTit"><span class="info">예　금　주</span>케이티지엘에스(주)</p>
			</div>
			<div class="accountBtn">
				<p class="redBg"><a href="http://logis.atoz-erp.com/kbstar/common/tax_03.php?tax_date=2019-04-12&p_co_id=null&pmode=A&df_id=55211">세금계산서보기</a></p>
				<p class="navyBg"><a href="">바로납부</a></p>
			</div>
		</div>
	</div>
	<!--//입금계좌-->	
</div><!-- // invoiceWrap-->


<script>
    var date = new Date();
    var setdate = new Date(<?=substr($month,0,4)?>, <?=substr($month,5,3)?>, 01);
    date.setMonth(date.getMonth()-1);
    var myDatepicker = $('.datepicker-here').datepicker({
        language: 'ko',
        minView: "months",
        view: "months",
        autoClose: true,
        position: "bottom center",
        startDate: setdate,
        maxDate: date,
        onSelect: function (fd, date) {
            // Do nothing if selection was cleared
            if (!date) return;
            location.href='/expert/design/invoice/'+fd;
        }
    }).data('datepicker');
</script>