<div class="contractWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">계약현황</h2>		
	<section class="topCommon">	
		<!--상단 및 타이틀 box-->
		<div class="box">
			<!-- 폰트 레드컬러로 변경시  boxText css-->
			<div class="boxTextNavy "><span>2018.04.25 ~ 2019.04.25</span></div>			
   		</div>
   		
		<!--계약상세정보-->
		<div class="infoDetail">   		
			<h2 class="infoTit">계약상세정보</h2>	
			<!-- div 테이블 변경시 table-2cols table-4cols table-5cols -->
			<div class="table table-2cols">			
				<div class="cell40 tableBTop">소속</div>
				<div class="cell60 tableBTop">(주)아이디일일구닷컴</div>

				<div class="cell40">계약명</div>
				<div class="cell60">분동운반용역 </div>
				
				<div class="cell40">위·수탁관리사</div>
				<div class="cell60">(주)아이디일일구닷컴 </div>
				
				<div class="cell40">차량번호</div>
				<div class="cell60">서울85바4503 </div>
				
				<div class="cell40">계약시작일</div>
				<div class="cell60">2018.04.25 </div>
				
				<div class="cell40">계약종료일</div>
				<div class="cell60">2019.04.25 </div>				
			</div>
			<div class="commonBtn">
				<button class="navyBg">재계약 (전자계약) 신청</button>
				<button class="redBg">계약해지요청</button>
			</div>				
		</div>
		<!--//계약상세정보-->
			
	</section>
</div>