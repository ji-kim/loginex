<div class="allocWrap">
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">배차현황</h2>		
	<section class="topCommon">	
		<!--상단 및 타이틀 box-->
		<div class="box">
			<!-- 폰트 레드컬러로 변경시  boxText css-->
			<div class="boxTextNavy">2019.04.12</div>			
   		</div>   		
	</section>
	<!--상단 및 타이틀 box-->	
	
	
	<!--01 배차정보-->
	<section class="dispatch">		
		<!--테이블 1칸-->
		<p class="title"><span class="subTitle">01</span>09:30 ~ 서울지역본부</p>
		<div class="table table-cols">
			<div class="table-cell">서울신원초등학교 양천구 남부순환로 58길 32 (신월동)</div>
			<div class="table-cell"><span>수량 :　</span>1<span>　중량 :　</span>1250<span>　종류 :　</span>완성</div>
			<div class="table-cell"><span>검사원 :　</span>장홍석　010-7337-3827<br/>
				<button class="navyBg">검사원 전화연결</button>
				<button class="redBg">네비게이션</button>
			</div>
		</div>
		
		<!--테이블 4칸-->
		<div class="table table-4cols">			
			<div class="table-cell tableBTop">수리항목</div>
			<div class="table-cell tableBTop">공동운반</div>
			<div class="table-cell tableBTop">실운반</div>
			<div class="table-cell tableBTop">작업인원</div>
			
			<div class="table-cell center">1</div>
			<div class="table-cell center">0</div>
			<div class="table-cell center">1</div>
			<div class="table-cell center">2명</div>			
		</div>
				
		<div class="sign">
			<h3 class="signTit">서명</h3>
			<div class="boxWrap">
				<p class="box">
					
				</p>
			</div>
		</div>    
	</section>
	<!--//01 배차정보-->
	
	<p class="line"></p>
	
	<!--02 배차정보-->
	<section class="dispatch">		
		<!--테이블 1칸-->
		<p class="title"><span class="subTitle">02</span>10:10 ~ 서울지역본부</p>
		<div class="table table-cols">
			<div class="table-cell">구로구항동지구1BL중흥S클래스 구로구 연동로 303 서울 구로구 항동 100-5번지 일원</div>
			<div class="table-cell"><span>수량 :</span>4　<span>중량 :</span>1437.5　<span>종류 :</span>완성　</div>
			<div class="table-cell"><span>검사원 :　</span>임동수　017-320-0920<br/>
				<button class="navyBg">검사원 전화연결</button>
				<button class="redBg">네비게이션</button>
			</div>
		</div>
		
		<!--테이블 4칸-->
		<div class="table table-4cols">			
			<div class="table-cell tableBTop">수리항목</div>
			<div class="table-cell tableBTop">공동운반</div>
			<div class="table-cell tableBTop">실운반</div>
			<div class="table-cell tableBTop">작업인원</div>
			
			<div class="table-cell center">4</div>
			<div class="table-cell center">0</div>
			<div class="table-cell center">4</div>
			<div class="table-cell center">2명</div>			
		</div>
		
		<div class="sign">
			<h3 class="signTit">서명</h3>
			<div class="boxWrap">
				<p class="box">
					
				</p>
			</div>
		</div>
	</section>
	<!-- // 02 배차정보-->
	
</div> <!--//allocWrap-->
