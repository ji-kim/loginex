<div class="payHistoryWrap">
	
	<!--상단 및 타이틀 box-->
	<h2 class="subTitle">Q & A</h2>		
	
	<!--수납정보-->
	<div class="basicDeduction">
		<h2 class="mgt20">문의목록</h2>
		<table class="basicTable">
			<tr>
				<th width="27%">문의일자</th>
				<th>제목</th>
				<th>상태</th>				
			</tr>
			
			<?
			if(count($list) > 0){	
				foreach($list as $data){?>
					<tr>
						<td class="center"><?=$data->created_date?></td>
						<td class="left"><a href="/expert/qa/view/<?=$data->qna_id?>"><?=$data->title?></a></td>
						<td class="left"><?=empty($data->answer) ? "답변대기중" : "답변완료"?></td>
						
					</tr>
				<?}?>
			<?}else{?>
				<tr>
					<td class="center" colspan="3">등록된 질문이 없습니다.</td>
					
				</tr>
			<?}?>
			<!--tr>
				<td class="center">2019.02.12</td>
				<td>775,000</td>
				<td>전자결제</td>
				<td></td>
			</tr>
			<tr>
				<td class="center">2019.03.18</td>
				<td>882,000</td>
				<td>무통장</td>
				<td></td>
			</tr-->
		</table>
		<h2 class="mgt20">
			<a href="/expert/qa/write">작성하기</a>
		</h2>
	</div>
	<!--//수납정보-->
	
</div><!--//contractWrap-->
