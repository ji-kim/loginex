<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class RestModel {
    public $code;
    public $message;
    public $content;
    //public $page;
}

class User {
    public $idx;
    public $name;
    public $company;
    public $type;
    public $car_1;
    public $car_3;
    public $car_4;
    public $car_info;
    public $is_connected;
}

class Driverinfo{
    //드라이버 정보
    public $driver_idx; //기본고유키값
    public $driver_type; //차량종류
    public $driver_car_1; //차량번호
    public $driver_car_3; // 용도
    public $driver_car_4; //차 이름
    public $driver_car_5; // 연식
    public $driver_carinfo_11; //톤수
    public $driver_co_name; //회사명
    public $driver_name; //기사명
    public $driver_loginid; //유저아이디
    public $driver_contract_start; //계약시작
    public $driver_contract_end; //계약시작

    //자동차 보험
    public $car_ins_co_id; // 코드번호
    public $car_ins_co_name; // 보험명
    public $car_type; // 자동차 보험
    public $car_pay_cnt; //분납횟수
    public $car_active_yn; //보험 사용 여부 Y/N
    public $car_start_date; // 자동차 보험 시작날
    public $car_end_date; //자동차보험 종료날

    //적재물 보험
    public $load_ins_co_id; // 보험코드번호
    public $load_ins_co_name; // 보험명
    public $load_type; // 적재물 보험
    public $load_pay_cnt; //분납횟수
    public $load_active_yn; // 보험 사용 여부 Y/N
    public $load_start_date; // 적재물 보험 시작날
    public $load_end_date; //적재물 보험 종료날

    //자동차 정기검사
    public $inspection_active_yn; //차량 검사여부
    public $inspection_laste_check; // 최종검사일
    public $inspection_next_start; //다음검사시작일
    public $inspection_next_expired; //다음검사만료일
}
//자동차보험
class drivercar{
    public $code;
    public $type;
    public $active_yn;
    public $start_date;
    public $end_date;
}

//적재보험
class drverload{
    public $code;
    public $type;
    public $active_yn;
    public $start_date;
    public $end_date;
}

//자동차 검사
class Carinfo{
    public $active_yn;
    public $laste_check;
    public $next_start;
    public $next_expired;
}

Class Page{
    public $next;
    public $prev;
    public $totalPages;
    public $pageNumber;
    public $numberOfElements;
}

Class loginUser{
    public $user_id;
    public $user_name;
    public $photo_url;
    public $company;
}


/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class V1 extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        //model
        $this->load->model('Api_model');
        //pages
        $this->load->library('pagination');


    }

    //전체 리스트
    public function driverlist_get(){

        $this->load->library('pagination');

        $user = new User();
        $page = new Page();
        $model = new RestModel();

        //$start = $this->input->get("start");
        $limit = $this->input->get("limit");
        $pagenumber = $this->input->get("page");
        $search = $this->input->get("search");
        $is_connected = $this->input->get('is_connected');
        //echo $is_connected;
        //exit;
        //10
        if($limit === '' || $limit === null || empty($limit)){
            $limit = '15';
        }else{
            $limit = $this->input->get("limit");
        }

        //page number
        /*
        if($pagenumber === '' || $pagenumber === null || empty($pagenumber)){
           $pagenumber = 15;
            //$start = 0;
        }else{
            //$pagenumber = $this->input->get("page");
            $start = $pagenumber * $limit;
        }
*/
        //토탈 카운트
        $cnt_values = $this->Api_model->search_list($search,$is_connected);

        //echo $cnt;

        foreach ($cnt_values as $row){
            $cnt = $row->cnt;
        }

        //echo $cnt;
        //exit;

        $limit = $limit ;
        $block = 5;

        $pageNum = ceil($cnt/$limit); // 총 페이지
        $e_page = $pagenumber*$block;
        if($pagenumber < $pageNum && $cnt > $limit ){
            $next_value = "true";
        }else{
            $next_value  = "false";
        }

        if($pagenumber <= $pageNum && 1 <$pagenumber ){
            $prev_value = "true";
        }else{
            $prev_value  = "false";
        }

        if($pagenumber === '' || $pagenumber === null || empty($pagenumber)){
            $pagenumber = 1;
        }

        //page
        $page->numberOfElements = $cnt;
        $page->pageNumber = $pagenumber;
        $page->totalPages = $pageNum;
        $page->next = $next_value;
        $page->prev = $prev_value;

        if($pagenumber === '1' || $pagenumber === '0' || $pagenumber === null || empty($pagenumber)){
            $start = 0 ;
        }else if ($pagenumber >= 1){
            $start = ($pagenumber-1) * $limit;
        }else{
            $start = 0 ;
        }
        /*
        echo "start =>".$start;
        echo "<br>";
        echo "limit =>".$limit;
        echo "<br>";
        echo "pageNumber =>".$pagenumber;
        echo "<br>";
        */

        //$count_result = $this->Api->search_list($start,$limit,$search);

        $result = $this->Api_model->user_list($start,$limit,$search,$is_connected);

        $model->content = json_decode(json_encode($result),true);

        if($result){
            $model->code = REST_Controller::HTTP_OK;
            $model->message = 'Success';
            $model->info = $page;
            $this->response($model, REST_Controller::HTTP_OK);
        } else{
            $this->response("No record found", REST_Controller::HTTP_NOT_FOUND);
        }
    }

    //상세정보
    public function driverdetail_get(){


        $idx = $this->input->get("idx");
        $loginid = $this->input->get('loginid');
        $driverinfo = new Driverinfo();
        $model = new RestModel();

        //107-19-30578
        //운전자정보 상세
        $this->db->select('b.idx,b.type,b.car_1,b.car_3,b.car_4,b.carinfo_11,a.co_name,a.driver,a.userid,b.car_5,a.N,a.O', false);
        $this->db->from("tbl_members a");
        $this->db->join('tbl_asset_truck b','a.tr_id = b.idx','inner');
        //$this->db->where('b.idx', $idx);
        $this->db->where('a.userid',$loginid);
        $this->db->order_by('b.idx','desc');

        $query = $this->db->get();
        $driver_info = $query->result();

        //echo $this->db;

        //public $driver_contract_start; //계약시작

        foreach ($driver_info as $row){
            $dirver_idx =  $row->idx; //운전자 idx
            $driver_type = $row->type;
            $driver_car_1 = $row->car_1;
            $driver_car_3 = $row->car_3;
            $driver_car_4 = $row->car_4;
            $driver_car_5 = $row->car_5;
            $driver_carinfo_11 = $row->carinfo_11;
            $driver_contract_start = $row->N;
            $driver_contract_end = $row->O;
            $driver_co_name = $row->co_name;
            $driver_name = $row->driver;
            $driver_loginid = $row->userid;
        }

        if(!empty($dirver_idx)) {
            $driverinfo->driver_idx = $dirver_idx;
        }
        if(!empty($driver_type)){
            $driverinfo->driver_type = $driver_type;
        }
        if(!empty($driver_car_1)){
            $driverinfo->driver_car_1 = $driver_car_1;
        }
        if(!empty($driver_car_3)){
            $driverinfo->driver_car_3 = $driver_car_3;
        }
        if(!empty($driver_car_4)){
            $driverinfo->driver_car_4 = $driver_car_4;
        }
        if(!empty($driver_car_5)){
            $driverinfo->driver_car_5 = $driver_car_5;
        }
        if(!empty($driver_carinfo_11)){
            $driverinfo->driver_carinfo_11 = $driver_carinfo_11;
        }
        if(!empty($driver_contract_start)){
            $driverinfo->driver_contract_start = $driver_contract_start;
        }
        if(!empty($driver_contract_end)){
            $driverinfo->driver_contract_end = $driver_contract_end;
        }
        if(!empty($driver_co_name)){
            $driverinfo->driver_co_name = $driver_co_name;
        }
        if(!empty($driver_name)){
            $driverinfo->driver_name = $driver_name;
        }
        if(!empty($driver_loginid)){
            $driverinfo->driver_loginid = $driver_loginid;
        }

        /*
        $driverinfo->driver_idx = $dirver_idx;
        $driverinfo->driver_type = $driver_type;
        $driverinfo->driver_car_1 = $driver_car_1;
        $driverinfo->driver_car_3 = $driver_car_3;
        $driverinfo->driver_car_4 = $driver_car_4;
        $driverinfo->driver_carinfo_11 = $driver_carinfo_11;
        $driverinfo->driver_co_name = $driver_co_name;
        $driverinfo->driver_name = $driver_name;
*/

        //자동차 보험 상세
        $this->db->select('a.ins_co_id,a.type,a.pay_cnt,a.active_yn,a.start_date,a.end_date,a.idx');
        $this->db->from('tbl_asset_truck_insur a');
        $this->db->join('tbl_members b','a.tr_id = b.tr_id','inner');
        $this->db->where('b.userid',$loginid);
        $this->db->like('a.type','자동차');
        $this->db->order_by('a.idx','desc');
        $this->db->limit('1','1');

        $query = $this->db->get();
        $driver_insur1 = $query->result();

        foreach ($driver_insur1 as $row){
            $car_ins_co_id = $row->ins_co_id;
            $car_type = $row->type;
            $car_pay_cnt = $row->pay_cnt;
            $car_active_yn = $row->active_yn;
            $car_start_date = $row->start_date;
            $car_end_date = $row->end_date;
        }


        if(empty($car_ins_co_id) === true){
            $driverinfo->car_ins_co_id = null;
            $driverinfo->car_ins_co_name = null;
            $driverinfo->car_type = null;
            $driverinfo->car_pay_cnt = null;
            $driverinfo->car_active_yn = null;
            $driverinfo->car_start_date = null;
            $driverinfo->car_end_date = null;
        }else{
        //조건문 자동차 보험종류
        if($car_ins_co_id === '1'){
            $car_ins_co_name = 'LIG';
        }else if($car_ins_co_id === '2'){
            $car_ins_co_name = '동부화재';
        }else if($car_ins_co_id === '3'){
            $car_ins_co_name = '화물공제조합';
        }else if($car_ins_co_id === '4'){
            $car_ins_co_name = '삼성화재';
        }else if($car_ins_co_id === '5'){
            $car_ins_co_name = '현대해상';
        }else if($car_ins_co_id === '6') {
            $car_ins_co_name = '교보';
        }else if($car_ins_co_id === '7'){
            $car_ins_co_name = '흥국화재';
        }else if($car_ins_co_id === '8'){
            $car_ins_co_name = 'AXI손해보험';
        }else if($car_ins_co_id === '10'){
            $car_ins_co_name = '동부-승용';
        }else if($car_ins_co_id === '11'){
            $car_ins_co_name = '메리츠화재';
        }else{
            $car_ins_co_name = '기타';
        }

        $driverinfo->car_ins_co_id = $car_ins_co_id;
        $driverinfo->car_ins_co_name = $car_ins_co_name;
        $driverinfo->car_type = $car_type;
        $driverinfo->car_pay_cnt = $car_pay_cnt;
        $driverinfo->car_active_yn = $car_active_yn;
        $driverinfo->car_start_date = $car_start_date;
        $driverinfo->car_end_date = $car_end_date;
        }

        //적재물 보험 상세
        $this->db->select('a.ins_co_id,a.type,a.pay_cnt,a.active_yn,a.start_date,a.end_date,a.idx');
        $this->db->from('tbl_asset_truck_insur a');
        $this->db->join('tbl_members b','a.tr_id = b.tr_id','inner');
        $this->db->where('b.userid',$loginid);
        $this->db->like('a.type','적재물');
        $this->db->order_by('a.idx','desc');
        $this->db->limit('1','1');

        $query = $this->db->get();
        $driver_insur2 = $query->result();

        foreach ($driver_insur2 as $row){
            $load_ins_co_id = $row->ins_co_id;
            $load_type = $row->type;
            $load_pay_cnt = $row->pay_cnt;
            $load_active_yn = $row->active_yn;
            $load_start_date = $row->start_date;
            $load_end_date = $row->end_date;
        }

        //조건문 적재물 보험종류
        if(empty($load_ins_co_id) === true) {
            $driverinfo->load_ins_co_id = null;
            $driverinfo->load_ins_co_name = null;
            $driverinfo->load_type = null;
            $driverinfo->load_pay_cnt = null;
            $driverinfo->load_active_yn = null;
            $driverinfo->load_start_date = null;
            $driverinfo->load_end_date = null;
        }else{

        if($load_ins_co_id === '1'){
            $load_ins_co_name = 'LIG';
        }else if($load_ins_co_id === '2'){
            $load_ins_co_name = '동부화재';
        }else if($load_ins_co_id === '3'){
            $load_ins_co_name = '화물공제조합';
        }else if($load_ins_co_id === '4'){
            $load_ins_co_name = '삼성화재';
        }else if($load_ins_co_id === '5'){
            $load_ins_co_name = '현대해상';
        }else if($load_ins_co_id === '6') {
            $load_ins_co_name = '교보';
        }else if($load_ins_co_id === '7'){
            $load_ins_co_name = '흥국화재';
        }else if($load_ins_co_id === '8'){
            $load_ins_co_name = 'AXI손해보험';
        }else if($load_ins_co_id === '10'){
            $load_ins_co_name = '동부-승용';
        }else if($load_ins_co_id === '11'){
            $load_ins_co_name = '메리츠화재';
        }

        $driverinfo->load_ins_co_id = $load_ins_co_id;
        $driverinfo->load_ins_co_name = $load_ins_co_name;
        $driverinfo->load_type = $load_type;
        $driverinfo->load_pay_cnt = $load_pay_cnt;
        $driverinfo->load_active_yn = $load_active_yn;
        $driverinfo->load_start_date = $load_start_date;
        $driverinfo->load_end_date = $load_end_date;
        }



        $this->db->select('a.active_yn,a.laste_check,a.next_start,a.next_expired');
        $this->db->from('tbl_asset_truck_check a');
        $this->db->join('tbl_members b','a.tr_id = b.tr_id','inner');
        $this->db->where('b.userid',$loginid);
        $this->db->order_by('idx','desc');
        $this->db->limit('1','1');


        $query = $this->db->get();
        $car_info  = $query->result();

        foreach ($car_info as $row){
            $inspection_active_yn = $row->active_yn;
            $inspection_laste_check = $row->laste_check;
            $inspection_next_start = $row->next_start;
            $inspection_next_expired = $row->next_expired;
        }

        if(empty($inspection_active_yn) === true){
            $driverinfo->inspection_active_yn = null;
            $driverinfo->inspection_laste_check = null;
            $driverinfo->inspection_next_start = null;
            $driverinfo->inspection_next_expired = null;
        }else{
            $driverinfo->inspection_active_yn = $inspection_active_yn;
            $driverinfo->inspection_laste_check = $inspection_laste_check;
            $driverinfo->inspection_next_start = $inspection_next_start;
            $driverinfo->inspection_next_expired = $inspection_next_expired;
        }


        //$result = $this->Api_model->driver_info($idx);

        //$aa->idx = 11;
        //$model->contnet = json_encode($date);
        $model->content =  json_decode(json_encode($driverinfo),true);
        //$model->content_car = json_decode(json_encode($result1),true);
        //$model->content_car2 = json_decode(json_encode($result2),true);
        //$model->content = json_decode(json_encode($result2),true);
        //$model->content = json_decode(json_encode($result3),true);
        //$model->content = json_decode(json_encode($result),true);
        //$model->content = $car_result;


        if(!empty($driver_loginid)){
            $model->code = REST_Controller::HTTP_OK;
            $model->message = 'Success';
            //$model->info = $page;
            $this->response($model, REST_Controller::HTTP_OK);
        } else{
            $this->response("No record found", REST_Controller::HTTP_NOT_FOUND);

        }

    }

    // 유저 접속 여부 업데이트
    public function updateUserConnected_post() {
        //$isConnected = $this->input->post('isConnected');
//        $isConnected = $this->input->raw_input_stream('isConnected',true);
        $stream = $this->input->raw_input_stream;
        $ob = json_decode($stream);

        $isConnected = $ob->isConnected;
        $clientId = $ob->clientId;

        try {
            if ($isConnected === null || $clientId === null) {
                $obj = new stdClass();

                $obj->flag = false;

                $this->response($obj, REST_Controller::HTTP_OK);
            }

            $sql = 'update tbl_members set is_connected = ' . $isConnected . ' where userid = "' . $clientId . '"';
            $this->db->set_dbprefix(null);
            $this->db->query($sql);

            $obj->flag = true;

            $this->response($obj, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
    //is_connected(접속자 정보) 전부 0으로 처리하는 부분
    public function updateAllUserConnected_post($stream){

        $model = new RestModel();

        try {
            $sql = 'update tbl_members set is_connected = 0';
            $this->db->set_dbprefix(null);
            $this->db->query($sql);

            $obj = new stdClass();
            $obj->flag = true;

            $this->response($obj, REST_Controller::HTTP_OK);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    //사용자 로그인 관련( 개발용)
    public function login_get(){

        $this->load->library('encrypt');
        $userid = $this->input->get('userid');
        $userpasswd = $this->input->get('userpasswd');

        $result = $this->Api_model->user_login($userid,$userpasswd);

        foreach($result as $row){
            $user_name = $row->username;
            $email = $row->email;
            $fullname = $row->fullname;
            $avatar = $row->avatar;
            $company = $row->company;
            $user_id = $row->user_id;
            $last_login = $row->last_login;
            $role_id = $row->role_id;
        }

        if($result){

            $data = array(
                'message' => '로그인 성공!',
                'user_name' => $user_name,
                'email' => $email,
                'name' => $fullname,
                'photo' => $avatar,
                'client_id' => $company,
                'user_id' => $user_id,
                'last_login' => $last_login,
                'online_time' => time(),
                'loggedin' => TRUE,
                'user_type' => $role_id,
                'user_flag' => 2,
                'url' => (!empty($url) ? $url : 'client/dashboard'),
            );

            $this->session->set_userdata($data);
            $this->response($data, REST_Controller::HTTP_OK);
        }else{
            echo "로그인 실패... !";
        }
    }

    //사용자 정보로 데이터 가져오기
    public function user_date_get(){

        $model = new RestModel();
        $user = new loginUser();
        $userid = $this->session->userdata('user_name');

        $result = $this->Api_model->user_date($userid);

        foreach($result as $row){
            $user->user_id = $row->username;
            $user->user_name = $row->fullname;
            $user->photo_url = $row->avatar;
            $user->company = $row->company;

        }

        $model->content = json_decode(json_encode($user),true);

        if($result){
            $model->code = REST_Controller::HTTP_OK;
            $model->message = 'Success';
            $this->response($model, REST_Controller::HTTP_OK);
        }else{
            $this->response("No record found", REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // 관제에서 사용할 사이드 메뉴 API
    public function sideMenu_get() {

        $CI = &get_instance();
        $designations_id = $CI->session->userdata('designations_id');
        $user_type = $CI->session->userdata('user_type');

        // 로그인 정보가 없으면 메인으로 이동
        if(!isset($designations_id)) { redirect('/'); }

        if ($user_type != 1) {// query for employee user role
            $CI->db->select('tbl_user_role.*', FALSE);
            $CI->db->select('tbl_menu.*', FALSE);
            $CI->db->from('tbl_user_role');
            $CI->db->join('tbl_menu', 'tbl_user_role.menu_id = tbl_menu.menu_id', 'left');
            $CI->db->where('tbl_user_role.designations_id', $designations_id);
            $CI->db->where('tbl_menu.status', 1);
            $CI->db->order_by('sort');
            $query_result = $CI->db->get();
            $user_menu = $query_result->result();
        } else { // get all menu for admin
            $user_menu = $CI->db->where('status', 1)->order_by('sort', 'time')->get('tbl_menu')->result();
        }
        // Create a multidimensional array to conatin a list of items and parents
        $menu = array(
            'items' => array(),
            'parents' => array()
        );

        foreach ($user_menu as $v_menu) {
            $menu['items'][$v_menu->menu_id] = $v_menu;
            $menu['parents'][$v_menu->parent][] = $v_menu->menu_id;
        }

        // Builds the array lists with data from the menu table
        $output = $this->buildMenu(0, $menu);

        echo json_encode($output);
    }

    // 사이드메뉴 2차메뉴 생성
    public function buildMenu($parent, $menu, $sub = NULL)
    {
        if (isset($menu['parents'][$parent])) {

            foreach ($menu['parents'][$parent] as $itemId) {
                if (!isset($menu['parents'][$itemId])) {
                    $menu_arr[] =  $menu['items'][$itemId];
                }
                if (isset($menu['parents'][$itemId])) {
                    $menu['items'][$itemId]->child = self::buildMenu($itemId, $menu, $menu['items'][$itemId]->label);
                    $menu_arr[] =  $menu['items'][$itemId];
                }
            }
        }
        return $menu_arr;
    }
}