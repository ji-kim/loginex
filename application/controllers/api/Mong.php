<?php

class Mong extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Mongo_model');
    }

    // 차량 운행 경로 데이터
    public function gpsLog() {

        //get 데이터 받기
        $carnum = $this->input->get("carnum");
        $date   = $this->input->get('date');
        $inout  = $this->input->get('inout');
        $date   = ($date === null) ? date('Y-m-d') : $date;
        $inout  = ($inout === null) ? "all" : $inout;

        if ($carnum === null) {
            $model = new stdClass();
            $model->message = 'carnum is required';
            echo json_encode($model);
            exit;
        }

        $data = $this->Mongo_model->gpsLog($carnum, $date, $inout);
        echo json_encode($data);
    }

    // 출근 차량 전체 목록
    public function worklist() {
        $data = $this->Mongo_model->getworklist();
        echo json_encode($data);
    }

}

?>