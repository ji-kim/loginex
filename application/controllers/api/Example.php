<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class RestModel {
    public $code;
    public $message;
    public $content;
    //public $page;
}

class User {
    public $idx;
    public $name;
    public $company;
    public $type;
    public $car_1;
    public $car_3;
    public $car_4;
    public $car_info;
}
Class Page{
    public $next;
    public $prev;
    public $totalPages;
    public $pageNumber;
    public $numberOfElements;
}



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Example extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        //model
        $this->load->model('Api_model');
        //pages
        $this->load->library('pagination');


    }



    //전체 리스트
    public function userlist_get(){

        $this->load->library('pagination');

        $user = new User();
        $page = new Page();
        $model = new RestModel();

        //$start = $this->input->get("start");
        $limit = $this->input->get("limit");
        $pagenumber = $this->input->get("page");
        $search = $this->input->get("search");
        $is_connected = $this->input->get('is_connected');

        //10
        if($limit === '' || $limit === null || empty($limit)){
            $limit = '15';
        }else{
            $limit = $this->input->get("limit");
        }

        //토탈 카운트
        $cnt_values = $this->Api_model->search_list($search,$is_connected);

        foreach ($cnt_values as $row){
            $cnt = $row->cnt;
        }

        $limit = $limit ;
        $block = 5;
        //echo $cnt."<br>";
        //echo $limit;
        //exit;
        $pageNum = ceil($cnt/$limit); // 총 페이지
        $e_page = $pagenumber*$block;
        if($pagenumber < $pageNum && $cnt > $limit ){
            $next_value = "true";
        }else{
            $next_value  = "false";
        }

        if($pagenumber <= $pageNum && 1 <$pagenumber ){
            $prev_value = "true";
        }else{
            $prev_value  = "false";
        }

        if($pagenumber === '' || $pagenumber === null || empty($pagenumber)){
            $pagenumber = 1;
        }

        //page
        $page->numberOfElements = $cnt;
        $page->pageNumber = $pagenumber;
        $page->totalPages = $pageNum;
        $page->next = $next_value;
        $page->prev = $prev_value;

        if($pagenumber === '1' || $pagenumber === '0' || $pagenumber === null || empty($pagenumber)){
            $start = 0 ;
        }else if ($pagenumber >= 1){
            $start = ($pagenumber-1) * $limit;
        }else{
            $start = 0 ;
        }


        $result = $this->Api_model->user_list($start,$limit,$search,$is_connected);

        $model->content = json_decode(json_encode($result),true);

        if($result){
            $model->code = REST_Controller::HTTP_OK;
            $model->message = 'Success';
            $model->info = $page;
            $this->response($model, REST_Controller::HTTP_OK);
        } else{
            $model->code = FALSE;
            $model->message = FALSE;
            $this->response($model, REST_Controller::HTTP_NOT_FOUND);
            //$this->response("No record found", REST_Controller::HTTP_NOT_FOUND);
        }
    }


    //Test
    public function delex_get(){
        // Users from a data store e.g. database
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        $user = new User();
        $model = new RestModel();

        /*$user->name = "jihoon";
        $user->age = 22;*/



        $user->idx = 1;
        $user->name = '한성복';
        $user->company = '(주)델타온';
        $user->type = '내장탑';
        $user->car_1 = "서울82바6944";
        $user->car_3 = "영업용";
        $user->car_4 = "이-마이티";
        $user->car_info = "3.5톤";

        $model->code = 200;
        $model->message = "True";
        
        $model->content = array($user,$user,$user);


        $users= '1';

/*
        $users = [(object) array(
            'contents' => (object) array(
                'idx' => 1, 'name' => '한성복', 'company' => '(주)델타온', 'type' => '내장탑', 'car_1' => '서울82바6944', 'car_3' => '영업용','car_4' => '이-마이티','car_info' => '3.5톤'
            )
        ),
        (object) array(
            'contents' => (object) array(
                'idx' => 2, 'name' => '이성복', 'company' => '(주)델타온', 'type' => '파워게이트', 'car_1' => '경기82바6944', 'car_3' => '자가용','car_4' => '이-마이티','car_info' => '5.5톤'
            )
        ),
        (object) array(
            'contents' => (object) array(
                'idx' => 3, 'name' => '셋성복', 'company' => '(주)델타온', 'type' => '소형화물', 'car_1' => '천안82바6944', 'car_3' => '용달영업용','car_4' => '이-마이티','car_info' => '1톤'
            )
        )];
*/


        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($model, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $user = NULL;

        if (!empty($users))
        {
            foreach ($users as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $user = $value;
                }
            }
        }

        if (!empty($user))
        {
            $this->set_response( $users , REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }


    //Test


    public function test_list_get(){

        $user = new User();
        $model = new RestModel();

        $result = $this->Api_model->test_list();


        $model->content = json_decode(json_encode($result),true);

        if($result){
            $model->code = REST_Controller::HTTP_OK;
            $model->message = 'Success';
            $this->response($model, REST_Controller::HTTP_OK);
        } else{
            $this->response("No record found", REST_Controller::HTTP_NOT_FOUND);
        }
    }





    //Sample

    public function users_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['idx' => 1, 'name' => '한성복', 'company' => '(주)델타온', 'type' => '내장탑', 'car_1' => '서울82바6944', 'car_3' => '영업용','car_4' => '이-마이티','car_info' => '3.5톤',],
            ['idx' => 2, 'name' => '이성복', 'company' => '(주)델타온', 'type' => '파워게이트', 'car_1' => '경기82바6944', 'car_3' => '자가용','car_4' => '이-마이티','car_info' => '5.5톤',],
            ['idx' => 3, 'name' => '셋성복', 'company' => '(주)델타온', 'type' => '소형화물', 'car_1' => '천안82바6944', 'car_3' => '용달영업용','car_4' => '이-마이티','car_info' => '1톤',],
        ];


        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $user = NULL;

        if (!empty($users))
        {
            foreach ($users as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $user = $value;
                }
            }
        }

        if (!empty($user))
        {
            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function users_post()
    {
        // $this->some_model->update_user( ... );
        $message = [
            'id' => 100, // Automatically generated by the model
            'name' => $this->post('name'),
            'email' => $this->post('email'),
            'message' => 'Added a resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
