<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class RestModel {
    public $code;
    public $message;
    public $content;
}

class User {
    public $idx;
    public $name;
    public $company;
    public $type;
    public $car_1;
    public $car_3;
    public $car_4;
    public $car_info;
}
Class Page{
    public $next;
    public $prev;
    public $totalPages;
    public $pageNumber;
    public $numberOfElements;
}

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Delex extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        //model
        $this->load->model('Api_model');
        //pages
        $this->load->library('pagination');


    }



    //전체 리스트
    public function userlist_get(){

        $this->load->library('pagination');

        $user = new User();
        $page = new Page();
        $model = new RestModel();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");
        $pagenumber = $this->input->get("page");
        $search = $this->input->get("search");

        //10
        if($limit === '' || $limit === null){
            $limit = '5';
        }else{
            $limit = $this->input->get("limit");
        }

        //page number
        if($pagenumber === '' || $pagenumber === null){
            $pagenumber = 1;
        }else{
            $pagenumber = $this->input->get("page");
        }

        //토탈 카운트
        $cnt = $this->Api_model->user_count();

        $limit = 5 ;
        $block = 5;
        $pageNum = ceil($cnt/$limit); // 총 페이지
        $e_page = $pagenumber*$block;
        if($pagenumber < $pageNum){
            $next_value = "true";
        }else{
            $next_value  = "false";
        }

        if($pagenumber <= $pageNum && 1 <$pagenumber ){
            $prev_value = "true";
        }else{
            $prev_value  = "false";
        }

        //page
        $page->numberOfElements = $cnt;
        $page->pageNumber = $pagenumber;
        $page->totalPages = $pageNum;
        $page->next = $next_value;
        $page->prev = $prev_value;


        //$page->totalPages = $config['per_page'];

        $start = $pagenumber * $limit;

        $count_result = $this->Api->search_list($start,$limit,$search);

        $result = $this->Api_model->user_list($start,$limit,$search);

        $model->content = json_decode(json_encode($result),true);

        if($result){
            $model->code = REST_Controller::HTTP_OK;
            $model->message = 'Success';
            $model->info = $page;
            $this->response($model, REST_Controller::HTTP_OK);
        } else{
            $this->response("No record found", REST_Controller::HTTP_NOT_FOUND);
        }
    }



    public function test_list_get(){
      $user = new User();
      $model = new RestModel();

      $result = $this->Api_model->tset_list();


      $model->content = json_decode(json_encode($result),true);

      if($result){
          $model->code = REST_Controller::HTTP_OK;
          $model->message = 'Success';
          $this->response($model, REST_Controller::HTTP_OK);
      } else{
          $this->response("No record found", REST_Controller::HTTP_NOT_FOUND);
      }
  }

}
