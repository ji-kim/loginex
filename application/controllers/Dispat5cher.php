<?php

class Dispatcher extends CI_Controller
{

    /**
     * Dispatcher constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {

        $this->load->view('../../dispatcher/index.html');
    }
}