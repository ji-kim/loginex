<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Ins_isp extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
        $this->load->model('expert/Ins_isp_model');
    }

    public function index()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
		}
        if(isset($data['truck']['idx'])) {
            $data['ins'] = $this->Ins_isp_model->insurance_info($data['truck']['idx']); //보험정보
            $data['isp'] = $this->Ins_isp_model->inspection_info($data['truck']['idx']); //보험정보
		}
		
		$data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/ins_isp', $data);
        $this->load->view('expert/tail');
    }

}