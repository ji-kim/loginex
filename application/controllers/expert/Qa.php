<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Qa extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
		$this->load->model('expert/Qa_model');
    }

    public function qa_list()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
		$data['list']	= $this->Qa_model->qnaList();
		$data['member'] = $member;


        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/qa_list', $data);
        $this->load->view('expert/tail');
    }

	public function view($index)
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
		$data['data']	= $this->Qa_model->qnaView($index);
		$data['member'] = $member;


        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/qa_view', $data);
        $this->load->view('expert/tail');
    }

	public function write()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
		$data['member'] = $member;


        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/qa_write', $data);
        $this->load->view('expert/tail');
    }

	public function writeaction()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member = $this->Member_model->member($userid); //회원정보
		$this->Qa_model->qnaWrite();
        redirect('/expert/qa');
    }

}