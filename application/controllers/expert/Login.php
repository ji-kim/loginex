<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Login extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
    }

    public function login()
    {
        $mode = $this->input->post('mode');
        $data = array();

        if($mode == "logincheck") {
            $userid = $this->input->post('userid');
            $data['userid'] = $userid;
            $userid = trim(str_replace("-","",$userid));
            $userpwd = $this->input->post('userpwd');

            $result = $this->Member_model->member($userid);
            $data['result'] = $result;

            if(empty($result)) {
                $data['msg'] = "아이디가 존재하지 않습니다.";
            } else if($result['userpwd'] != $userpwd) {
                $data['msg'] = "비밀번호가 일치하지 않습니다.";
            } else {
                // 로그인한 회원의 차량정보 조회
                $truck_result	= $this->Delivery_model->asset_truck($result['tr_id']); //차량정보
                // 차량정보가 있으면 차량번호 세션저장
                if(isset($truck_result['car_1'])) {
                    $this->session->set_userdata('carnum', $truck_result['car_1']);
                }
                // 로그인 성공시 회원 아이디 세션저장
                $this->session->set_userdata('userid', $userid);
                $this->session->set_userdata('dp_id',$result['dp_id']);
                // 로그인시 인보이스 페이지에 출근처리를 위해 login 붙혀서 호출
                redirect('/expert/invoice/login');
            }
        } else {
            if(!empty($this->session->userid)) {
                redirect('/expert/invoice');
            }
        }

        $this->load->view('expert/header');
        $this->load->view('expert/login', $data);
        $this->load->view('expert/tail');
    }

    public function logout()
    {
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('carnum');
        $this->session->unset_userdata('dp_id');
        $this->session->unset_userdata('DB_HOST');
        session_destroy();
        redirect('/expert/login');
    }

    public function setDeviceToken(){
        //$userid = "204-17-37693";
        //$device_token = "asdf";
        $userid = $this->input->post('userid');
        $device_token = $this->input->post('device_token');

        if(!empty($userid) && !empty($device_token)){
            $this->Member_model->setDeviceToken($userid,$device_token);
        }
    }

}