<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Contract extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
        $this->load->model('expert/Contract_model');
    }

    public function index()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
		}
		
		$data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/contract', $data);
        $this->load->view('expert/tail');
    }

	public function sign()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
		$data['member'] = $member;
		
		
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/contract_sign', $data);
        $this->load->view('expert/tail');
    }

	public function signAction()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
		$data['member'] = $member;

		$insertData = array();
		$insertData['econtract_legal'] = $data['truck']['ws_co_id'];
		$insertData['userid'] = $userid;
		$insertData['contract_date'] = $this->input->post("contract_date_y")."-".$this->input->post("contract_date_m")."-".$this->input->post("contract_date_d");

		if($member['O'] != ""){			
			$insertData['contract_start_date'] = date("Y-m-d",strtotime("+1 day",strtotime($member['O'])));			
		}else{
			$insertData['contract_start_date'] = $insertData['contract_date'];
		}
		$insertData['contract_end_date'] = date("Y-m-d",strtotime("+2 year",strtotime($insertData['contract_start_date'])));
		
		//서명 이미지를 담는다.
		$sign_uri = $this->input->post("contract_sign");
		$encoded_image = explode(",", $sign_uri)[1];
		$decoded_image = base64_decode($encoded_image);
		$filename = "/uploads/contract_sign/".$userid."_".$insertData['contract_date']."_".time().".png";
		file_put_contents("/home/demo.kiff.or.kr".$filename, $decoded_image);
		$insertData['contract_sign'] = $filename;
		$insertData['contract_state'] = "W";

		$this->Contract_model->signWrite($insertData);
	
		redirect('expert/contract/sign');
    }

}