<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Allocation extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
        $this->load->model('expert/Allocation_model');
    }

    public function index($work_date = NULL)
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }
		$data['work_date'] = (!empty($work_date))?$work_date:date("Y-m-d");

        $member         = $this->Member_model->member($userid); //회원정보
		$mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
		$data['all_work_list'] = $this->Allocation_model->get_work_list($data['work_date'], $member['dp_id']);
		
		$data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/allocation', $data);
        $this->load->view('expert/tail');
    }

}