<?php
/**
 * Created by PhpStorm.
 * User: Ji-kim
 * Date: 2019-03-22
 * Time: 오후 2:20
 */

class Notice extends Ci_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('expert/Member_model');
        $this->load->model('expert/Delivery_model');
        $this->load->model('expert/Notice_model');
    }

    public function notice_list()
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
		}
		
		$data['list'] = $this->Notice_model->noticeList();

		$data['member'] = $member;

        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/notice/list', $data);
        $this->load->view('expert/tail');
    }

    public function view($id)
    {
        // 로그인 세션이 없으면 로그인 페이지로
        if(empty($this->session->userid)) {
            redirect('/expert/login');
        } else {
            $userid = $this->session->userid;
        }

        $member         = $this->Member_model->member($userid); //회원정보
        $data['truck']	= $this->Delivery_model->asset_truck($member['tr_id']); //차량정보
        $mf     = $this->Delivery_model->delivery_fee($member['userid'], date("Y-m")); //위수탁관리비
        if(isset($data['mf']['ct_id'])) {
            $data['ct'] = $this->Delivery_model->contract_info($mf['ct_id']); //계약정보
        }

        $data['data'] = $this->Notice_model->noticeView($id);
        $data['member'] = $member;
        $this->load->view('expert/header');
        $this->load->view('expert/menutop', $data);
        $this->load->view('expert/notice/view', $data);
        $this->load->view('expert/tail');
    }

}