<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Asset extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('asset_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

	//등록차량 삭제 현행 완전삭제에서 마스터테이블 deleted 값만 변경하는걸로 수정
    public function delete_car($tr_id, $redir)
    {
        $this->asset_model->_table_name = "tbl_asset_truck"; // table name
        $this->asset_model->_primary_key = "idx"; // $id
        $this->asset_model->delete($tr_id);

		$this->db->query("delete from tbl_asset_truck_check where tr_id='{$tr_id}'");
		$this->db->query("delete from tbl_asset_truck_dcheck where tr_id='{$tr_id}'");
		$this->db->query("delete from tbl_asset_truck_files where tr_id='{$tr_id}'");
		$this->db->query("delete from tbl_asset_truck_gongT where tr_id='{$tr_id}'");
		$this->db->query("delete from tbl_asset_truck_info where tr_id='{$tr_id}'");
		$this->db->query("delete from tbl_asset_truck_insur where tr_id='{$tr_id}'");
		$this->db->query("delete from tbl_asset_truck_insur_sub where tr_id='{$tr_id}'");
		$this->db->query("delete from tbl_asset_truck_tcheck where tr_id='{$tr_id}'");

		$type = 'success';
        $msg = "차량이 삭제되었습니다.";
        set_message($type, $msg);
        redirect('admin/asset/'.$redir);
    }


	public function test_qry($action = NULL, $id = NULL)
	{
        $data['title'] = '차량배차관리';
        $data['active'] = 1;

		$this->asset_model->db->select('b.co_name, a.type, b.ceo, a.car_5, a.carinfo_11, a.car_3, b.N, b.O,');
		$this->asset_model->db->from('tbl_asset_truck a');
		$this->asset_model->db->join('tbl_members b', 'b.dp_id = a.inv_co_id', 'left');
		$this->asset_model->db->join('tbl_asset_truck_insur c', 'c.tr_id = a.ws_co_id', 'left'); 
		$this->asset_model->db->where('c.type', '자동차');
		$this->asset_model->db->join('tbl_asset_truck_insur d', 'd.tr_id = a.ws_co_id', 'left'); 
		$this->asset_model->db->where('d.type', '적재물');
		$this->asset_model->db->where('ad.idx', '9');

        $data['subview'] = $this->load->view('admin/asset/car_scheduling', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	// 배차관리
	public function car_scheduling($action = NULL, $id = NULL)
	{
        $data['title'] = '차량배차관리';
            $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/asset/car_scheduling', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	// 등록대기차량
	public function car_ready_list($action = NULL, $id = NULL)
	{
        $tr_id = $id;
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        //if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
       // if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'add') {
            $data['active'] = 2;
            $data['tr_id'] = $tr_id;
			$qry = "select tr.*, ws.co_name, ws.fax, ws.ceo, ws.co_tel, ws.co_address, ws.bs_number from tbl_members ws left join tbl_asset_truck tr on ws.dp_id = tr.ws_co_id";
			$qry .= " where tr.idx = '".$tr_id."'";
			$data['car_info'] = $this->asset_model->db->query($qry)->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '차량정보등록대기현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver');
		$this->asset_model->db->join('tbl_members mb', 'mb.dp_id = at.owner_id', 'left');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
       // $this->asset_model->db->where('mb.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'R');
		$this->asset_model->_order_by = 'car_1';
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}

		if($data['ws_co_id'] == "etc") {
			//$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		//} else {
		}
        $data['all_car_info'] = $this->asset_model->get();
        $data['total_cnt'] = count($data['all_car_info']);

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_ready_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	public function car_gongt_list($action = NULL, $id = NULL)
	{
        $tr_id = $id;
        $data['list_mode'] = "GTE";
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        //if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
       // if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'add') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
			if(!empty($data['car_info']->inv_co_id)) $data['inv_co_info'] = $this->db->where('dp_id', $data['car_info']->inv_co_id)->get('tbl_members')->row();
			if(!empty($data['car_info']->ws_co_id)) $data['ws_co_info'] = $this->db->where('dp_id', $data['car_info']->ws_co_id)->get('tbl_members')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '공T현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at'; //table name
        $this->asset_model->db->select('at.*, dp.co_name, dp.ceo, dp.driver, ws.co_name as ws_co_name');
		$this->asset_model->db->join('tbl_members dp', 'dp.dp_id = at.inv_co_id', 'left');
		$this->asset_model->db->join('tbl_members ws', 'ws.dp_id = at.ws_co_id', 'left');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'G');
		$this->asset_model->_order_by = 'car_1';
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}

		if($data['ws_co_id'] == "etc") {
			//$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		//} else {
		}
        $data['all_car_info'] = $this->asset_model->get();
        $data['total_cnt'] = count($data['all_car_info']);

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_gongt_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	public function cron_check_trucks()
	{
        $sql = "update tbl_members set car_status = 'R' where mb_type='partner' and O <= '".date('Y-m-d')."'";
		$this->db->query($sql);
	}

	public function car_return_list($action = NULL, $id = NULL)
	{
        $tr_id = $id;
		$data['page'] = $this->input->post('page', true);
		if(empty($data['page']) || $data['page'] < 1) $data['page'] =  1;
        $data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "ALL";
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($data['limit'])) $data['limit'] = 20;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '번호반납요청현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at, tbl_members mb'; //table name
        $this->asset_model->db->select('at.*');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('mb.tr_id = at.idx');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'R');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
		$data['total_count'] = count($this->asset_model->get());
		$data['total_page']  = ceil($data['total_count'] / $data['limit']);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $data['limit']; // 시작 열을 구함

        $this->asset_model->_table_name = 'tbl_asset_truck at'; //table name

		$this->asset_model->db->select('at.*, dp.co_name, dp.ceo, dp.driver, dp.N, dp.O, bc.co_name as bc_co_name'); //, bc.co_name as bc_co_name');
		$this->asset_model->db->join('tbl_members dp', 'dp.dp_id = at.inv_co_id', 'left');
		$this->asset_model->db->join('tbl_members bc', '(at.baecha_co_id = bc.code and bc.mb_type = \'customer\')', 'left');


		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'R');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
		$this->asset_model->db->limit($data['limit'], $data['from_record']);
        $data['all_car_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_return_list', $data, true);
        $this->load->view('admin/_layout_main_nd', $data);
	}

	//공티설정
    public function set_gongT($id)
    {
        $data['assign_user'] = $this->asset_model->allowad_user('57');
        $data['tr_id'] = $id;
        $data['tr_info'] = $this->asset_model->check_by(array('idx' => $id), 'tbl_asset_truck');
        $data['modal_subview'] = $this->load->view('admin/asset/_modal_gongT', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }
	
	//공티이 저장
    public function set_gongT_save($id)
    { 
        $data['assign_user'] = $this->asset_model->allowad_user('57');
		if(!empty($id)) {
			$sql = "update  tbl_asset_truck set return_cls_date = '".$this->input->post('gongT_cls_date', true)."' ";
			$sql .= ",gongT_date = '".$this->input->post('gongT_date', true)."' ";
			$sql .= ",gongT_yn = 'Y', car_status ='G'";
			$sql .= "where idx = '".$id."'";
			$this->db->query($sql);
            $type = "success";
            $message = $msg;
            set_message($type, $message);

        }
        redirect($_SERVER['HTTP_REFERER']);
    }

	//번호반납설정
    public function set_rtplate($id)
    {
        $data['assign_user'] = $this->asset_model->allowad_user('57');
        $data['tr_info'] = $this->asset_model->check_by(array('idx' => $id), 'tbl_asset_truck');
        $data['modal_subview'] = $this->load->view('admin/asset/_modal_rtplate', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

	//팝업 차량 선택
    public function select_truck($params = NULL, $page = NULL)
    {

		$data['params'] = $params;
		$data['page'] = $page;
		$data['search_field']		= $this->input->post('search_field', true);
		$data['search_keyword']		= $this->input->post('search_keyword', true);
		$data['car_status']			= $this->input->post('car_status', true);
		if(empty($data['car_status'])) $data['car_status'] = "A";

		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

        $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
        $this->asset_model->db->where('tbl_asset_truck.is_deleted', 'N');
        
		if(!empty($data['search_field']) && !empty($data['search_keyword']))
			$this->asset_model->db->where('tbl_asset_truck.'.$data['search_field'].' like ', '%'.$data['search_keyword'].'%');
		if(!empty($data['car_status']))	$this->asset_model->db->where('tbl_asset_truck.car_status', $data['car_status']);
		$data['total_count'] = count($this->asset_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
        $this->asset_model->_order_by = 'idx';
        $this->asset_model->db->where('tbl_asset_truck.is_deleted', 'N')->where('tbl_asset_truck.car_status <> \'L\'');

		if(!empty($data['search_field']) && !empty($data['search_keyword']))
			$this->asset_model->db->where('tbl_asset_truck.'.$data['search_field'].' like ', '%'.$data['search_keyword'].'%');
		if(!empty($data['car_status']))	$this->asset_model->db->where('tbl_asset_truck.car_status', $data['car_status']);
		$this->asset_model->db->limit($limit, $data['from_record']);
        $data['all_truck_group'] = $this->asset_model->get();

		$data['title'] = '차량 선택';
		$data['subview'] = $this->load->view('admin/asset/pop_truck_list', $data, true);
        $this->load->view('admin/_layout_pop_truck_list', $data);
    }


	//차량정보 업데이트 창
    public function pop_carinfo($md = 'car_info', $tr_id, $action = NULL, $info_id = NULL, $page = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
		$data['action'] = $action;
		$data['info_id'] = $info_id;
		$data['md'] = $md;
		$data['tr_id'] = $tr_id;
		$data['page'] = $page;

		$this->asset_model->_table_name = 'tbl_asset_truck'; //table name
		$this->asset_model->db->where('tbl_asset_truck.idx', $tr_id);
		$data['tr'] = $this->asset_model->get();


/*
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

		//거래처
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->db->where('tbl_members.mb_type', 'customer');
        $this->asset_model->_order_by = 'co_name';
        $data['all_customer_group'] = $this->asset_model->get();

		//지입회사
        $this->asset_model->db->select('distinct(X)');
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->db->where('tbl_members.mb_type', 'partner');
        $this->asset_model->_order_by = 'X';
        $data['all_jiip_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->db->where('tbl_members.mb_type', $co_type);
		$data['total_count'] = count($this->asset_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'E';
        $this->asset_model->db->where('tbl_members.mb_type', $co_type);
		$this->asset_model->db->limit($limit, $data['from_record']);

        $data['all_company_info'] = $this->asset_model->get();
*/
		if($md == "car_info") {
				$data['master_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
				if(!empty($data['master_info'])) { 
					if(!empty($data['master_info']->inv_co_id)) $data['m_inv_co_info'] = $this->db->where('dp_id', $data['master_info']->inv_co_id)->get('tbl_members')->row();
					if(!empty($data['master_info']->ws_co_id)) $data['m_ws_co_info'] = $this->db->where('dp_id', $data['master_info']->ws_co_id)->get('tbl_members')->row();
					if(!empty($data['master_info']->baecha_co_id)) $data['m_ws_baecha_info'] = $this->db->where('code', $data['master_info']->baecha_co_id)->where('mb_type', 'customer')->get('tbl_members')->row();
				}
			if ($action == 'edit_info') {
				$data['active'] = 2;
                $data['truck_info'] = $this->db->where('idx', $info_id)->get('tbl_asset_truck_info')->row();
				if(!empty($data['truck_info']->inv_co_id)) $data['inv_co_info'] = $this->db->where('dp_id', $data['truck_info']->inv_co_id)->get('tbl_members')->row();
				if(!empty($data['truck_info']->ws_co_id)) $data['ws_co_info'] = $this->db->where('dp_id', $data['truck_info']->ws_co_id)->get('tbl_members')->row();
				if(!empty($data['truck_info']->baecha_co_id)) $data['ws_baecha_info'] = $this->db->where('code', $data['truck_info']->baecha_co_id)->where('mb_type', 'customer')->get('tbl_members')->row();

			} else {
				$data['active'] = 1;
			}
			// 최초 데이터 히스토리 삽입

			$this->asset_model->_table_name = 'tbl_asset_truck_info'; //table name
			$this->asset_model->db->where('tbl_asset_truck_info.tr_id', $tr_id);
			$chk = count($this->asset_model->get());
			if($chk==0) { // 이력이 등록된게 없을때 최초가 기본이력으로 등록
                $master = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();

				$sql_add = "";
				if(!empty($master->owner_id)) $sql_add .= ", owner_id = '".$master->owner_id."'";
				if(!empty($master->owner_name)) $sql_add .= ", owner_name = '".$master->owner_name."'";
				if(!empty($master->driver_name)) $sql_add .= ", driver_name = '".$master->driver_name."'";
				if(!empty($master->car_1)) $sql_add .= ", car_1 = '".$master->car_1."'";
				if(!empty($master->car_7)) $sql_add .= ", car_7 = '".$master->car_7."'";
				if(!empty($master->car_3)) $sql_add .= ", car_3 = '".$master->car_3."'";
				if(!empty($master->car_4)) $sql_add .= ", car_4 = '".$master->car_4."'";
				if(!empty($master->mode)) $sql_add .= ", mode = '".$master->mode."'";
				if(!empty($master->car_5)) $sql_add .= ", car_5 = '".$master->car_5."'";
				if(!empty($master->type)) $sql_add .= ", type = '".$master->type."'";
				if(!empty($master->motor_mode)) $sql_add .= ", motor_mode = '".$master->motor_mode."'";
				if(!empty($master->headquarter)) $sql_add .= ", headquarter = '".$master->headquarter."'";
				if(!empty($master->length)) $sql_add .= ", length = '".$master->length."'";
				if(!empty($master->width)) $sql_add .= ", width = '".$master->width."'";
				if(!empty($master->height)) $sql_add .= ", height = '".$master->height."'";
				if(!empty($master->max_load)) $sql_add .= ", max_load = '".$master->max_load."'";
				if(!empty($master->fuel_type)) $sql_add .= ", fuel_type = '".$master->fuel_type."'";
				if(!empty($master->carinfo_9)) $sql_add .= ", carinfo_9 = '".$master->carinfo_9."'";
				if(!empty($master->carinfo_11)) $sql_add .= ", carinfo_11 = '".$master->carinfo_11."'";
				if(!empty($master->baecha_co_id)) $sql_add .= ", baecha_co_id = '".$master->baecha_co_id."'";
				if(!empty($master->baecha_co_name)) $sql_add .= ", baecha_co_name = '".$master->baecha_co_name."'";
				if(!empty($master->ws_co_id)) $sql_add .= ", ws_co_id = '".$master->ws_co_id."'";
				if(!empty($master->ws_co_name)) $sql_add .= ", ws_co_name = '".$master->ws_co_name."'";
				if(!empty($master->inv_co_id)) $sql_add .= ", inv_co_id = '".$master->inv_co_id."'";
				if(!empty($master->inv_co_name)) $sql_add .= ", inv_co_name = '".$master->inv_co_name."'";
			
				$sql = "INSERT INTO tbl_asset_truck_info  SET tr_id='$tr_id', reason = '최초등록'".$sql_add;
				$sql .= ",reg_datetime=now(), is_master='Y'";
				$this->db->query($sql);
				$data['qry'] = $sql;
			}

       // $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
	//	$this->asset_model->db->where('tbl_asset_truck.idx', $tr_id);
     //   $this->asset_model->_order_by = 'idx';
        //$data['cur_info'] = $this->asset_model->get();
			//$data['test'] = $cur_info['car_1'].'차랑정보관리';
			$data['title'] = '차랑정보관리';
			$this->asset_model->_table_name = 'tbl_asset_truck_info'; //table name
			$this->asset_model->db->where('tbl_asset_truck_info.tr_id', $tr_id); //->where('tbl_asset_truck_info.isq_master', 'Y');
			$this->asset_model->_order_by = 'reg_datetime desc';
			$data['all_info_group'] = $this->asset_model->get();
		} else if($md == "car_ins") { 
			if ($action == 'edit_ins_info') {
				$data['spay_cnt']		= "";
				if(!empty($this->input->post('spay_cnt', true))) $data['spay_cnt']		= $this->input->post('spay_cnt', true); 
				if(!empty($data['ins_co_id'])) $data['ins_co_id'] = $this->input->post('ins_co_id', true);
				
				if(!empty($this->input->post('ins_id', true))) {
					$data['ins_id']		= $this->input->post('ins_id', true);
					$data['ins'] = $this->db->where('idx', $data['ins_id'])->get('tbl_asset_truck_insur')->row();
					if(!empty($data['ins']->spay_cnt)) $data['spay_cnt'] = $data['ins']->spay_cnt;
					if(!empty($this->input->post('spay_cnt', true))) $data['spay_cnt']	= $this->input->post('spay_cnt', true);
					$data['ins_co_id']				= $data['ins']->ins_co_id;
				}

				$data['active'] = 2;
			} else {
				$data['active'] = 1;
			}

			$data['title'] = '자동차보험관리';
			$this->asset_model->db->select('ti.*, ic.deptname');
			$this->asset_model->_table_name = 'tbl_asset_truck_insur ti'; //table name
			$this->asset_model->db->where('ti.tr_id', $tr_id)->where('ti.type', '자동차');
			$this->asset_model->db->join('tbl_insur_company ic', 'ic.idx = ti.ins_co_id', 'left');
			$this->asset_model->_order_by = 'ti.end_date desc';
			$data['all_ins_group'] = $this->asset_model->get();
		} else if($md == "car_lins") {

			if ($action == 'car_lins_edit') {
				$data['spay_cnt']		= $this->input->post('spay_cnt', true);
				if(!empty($data['spay_cnt'])) {
					$data['ins_co_id']		= $this->input->post('ins_co_id', true);
				} 
				
				$data['lins_id']		= $this->input->post('lins_id', true);
				if(!empty($data['lins_id'])) {
					$data['lins'] = $this->db->where('idx', $data['lins_id'])->get('tbl_asset_truck_insur')->row();
					$data['ins_co_id']		= $data['lins']->ins_co_id;
				}

				$data['active'] = 2;
			} else {
				$data['active'] = 1;
			}

			$data['title'] = '적재물보험관리';
			$this->asset_model->_table_name = 'tbl_asset_truck_insur'; //table name
			$this->asset_model->db->where('tbl_asset_truck_insur.tr_id', $tr_id)->where('tbl_asset_truck_insur.type', '적재물');
			$this->asset_model->_order_by = 'end_date desc';
			$data['all_lins_group'] = $this->asset_model->get();
		} else if($md == "car_pch") {
			$data['title'] = '구매관리';

			$data['idx'] = $this->input->post('idx', true);
		} else if($md == "car_sell") $data['title'] = '매각정보';
		else if($md == "car_chk") {
			if ($action == 'edit_lins_info') {
				$data['spay_cnt']		= $this->input->post('spay_cnt', true);
				$data['ins_co_id']		= $this->input->post('ins_co_id', true);
				$data['closing']		= $this->input->post('closing', true);
				$data['cls_pay_date']	= $this->input->post('cls_pay_date', true);
				$data['cls_gj_year']	= $this->input->post('cls_gj_year', true);
				$data['cls_gj_month']	= $this->input->post('cls_gj_month', true);

				//대인배상 Ⅰ
				$data['person1'] = $this->input->post('person1', true);
				$data['person2'] = $this->input->post('person2', true);
				$data['object'] = $this->input->post('object', true);
				$data['self_body'] = $this->input->post('self_body', true);
				$data['self_car'] = $this->input->post('self_car', true);
				$data['call_svc'] = $this->input->post('call_svc', true);
				$data['start_date'] = $this->input->post('start_date', true);
				$data['end_date'] = $this->input->post('end_date', true);

				$data['active'] = 2;
			} else {
				$data['active'] = 1;
			}
			$data['title'] = '정기(정밀)검사';
			$this->asset_model->_table_name = 'tbl_asset_truck_check'; //table name
			$this->asset_model->db->where('tbl_asset_truck_check.tr_id', $tr_id);
			$this->asset_model->_order_by = 'change_date';
			$data['all_check_group'] = $this->asset_model->get();
		} else if($md == "car_own") $data['title'] = '차주정보관리';
		
		$data['subview'] = $this->load->view('admin/asset/pop_'.$md, $data, true);
        $this->load->view('admin/_layout_pop_car', $data);
    }

    public function pop_del_carinfo($tr_id = NULL, $info_id = NULL)
    {
		if(!empty($info_id)) {
			$sql = "delete from tbl_asset_truck_info where idx='".$info_id."'";
			$this->db->query($sql);

			$message = "자동차 정보가 저장되었습니다.";
			$type = 'success';
				
			set_message($type, $message);
		}
        redirect('/admin/asset/pop_carinfo/car_info/'.$tr_id); //redirect page
	}


    public function pop_save_carinfo($action = NULL, $tr_id = NULL, $info_id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');

		//위수탁사 소속체크
		$sql_belongs = "";
		if(!empty($this->input->post('ws_co_id'))) {
			$ws = $this->db->where('dp_id', $this->input->post('ws_co_id'))->get('tbl_members')->row();
			if($ws->mb_type == "group") $belongs = "W"; else $belongs = "O";
			$sql_belongs = ",belongs = '".$belongs."'";
		}

		$tr = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
		$sql_files = "";
        if (!empty($_FILES['attach1']['name'])) {
            $val = $this->asset_model->customUploadFile('attach1','car_file');
            //$val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
			$sql_files .= ", attach1 = '".$val['path']."'";
        }
        if (!empty($_FILES['attach2']['name'])) {
            $val = $this->asset_model->customUploadFile('attach2','car_file');
            //$val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
			$sql_files .= ", attach2 = '".$val['path']."'";
        }
        if (!empty($_FILES['attach3']['name'])) {
            $val = $this->asset_model->customUploadFile('attach3','car_file');
            //$val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
			$sql_files .= ", attach3 = '".$val['path']."'";
        }
        if (!empty($_FILES['attach4']['name'])) {
            $val = $this->asset_model->customUploadFile('attach4','car_file');
            //$val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
			$sql_files .= ", attach4 = '".$val['path']."'";
        }
        if (!empty($_FILES['attach5']['name'])) {
            $val = $this->asset_model->customUploadFile('attach5','car_file');
            //$val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
			$sql_files .= ", attach5 = '".$val['path']."'";
        }
        if (!empty($_FILES['cert_file']['name'])) {
            $val = $this->asset_model->customUploadFile('cert_file','car_file');
           // $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
			$sql_files .= ", cert_file = '".$val['path']."'";
        }
		$sql_add = "";
		if(!empty($this->input->post('car_type', true))) $sql_add .= ", type = '".$this->input->post('car_type')."'";
		if(!empty($this->input->post('owner_id', true))) $sql_add .= ", owner_id = '".$this->input->post('owner_id')."'";
		if(!empty($this->input->post('owner_name', true))) $sql_add .= ", owner_name = '".$this->input->post('owner_name')."'";
		if(!empty($this->input->post('driver_name', true))) $sql_add .= ", driver_name = '".$this->input->post('driver_name')."'";
		//if(!empty($this->input->post('car_1', true))) $sql_add .= ", car_1 = '".$this->input->post('car_1')."'";
		if(!empty($this->input->post('car_7', true))) $sql_add .= ", car_7 = '".$this->input->post('car_7')."'";
		if(!empty($this->input->post('car_3', true))) $sql_add .= ", car_3 = '".$this->input->post('car_3')."'";
		if(!empty($this->input->post('car_4', true))) $sql_add .= ", car_4 = '".$this->input->post('car_4')."'";
		if(!empty($this->input->post('car_mode', true))) $sql_add .= ", mode = '".$this->input->post('car_mode')."'";
		if(!empty($this->input->post('car_5', true))) $sql_add .= ", car_5 = '".$this->input->post('car_5')."'";
		if(!empty($this->input->post('motor_mode', true))) $sql_add .= ", motor_mode = '".$this->input->post('motor_mode')."'";
		if(!empty($this->input->post('headquarter', true))) $sql_add .= ", headquarter = '".$this->input->post('headquarter')."'";
		if(!empty($this->input->post('length', true))) $sql_add .= ", length = '".$this->input->post('length')."'";
		if(!empty($this->input->post('width', true))) $sql_add .= ", width = '".$this->input->post('width')."'";
		if(!empty($this->input->post('height', true))) $sql_add .= ", height = '".$this->input->post('height')."'";
		if(!empty($this->input->post('max_load', true))) $sql_add .= ", max_load = '".$this->input->post('max_load')."'";
		if(!empty($this->input->post('fuel_type', true))) $sql_add .= ", fuel_type = '".$this->input->post('fuel_type')."'";
		if(!empty($this->input->post('carinfo_9', true))) $sql_add .= ", carinfo_9 = '".$this->input->post('carinfo_9')."'";
		if(!empty($this->input->post('carinfo_11', true))) $sql_add .= ", carinfo_11 = '".$this->input->post('carinfo_11')."'";
		if(!empty($this->input->post('baecha_co_id', true))) $sql_add .= ", baecha_co_id = '".$this->input->post('baecha_co_id')."'";
		if(!empty($this->input->post('baecha_co_name', true))) $sql_add .= ", baecha_co_name = '".$this->input->post('baecha_co_name')."'";
		if(!empty($this->input->post('ws_co_id', true))) $sql_add .= ", ws_co_id = '".$this->input->post('ws_co_id')."'";
		if(!empty($this->input->post('ws_co_name', true))) $sql_add .= ", ws_co_name = '".$this->input->post('ws_co_name')."'";
		if(!empty($this->input->post('inv_co_id', true))) $sql_add .= ", inv_co_id = '".$this->input->post('inv_co_id')."'";
		if(!empty($this->input->post('inv_co_name', true))) $sql_add .= ", inv_co_name = '".$this->input->post('inv_co_name')."'";

				// 차주할당대기 차량 -> 상태 변경  , 이력 저장필요
		$sql_status = "";
		if($tr->car_status == 'N') {
			$sql_status = ",car_status='A'";
		}

		if($action == "edit") {
			$car_1 = "";
			if(!empty($this->input->post('car_1'))) $car_1 = $this->input->post('car_1');
			if(!empty($this->input->post('change_date'))) $change_date = $this->input->post('change_date');
			if($info_id) {
				$sql = "update tbl_asset_truck_info SET car_1='$car_1', change_date='$change_date' ".$sql_add.$sql_files.$sql_belongs." where idx='".$info_id."'";
				$this->db->query($sql);
			}

			if($tr_id) {
				//$sql = "update tbl_asset_truck SET car_1='$car_1' ".$sql_add." where idx='".$tr_id."'";
				//$this->db->query($sql);
			}

			if(!empty($this->input->post('is_master')) && $this->input->post('is_master') == "Y") {
				// 3. 마스터 업데이트
				$sql = "UPDATE tbl_asset_truck SET car_1='$car_1'".$sql_add.$sql_files.$sql_status.$sql_belongs;
				$sql .= " WHERE idx='$tr_id'";
				$this->db->query($sql);
			}
		} else {
			$car_1 = "";
			if(!empty($this->input->post('car_1'))) $car_1 = $this->input->post('car_1');
			if(!empty($this->input->post('change_date'))) $change_date = $this->input->post('change_date');

			$sql_keep = "";
			if(!empty($this->input->post('keep_car_1', true))) $sql_keep .= ", keep_car_1 = '".$this->input->post('keep_car_1')."'";
			if(!empty($this->input->post('keep_owner', true))) $sql_keep .= ", keep_owner = '".$this->input->post('keep_owner')."'";
			if(!empty($this->input->post('keep_type', true))) $sql_keep .= ", keep_type = '".$this->input->post('keep_type')."'";
			if(!empty($this->input->post('keep_car_3', true))) $sql_keep .= ", keep_car_3 = '".$this->input->post('keep_car_3')."'";
			if(!empty($this->input->post('keep_car_4', true))) $sql_keep .= ", keep_car_4 = '".$this->input->post('keep_car_4')."'";
			if(!empty($this->input->post('keep_mode', true))) $sql_keep .= ", keep_mode = '".$this->input->post('keep_mode')."'";
			if(!empty($this->input->post('keep_car_5', true))) $sql_keep .= ", keep_car_5 = '".$this->input->post('keep_car_5')."'";
			if(!empty($this->input->post('keep_car_7', true))) $sql_keep .= ", keep_car_7 = '".$this->input->post('keep_car_7')."'";
			if(!empty($this->input->post('keep_motor_mode', true))) $sql_keep .= ", keep_motor_mode = '".$this->input->post('keep_motor_mode')."'";
			if(!empty($this->input->post('keep_headquarter', true))) $sql_keep .= ", keep_headquarter = '".$this->input->post('keep_headquarter')."'";
			if(!empty($this->input->post('keep_carinfo', true))) $sql_keep .= ", keep_carinfo = '".$this->input->post('keep_carinfo')."'";
			if(!empty($this->input->post('keep_baecha_co_id', true))) $sql_keep .= ", keep_baecha_co_id = '".$this->input->post('keep_baecha_co_id')."'";
			if(!empty($this->input->post('keep_inv_co_id', true))) $sql_keep .= ", keep_inv_co_id = '".$this->input->post('keep_inv_co_id')."'";
			if(!empty($this->input->post('keep_ws_co_id', true))) $sql_keep .= ", keep_ws_co_id = '".$this->input->post('keep_ws_co_id')."'";
			if(!empty($this->input->post('keep_carinfo_9', true))) $sql_keep .= ", keep_carinfo_9 = '".$this->input->post('keep_carinfo_9')."'";
			if(!empty($this->input->post('keep_carinfo_11', true))) $sql_keep .= ", keep_carinfo_11 = '".$this->input->post('keep_carinfo_11')."'";

			//반납예정관련
			if(!empty($this->input->post('return_ready', true))) $sql_add .= ", return_ready = '".$this->input->post('return_ready')."'";
			if(!empty($this->input->post('return_due_date', true))) $sql_add .= ", return_due_date = '".$this->input->post('return_due_date')."'";
			if(!empty($this->input->post('return_cls_date', true))) $sql_add .= ", return_cls_date = '".$this->input->post('return_cls_date')."'";

		
			if (!empty($created) || !empty($edited) || !empty($tr_id)) {

				// 1. 기존이력 is_master = '0'
				$sql = "update tbl_asset_truck_info  SET is_master='N' where is_master='Y' and tr_id='$tr_id'";
				$this->db->query($sql);

				// 2. 새 이력 삽입 $sql_keep
				$sql = "INSERT INTO tbl_asset_truck_info  SET tr_id='$tr_id', car_1='$car_1'".$sql_add.$sql_files.$sql_belongs;
				$sql .= ",reg_datetime='".date('Y-m-d h:i:s')."', is_master='Y'";
				if(!empty($this->input->post('reason', true))) $sql .= ", reason = '".$this->input->post('reason')."'";
				if(!empty($this->input->post('remark', true))) $sql .= ", remark = '".$this->input->post('remark')."'";
				$this->db->query($sql);


				// 3. 마스터 업데이트
				$sql = "UPDATE tbl_asset_truck  SET car_1='$car_1'".$sql_add.$sql_files.$sql_status.$sql_belongs;
				$sql .= " WHERE idx='$tr_id'";
				$this->db->query($sql);

				// 

				$message = $sql."자동차 정보가 저장되었습니다.";
				$type = 'success';
				
				set_message($type, $message);
			}
		}

		//차주할당 대기(N)의 경우 차주등록시 -> 차주할당대기 리스트에서 사라지고 차량자산목록(A)으로 등재
		if((!empty($this->input->post('car_status')) && $this->input->post('car_status') == "N") && !empty($this->input->post('inv_co_id'))) {
			$sql = "UPDATE tbl_asset_truck SET car_status='A' WHERE idx='$tr_id'";
			$this->db->query($sql);
		}

        redirect('/admin/asset/pop_carinfo/car_info/'.$tr_id); //redirect page
	}

	// 팝업 자동차보험 정보 저장
    public function pop_save_carinfo_ins($id = NULL)
    {
		$tr_id		= $id;
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
		$ins_id = $this->input->post('ins_id', true);
        if (!empty($created) || !empty($edited)) {
			$sql_upload = "";
            $this->asset_model->_table_name = 'tbl_asset_truck_insur'; //table name
            if(!empty($ins_id)) $result = $this->asset_model->get_by(array('idx' => $ins_id), true);

            if (!empty($_FILES['cert_file1']['name'])) {
                $val = $this->asset_model->customUploadFile('cert_file1','car_file');
                //$val == TRUE || redirect('pop_carinfo/car_ins/'.$tr_id.'/edit_ins_info');
				$sql_upload .= ", cert_file1 = '".$val['path']."'";
            }

			if (!empty($_FILES['cert_file2']['name'])) {
                $val = $this->asset_model->customUploadFile('cert_file2','car_file');
               // $val == TRUE || redirect('pop_carinfo/car_ins/'.$tr_id.'/edit_ins_info');
				$sql_upload .= ", cert_file2 = '".$val['path']."'";
            }
            if (!empty($_FILES['cert_file3']['name'])) {
                $val = $this->asset_model->customUploadFile('cert_file3','car_file');
              //  $val == TRUE || redirect('pop_carinfo/car_ins/'.$tr_id.'/edit_ins_info');
				$sql_upload .= ", cert_file3 = '".$val['path']."'";
            }

			
			if (!empty($result)) {  
				$sql = "UPDATE tbl_asset_truck_insur SET ";
				$sql .= "type = '자동차'";
				$sql .= ",tr_id		= '".$tr_id."'";
				$sql .= ",ins_co_id	= '".$this->input->post('ins_co_id', true)."'";
				$sql .= ",active_yn	= '".$this->input->post('active_yn', true)."'";
				$sql .= ",pay_cnt	= '".$this->input->post('spay_cnt', true)."'";
				$sql .= ",person1	= '".$this->input->post('person1', true)."'";
				$sql .= ",person2	= '".$this->input->post('person2', true)."'";
				$sql .= ",closing	= '".$this->input->post('closing', true)."'";
				$sql .= ",cls_pay_date = '".$this->input->post('cls_pay_date', true)."'";
				$sql .= ",cls_gj_date = '".$this->input->post('cls_gj_date', true)."'";
				$sql .= ",exp_date	= '".$this->input->post('exp_date', true)."'";
				$sql .= ",total		= '".$this->input->post('tot_amount', true)."'";
				$sql .= ",object	= '".$this->input->post('object', true)."'";
				$sql .= ",self_body = '".$this->input->post('self_body', true)."'";
				$sql .= ",self_car	= '".$this->input->post('self_car', true)."'";
				$sql .= ",call_svc	= '".$this->input->post('call_svc', true)."'";
				$sql .= ",etc		= '".$this->input->post('etc', true)."'";
				$sql .= ",start_date = '".$this->input->post('start_date', true)."'";
				$sql .= ",end_date	= '".$this->input->post('end_date', true)."'";
				$sql .= ",co_pay	= '".$this->input->post('co_pay', true)."'" . $sql_upload;
				$sql .= " WHERE idx = '".$ins_id."'";
				$this->db->query($sql);
			} else {
				$sql = "UPDATE tbl_asset_truck_insur  SET active_yn ='N' WHERE tr_id='$tr_id' and type='자동차'";
				$this->db->query($sql);

				//,closing='$closing',cls_pay_date='$cls_pay_date',cls_gj_date='{$cls_gj_year}-{$cls_gj_month}',exp_date='$exp_date'
				$sql = "INSERT INTO tbl_asset_truck_insur  SET type='자동차'";
				$sql .= ",tr_id	= '".$tr_id."'";
				$sql .= ",ins_co_id	= '".$this->input->post('ins_co_id', true)."'";
				$sql .= ",pay_cnt	= '".$this->input->post('spay_cnt', true)."'";
				$sql .= ",person1	= '".$this->input->post('person1', true)."'";
				$sql .= ",person2	= '".$this->input->post('person2', true)."'";
				$sql .= ",closing	= '".$this->input->post('closing', true)."'";
				$sql .= ",cls_pay_date = '".$this->input->post('cls_pay_date', true)."'";
				$sql .= ",cls_gj_date = '".$this->input->post('cls_gj_date', true)."'";
				$sql .= ",exp_date	= '".$this->input->post('exp_date', true)."'";
				$sql .= ",total		= '".$this->input->post('tot_amount', true)."'";
				$sql .= ",object	= '".$this->input->post('object', true)."'";
				$sql .= ",self_body = '".$this->input->post('self_body', true)."'";
				$sql .= ",self_car	= '".$this->input->post('self_car', true)."'";
				$sql .= ",call_svc	= '".$this->input->post('call_svc', true)."'";
				$sql .= ",etc		= '".$this->input->post('etc', true)."'";
				$sql .= ",start_date = '".$this->input->post('start_date', true)."'";
				$sql .= ",end_date	= '".$this->input->post('end_date', true)."'";
				$sql .= ",active_yn = 'Y'";
				$sql .= ",co_pay	= '".$this->input->post('co_pay', true)."'" . $sql_upload;
				$this->db->query($sql);
				$ins_id = $this->db->insert_id(); 
			}
			$data['qry'] = $sql;
			$data['cnt'] = count($this->input->post('chk[]', true));


			//하위정보 저장
			$this->db->query("delete from tbl_asset_truck_insur_sub where pid='{$ins_id}'");
			//$data['subdata'] = "";
			//$subdatas = "";
			for ($i=0; $i<count($this->input->post('chk[]', true)); $i++) 
			{
				// 실제 번호를 넘김 
				$k = $this->input->post('chk['.$i.']', true);
				//echo ("$i <br/>");

				$pay_cnt = $this->input->post('pay_cnt['.$i.']', true);
				$pay_amt = $this->input->post('pay_amt['.$i.']', true);
				//$pay_year = $this->input->post('pay_year['.$i.']', true);
				//$pay_month = $this->input->post('pay_month['.$i.']', true);

				$pay_date = $this->input->post('pay_date['.$i.']', true);
				$gj_month = $this->input->post('gj_month['.$i.']', true);
				//$cls_date = $this->input->post('cls_date['.$i.']', true); 
				$pay_by = $this->input->post('pay_by['.$i.']', true);

				$sql = "INSERT INTO tbl_asset_truck_insur_sub SET pid='$ins_id',tr_id='$tr_id',pay_amt='$pay_amt',pay_cnt='$pay_cnt',gj_month='$gj_month',pay_date='$pay_date',pay_by='$pay_by'";
				$this->db->query($sql);
				//$subdatas .= "<br/>".$sql;
			}
			//$data['subdata'] = $subdatas;

			//echo $sql."<br/>";

			$message = "자동차보험 정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
		
		//$data['qry'] = $sql."<br/>";
        redirect('admin/asset/pop_carinfo/car_ins/'.$tr_id); //redirect page
		//$data['subview'] = $this->load->view('admin/asset/test', $data, true);
        //$this->load->view('admin/_layout_pop_car', $data);
    }

    public function save_car($id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
            $result = $this->asset_model->get_by(array('idx' => $id), true);

			//위수탁사 소속체크
			$belongs = "";
			if(!empty($this->input->post('ws_co_id'))) {
				$ws = $this->db->where('dp_id', $this->input->post('ws_co_id'))->get('tbl_members')->row();
				if($ws->mb_type == "group") $belongs = "W"; else $belongs = "O";
			}

			$car_status = "N";
			if(!empty($this->input->post('inv_co_id', true))) $car_status = "A";
            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",p_tr_id='".$id."'";
				$sql .= ",belongs='".$belongs."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co_id='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('car_type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",car_status='".$car_status."'";
				$sql .= ",reg_datetime='".date('Y-m-d h:i:s')."'" ;
				$sql .= " WHERE idx = '".$id."'";
				$this->db->query($sql);
			} else {
				$sql = "INSERT INTO tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",belongs='".$belongs."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co_id='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('car_type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",car_status='".$car_status."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'" ;
				$this->db->query($sql);
				$id = $this->db->insert_id();

				// 요청 취소됨----나중에 삭제여부 확인
				$sql = "UPDATE tbl_asset_truck SET p_tr_id='".$id."' WHERE idx = '".$id."'";
				$this->db->query($sql);
			}

			$sql_files = "";
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
               // $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach1 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach2']['name'])) {
                $val = $this->asset_model->customUploadFile('attach2','car_file');
               // $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach2 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach3']['name'])) {
                $val = $this->asset_model->customUploadFile('attach3','car_file');
               // $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach3 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach4']['name'])) {
                $val = $this->asset_model->customUploadFile('attach4','car_file');
              //  $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach4 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach5']['name'])) {
                $val = $this->asset_model->customUploadFile('attach5','car_file');
              //  $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach5 = '".$val['path']."'";
            }
            if (!empty($_FILES['cert_file']['name'])) {
                $val = $this->asset_model->customUploadFile('asset_model','car_file');
             //   $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", cert_file = '".$val['path']."'";
            }

			$sql = "UPDATE tbl_asset_truck SET p_tr_id='".$id."'".$sql_files." WHERE idx = '".$id."'";
			$this->db->query($sql);
/*
            $this->asset_model->_table_name = 'tbl_asset_truck_files';
            $result = $this->asset_model->get_by(array('tr_id' => $id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'";
				$sql .= $sql_files;
				$sql .= " WHERE tr_id = '".$id."'";
			} else {
				$sql = "INSERT INTO tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'" . $sql_files;
			}
			$this->db->query($sql);
*/
			$message = "차량정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }

		$redir = $this->input->post('redir', true);
		if(empty($redir)) $redir = "car_list_all";
        redirect('admin/asset/'.$redir.'/'.$ws_co_id); //redirect page
    } 

	// 정보등록대기 차량 정보등록
	public function save_carinfo($tr_id = NULL)
	{
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

			$pidx = $this->input->post('pidx', true);
			$sql_files = "";
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach1 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach2']['name'])) {
                $val = $this->asset_model->customUploadFile('attach2','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach2 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach3']['name'])) {
                $val = $this->asset_model->customUploadFile('attach3','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach3 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach4']['name'])) {
                $val = $this->asset_model->customUploadFile('attach4','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach4 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach5']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach5 = '".$val['path']."'";
            }

			$belongs = "";
			if(!empty($ws_co_id)) {
				$ws = $this->db->where('dp_id', $ws_co_id)->get('tbl_members')->row();
				if($ws->mb_type == "group") $belongs = "W"; else $belongs = "O";
			}

            $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
            $result = $this->asset_model->get_by(array('idx' => $id), true);

				$sql = "UPDATE tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",belongs='".$belongs."'";
				$sql .= ",car_status='A'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= "WHERE idx ='".$tr_id."'";
				$this->db->query($sql);
				$id = $this->db->insert_id();

            $this->asset_model->_table_name = 'tbl_asset_truck_files';
            $result = $this->asset_model->get_by(array('tr_id' => $id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'";
				$sql .= $sql_files;
				$sql .= " WHERE tr_id = '".$id."'";
			} else {
				$sql = "INSERT INTO tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'" . $sql_files;
			}
			$this->db->query($sql);


			$message = "차량정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/asset/car_ready_list/'.$ws_co_id); //redirect page
	}

    public function save_GTcar($id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

			$sql_files = "";
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach1 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach2']['name'])) {
                $val = $this->asset_model->customUploadFile('attach2','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach2 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach3']['name'])) {
                $val = $this->asset_model->customUploadFile('attach3','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach3 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach4']['name'])) {
                $val = $this->asset_model->customUploadFile('attach4','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach4 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach5']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/asset/car_list/'.$ws_co_id);
				$sql_files .= ", attach5 = '".$val['path']."'";
            }

            $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
            $result = $this->asset_model->get_by(array('idx' => $id), true);

			//공T/E현황에서 -> 신규등록 클릭 -> 기존정보 이력으로 남고 -> 신규차량생성되면서 차주할당대기로 이동

				$sql = "INSERT INTO tbl_asset_truck SET ";
				$sql .= "p_tr_id='".$this->input->post('pidx', true)."'";
				$sql .= ",gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co_id='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",car_status='N'".$sql_files;
				$this->db->query($sql);
				$id = $this->db->insert_id();

// 단일테이블로 처리
//            $this->asset_model->_table_name = 'tbl_asset_truck_files';
//           $result = $this->asset_model->get_by(array('tr_id' => $id), true);

            if (!empty($this->input->post('pidx', true))) { // 기존차량 이력으로 이동
				$sql = "UPDATE tbl_asset_truck SET car_status='H' WHERE idx = '".$this->input->post('pidx', true)."'";
				$this->db->query($sql);
			}

			$message = "차량정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/asset/car_gongt_list/'.$ws_co_id); //redirect page
    } 

    public function car_registration($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차등록증재교부';
        $data['subview'] = $this->load->view('admin/asset/car_registration', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_daepae($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차대패신청';
        $data['subview'] = $this->load->view('admin/asset/car_daepae', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_transfer($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차이전등록';
        $data['subview'] = $this->load->view('admin/asset/car_transfer', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_info($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차정보변경';
        $data['subview'] = $this->load->view('admin/asset/car_info', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_insur($action = NULL, $id = NULL)
    {
        $tr_id = $id;
        $data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "ALL";
        $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
        if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '만기 보험 현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver, mb.N, mb.O');
		$this->asset_model->db->join('tbl_members mb', 'mb.dp_id = at.inv_co_id', 'left');
		$this->asset_model->db->join('tbl_asset_truck_insur ins', 'ins.tr_id = at.idx', 'left');
		$today = $data['df_month']."-".date("d"); 
		$before= date("Y-m-d", strtotime($today.'-1 month')); 
		$after = date("Y-m-d", strtotime($today.'+1 month')); 
        $this->asset_model->db->where('ins.active_yn', 'Y')->where('ins.type', '자동차')->where('ins.end_date >=', $before)->where('ins.end_date <=', $after); //
        $this->asset_model->db->where('at.is_deleted', 'N');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			//일시적용 수정요망
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_insur', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_inspection($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차검사 목록';
        $data['subview'] = $this->load->view('admin/asset/car_inspection', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_garage($action = NULL, $id = NULL)
    {
        $member_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->asset_model->can_action('tbl_members', 'edit', array('asset_id' => $id));
			$this->db->where('member_id', $asset_id)->get('tbl_member')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

		$data['title'] = '차고지 목록';

        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_id';
        $data['all_garage_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_garage', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 
	
    public function car_assign_ready($action = NULL, $id = NULL)
    {

        $tr_id = $id;
        $data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "CSR";
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '차주할당대기차량 목록';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at'; //table name
        $this->asset_model->db->select('at.*');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'N');
		$this->asset_model->_order_by = 'car_1';
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        // get all language
//        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->asset_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->asset_model->get_permission('tbl_assets');

        $data['all_designation_info'] = $this->asset_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/asset/car_assign_ready', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }    
	
    public function car_list($action = NULL, $id = NULL)
    {

        $tr_id = $id;
        $data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "ALL";
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
		//임시처리 수정요망
		if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
        if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '차량자산 목록';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at'; //table name
		$this->asset_model->db->select('at.*, dp.co_name, dp.ceo, dp.driver, dp.N, dp.O, bc.co_name as bc_co_name'); //, bc.co_name as bc_co_name');
		$this->asset_model->db->join('tbl_members dp', 'dp.dp_id = at.inv_co_id', 'left');
		$this->asset_model->db->join('tbl_members bc', '(at.baecha_co_id = bc.code and bc.mb_type = \'customer\')', 'left');


		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('dp.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.gongT_yn', 'N')->where('at.car_status', 'A');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        // get all language
//        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->asset_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->asset_model->get_permission('tbl_assets');

        $data['all_designation_info'] = $this->asset_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/asset/car_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }    
	
    public function car_regular_inspection($action = NULL, $id = NULL)
    {

        $tr_id = $id;
        $data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "ALL";
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
		//임시처리 수정요망
		if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
        if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '정기검사 대기현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at, tbl_members mb'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('mb.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.gongT_yn', 'N')->where('at.car_status', 'A')->where('0');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_regular_inspection', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }    


    public function car_list_all($action = NULL, $page = NULL, $filter1 = NULL, $filter2 = NULL, $ws_co_id = NULL, $baecha_co_id = NULL, $search_keyword = NULL, $rows = NULL, $id = NULL)
    {
        $data['title']			= '차량 목록';
        $data['page']			= (!empty($page))?$page:1;
		$data['filter1']			= (!empty($filter1))?$filter1:"A";
		$data['filter2']			= (!empty($filter2))?$filter2:"ALL";
		$data['ws_co_id']		= (empty($ws_co_id) || $ws_co_id == "all")?"":$ws_co_id;
		$data['baecha_co_id']	= $baecha_co_id;
		$data['search_keyword']	= $search_keyword;
		$data['action']			= $action;
		$data['rows']			= (!empty($rows))?$rows:"20";

		$tr_id					= $id;

        if ($action == 'edit') {
            $data['active'] = 2;
            $can_edit = $this->asset_model->can_action('tbl_asset_truck', 'edit', array('idx' => $tr_id));
			//$this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
            }
        } else {
            $data['active'] = 1;
        }
		
		if(empty($data['from_record'])) $data['from_record'] = 0;
        $this->asset_model->_table_name = 'tbl_asset_truck tr'; //table name          
		$this->asset_model->db->join('tbl_members dp', 'dp.dp_id = tr.inv_co_id', 'left');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->asset_model->db->where('(car_1 like \'%'.$data['search_keyword'].'%\' OR car_7 like \'%'.$data['search_keyword'].'%\' OR ws_co_name like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR inv_co_name like \'%'.$data['search_keyword'].'%\')' );
		}
		if(!empty($data['ws_co_id'])) $this->asset_model->db->or_where('tr.ws_co_id', $data['ws_co_id'] );
		if(!empty($data['filter1']) && $data['filter1']<>"A") $this->asset_model->db->where('tr.belongs', $data['filter1'] );
        $this->asset_model->db->where('tr.is_deleted', 'N')->where('tr.car_status', 'A');
        //$this->asset_model->db->where('dp.del_yn', 'N'); //->where('dp.partner_status', 'Y');
/*
        $this->asset_model->_table_name = 'tbl_asset_truck'; //table name          
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->asset_model->db->or_where('car_1 like', '%'.$data['search_keyword'].'%' )->or_where('car_7 like', '%'.$data['search_keyword'].'%' )->or_where('ws_co_name like', '%'.$data['search_keyword'].'%' )->or_where('inv_co_name like', '%'.$data['search_keyword'].'%' );
		}
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->or_where('ws_co_id', $data['ws_co_id'] );
		}
        $this->asset_model->db->where('is_deleted', 'N')->where('gongT_yn', 'N');
*/
		$data['total_count'] = count($this->asset_model->get());

		$data['total_page']  = ceil($data['total_count'] / $data['rows']);  // 전체 페이지 계산
		$data['from_record'] = ($data['page'] - 1) * $data['rows']; // 시작 열을 구함

        $this->asset_model->_table_name = 'tbl_asset_truck tr'; //table name          
		$this->asset_model->db->select('tr.*, dp.co_name as ow_co_name, dp.ceo, dp.N, dp.O, bc.co_name as bc_co_name'); //, bc.co_name as bc_co_name');
		$this->asset_model->db->join('tbl_members dp', 'dp.dp_id = tr.inv_co_id', 'left');
		$this->asset_model->db->join('tbl_members bc', '(tr.baecha_co_id = bc.code and bc.mb_type = \'customer\')', 'left');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->asset_model->db->where('(car_1 like \'%'.$data['search_keyword'].'%\' OR car_7 like \'%'.$data['search_keyword'].'%\' OR ws_co_name like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR inv_co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\')' );

		}
		if(!empty($data['ws_co_id'])) $this->asset_model->db->where('tr.ws_co_id', $data['ws_co_id'] );
		if(!empty($data['filter1']) && $data['filter1']<>"A") $this->asset_model->db->where('tr.belongs', $data['filter1'] );
        $this->asset_model->db->where('tr.is_deleted', 'N')->where('tr.car_status', 'A');
        //$this->asset_model->db->where('dp.del_yn', 'N'); //->where('dp.partner_status', 'Y');

        $this->asset_model->_order_by = 'tr.car_1';
		if ($action != 'excel') $this->asset_model->db->limit($data['rows'], $data['from_record']);
        $data['all_car_info'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();


         if ($action == 'excel') {
			$data['subview'] = $this->load->view('admin/asset/car_list_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
		} else {
			$data['subview'] = $this->load->view('admin/asset/car_list_all', $data, true);
			$this->load->view('admin/_layout_main_nd', $data);
		}
    }

	

}
