<?php

class Dispatcher extends CI_Controller
{

    /**
     * Dispatcher constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('login_model');
    }

    public function index() {

        $dashboard = $this->session->userdata('url');

        $this->login_model->loggedin() == FALSE && redirect($dashboard);

        $this->load->view('../../dispatcher/index.html');
    }
}
?>