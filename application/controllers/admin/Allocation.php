<?php
/**
 * Description of Allocation
 *
 * @author yo
 */
class Allocation extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('allocation_model');
    }

    public function alloc_request($id=NULL)
    {
        $data['title'] = "배차요청";
        $data['permission_user'] = $this->allocation_model->all_permission_user('36');
		$data['branch_id'] = $id;
		if(empty($id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		//지사목록
		$filterBy = null;
        $data['all_branch_list'] = $this->allocation_model->get_branchs($filterBy);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers_by_branch($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

        $data['subview'] = $this->load->view('admin/allocation/alloc_request', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function alloc_list($id=NULL)
    {
        $data['title'] = "배차목록";
        $data['permission_user'] = $this->allocation_model->all_permission_user('36');
		$data['branch_id'] = $id;
		if(empty($id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		//지사목록
		$filterBy = null;
        $data['all_branch_list'] = $this->allocation_model->get_branchs($filterBy);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers_by_branch($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

        $data['subview'] = $this->load->view('admin/allocation/alloc_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function config($id=NULL)
    {
        $data['title'] = "배차설정";
        $data['permission_user'] = $this->allocation_model->all_permission_user('36');
		$data['branch_id'] = $id;
		if(empty($id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		//지사목록
		$filterBy = null;
        $data['all_branch_list'] = $this->allocation_model->get_branchs($filterBy);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers_by_branch($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

        $data['subview'] = $this->load->view('admin/allocation/config', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function edit_driver_work($alloc_date = NULL, $branch_id = NULL, $work_id)
    {
        $data['title'] = "작업등록";
        $data['alloc_date'] = (!empty($alloc_date))?$alloc_date:"";
        $data['branch_id'] = (!empty($branch_id))?$branch_id:"";

        $data['permission_user'] = $this->allocation_model->all_permission_user('36');
		$data['work_id'] = $work_id;

		//작업정보
		$data['work_info'] = $this->allocation_model->get_work($work_id);

		//기사정보
		if(!empty($data['work_info']->driver_id)) {
			$data['driver_info'] = $this->db->where('dp_id', $data['work_info']->driver_id)->get('tbl_members')->row();
		}

		$data['modal_subview'] = $this->load->view('admin/allocation/_modal_driver_work', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_driver_work($alloc_date = NULL, $branch_id = NULL, $work_id)
    {
		$sql = "UPDATE tbl_allocation_works SET ";
		$sql .= "real_cnt='".$this->input->post('real_cnt', true)."', driver_confirm = '1'";
		$sql .= " WHERE idx = '".$work_id."'";
		$this->db->query($sql);

		$message = "기사 작업정보가 확정되었습니다.";
        $type = 'success';
        set_message($type, $message);

		redirect('admin/allocation/scheduling/'.$alloc_date.'/list/'.$branch_id); //redirect page
    }

    public function cancel_driver_work($alloc_date = NULL, $branch_id = NULL, $work_id)
    {
		$sql = "UPDATE tbl_allocation_works SET driver_confirm = '0' WHERE idx = '".$work_id."'";
		$this->db->query($sql);

		$message = "기사 작업정보가 취소되었습니다.";
        $type = 'success';
        set_message($type, $message);

		redirect('admin/allocation/scheduling/'.$alloc_date.'/list/'.$branch_id); //redirect page
    }

    public function edit_driver_works($alloc_date = NULL, $branch_id = NULL, $work_id)
    {
        $data['title'] = "작업등록";
        $data['alloc_date'] = (!empty($alloc_date))?$alloc_date:"";
        $data['branch_id'] = (!empty($branch_id))?$branch_id:"";


        $data['permission_user'] = $this->allocation_model->all_permission_user('36');
		$data['work_id'] = $work_id;

		//작업정보
		$data['work_info'] = $this->allocation_model->get_work($work_id);

		//기사정보
		if(!empty($data['work_info']->driver_id)) {
			$data['driver_info'] = $this->db->where('dp_id', $data['work_info']->driver_id)->get('tbl_members')->row();
		}

		$data['modal_subview'] = $this->load->view('admin/allocation/_modal_driver_work', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function scheduling($alloc_date=NULL, $action = NULL, $branch_id = NULL, $driver_id = NULL, $work_id = NULL)
    {
        $data['title'] = "배차등록";
		$data['alloc_date'] = (!empty($alloc_date))?$alloc_date:date('Y-m-d');
		$data['action'] = (!empty($action))?$action:"list";
		$data['branch_id'] = $branch_id;
		if(empty($branch_id)) $data['branch_id'] = $this->allocation_model->get_first_branch();
		$data['driver_id'] = $driver_id;
		$data['work_id'] = $work_id;

		//배차 추가
		if($action == "add_schedule") {
			$sql = "INSERT INTO tbl_allocation_works SET alloc_date='" . $data['alloc_date'] . "'";
			$sql .= ",br_id	= '". $data['branch_id'] ."'";
			$this->db->query($sql);

			$message = "배차 일정이 추가되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
			redirect('admin/allocation/scheduling/'.$data['alloc_date'].'/list/'.$data['branch_id']); //redirect page
		//배차 삭제
		} else if($action == "delete_schedule") {
			$sql = "DELETE FROM tbl_allocation_works WHERE idx='" . $data['work_id'] . "'";
			$this->db->query($sql);

			$message = "배차 일정이 삭제되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
			redirect('admin/allocation/scheduling/'.$data['alloc_date'].'/list/'.$data['branch_id']); //redirect page
		//배차항목 업데이트
		} else if($action == "update_field") {
            $field_name = $this->input->post('sfield', true);
            $field_value = $this->input->post('svalue', true);
            $work_id = $this->input->post('swork_id', true);
			$sql = "UPDATE tbl_allocation_works SET " . $field_name . " = '" . $field_value . "' WHERE idx='" . $work_id . "'";
			$this->db->query($sql);

			exit;
		//등록완료
		} else if($action == "finish") {
			$sql = "UPDATE tbl_allocation_works SET is_register = '1' WHERE alloc_date='" . $data['alloc_date'] . "' and br_id='" . $data['branch_id'] . "'";
			$this->db->query($sql);
			$message = "배차 일정 등록이 완료되었습니다.";
            $type = 'success';
			redirect('admin/allocation/scheduling/'.$data['alloc_date'].'/list/'.$data['branch_id']); //redirect page
		//등록취소
		} else if($action == "cancel") {
			$sql = "UPDATE tbl_allocation_works SET is_register = '0' WHERE alloc_date='" . $data['alloc_date'] . "' and br_id='" . $data['branch_id'] . "'";
			$this->db->query($sql);
			$message = "배차 일정 등록이 취소되었습니다.";
            $type = 'success';
			redirect('admin/allocation/scheduling/'.$data['alloc_date'].'/list/'.$data['branch_id']); //redirect page
		//확정
		} else if($action == "confirm") {
			$sql = "UPDATE tbl_allocation_works SET is_confirm = '1' WHERE alloc_date='" . $data['alloc_date'] . "' and br_id='" . $data['branch_id'] . "'";
			$this->db->query($sql);
		//확정취소
		} else if($action == "confirm_cancel") {
			//확정취소시 할당건 취소
			$sql = "UPDATE tbl_allocation_works SET is_confirm = '0',subwork_status = '1' WHERE alloc_date='" . $data['alloc_date'] . "' and br_id='" . $data['branch_id'] . "'";
			$this->db->query($sql);
			redirect('admin/allocation/scheduling/'.$data['alloc_date'].'/list/'.$data['branch_id']); //redirect page
		//기사등록
		} else if($action == "driver_confirm") {
			$sql = "UPDATE tbl_allocation_works SET driver_confirm = '1' WHERE wk_id='" . $data['work_id'] . "'";
			$this->db->query($sql);
			redirect('admin/allocation/scheduling/'.$data['alloc_date'].'/list/'.$data['branch_id']); //redirect page
		//기사등록취소
		} else if($action == "driver_cancel") {
			$sql = "UPDATE tbl_allocation_works SET driver_confirm = '0' WHERE wk_id='" . $data['work_id'] . "'";
			$this->db->query($sql);
			redirect('admin/allocation/scheduling/'.$data['alloc_date'].'/list/'.$data['branch_id']); //redirect page
		}

		// 등록상태
		$data['work_status'] = $this->allocation_model->get_work_status($data['alloc_date'], $data['branch_id']);

		//지사목록
        $data['all_branch_group'] = $this->allocation_model->get_branchs();
        $data['all_branch_list'] = $this->allocation_model->get_branchs($data['branch_id']);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers_by_branch($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

		// 검사원
		$data['all_checker_list'] = $this->allocation_model->get_checkers($data['branch_id']);

		//작업리스트
		$data['work_list'] = $this->allocation_model->work_list_all($data['alloc_date'], $data['branch_id']);


        $data['subview'] = $this->load->view('admin/allocation/scheduling', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function finish($alloc_date=NULL, $action = NULL, $branch_id = NULL, $driver_id = NULL)
    {
        $data['title'] = "배차완료";
		$data['alloc_date'] = $alloc_date;
		$data['driver_confirm']	= "1";
		$data['action'] = (!empty($action))?$action:"list_by_branch";
		$data['driver_id'] = (!empty($driver_id))?$driver_id:"";

		if(empty($data['alloc_date'])) $data['alloc_date'] = date('Y-m-d');
		$data['branch_id'] = $branch_id;
		if(empty($branch_id)) {
			$data['branch_id'] = $this->allocation_model->get_first_branch();
		}

		// 등록상태
		$data['work_status'] = $this->allocation_model->get_work_status($data['alloc_date'], $data['branch_id']);

		//지사목록
        $data['all_branch_group'] = $this->allocation_model->get_branchs();
        $data['all_branch_list'] = $this->allocation_model->get_branchs($data['branch_id']);

		// all drivers
		$data['all_driver_list'] = $this->allocation_model->get_drivers_by_branch($data['branch_id']);
		$data['total_driver_count'] = count($data['all_driver_list']);

		// 검사원
		$data['all_checker_list'] = $this->allocation_model->get_checkers($data['branch_id']);

		//작업리스트(완료건만)
		$data['work_list'] = $this->allocation_model->work_list_all($data['alloc_date'], $data['branch_id'], $driver_id, $data['driver_confirm']);

        $data['subview'] = $this->load->view('admin/allocation/finish', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function statistics($date_from=NULL, $date_to=NULL, $action = NULL)
    {
        $data['title'] = "운반통계";
		$data['date_from'] = (!empty($date_from))?$date_from:date("Y-m")."-01";
		$data['date_to'] = (!empty($date_to))?$date_to:date("Y-m-d");
		$data['action'] = (!empty($action))?$action:"list_by_real";

		//지사목록
        $data['all_branch_group'] = $this->allocation_model->get_branchs();

        $data['subview'] = $this->load->view('admin/allocation/statistics', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function set_unit_price()
    {
        $data['title'] = "단가 전체 설정";
		
		$data['modal_subview'] = $this->load->view('admin/allocation/_modal_unit_price', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }




}