<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Basic extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('basic_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

    public function save_partner_____($id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $this->basic_model->_table_name = 'tbl_asset_truck'; //table name
            $result = $this->basic_model->get_by(array('tr_id' => $id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co_id='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",car_remark='".$this->input->post('car_remark', true)."'";
				$sql .= ",reg_datetime='".date('Y-m-d h:i:s')."'";
				$sql .= " WHERE tr_id = '".$id."'";
				$this->db->query($sql);
			} else {
				$sql = "INSERT INTO tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co_id='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",car_remark='".$this->input->post('car_remark', true)."'";
				$this->db->query($sql);
			}

			$message = "차량정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

	public function partner_daepae($dp_id) {
        $created = can_action('83', 'created');
        $edited = can_action('83', 'edited');
		$data['dp_css'] = "<link href=\"". base_url() . "asset/css/dp_common.css\" rel=\"stylesheet\" type=\"text/css\" />";
		$data['dp_css'] .= "<link href=\"". base_url() . "asset/css/dp_style.css\" rel=\"stylesheet\" type=\"text/css\" />";

		$data['title'] = '대.폐차신청';

		//기사 정보
		$data['partner_info'] = $this->basic_model->db->where('dp_id', $dp_id)->get('tbl_members')->row();

		//차량정보
		$data['truck_info'] = $this->basic_model->db->where('inv_co_id', $dp_id)->get('tbl_asset_truck')->row();

		//위수탁사정보
		if(!empty($data['truck_info']->ws_co_id)) {
			$data['ws_co_info'] = $this->basic_model->db->where('dp_id', $data['truck_info']->ws_co_id)->get('tbl_members')->row();
		}

		$data['subview'] = $this->load->view('admin/basic/partner_daepae', $data, true);
        $this->load->view('admin/_layout_dp_print', $data);
	}

	public function real_demander($action = NULL, $id = NULL) {
        $created = can_action('83', 'created');
        $edited = can_action('83', 'edited');
        $data['title'] = "실수요처";
	}

    public function pop_contract($id)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        $data['title'] = "전자계약";

		$this->basic_model->_table_name = 'tbl_members'; //table name
		$this->basic_model->db->where('dp_id', $id);
		$data['partner_info'] = $this->basic_model->get();

		$data['subview'] = $this->load->view('admin/basic/pop_contract', $data, true);
        $this->load->view('admin/_layout_pop_partner', $data);
    }

    public function sudang_gongje($mode = NULL, $action = NULL, $id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');

		$data['mode'] = $mode;
        $data['load_page'] = "sudang";

        if(empty($data['mode'])) $data['mode'] = "sudang";
		if($data['mode'] == "sudang") {
			$data['title'] = "수당항목설정";
			$data['add_type'] = "S";
		} if($data['mode'] == "gongje") {
			$data['title'] = "공제(위.수탁관리비)항목 설정";
			$data['add_type'] = "GW";
		} if($data['mode'] == "ggongje") {
			$data['title'] = "공제(일반)항목 설정";
			$data['add_type'] = "GN";
		} if($data['mode'] == "rgongje") {
			$data['title'] = "공제(환급)항목 설정";
			$data['add_type'] = "GR";
		}

		if($action == "insert") {
			$data['add_type'] = $this->input->post('add_type', true);
			$sql = "INSERT INTO tbl_settings_sg SET ";
			$sql .= "add_type ='".$this->input->post('add_type', true)."'";
			$sql .= ",title='".$this->input->post('title', true)."'";
			$sql .= ",dp_order='".$this->input->post('dp_order', true)."'";
			$sql .= ",remark='".$this->input->post('remark', true)."'";
			$this->db->query($sql);

			$message = "정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
	        redirect('admin/basic/sudang_gongje/'.$mode.'/'); //redirect page
		} else if($action == "delete") {
			$this->db->query("delete from tbl_settings_sg where idx='{$id}'");

			$message = "정보가 삭제되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
	        redirect('admin/basic/sudang_gongje/'.$mode.'/'); //redirect page
		} else {
			$this->basic_model->_table_name = 'tbl_settings_sg'; //table name
			$this->basic_model->_order_by = 'dp_order';
			$this->basic_model->db->where('add_type', $data['add_type']);
			$data['all_item_group'] = $this->basic_model->get();
		}

        $data['subview'] = $this->load->view('admin/basic/sudang_gongje', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

	//파트너 삭제
    public function delete_partner($page = NULL, $filter = NULL, $ws_co_id = NULL, $baecha_co_id = NULL, $search_keyword = NULL, $dp_id)
    {
        $this->basic_model->_table_name = "tbl_members"; // table name
        $this->basic_model->_primary_key = "dp_id"; // $id
        $this->basic_model->delete($dp_id);

		$this->db->query("delete from tbl_members_bank where dp_id='{$dp_id}'");
		$this->db->query("delete from tbl_members_history where dp_id='{$dp_id}'");

		$type = 'success';
        $msg = "파트너 정보가 삭제되었습니다.";
        set_message($type, $msg);
        redirect('admin/basic/partner_list_all/list/'.$page.'/'.$filter.'/'.$ws_co_id.'/'.$baecha_co_id.'/'.$search_keyword); //redirect page
    }

    public function set_mfee($id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['mfee_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_mfee', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_mfee($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $mfeeset_id = $this->input->post('mfeeset_id', true);
            $dp_id = $this->input->post('dp_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);

/*			$data['wst_mfee'] = $this->input->post('wst_mfee', true);
			$data['mfee_vat'] = $this->input->post('mfee_vat', true);
			$data['org_fee'] = $this->input->post('org_fee', true);
			$data['org_fee_vat'] = $this->input->post('org_fee_vat', true);
			$data['env_fee'] = $this->input->post('env_fee', true);
			$data['env_fee_vat'] = $this->input->post('env_fee_vat', true);
			$data['car_tax'] = $this->input->post('car_tax', true);
			$data['car_tax_vat'] = $this->input->post('car_tax_vat', true);
			$data['grg_fee'] = $this->input->post('grg_fee', true);
			$data['grg_fee_vat'] = $this->input->post('grg_fee_vat', true);
			$data['etc'] = $this->input->post('etc', true);
*/
			//기존등록정보 is_master = 'N'
			$sql = "UPDATE tbl_delivery_fee_fixmfee_set SET master_yn='N' WHERE dp_id = ?";
			$this->db->query($sql, array($dp_id));

			//신규 insert
			$sql = "INSERT INTO tbl_delivery_fee_fixmfee_set SET ";
			$sql .= "dp_id='".$dp_id."'";
			$sql .= ",wst_mfee='".$this->input->post('wst_mfee', true)."'";
			$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
			$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
			$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
			$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
			$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
			$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
			$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
			$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
			$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
			$sql .= ",etc='".$this->input->post('etc', true)."'";
			$sql .= ",master_yn = 'Y'";
			$sql .= ",reg_date = '".date('Y-m-d', time())."'";
			$this->db->query($sql);

/*			$this->cowork_model->_table_name = 'tbl_delivery_fee_fixmfee_set'; // table name
			$this->cowork_model->_primary_key = 'idx'; // $id
			$this->cowork_model->save($data, $mfeeset_id);*/

			//$delivery_fee_fixmfee_set_info = $this->db->where('idx', $mfeeset_id)->get('tbl_delivery_fee_fixmfee_set')->row();
			$activities = array(
				'user' => $this->session->userdata('user_id'),
				'module' => 'delivery_fee_fixmfee_set',
				'module_field_id' => $mfeeset_id,
				'activity' => 'activity_update_delivery_fee_fixmfee_set',
				'icon' => 'fa-user',
				'value1' => 'update'
			);
			$this->cowork_model->_table_name = 'tbl_activities';
			$this->cowork_model->_primary_key = "activities_id";
			$this->cowork_model->save($activities);

/*
            $wst_mfee = $this->input->post('wst_mfee', true);
            $mfee_vat = $this->input->post('mfee_vat', true);
            $org_fee = $this->input->post('org_fee', true);
            $org_fee_vat = $this->input->post('org_fee_vat', true);
            $env_fee = $this->input->post('env_fee', true);
            $env_fee_vat = $this->input->post('env_fee_vat', true);
            $car_tax = $this->input->post('car_tax', true);
            $car_tax_vat = $this->input->post('car_tax_vat', true);
            $grg_fee = $this->input->post('grg_fee', true);
            $grg_fee_vat = $this->input->post('grg_fee_vat', true);
            $etc = $this->input->post('etc', true);

			$activities = array(
				'wst_mfee' => $wst_mfee,
				'mfee_vat' => $mfee_vat,
				'org_fee' => $org_fee,
				'org_fee_vat' => $org_fee_vat,
				'env_fee' => $env_fee,
				'env_fee_vat' => $env_fee_vat,
				'car_tax' => $car_tax,
				'car_tax_vat' => $car_tax_vat,
				'grg_fee' => $grg_fee,
				'grg_fee_vat' => $grg_fee_vat,
				'etc' => $etc
			);
			$this->db->where('idx', $mfeeset_id);
			$this->cowork_model->_table_name = 'tbl_delivery_fee_fixmfee_set';
			$this->cowork_model->_primary_key = "idx";
			$this->cowork_model->save($activities);
*/
			$message = "관리비 설정이 수정되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function logis_settings()
    {
        $settings = $this->input->get('settings', TRUE) ? $this->input->get('settings', TRUE) : 'settings_mfee';
        $data['title'] = "기초데이터설정"; //Page title
        $can_do = can_do(111);
        if (!empty($can_do)) {
            $data['load_setting'] = $settings;
        } else {
            $data['load_setting'] = 'not_found';
        }
        $data['page'] = "기초데이터설정";

        $data['subview'] = $this->load->view('admin/basic/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
	}

    public function customer_list($action = NULL, $id = NULL)
    {

        $dp_id = $id;

        if ($action == 'edit_customer') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
				$qry = "select * from tbl_members  where dp_id = '".$dp_id."'";
				$data['customer_info'] = $this->basic_model->db->query($qry)->row();
            }

        } else {
            $data['active'] = 1;
        }

        $data['title'] = '거래처 목록';

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.mb_type', 'customer');
        $data['all_customer_info'] = $this->basic_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
        $data['subview'] = $this->load->view('admin/basic/customer_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

	// 거래처 저장
    public function save_customer($id = NULL)
    {
		$dp_id = $id;
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

			$this->basic_model->_table_name = 'tbl_members'; //table name
            $result = $this->basic_model->get_by(array('dp_id' => $dp_id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_members SET ";
				$sql .= "code='".$this->input->post('code', true)."'";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",tax_yn='".$this->input->post('tax_yn', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",homepage='".$this->input->post('homepage', true)."'";
				$sql .= " WHERE dp_id = '".$dp_id."'";
				$this->db->query($sql);
			} else { 
				//아이디 정보가 없을경우 1. 주민등록번호 없으면 2. 사업자등록번호


				$sql = "INSERT INTO tbl_members SET ";
				$sql .= "mb_type='customer'";
				$sql .= ",code='".$this->input->post('code', true)."'";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",co_address='".$this->input->post('co_address', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",homepage='".$this->input->post('homepage', true)."'";
				$this->db->query($sql);
				$dp_id = $this->db->insert_id();
			}

			$message = "거래처정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/basic/customer_list/'); //redirect page
    } 

    public function customer_details($id, $action = null)
    {
        if ($action == 'add_contacts') {
            // get all language
            $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
            // get all location
            $this->basic_model->_table_name = 'tbl_locales';
            $this->basic_model->_order_by = 'name';
            $data['locales'] = $this->basic_model->get();
            $data['company'] = $id;
            $user_id = $this->uri->segment(6);
            if (!empty($user_id)) {
                // get all user_info by user id
                $data['account_details'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_account_details');

                $data['user_info'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_users');
            }

        }
        $data['title'] = "거래처 상세 정보"; //Page title
        // get all client details
        $this->basic_model->_table_name = "tbl_members"; //table name
        $this->basic_model->_order_by = "dp_id";
        $data['customer_details'] = $this->basic_model->get_by(array('dp_id' => $id), TRUE);

        $data['subview'] = $this->load->view('admin/basic/customer_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

	public function set_partner_payment($action = NULL, $id = NULL, $page, $list_mode, $ws_co_id=NULL)
    {
		$data['page'] = $page;
		$data['list_mode'] = $list_mode;
		$data['ws_co_id'] = $ws_co_id;
		$data['dp_id'] = $id;

        if ($action == 'save') {
			$sql = "update tbl_members set pay_account_no = '".$this->input->post('pay_account_no', true)."' where dp_id='".$data['dp_id']."'";
			$this->basic_model->db->query($sql);

			$message = "파트너 결제정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
			if(empty($ws_co_id) || $ws_co_id == "") 
				redirect('admin/basic/Payment/'); //redirect page
			else
				redirect('admin/basic/partner_list/'.$ws_co_id); //redirect page
		} else {
			$data['dp'] = $this->db->where('dp_id', $id)->get('tbl_members')->row();
			if(!empty($data['dp']->tr_id)) $data['tr'] = $this->db->where('idx', $data['dp']->tr_id)->get('tbl_asset_truck')->row();

			$this->basic_model->_table_name = 'tbl_members_bank'; //table name
			$this->basic_model->_order_by = 'account_name asc';
			//$this->basic_model->db->where('1');
			$data['all_bank_info'] = $this->basic_model->get();
		}

		$data['modal_subview'] = $this->load->view('admin/basic/_modal_partner_set_payment', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

	public function set_partner_status($action = NULL, $id = NULL, $page, $list_mode, $co_code = NULL)
    {
		$data['page'] = $page;
		$data['list_mode'] = $list_mode;
		$data['co_code'] = $co_code;
		$data['dp_id'] = $id;

		$data['dp'] = $this->db->where('dp_id', $id)->get('tbl_members')->row();
		if(!empty($data['dp']->tr_id)) $data['tr'] = $this->db->where('idx', $data['dp']->tr_id)->get('tbl_asset_truck')->row();
		$data['modal_subview'] = $this->load->view('admin/basic/_modal_partner_set_contract', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_partner_status($id = NULL, $status = NULL)
    {
		$data['page'] = $this->input->post('page', true);
		$data['list_mode'] = $this->input->post('list_mode', true);
		$data['co_code'] = $this->input->post('co_code', true);
		$data['tr_id'] = $this->input->post('tr_id', true);
		$data['out_date'] = $this->input->post('out_date', true);
		$data['gongT_date'] = $this->input->post('gongT_date', true);
		$data['return_cls_date'] = $this->input->post('return_cls_date', true);

		if(!empty($id)) {
			// 로그정보
			$dp = $this->basic_model->db->query("select * from tbl_members where dp_id = '".$id."'")->row();
			//현규씨작업
			if(!empty($dp->tr_id)) $tr = $this->basic_model->db->query("select car_1 from tbl_asset_truck where idx = '".$dp->tr_id."' ")->row();

			$sql = "INSERT INTO tbl_members_history SET dp_id='".$id."'";
			$sql .= ",ct_fdate = '".$dp->N."'";
			$sql .= ",ct_tdate = '".$dp->O."'";
			$sql .= ",rco_code = '".$dp->code."'";
			if(!empty($dp->tr_id)) $sql .= ",tr_id = '".$dp->tr_id."'";
			if(!empty($tr->car_1)) $sql .= ",car_no = '".$tr->car_1."'";
			$sql .= ",out_date = '" . $data['out_date'] . "'";
			$sql .= ",reg_datetime = '" . date('Y-m-d h:i:s') . "'";
			$this->db->query($sql);

			// 정보 변경
			$sql = "UPDATE tbl_members SET partner_status='".$status."' WHERE dp_id = '".$id."'";
			$this->db->query($sql);

			//차량/파트너(초기화) 상태 변경
			$this->db->query($sql);
			if($status == "N") {
				if(!empty($dp->tr_id)) {
					$sql = "UPDATE tbl_asset_truck SET car_status ='G',gongT_date ='".$data['gongT_date']."',return_cls_date ='".$data['return_cls_date']."' WHERE idx = '".$dp->tr_id."'";
					$this->db->query($sql);

					$sql = "UPDATE tbl_members SET tr_id ='' WHERE dp_id = '".$id."'";
					$this->db->query($sql);
				}
			}

			$message = "파트너 상태정보가 변경되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }

		if(empty($ws_co_id) || $ws_co_id == "") 
			redirect('admin/basic/Payment/'.$ws_co_id); //redirect page
		else
			redirect('admin/basic/partner_list/'.$ws_co_id); //redirect page
    }

	// 자동차 검사 기준 설정( 사업자 기준 /  개인기준)
    public function set_certtype($partner_id, $status)
    {
        $edited = can_action('57', 'edited');
        if (!empty($edited)) {

            $data['dp_id'] = $partner_id;
            $data['cert_id_type'] = $status;
            $this->basic_model->_table_name = 'tbl_members';
            $this->basic_model->_primary_key = 'dp_id';
            $this->basic_model->save($data, $partner_id);
            // messages for user
            $type = "success";
            $message = lang('change_status');
            set_message($type, $message);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

	public function partner_list($action = NULL, $id = NULL)
    {
		$dp_id = $id;
		$data['page'] = $this->input->post('page', true);
		if(empty($data['page']) || $data['page'] < 1) $data['page'] =  1;
		$data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "ALL";

		$data['ws_co_id'] = $this->input->post('ws_co_id', true);
        if (empty($data['ws_co_id']) || $data['ws_co_id'] == "all") $data['ws_co_id'] = "1415";

		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);
        if (empty($data['baecha_co_id'])) $data['baecha_co_id'] = "kn4021";

        if ($action == 'edit_partner') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
				$qry = "select dp.*, tr.car_1, tr.car_7, tr.car_3,tr.ws_co_id as twscoid,tr.ws_co_name as twsconame from tbl_members dp left join tbl_asset_truck tr on dp.dp_id = tr.inv_co_id";
				$qry .= " where dp.dp_id = '".$dp_id."'";
				$data['partner_info'] = $this->basic_model->db->query($qry)->row();

				//지사목록
				$this->basic_model->_table_name = 'tbl_members'; //table name
				$this->basic_model->db->where('mb_type', 'branch');
				$this->basic_model->_order_by = 'co_name asc';
				$data['all_branch_list'] = $this->basic_model->get();

            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '파트너 목록';

        $this->basic_model->_table_name = 'tbl_members dp'; //table name
		$this->basic_model->db->select('dp.*, tr.car_1, tr.car_7, tr.car_3, ws.co_name as ws_co_name, ws.co_tel as ws_co_tel, mb.bank_name as p_bank_name, mb.account_no as p_account_no, mb.account_name as p_account_name, br.co_name as br_name');
		$this->basic_model->db->join('tbl_asset_truck tr', 'tr.inv_co_id = dp.dp_id', 'left');
		$this->basic_model->db->join('tbl_members ws', 'ws.dp_id = tr.ws_co_id', 'left'); 
		$this->basic_model->db->join('tbl_members br', 'br.dp_id = dp.br_id', 'left'); 
		$this->basic_model->db->join('tbl_members_bank mb', 'mb.idx = dp.pay_account_no', 'left'); 

        $this->basic_model->_order_by = 'dp.dp_id';
        if(!empty($data['ws_co_id']) && $data['ws_co_id'] != "") $this->basic_model->db->where('tr.ws_co_id', $data['ws_co_id']);
		if(!empty($this->input->post('baecha_co_id', true))) $this->basic_model->db->where('tr.baecha_co_id', $this->input->post('baecha_co_id', true));
        $this->basic_model->db->where('dp.mb_type', 'partner');
		
		if($data['list_mode'] == "IN") { //계약중 파트너
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O >= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "WT" || $data['list_mode'] == "WTU") { //계약중료대기 파트너 
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O <= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "EP") { //계약중료 파트너
			$this->basic_model->db->where('dp.partner_status', 'N');
		}

        $data['all_partner_info'] = $this->basic_model->get();

		//지입사 목록
        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'co_name';
        $this->basic_model->db->where("mb_type='group' or mb_type='coop'");
        $data['all_jiip_info'] = $this->basic_model->get();

		//결제계좌
		$this->basic_model->_table_name = 'tbl_members_bank'; //table name
		$this->basic_model->_order_by = 'account_name asc';
		$data['all_bank_info'] = $this->basic_model->get();

		//현재기준 위수탁파트너사 목록
        $this->basic_model->_table_name = 'tbl_asset_truck at'; //table name
		$this->basic_model->db->select('distinct(at.ws_co_id), dp.co_name');
		$this->basic_model->db->join('tbl_members dp', 'dp.dp_id = at.ws_co_id', 'left');
        $this->basic_model->_order_by = 'dp.co_name asc';
        $data['all_ws_info'] = $this->basic_model->get();

		//배차지 목록
        $this->basic_model->_table_name = 'tbl_asset_truck at'; //table name
		$this->basic_model->db->select('distinct(at.baecha_co_id), dp.co_name');
		$this->basic_model->db->join('tbl_members dp', 'dp.code = at.baecha_co_id', 'left')->where('dp.mb_type', 'customer');
        $this->basic_model->_order_by = 'dp.co_name asc';
        $data['all_bc_info'] = $this->basic_model->get();
        $data['title'] = '파트너 목록';

		$data['subview'] = $this->load->view('admin/basic/partner_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function partner_list_all($action = NULL, $page = NULL, $filter = NULL, $ws_co_id = NULL, $baecha_co_id = NULL, $search_keyword = NULL, $dp_id = NULL)
    {
		$data['title']			= '파트너 목록';
        $data['page']			= (!empty($page))?$page:1;
		$data['filter']			= (!empty($filter))?$filter:"ALL";
		$data['ws_co_id']		= (empty($ws_co_id) || $ws_co_id == "all")?"":$ws_co_id;
		$data['baecha_co_id']	= $baecha_co_id;
		$data['search_keyword']	= $search_keyword;
		$data['action']			= $action;

		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

        if ($action == 'edit_partner') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
				$qry = "select dp.*, tr.car_1, tr.car_7, tr.car_3,tr.ws_co_id as twscoid,tr.ws_co_name as twsconame from tbl_members dp left join tbl_asset_truck tr on dp.dp_id = tr.inv_co_id";
				$qry .= " where dp.dp_id = '".$dp_id."'";
				$data['partner_info'] = $this->basic_model->db->query($qry)->row();

				//지사목록
				$this->basic_model->_table_name = 'tbl_members'; //table name
				$this->basic_model->db->where('mb_type', 'branch');
				$this->basic_model->_order_by = 'co_name asc';
				$data['all_branch_list'] = $this->basic_model->get();

            }
        } else {
            $data['active'] = 1;
        }

		// 차량정보갱신
		$this->basic_model->_order_by = NULL;
		if($data['filter'] == "WTU") { 
			$this->basic_model->_table_name = 'tbl_members dp'; //table name          
			$this->basic_model->db->join('tbl_asset_truck tr', 'dp.tr_id = tr.idx', 'left');
			if(!empty($this->input->post('baecha_co_id', true))) $this->basic_model->db->where('tr.baecha_co_id', $this->input->post('baecha_co_id', true));
			if(!empty($data['search_keyword'])) {
				$this->basic_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.reg_number like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR dp.U like \'%'.$data['search_keyword'].'%\' OR dp.W like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\')' );
			}
			$this->basic_model->db->where('dp.mb_type', 'partner');
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O <= '".date('Y-m-d', strtotime("today + 35 days"))."')");
			$data['all_partner_info'] = $this->basic_model->get();
			if (!empty($data['all_partner_info'])) {
				foreach ($data['all_partner_info'] as $partner_details) {
					// 1.  (요청일 : 파트너계약종료일) 차량정보 동시적으로  파트너 및 해당 파트너의 차량번호등 종료대기현황 > 번호반납예정등록현황 동시발생
					if(!empty($partner_details->tr_id)) {
						$return_ready = "Y";
						$return_due_date = $partner_details->O;

						$sql = "UPDATE tbl_asset_truck SET car_status='R', return_due_date='$return_due_date' WHERE idx='".$partner_details->tr_id."'";
						$this->db->query($sql);
					}
				}
			}
		}

        $this->basic_model->_table_name = 'tbl_members dp'; //table name          
		$this->basic_model->db->select('dp.dp_id');
		$this->basic_model->db->join('tbl_asset_truck tr', 'dp.tr_id = tr.idx', 'left');
		$this->basic_model->db->join('tbl_members ws', 'ws.dp_id = tr.ws_co_id', 'left'); 
		$this->basic_model->db->join('tbl_members_bank mb', 'mb.idx = dp.pay_account_no', 'left'); 
		if(!empty($this->input->post('baecha_co_id', true))) $this->basic_model->db->where('tr.baecha_co_id', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->basic_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.reg_number like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR dp.U like \'%'.$data['search_keyword'].'%\' OR dp.W like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\')' );
		}
        $this->basic_model->db->where('dp.mb_type', 'partner');
		if($data['filter'] == "IN") { //계약중 파트너
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O >= '".date('Y-m-d', time())."')");
		} else if($data['filter'] == "WT" || $data['filter'] == "WTU") { //계약중료대기 파트너 
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O <= '".date('Y-m-d', strtotime("today + 35 days"))."')");
		} else if($data['filter'] == "EP") { //계약중료 파트너
			$this->basic_model->db->where('dp.partner_status', 'N');
		}
		if(!empty($data['ws_co_id'])) $this->basic_model->db->where('tr.ws_co_id',$data['ws_co_id']);
		$data['total_count'] = count($this->basic_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->basic_model->_table_name = 'tbl_members dp'; //table name
		$this->basic_model->db->select('dp.*, tr.car_1, tr.car_7, tr.car_3, ws.co_name as ws_co_name, ws.co_tel as ws_co_tel, mb.bank_name as p_bank_name, mb.account_no as p_account_no, mb.account_name as p_account_name, br.co_name as br_name');
		$this->basic_model->db->join('tbl_asset_truck tr', 'tr.inv_co_id = dp.dp_id', 'left');
		$this->basic_model->db->join('tbl_members ws', 'ws.dp_id = tr.ws_co_id', 'left'); 
		$this->basic_model->db->join('tbl_members_bank mb', 'mb.idx = dp.pay_account_no', 'left'); 
		$this->basic_model->db->join('tbl_members br', 'br.dp_id = dp.br_id', 'left'); 
        $this->basic_model->_order_by = 'dp.co_name';
		if(!empty($this->input->post('baecha_co_id', true))) $this->basic_model->db->where('tr.baecha_co_id', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->basic_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.reg_number like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR dp.U like \'%'.$data['search_keyword'].'%\' OR dp.W like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\')' );
		}
		if(!empty($data['ws_co_id'])) $this->basic_model->db->where('tr.ws_co_id',$data['ws_co_id']);
        $this->basic_model->db->where('dp.mb_type', 'partner');
		if($data['filter'] == "IN") { //계약중 파트너
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O >= '".date('Y-m-d', time())."')");
		} else if($data['filter'] == "WT" || $data['filter'] == "WTU") { //계약중료대기 파트너 
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O <= '".date('Y-m-d', strtotime("today + 35 days"))."')");
		} else if($data['filter'] == "EP") { //계약중료 파트너
			$this->basic_model->db->where('dp.partner_status', 'N');
		}
		if ($action != 'excel') $this->basic_model->db->limit($limit, $data['from_record']);
		//if($data['filter'] <> "WTU") $this->basic_model->db->limit($limit, $data['from_record']);
        $data['all_partner_info'] = $this->basic_model->get();

		//결제계좌
		$this->basic_model->_table_name = 'tbl_members_bank'; //table name
		$this->basic_model->_order_by = 'account_name asc';
		$data['all_bank_info'] = $this->basic_model->get();

		//현재기준 위수탁파트너사 목록
        $this->basic_model->_table_name = 'tbl_asset_truck at'; //table name
		$this->basic_model->db->select('distinct(at.ws_co_id), dp.co_name');
		$this->basic_model->db->join('tbl_members dp', 'dp.dp_id = at.ws_co_id', 'left');
        $this->basic_model->_order_by = 'dp.co_name asc';
        $data['all_ws_info'] = $this->basic_model->get();

		//배차지 목록
        $this->basic_model->_table_name = 'tbl_asset_truck at'; //table name
		$this->basic_model->db->select('distinct(at.baecha_co_id), dp.co_name');
		$this->basic_model->db->join('tbl_members dp', 'dp.code = at.baecha_co_id', 'left')->where('dp.mb_type', 'customer');
        $this->basic_model->_order_by = 'dp.co_name asc';
        $data['all_bc_info'] = $this->basic_model->get();

        if ($action == 'excel') {
			$data['subview'] = $this->load->view('admin/basic/partner_list_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
		} else {
			$data['subview'] = $this->load->view('admin/basic/partner_list_all', $data, true);
			$this->load->view('admin/_layout_main_nd', $data);
		}
    }

	// 파트너저장
    public function save_partner($page = NULL, $filter = NULL, $ws_co_id = NULL, $baecha_co_id = NULL, $search_keyword = NULL, $id = NULL)
    {
		$dp_id = $id;
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

			$sql_upload = "";
            if (!empty($_FILES['mb_pic']['name'])) {
                $val = $this->basic_model->uploadImage('mb_pic','partner_avatar');
                //$val == TRUE || redirect('admin/basic/partner_list_all/'.$ws_co_id);
				$sql_upload .= ", mb_pic = '".$val['path']."'";
            }
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->basic_model->customUploadFile('attach1','partner_file');
                //$val == TRUE || redirect('admin/basic/partner_list_all/'.$ws_co_id);
				$sql_upload .= ", attach1 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach2']['name'])) {
                $val = $this->basic_model->customUploadFile('attach2','partner_file');
               //$val == TRUE || redirect('admin/basic/partner_list_all/'.$ws_co_id);
				$sql_upload .= ", attach2 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach3']['name'])) {
                $val = $this->basic_model->customUploadFile('attach3','partner_file');
               // $val == TRUE || redirect('admin/basic/partner_list_all/'.$ws_co_id);
				$sql_upload .= ", attach3 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach4']['name'])) {
                $val = $this->basic_model->customUploadFile('attach4','partner_file');
               // $val == TRUE || redirect('admin/basic/partner_list_all/'.$ws_co_id);
				$sql_upload .= ", attach4 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach5']['name'])) {
                $val = $this->basic_model->customUploadFile('attach1','partner_file');
               // $val == TRUE || redirect('admin/basic/partner_list_all/'.$ws_co_id);
				$sql_upload .= ", attach5 = '".$val['path']."'";
            }
			
			$this->basic_model->_table_name = 'tbl_members'; //table name
            $result = $this->basic_model->get_by(array('dp_id' => $dp_id), true);

            if (!empty($result)) {
				// 기존 선택차량이 있고 변경 되었을 경우 - 기사없음 상태로 변경
                $ptr = $this->db->where('idx', $this->input->post('tr_id', true))->get('tbl_asset_truck')->row();
				if(   (!empty($ptr->tr_id) && !empty($this->input->post('tr_id', true))) && ($ptr->tr_id != $this->input->post('tr_id', true))  
					|| (!empty($ptr->tr_id) && empty($this->input->post('tr_id', true)))  		) {
					$sql = "UPDATE tbl_asset_truck SET car_status='N' WHERE idx = '".$this->input->post('tr_id', true)."'";
					$this->db->query($sql);
				}

				$sql = "UPDATE tbl_members SET ";
				$sql .= "code='".$this->input->post('code', true)."'";
				$sql .= ",userid='".$this->input->post('userid', true)."'";
				$sql .= ",userpwd='".$this->input->post('userpwd', true)."'";
				$sql .= ",br_id='".$this->input->post('br_id', true)."'";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",driver='".$this->input->post('driver', true)."'";
				$sql .= ",position='".$this->input->post('position', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",L='".$this->input->post('L', true)."'";
				$sql .= ",M='".$this->input->post('M', true)."'";
				$sql .= ",N='".$this->input->post('N', true)."'";
				$sql .= ",O='".$this->input->post('O', true)."'";
				$sql .= ",tax_yn='".$this->input->post('tax_yn', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",T='".$this->input->post('T', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",tr_id='".$this->input->post('tr_id', true)."'";
				$sql .= ",U='".$this->input->post('U', true)."'";
				$sql .= ",V='".$this->input->post('V', true)."'";
				$sql .= ",W='".$this->input->post('W', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",X='".$this->input->post('X', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",Z='".$this->input->post('Z', true)."'";
				$sql .= ",AA='".$this->input->post('AA', true)."'";
				$sql .= ",AB='".$this->input->post('AB', true)."'";
				$sql .= ",AC='".$this->input->post('AC', true)."'";
				$sql .= ",AD='".$this->input->post('AD', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",co_address='".$this->input->post('co_address', true)."'";
				$sql .= ",remark ='".$this->input->post('remark', true)."'";
				$sql .= ",attach1_text='".$this->input->post('attach1_text', true)."'";
				$sql .= ",attach2_text='".$this->input->post('attach2_text', true)."'";
				$sql .= ",attach3_text='".$this->input->post('attach3_text', true)."'";
				$sql .= ",attach4_text='".$this->input->post('attach4_text', true)."'";
				$sql .= ",attach5_text='".$this->input->post('attach5_text', true)."'". $sql_upload ;
				$sql .= " WHERE dp_id = '".$dp_id."'";
				$this->db->query($sql);
			} else { 
				//아이디 정보가 없을경우 1. 주민등록번호 없으면 2. 사업자등록번호


				$sql = "INSERT INTO tbl_members SET ";
				$sql .= "mb_type='partner'";
				$sql .= ",code='".$this->input->post('code', true)."'";
				$sql .= ",userid='".$this->input->post('userid', true)."'";
				$sql .= ",userpwd='1111'";
				$sql .= ",br_id='".$this->input->post('br_id', true)."'";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",driver='".$this->input->post('driver', true)."'";
				$sql .= ",position='".$this->input->post('position', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",L='".$this->input->post('L', true)."'";
				$sql .= ",M='".$this->input->post('M', true)."'";
				$sql .= ",N='".$this->input->post('N', true)."'";
				$sql .= ",O='".$this->input->post('O', true)."'";
				$sql .= ",tax_yn='".$this->input->post('tax_yn', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",T='".$this->input->post('T', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",tr_id='".$this->input->post('tr_id', true)."'";
				$sql .= ",U='".$this->input->post('U', true)."'";
				$sql .= ",V='".$this->input->post('V', true)."'";
				$sql .= ",W='".$this->input->post('W', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",X='".$this->input->post('X', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",Z='".$this->input->post('Z', true)."'";
				$sql .= ",AA='".$this->input->post('AA', true)."'";
				$sql .= ",AB='".$this->input->post('AB', true)."'";
				$sql .= ",AC='".$this->input->post('AC', true)."'";
				$sql .= ",AD='".$this->input->post('AD', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",co_address='".$this->input->post('co_address', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",del_yn='N'";
				$sql .= ",partner_status='Y'";
				$sql .= ",attach1_text='".$this->input->post('attach1_text', true)."'";
				$sql .= ",attach2_text='".$this->input->post('attach2_text', true)."'";
				$sql .= ",attach3_text='".$this->input->post('attach3_text', true)."'";
				$sql .= ",attach4_text='".$this->input->post('attach4_text', true)."'";
				$sql .= ",attach5_text='".$this->input->post('attach5_text', true)."'". $sql_upload ;
				$this->db->query($sql);
				$dp_id = $this->db->insert_id();
			}

			//공티이 차량일경우 대기로 이동
			if(!empty($this->input->post('tr_id', true))) { 
                $tr = $this->db->where('idx', $this->input->post('tr_id', true))->get('tbl_asset_truck')->row();
                $last_owner = $this->db->where('tr_id', $this->input->post('tr_id', true))->get('tbl_members')->row();

				if($tr->car_status=="G") { //변경 차량이 공티이 차량일경우 이력으로 넘기고 새 차량 삽입(번호/소속), 변경된 차량 키 저장
					$sql = "UPDATE tbl_asset_truck SET car_status='H' WHERE idx = '".$this->input->post('tr_id', true)."'";
					$this->db->query($sql);

					$sql = "INSERT INTO tbl_asset_truck SET ";
					$sql .= "p_tr_id='".$this->input->post('tr_id', true)."'";
					$sql .= ",gr_id='".$tr->gr_id."'";
					$sql .= ",car_5='".$tr->car_5."'"; //년식
					$sql .= ",car_3='".$tr->car_3."'"; //용도
					$sql .= ",car_1='".$tr->car_1."'";
					$sql .= ",last_owner_id='".$last_owner->dp_id."'"; //최종차주
					$sql .= ",ws_co_id='".$tr->ws_co_id."'";
					$sql .= ",car_status='R'";
					$this->db->query($sql);
					$new_tr_id = $this->db->insert_id();

					$sql = "UPDATE tbl_members SET tr_id='".$new_tr_id."' WHERE dp_id = '".$dp_id."'";
					$this->db->query($sql);
				} else { //공티이 차량이 아닐경우
					$sql = "UPDATE tbl_asset_truck SET car_status='A' WHERE idx = '".$this->input->post('tr_id', true)."'";
					$this->db->query($sql);
				}
			}


			$message = "파트너정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/basic/partner_list_all/list/'.$page.'/'.$filter.'/'.$ws_co_id.'/'.$baecha_co_id.'/'.$search_keyword); //redirect page
    } 

    public function set_tr_uprice()
    {
        $sc_id = $this->input->post('sc_id', true);
        $field = $this->input->post('field', true);
        $val = $this->input->post('val', true);
			$sql = "UPDATE tbl_scontract_co SET ".$field."='".$val."' WHERE idx = '".$sc_id."'";
			$this->db->query($sql);
			$message = "저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
    } 

	public function set_item($ct_id = NULL, $type = 'sudang', $cat = '일반', $action = NULL, $id = NULL)
    {
        $created = can_action('123', 'created');
        $edited = can_action('123', 'edited');

		$data['ct_id'] = $ct_id;
		$data['type'] = $type;
		$data['cat'] = $cat;
        $data['contract_info'] = $this->basic_model->db->where('idx', $data['ct_id'])->get('tbl_contract')->row();

		$data['edit_item_info'] = "";
        if ($action == 'edit_item') {
            $data['active'] = 2;
            if (!empty($id) && !empty($edited)) {
                $data['edit_item_info'] = $this->basic_model->check_by(array('idx' => $id), 'tbl_dp_assign_item');
            }
        } else if ($action == 'tr_delete') { // 거래처 삭제
			$this->basic_model->_table_name = "tbl_scontract_co"; // table name
			$this->basic_model->_primary_key = "idx"; // $id
			$this->basic_model->delete($id);
			exit;
            $data['active'] = 1;
        } else {
            $data['active'] = 1;
        }


        if ($action == 'update') {
            if (!empty($created) || !empty($edited)) {
                $this->basic_model->_table_name = 'tbl_dp_assign_item';
                $this->basic_model->_primary_key = 'idx';

                $item_data['ws_co_id'] = $ct_id;
                $item_data['p_category'] = $cat;
                $item_data['type'] = $type;
                $item_data['item'] = $this->input->post('item', TRUE);
                $item_data['remark'] = $this->input->post('remark', TRUE);

				if (!empty($id)) { 
                    $item_id = array('idx !=' => $id);
                } else {  
                    $item_id = null;
                }
                $where = array('ct_id' => $ct_id, 'item' => $item_data['item']);
                $check_item = $this->basic_model->check_update('tbl_dp_assign_item', $where, $item_id);
                if (!empty($check_item)) { 
                    // massage for user
                  //  $type = 'error';
                  //  $msg = "<strong style='color:#000'>" . $item_data['item'] . '</strong> 은 이미 존재합니다 ';
                } else { // save and update query
                    $id = $this->basic_model->save($item_data, $id);

                    $activity = array(
                        'user' => $this->session->userdata('user_id'),
                        'module' => 'basic',
                        'module_field_id' => $id,
                        'activity' => ('activity_added_a_dp_item'),
                        'value1' => $item_data['item']
                    );
                    $this->basic_model->_table_name = 'tbl_activities';
                    $this->basic_model->_primary_key = 'activities_id';
                    $this->basic_model->save($activity);

                    // messages for user
                   // $type = "success";
                   // $msg = "항목이 추가되었습니다.";
                }
               // $message = $msg;
                //set_message($type, $message);
            }
            redirect('admin/basic/set_item/'.$ct_id.'/'.$type.'/');
       // } else {
       //     $data['title'] = lang('income_category'); //Page title
       //     $data['load_setting'] = 'income_category';
        }
		if($type == "tr_item") { //거래항목설정
			$this->basic_model->_table_name = 'tbl_contract_detail'; //table name
			$this->basic_model->db->where('tbl_contract_detail.ct_id', $data['ct_id']);
			$this->basic_model->db->where('tbl_contract_detail.depth', '1');
			$this->basic_model->_order_by = 'idx';
			$data['all_item_group'] = $this->basic_model->get();
		} else if($type == "tr_uprice") { //거래단가설정
			$this->basic_model->_table_name = 'tbl_contract_detail'; //table name
			$this->basic_model->db->where('tbl_contract_detail.ct_id', $data['ct_id']);
			$this->basic_model->db->where('tbl_contract_detail.depth', '1');
			$this->basic_model->_order_by = 'idx';
			$data['all_item_group'] = $this->basic_model->get();

			$this->basic_model->db->select('a.*', false);
			$this->basic_model->db->select('b.ceo, b.bs_number', false);
			$this->basic_model->_table_name = 'tbl_scontract_co a'; //table name
			$this->basic_model->db->join('tbl_members b', 'a.dp_id = b.dp_id', 'left');
			$this->basic_model->db->where('a.ct_id', $data['ct_id']);
			$this->basic_model->_order_by = 'b.ceo asc';
			$data['all_uprice_group'] = $this->basic_model->get();
		} else { 
			$this->basic_model->_table_name = 'tbl_dp_assign_item'; //table name
			$this->basic_model->db->where('tbl_dp_assign_item.type', $type);
			$this->basic_model->db->where('tbl_dp_assign_item.p_category', $cat);
			$this->basic_model->db->where('tbl_dp_assign_item.ct_id', $ct_id);
			$this->basic_model->_order_by = 'item';
			$data['all_item_group'] = $this->basic_model->get();
		}

		if($type == "tr_item") { //거래항목설정
			$data['title'] = '거래항목 설정';


			$data['subview'] = $this->load->view('admin/basic/pop_tr_item', $data, true);
		} else if($type == "tr_uprice") { //거래단가설정
			$data['title'] = '거래단가 설정';
			$data['subview'] = $this->load->view('admin/basic/pop_tr_uprice', $data, true);
		} else if($type == "sudang") {
			$data['title'] = '수당항목 설정';
			if($cat == "01") $data['title'] = "<font style='font-weight:bold;color:blue;'>각종</font>".$data['title'];
			$data['subview'] = $this->load->view('admin/basic/pop_set_item', $data, true);
		} else if($type == "gongje") {
			$data['title'] = '공제항목 설정';
			if($cat == "01") $data['title'] = "<font style='font-weight:bold;color:red;'>위.수탁관리비</font>".$data['title'];
			else if($cat == "02") $data['title'] = "<font style='font-weight:bold;color:red;'>일반</font>".$data['title'];
			else if($cat == "03") $data['title'] = "<font style='font-weight:bold;color:red;'>환급형</font>".$data['title'];
			$data['subview'] = $this->load->view('admin/basic/pop_set_item', $data, true);
		}
        $this->load->view('admin/_layout_pop_set_item', $data);
	}

    public function tr_add_partner($ct_id, $page = NULL)
    {
		$data['ct_id'] = $ct_id;
		$data['co_type'] = $this->input->post('co_type', true);
		$data['mode'] = $this->input->post('mode', true);
		$data['dp_id'] = $this->input->post('dp_id', true);
		$data['sc_id'] = $this->input->post('sc_id', true);
		$data['page'] = $page;
		
		if(empty($data['co_type'])) $data['co_type'] = "partner";
		if(empty($data['page'])) $data['page'] = 1;

		$data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
		
		if(!empty($data['mode']) && !empty($data['dp_id'])) {
			if($data['mode'] == "ADD") {
				$sql = "INSERT INTO tbl_scontract_co SET dp_id='".$data['dp_id']."', ct_id='".$ct_id."'";
				$this->db->query($sql);
			} else if($data['mode'] == "DEL" && !empty($data['sc_id'])) {
				$this->basic_model->_table_name = "tbl_scontract_co"; // table name
				$this->basic_model->_primary_key = "idx"; // $id
				$this->basic_model->delete($data['sc_id']);
			}
		}


		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

        $this->basic_model->_table_name = 'tbl_members'; //table name
		if(!empty($data['search_field']) && !empty($data['search_keyword'])) {
			$this->basic_model->db->where(''.$data['search_field'].' like', '%'.$data['search_keyword'].'%' );
		}
        if($data['co_type'] != "all") $this->basic_model->db->where('mb_type', $data['co_type']);
		$data['total_count'] = count($this->basic_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'co_name';
		if(!empty($data['search_field']) && !empty($data['search_keyword'])) {
			$this->basic_model->db->where(''.$data['search_field'].' like', '%'.$data['search_keyword'].'%' );
		}
        if($data['co_type'] != "all") $this->basic_model->db->where('mb_type', $data['co_type']);
		$this->basic_model->db->limit($limit, $data['from_record']);
		$this->basic_model->_order_by = 'co_name asc';
		$data['all_partner_group'] = $this->basic_model->get();


		$data['title'] = '거래 파트너 추가';
		$data['subview'] = $this->load->view('admin/basic/pop_tr_add_partner', $data, true);
        $this->load->view('admin/_layout_pop_add_partner', $data);
	}

	
	public function delete_item($ws_co_id = NULL, $type = 'sudang', $id)
    {
        $deleted = can_action('122', 'deleted');
        if (!empty($deleted)) {
            $item_info = $this->settings_model->check_by(array('idx' => $id), 'tbl_dp_assign_item');
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'basic',
                'module_field_id' => $id,
                'activity' => ('activity_delete_a_dp_item'),
                'value1' => $item_info->item,
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);

            $this->settings_model->_table_name = 'tbl_dp_assign_item';
            $this->settings_model->_primary_key = 'idx';
            $this->settings_model->delete($id);
            // messages for user
            $type = "success";
            $message = "항목이 삭제되었습니다";
            // messages for user
            echo json_encode(array("status" => $type, 'message' => $message));
            exit();
        } else {
            echo json_encode(array("status" => 'error', 'message' => lang('there_in_no_value')));
            exit();
        }
    }

    public function select_company($co_type = NULL, $params = NULL, $page = NULL)
    {

		$data['co_type'] = $co_type;
		$data['params'] = $params;
		$data['page'] = $page;

		$data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
		
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

        $this->basic_model->_table_name = 'tbl_members'; //table name
		if(!empty($data['search_field']) && !empty($data['search_keyword'])) {
			$this->basic_model->db->where('tbl_members.'.$data['search_field'].' like', '%'.$data['search_keyword'].'%' );
		}
        if($co_type != "all") $this->basic_model->db->where('tbl_members.mb_type', $co_type);
		$data['total_count'] = count($this->basic_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'co_name';
		if(!empty($data['search_field']) && !empty($data['search_keyword'])) {
			$this->basic_model->db->where('tbl_members.'.$data['search_field'].' like', '%'.$data['search_keyword'].'%' );
		}
        if($co_type != "all") $this->basic_model->db->where('tbl_members.mb_type', $co_type);
		$this->basic_model->db->limit($limit, $data['from_record']);

        $data['all_company_info'] = $this->basic_model->get();
		if($co_type == "partner") {
			$data['title'] = '파트너 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_partner_list', $data, true);
		} else if($co_type == "customer") {
			$data['title'] = '거래처 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_customer_list', $data, true);
		} else if($co_type == "coop") {
			$data['title'] = '협력사 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_coop_list', $data, true);
		} else if($co_type == "group") {
			$data['title'] = '그룹사 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_group_list', $data, true);
		} else if($co_type == "all") {
			$data['title'] = '전체 관계사';
			$data['subview'] = $this->load->view('admin/basic/pop_allco_list', $data, true);
		}
        $this->load->view('admin/_layout_pop', $data);
    }

    public function coop_list($action = NULL, $id = NULL)
    {

        $dp_id = $id;

        if ($action == 'edit_coop') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
				$qry = "select * from tbl_members  where dp_id = '".$dp_id."'";
				$data['coop_info'] = $this->basic_model->db->query($qry)->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '협력사 목록';

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.mb_type', 'coop');
        $data['all_coop_info'] = $this->basic_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_member');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/basic/coop_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function coop_details($id, $action = null)
    {
        if ($action == 'add_contacts') {
            // get all language
            $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
            // get all location
            $this->basic_model->_table_name = 'tbl_locales';
            $this->basic_model->_order_by = 'name';
            $data['locales'] = $this->basic_model->get();
            $data['company'] = $id;
            $user_id = $this->uri->segment(6);
            if (!empty($user_id)) {
                // get all user_info by user id
                $data['account_details'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_account_details');

                $data['user_info'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_users');
            }

        }
        $data['title'] = "협력사 상세 정보"; //Page title
        // get all coop details
        $this->basic_model->_table_name = "tbl_members"; //table name
        $this->basic_model->_order_by = "dp_id";
        $data['coop_details'] = $this->basic_model->get_by(array('dp_id' => $id), TRUE);

/*
        // get all invoice by client id
        $this->basic_model->_table_name = "tbl_invoices"; //table name
        $this->basic_model->_order_by = "dp_id";
        $data['customer_invoices'] = $this->customer_model->get_by(array('dp_id' => $id), FALSE);

        // get all estimates by customer id
        $this->customer_model->_table_name = "tbl_estimates"; //table name
        $this->customer_model->_order_by = "dp_id";
        $data['customer_estimates'] = $this->customer_model->get_by(array('dp_id' => $id), FALSE);

        // get customer contatc by customer id
        $data['customer_contacts'] = $this->customer_model->get_customer_contacts($id);
*/
        $data['subview'] = $this->load->view('admin/basic/coop_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function group_list($action = NULL, $id = NULL)
    {
        $dp_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('asset_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_member')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('dp_id', $dp_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '그룹사 목록';

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.mb_type', 'group');
        $data['all_group_info'] = $this->basic_model->get();

        $data['subview'] = $this->load->view('admin/basic/group_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function branch_list($action = NULL, $id = NULL)
    {

        $dp_id = $id;

        if ($action == 'edit_branch') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
				$qry = "select * from tbl_members  where dp_id = '".$dp_id."'";
				$data['branch_info'] = $this->basic_model->db->query($qry)->row();
            }

        } else {
            $data['active'] = 1;
        }

        $data['title'] = '지사 목록';

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.mb_type', 'branch');
        $data['all_branch_info'] = $this->basic_model->get();

        $data['subview'] = $this->load->view('admin/basic/branch_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

	// 지사 저장
    public function save_branch($id = NULL)
    {
		$dp_id = $id;
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

			$this->basic_model->_table_name = 'tbl_members'; //table name
            $result = $this->basic_model->get_by(array('dp_id' => $dp_id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_members SET ";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",tax_yn='".$this->input->post('tax_yn', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",homepage='".$this->input->post('homepage', true)."'";
				$sql .= " WHERE dp_id = '".$dp_id."'";
				$this->db->query($sql);
			} else { 
				//아이디 정보가 없을경우 1. 주민등록번호 없으면 2. 사업자등록번호


				$sql = "INSERT INTO tbl_members SET ";
				$sql .= "mb_type='branch'";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",co_address='".$this->input->post('co_address', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",homepage='".$this->input->post('homepage', true)."'";
				$this->db->query($sql);
				$dp_id = $this->db->insert_id();
			}

			$message = "거래처정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/basic/branch_list/'); //redirect page
    } 


	//지사 삭제
    public function delete_branch($dp_id)
    {
        $this->basic_model->_table_name = "tbl_members"; // table name
        $this->basic_model->_primary_key = "dp_id"; // $id
        $this->basic_model->delete($dp_id);

		$this->db->query("delete from tbl_members_bank where dp_id='{$dp_id}'");
		$this->db->query("delete from tbl_members_history where dp_id='{$dp_id}'");

		$type = 'success';
        $msg = "지사 정보가 삭제되었습니다.";
        set_message($type, $msg);
        redirect('admin/basic/branch_list');
    }

}