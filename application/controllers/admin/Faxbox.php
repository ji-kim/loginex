<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mailbox
 *
 * @author NaYeM
 */
class Faxbox extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('faxbox_model');
        $this->load->helper('ckeditor');
        $this->load->helper('text');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "350px"
            )
        );
        $data['dataTables'] = true;
        $data['select_2'] = true;
        $data['datepicker'] = true;
    }

    public function index($action = NULL, $id = NULL, $status = NULL)
    {
        $data['page'] = lang('faxbox');
        $data['title'] = "Faxbox";
        $user_id = $this->session->userdata('user_id');
        $tel = "023334444";




        if ($action == 'inbox') {
            $data['menu_active'] = 'inbox';
            $data['view'] = 'inbox';
            $data['get_sent_message'] = $this->faxbox_model->get_recv_message($tel);
        } elseif ($action == 'read_send_fax') {
            $data['menu_active'] = 'sent';
            $data['view'] = 'read_fax';
            //$data['read_mail'] = $this->mailbox_model->check_by(array('sent_id' => $id), 'tbl_sent');

        } elseif ($action == 'read_inbox_mail') {
            $data['menu_active'] = 'inbox';
            $data['view'] = 'read_mail';
            $data['reply'] = 1;
            $this->mailbox_model->_primary_key = 'inbox_id';
            $updata['view_status'] = '1';
            $this->mailbox_model->save($updata, $id);
        } elseif ($action == 'added_favourites') {
            $favdata['favourites'] = $status;
            $this->mailbox_model->_primary_key = 'inbox_id';
            $this->mailbox_model->save($favdata, $id);
            redirect('admin/mailbox/index/inbox');
        } elseif ($action == 'compose') {
            $data['view'] = 'compose_fax';
            $data['menu_active'] = 'sent';

        } else {
            $data['menu_active'] = 'sent';
            $data['view'] = 'sent';
            $data['faxdata'] = $this->faxbox_model->get_send_message($tel);
        }
        // get mailbox credintial from tbl_user

        $data['subview'] = $this->load->view('admin/faxbox/faxbox', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function send_fax()
    {
        $fax_num = explode(",",$this->input->post('TR_PHONE', TRUE));

        $data = array();
        // 먼저 팩스에 대한 정확한 기입을 시작한다.
        $data['TR_TYPE'] = "1";
        $data['TR_SENDDATE'] = date("Y-m-d H:i:s");
        // 세션에 저장된 번호로 발송을 할 것
        $data['TR_SENDFAXNUM'] = "023334444";
        $data['TR_MSGCOUNT'] = count($fax_num);
        $data['TR_SENDSTAT'] = '-';
        $data['TR_DOCNAME'] = $_FILES['attach_file']['name'];


        $file_path = "/lgwebfax/SendDoc/";
        rename($_FILES['attach_file']['tmp_name'] , $file_path.$_FILES['attach_file']['name']);

        //먼저 DB에 삽입후 BATCH_ID를 리턴받는다.

        $return_id = $this->faxbox_model->send_fax($data);

        // get all email address

        $serial_no = 1;
        foreach ($fax_num as $number) {
            $subdata = array();
            $subdata['TR_BATCHID'] = $return_id;
            $subdata['TR_SERIALNO'] = $serial_no++;
            $subdata['TR_SENDDATE'] = $data['TR_SENDDATE'];
            $subdata['TR_PHONE'] = $number;
            $this->faxbox_model->insert_send_fax_subdata($subdata);
        }


        redirect('admin/faxbox/index/sent');

    }

}
