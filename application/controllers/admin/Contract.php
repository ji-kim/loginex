<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contract extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('contract_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

	// 
	public function contract_list($ct_status = NULL, $action = NULL, $id = NULL)
	{
        $data['co_code'] = '';
        $data['page'] = 1;


           $data['active'] = 1;
        $data['title'] = '계약관리';
        $data['ct_status'] = $ct_status;
        if(empty($data['ct_status'])) $data['ct_status'] = 'IN';

		$data['all_contract_info'] = $this->contract_model->get_all_contract_info($data['ct_status']);
        $data['subview'] = $this->load->view('admin/contract/contract_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	// 
	public function scontract_list($sct_status = NULL, $action = NULL, $id = NULL)
	{
        $data['sct_status'] = $sct_status;
        $data['co_code'] = '';
        $data['page'] = 1;

		   $data['active'] = 1;
        $data['title'] = '하도급계약관리';
        if(empty($data['sct_status'])) $data['sct_status'] = 'IN';

		$data['all_scontract_info'] = $this->contract_model->get_all_scontract_info($data['sct_status']);
        $data['subview'] = $this->load->view('admin/contract/scontract_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}


}
