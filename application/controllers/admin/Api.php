<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Api extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        //$this->load->model('asset_model');
        //$this->load->model('utilities_model');
    }

    public function total_list(){
        $this->load->view("admin/api/total_list");
    }
}