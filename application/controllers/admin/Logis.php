<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Logis extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('logis_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

    public function close_mfee_id($df_month = NULL, $ct_id = NULL, $id = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql = "UPDATE tbl_delivery_fee SET is_closed='".$is_closed."' WHERE df_id = ?";
			$this->db->query($sql, array($id));
			$message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/logis/stransaction_write/list/'.$ct_id.'/'.$df_month); //redirect page
    }

    public function tax_all($ct_id, $df_month, $tax_date, $pmode)
    {
		$data['title'] = "세금계산서출력";
		// 총합계금액 필드 추가후 아래 로직 다 삭제할것
		$data['ct_id'] = $ct_id;
		$data['df_month'] = $df_month;
		$data['tax_date'] = $tax_date;
		$data['pmode'] = $pmode;

		//수당항목
		$this->logis_model->_table_name = 'tbl_settings_sg'; //table name
		$this->logis_model->db->where('tbl_settings_sg.add_type', 'S');
		$this->logis_model->_order_by = 'dp_order asc';
		$data['all_sudang_group'] = $this->logis_model->get();
		//공제

		//공제항목
		$this->logis_model->_table_name = 'tbl_settings_sg'; //table name ->where('ws_co_id', $data['gongje_req_co'])
		$this->logis_model->db->where('add_type<>', 'S');
		$this->logis_model->_order_by = 'title';
		$data['all_gongje_group'] = $this->logis_model->get();


		//거래처
		$this->logis_model->_table_name = 'tbl_contract_rco'; //table name
		$this->logis_model->db->where('ct_id', $data['ct_id']);
		$this->logis_model->_order_by = 'idx asc';
		$data['all_customer_group'] = $this->logis_model->get();

		//거래항목
		$this->logis_model->db->select('title');
		$this->logis_model->_table_name = 'tbl_contract_detail';         
		$this->logis_model->db->where('ct_id', $data['ct_id']);
		$this->logis_model->_order_by = 'ct_id asc';
		$data['all_stransaction_items'] = $this->logis_model->get();

		//리스트
		$this->logis_model->db->select('a.*, b.acc_vat_yn,b.tax_yn,b.AA as bank_name');
		$this->logis_model->_table_name = 'tbl_delivery_fee a';         
        $this->logis_model->db->join('tbl_members b', 'a.dp_id = b.dp_id', 'left');
		$this->logis_model->db->where('a.ct_id', $data['ct_id'])->where('a.df_month', $data['df_month']);
		$this->logis_model->db->where("(a.tr_type = 'T' OR a.tr_type = 'MT' )");
		$this->logis_model->_order_by = 'a.df_id asc';

		$data['all_stransaction_ready'] = $this->logis_model->get();
        $data['total_count'] = count($data['all_stransaction_ready']);

		$data['subview'] = $this->load->view('admin/logis/tax_all', $data, true);
        $this->load->view('admin/_layout_print', $data);
    }

    public function trans_details($id, $mode)
    {

        $this->logis_model->_table_name = 'tbl_delivery_fee'; //table name
        $df = $this->logis_model->get_by(array('df_id' => $id), true);

		if($mode == "TR")
			$data['title'] = substr($df->df_month,0,4)."년 ".substr($df->df_month,5,2)."월분 위탁배달(운송/반)수수료 지급명세표";
		else
			$data['title'] = substr($df->df_month,0,4)."년 ".substr($df->df_month,5,2)."월분 위.수탁관리(지입)비 청구(공제) 내역서";

		if($mode == "TR")
			$data['subview'] = $this->load->view('admin/logis/trans_details', $data, true);
		else
			$data['subview'] = $this->load->view('admin/logis/mfee_details', $data, true);
        $this->load->view('admin/_layout_pop_common', $data);
    }

    public function manage_sudang($id, $add_type)
    {
		$data['df_id'] = $id;
		$data['add_type'] = $add_type;
		if($add_type=="S") $data['title'] = "수당"; else $data['title'] = "공제"; 
        $this->logis_model->_table_name = 'tbl_delivery_fee_add'; //table name
        $this->logis_model->db->where('add_type', $add_type)->where('df_id', $id);
        $this->logis_model->_order_by = 'idx desc';
        $data['all_sudang_info'] = $this->logis_model->get();

        $data['modal_subview'] = $this->load->view('admin/logis/_modal_manage_sudang', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_sudang($id, $add_type)
    {
		$data['title'] = $this->input->post('title', true);
		$data['amount'] = $this->input->post('amount', true);
		$data['memo'] = $this->input->post('memo', true);
		$data['df_id'] = $id;
		$data['add_type'] = $add_type;

		$sql = "INSERT INTO tbl_delivery_fee_add SET ";
		$sql .= "df_id='".$data['df_id']."'";
		$sql .= ",title='".$data['title']."'";
		$sql .= ",amount='".$data['amount']."'";
		$sql .= ",memo='".$data['memo']."'";
		$sql .= ",add_type='".$data['add_type']."'";
		$sql .= ",apply_yn='Y'";
		$this->db->query($sql);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete_sudang($id)
    {
		$sql = "DELETE FROM tbl_delivery_fee_add WHERE idx='".$id."'";
		$this->db->query($sql);

        $type = "success";
        $message = $msg;
        set_message($type, $message);
        redirect($_SERVER['HTTP_REFERER']);
    }

	public function payment_generate($action = NULL, $id = NULL, $page = 1)
	{
		$data['dataTables'] = NULL;
		$data['page'] = $page;
        $data['title'] = '납부정보';

        //receive form input by post
		$this->input->post('gongje_req_co', true);
        $data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
        $data['ws_mode'] = $this->input->post('ws_mode', true);
        
        $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');
        $data['active'] = 1;
		
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

		$this->logis_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->logis_model->db->select('df.*');
		$this->logis_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->logis_model->db->where('df.df_month', $data['df_month']);
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' OR df.D like '%".$data['search_keyword']."%' OR tr.car_1 like '%".$data['search_keyword']."%' )");
		}
		$data['total_count'] = count($this->logis_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

	    $data['title'] = '지입공제내역';

		$this->logis_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->logis_model->db->select('df.*, pt.co_name, pt.ceo, pt.bs_number, pt.N, pt.O, pt.ws_co_id, pt.driver, tr.car_1');
		$this->logis_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->logis_model->db->where('df.df_month', $data['df_month']);
		if(!empty($data['search_keyword'])) { //D
			$this->logis_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' OR df.D like '%".$data['search_keyword']."%' OR tr.car_1 like '%".$data['search_keyword']."%' )");
		}
        $this->logis_model->_order_by = 'df.C';

		$this->logis_model->db->limit($limit, $data['from_record']);
		$data['all_delivery_fee_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/payment_generate', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	public function transaction_list($action = NULL, $id = NULL)
	{
        $data['co_code'] = '';
        $data['page'] = 1;
        $data['title'] = '계약관리';
        $data['subview'] = $this->load->view('admin/logis/transaction_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	public function stransaction_list($action = NULL, $id = NULL)
	{
        $data['co_code'] = '';
        $data['page'] = 1;
        $data['title'] = '계약관리';


        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';
        $this->logis_model->_table_name = 'tbl_contract ct'; //table name          
		$this->logis_model->db->join('tbl_members rco', 'ct.r_co_id = rco.userid');

		if(!empty($this->input->post('baecha_co_id', true))) $this->logis_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}
		if(!empty($data['ws_co_id'])) {
			$this->logis_model->db->or_where('tr.ws_co_id', $data['ws_co_id'] );
		}
        $this->logis_model->db->where('ct.is_contract', 'N')->where(' (TRIM(ct.ct_status) <> \'FN\')')->where('ct.is_subcontract','Y')->order_by('ct.ct_title', 'ASC');////현규씨가 계약이 N으로 해놓은 이유는?

        $data['all_scontract_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/stransaction_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	public function transaction_report($action = NULL, $id = NULL)
	{
        $data['co_code'] = '';
        $data['page'] = 1;
        $data['title'] = '계약관리';
        $data['subview'] = $this->load->view('admin/logis/transaction_report', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}





    public function transaction_ready($action = NULL, $id = NULL)
    {

        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';
        $this->logis_model->_table_name = 'tbl_contract ct'; //table name          
		$this->logis_model->db->join('tbl_members rco', 'ct.r_co_id = rco.userid');

		if(!empty($this->input->post('baecha_co_id', true))) $this->logis_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}
		if(!empty($data['ws_co_id'])) {
			$this->logis_model->db->or_where('tr.ws_co_id', $data['ws_co_id'] );
		}
        $this->logis_model->db->where('ct.is_contract', 'N')->where(' (TRIM(ct.ct_status) <> \'FN\' and TRIM(ct.ct_status) <> \'CN\')')->order_by('ct.ct_title', 'ASC');//->where('ct.is_subcontract','Y')//현규씨가 계약이 N으로 해놓은 이유는?

        $data['all_contract_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/transaction_ready', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function transaction_finish($action = NULL, $id = NULL)
    {

        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';

        $this->logis_model->_table_name = 'tbl_client'; //table name
        $this->logis_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->logis_model->get();
		
		$data['subview'] = $this->load->view('admin/logis/transaction_finish', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function stransaction_ready($action = NULL, $id = NULL)
    {
        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';
        $this->logis_model->_table_name = 'tbl_contract ct'; //table name          
		$this->logis_model->db->join('tbl_members rco', 'ct.r_co_id = rco.userid');

		if(!empty($this->input->post('baecha_co_id', true))) $this->logis_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}
		if(!empty($data['ws_co_id'])) {
			$this->logis_model->db->or_where('tr.ws_co_id', $data['ws_co_id'] );
		}
        $this->logis_model->db->where('ct.is_contract', 'N')->where(' (TRIM(ct.ct_status) <> \'FN\')')->where('ct.is_subcontract','Y')->order_by('ct.ct_title', 'ASC');////현규씨가 계약이 N으로 해놓은 이유는?

        $data['all_scontract_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/stransaction_ready', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function stransaction_finish($action = NULL)
    {
		$data['page'] = $this->input->post('page', true);
		$data['ct_id'] = $this->input->post('ct_id', true);
        $data['df_month'] = $this->input->post('df_month', true);

		if(empty($data['page']) || $data['page'] < 1) $data['page'] =  1;
		if(empty($data['ct_id'])) $data['ct_id'] =  '';
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');
		if(empty($limit)) $limit = 20;

        if ($action == 'detail_view') {
            $data['active'] = 2;
        } else {
            $data['active'] = 1;
        }
		
		$data['title'] = '하도급 거래등록 완료';

		//진행계약목록
		$this->logis_model->_table_name = 'tbl_contract ct'; //table name
		$this->logis_model->db->where('ct.is_contract', 'N')->where(' (TRIM(ct.ct_status) <> \'FN\' and TRIM(ct.ct_status) <> \'CN\')')->order_by('ct.ct_title', 'ASC');
		$data['all_contract_group'] = $this->logis_model->get();

		//거래항목 가져오기
		$this->logis_model->_table_name = 'tbl_contract_sub_info ci'; //table name
		$this->logis_model->db->where('ci.ct_id', $data['ct_id'])->where('ci.depth', '1')->_order_by = 'item';
		$data['all_items_group'] = $this->logis_model->get();

        $this->logis_model->_table_name = 'tbl_delivery_fee df'; //table name          
		$this->logis_model->db->join('tbl_members dp', 'dp.dp_id = df.dp_id');
		$this->logis_model->db->join('tbl_members ct', 'ct.dp_id = df.dp_id');
		$this->logis_model->db->where('df.status', '마감')->where('df.df_month', $data['df_month'])->where('df.ct_id', $data['ct_id']);
		if(!empty($this->input->post('co_code', true))) $this->logis_model->db->where('ct.mb_type', 'customer')->where('ct.code', $this->input->post('co_code', true));
        $this->logis_model->_table_name = 'tbl_delivery_fee df'; //table name          
//		$sql4 = " select * from az_contract_detail where ct_id='$pct_id' and depth='1' order by idx asc";
//$result4 = sql_query($sql4);
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}
		$data['total_count'] = count($this->logis_model->get());
		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함
		
        $this->logis_model->_table_name = 'tbl_delivery_fee df'; //table name          
		$this->logis_model->db->join('tbl_members dp', 'dp.dp_id = df.dp_id');
		$this->logis_model->db->join('tbl_members ct', 'ct.dp_id = df.dp_id');
		$this->logis_model->db->where('df.status', '마감')->where('df.df_month', $data['df_month'])->where('df.ct_id', $data['ct_id']);
		if(!empty($this->input->post('co_code', true))) $this->logis_model->db->where('ct.mb_type', 'customer')->where('ct.code', $this->input->post('co_code', true));
        $this->logis_model->_table_name = 'tbl_delivery_fee df'; //table name          
//		$sql4 = " select * from az_contract_detail where ct_id='$pct_id' and depth='1' order by idx asc";
//$result4 = sql_query($sql4);
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}
		$this->logis_model->db->limit($limit, $data['from_record']);
		$data['all_stransaction_info'] = $this->logis_model->get();

		if(!empty($data['ct_id'])) {
            $ct = $this->db->where('idx', $data['ct_id'])->get('tbl_contract')->row();

			//수당
			$this->logis_model->_table_name = 'tbl_settings_sg'; //table name
			$this->logis_model->db->where('tbl_settings_sg.add_type', 'S');
			$this->logis_model->_order_by = 'dp_order asc';
			$data['all_sudang_group'] = $this->logis_model->get();
			//공제
			$where_au = "(add_type = 'GW' OR add_type = 'GN' OR add_type = 'GR')";
			$this->logis_model->_table_name = 'tbl_settings_sg'; //table name
			$this->logis_model->db->where($where_au);
			$this->logis_model->_order_by = 'dp_order asc';
			$data['all_gongje_group'] = $this->logis_model->get();
		}

        $data['subview'] = $this->load->view('admin/logis/stransaction_finish', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }


    public function stransaction_write($action = NULL, $sct_id = NULL, $df_month = NULL)
    {

        $data['action'] = $action;
        $data['ct_id'] = $sct_id;
		$data['df_month'] = $df_month;

        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');
        $data['tax_date'] = $this->input->post('tax_date', true);
        if (empty($data['tax_date'])) $data['tax_date'] = date('Y-m-d');
        $data['del_all'] = $this->input->post('del_all', true);

		if (!empty($gongje_req_co)) $data['gongje_req_co'] = $gongje_req_co;
        else $data['gongje_req_co'] = $this->input->post('gongje_req_co', true);
        if (empty($data['gongje_req_co'])) $data['gongje_req_co'] = "1413";
        $data['gongje_req_co_name'] = $this->input->post('gongje_req_co_name', true);
        if (empty($data['gongje_req_co_name'])) $data['gongje_req_co_name'] = "케이티지엘에스(주)";

        $data['code'] = $this->input->post('code', true);
        $data['ws_mode'] = $this->input->post('ws_mode', true);
        $data['close_yn'] = $this->input->post('close_yn', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
        if ($data['ws_mode'] == 'RTR') { // 거래개별등록
			$field = $this->input->post('field', true);
			$df_id = $this->input->post('df_id', true);
			$val = $this->input->post('val', true);

			$sql = "UPDATE tbl_delivery_fee SET ".$field."='".$val."' WHERE df_id = '".$df_id."'";
			$this->db->query($sql);
			$message = "저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
			exit;
		}

        $data['active'] = 1;
        if ($action == 'generate') { // 하도급 거래 생성

			//기존 데이터 삭제
			$this->logis_model->_table_name = 'tbl_delivery_fee'; //table name
			$this->logis_model->db->where('df_month',$data['df_month']);
			$this->logis_model->db->where('ct_id',$data['ct_id']);
			$all_mfee_list = $this->logis_model->get();
			if (!empty($all_mfee_list)) {
				foreach ($all_mfee_list as $mfee_info) {
					//tbl_delivery_fee_add
					$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
					$this->db->query($sql);
					//tbl_delivery_fee_fixmfee
					$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
					$this->db->query($sql);
					//tbl_delivery_fee_gongje
					$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
					$this->db->query($sql);
					//tbl_delivery_fee_insur
					$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
					$this->db->query($sql);
					//tbl_delivery_fee_refund_gongje
					$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
					$this->db->query($sql);
					//tbl_delivery_fee_levy
					$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
					$this->db->query($sql);

					$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
					$this->db->query($sql);
				}
			}

			$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_month='".$data['df_month']."'"; // and ws_co_id = '".$data['ws_co_id']."'";
			$this->db->query($sql);
					
			$sql = "DELETE FROM tbl_delivery_fee WHERE ct_id='".$data['ct_id']."' and df_month='".$data['df_month']."'";
			$this->db->query($sql);

			//해당 파트너 리스팅
			$this->logis_model->_table_name = 'tbl_scontract_co a, tbl_members dp, tbl_asset_truck tr, tbl_members co, tbl_contract ct'; //table name
			$this->logis_model->db->select('a.*, dp.*, tr.car_1, tr.ws_co_id as ws_co_id, co.co_name as rco_name, co.dp_id as rco_id');
			$this->logis_model->db->where("a.dp_id = dp.dp_id and dp.tr_id = tr.idx and ( dp.code = co.code and co.mb_type = 'customer') and ct.idx = '".$data['ct_id']."'")->where('a.ct_id',$data['ct_id']);
			$data['all_dp_list'] = $this->logis_model->get();
			$data['dp_total_count'] = count($data['all_dp_list']);
			$data['action'] = "G";
		
        }

        $data['title'] = 'logis List';

		//수당항목
		$this->logis_model->_table_name = 'tbl_settings_sg'; //table name
		$this->logis_model->db->where('tbl_settings_sg.add_type', 'S');
		$this->logis_model->_order_by = 'dp_order asc';
		$data['all_sudang_group'] = $this->logis_model->get();
		//공제
/*
		$where_au = "(add_type = 'GW' OR add_type = 'GN' OR add_type = 'GR')";
		$this->logis_model->_table_name = 'tbl_settings_sg'; //table name
		$this->logis_model->db->where($where_au);
		$this->logis_model->_order_by = 'dp_order asc';
		$data['all_gongje_group'] = $this->logis_model->get();
*/
		//공제항목
		$this->logis_model->_table_name = 'tbl_settings_sg'; //table name ->where('ws_co_id', $data['gongje_req_co'])
		$this->logis_model->db->where('add_type<>', 'S');
		$this->logis_model->_order_by = 'title';
		$data['all_gongje_group'] = $this->logis_model->get();


		//거래처
		$this->logis_model->_table_name = 'tbl_contract_rco'; //table name
		$this->logis_model->db->where('ct_id', $data['ct_id']);
		$this->logis_model->_order_by = 'idx asc';
		$data['all_customer_group'] = $this->logis_model->get();

		//수당
//		$this->logis_model->_table_name = 'tbl_delivery_pay_added_items'; //table name
//		$this->logis_model->db->where('df_month', $data['df_month'])->where('ws_co_id', $ct->ws_co_id)->where('add_type', 'S');
//		$this->logis_model->_order_by = 'title';
//		$data['all_sudang_group'] = $this->logis_model->get();

		//거래항목
		$this->logis_model->db->select('title');
		$this->logis_model->_table_name = 'tbl_contract_detail';         
		$this->logis_model->db->where('ct_id', $data['ct_id']);
		$this->logis_model->_order_by = 'ct_id asc';
		$data['all_stransaction_items'] = $this->logis_model->get();

		//리스트
		$this->logis_model->db->select('a.*, b.acc_vat_yn,b.tax_yn,b.AA as bank_name');
		$this->logis_model->_table_name = 'tbl_delivery_fee a';         
        $this->logis_model->db->join('tbl_members b', 'a.dp_id = b.dp_id', 'left');
		$this->logis_model->db->where('a.ct_id', $data['ct_id'])->where('a.df_month', $data['df_month']);
		$this->logis_model->db->where("(a.tr_type = 'T' OR a.tr_type = 'MT' )");
		$this->logis_model->_order_by = 'a.df_id asc';

		$data['all_stransaction_ready'] = $this->logis_model->get();
        $data['total_count'] = count($data['all_stransaction_ready']);

        if ($data['ws_mode'] == 'BankExcel') { // 은행용 엑셀
			$data['exl_title'] = $data['df_month'];
			$data['subview'] = $this->load->view('admin/logis/stransaction_write_bank_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
        } else if ($data['ws_mode'] == 'Excel') { // 은행용 엑셀
			$data['exl_title'] = $data['df_month'];
			$data['subview'] = $this->load->view('admin/logis/stransaction_write_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
		} else {
			$data['subview'] = $this->load->view('admin/logis/stransaction_write', $data, true);
			$this->load->view('admin/_layout_main', $data);
		}
    }
   

}
